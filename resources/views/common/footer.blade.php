 <!-- ======= Footer ======= --> 
 <footer id="footer" class="footer">
    <div class="footer-top">
    <div class="container">
        <div class="row">
        <div class="col-xl-12" style="text-align:center;">
            <br>
            <img src="{{ asset('assets/img/brand/dateassurance-logo-for-body.png') }}" alt="dateassurance(tm) - the social network for safe, secure, and discreet encounters">
            <br>
            <span style="color:#ffffff;font-size:9px;text-transform:uppercase;">the social network for safe, secure, &amp; discreet encounters&trade;</span><br>
            <br>
            <span style="color:#ffffff;font-size:14px;">we support:</span>
            <br>
            <a href="http://www.rtalabel.org/?content=validate&amp;id=&amp;rating=RTA-5042-1996-1400-1577-RTA" target="_blank"><img src="{{ asset('assets/img/120x60_RTA-5042-1996-1400-1577-RTA_c.gif') }}" alt="RTA"></a><br>
            <br>
            <a href="../da-about.php">about</a> | <a href="../da-privacy.php">privacy notice</a> | <a href="../da-support.php">member support</a> | <a href="https://humantraffickinghotline.org/report-trafficking">report human trafficking</a><br>
            <br>
            <span style="color:#ffffff;font-size:12px;">
            <p>copyright &copy; 2020&nbsp;<i class="fas fa-hand-paper"></i>&nbsp;DOKKUM MEDIA COMPANY BV</p>
            </span>
        </div>
        </div>
    </div>
    </div>
</footer>