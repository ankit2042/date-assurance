<!-- ======= Header ======= -->
<header id="header" class="fixed-top header-transparent">
    <div class="container-fluid">

      <div class="row justify-content-center">
        <div class="col-xl-11 d-flex align-items-center">
          <a href="index.php" class="logo mr-auto"><img src="{{ asset('imgs/brand/dateassurance-logo-shield-home.png') }}" alt="" class="img-fluid"></a>

          <nav class="nav-menu d-none d-lg-block">
            <ul>
              <li class="active"><a href="{{ route('home-page') }}">home</a></li>
              <li><a href="../index.php#about">about</a></li>              
              <li><a href="../index.php#features">features</a></li>
              <li><a href="../index.php#pricing">membership plans</a></li>
              <li><a href="../index.php#portfolio">companion directory</a></li>
              @if(Auth::user())
                <li><a href="{{ route('member-sign-out') }}"><span style="color:#008eff;font-weight:600;"><i class="fas fa-sign-in-alt"></i> sign out</span></a></li>
              @else
                <li><a href="{{ route('member-login-handler') }}"><span style="color:#008eff;font-weight:600;"><i class="fas fa-sign-in-alt"></i> member login</span></a></li>
                <li><a href="../signin/da-comp-mbr_signin.php"><span style="color:#f300fe;font-weight:600;"><i class="fas fa-sign-in-alt"></i> companion login</a></li>
              @endif

            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->

  <!-- ======= Intro Section ======= -->