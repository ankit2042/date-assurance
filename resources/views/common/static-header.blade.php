<section class="top-nav">
    <nav class="navbar navbar-fixed-top navbar-expand-lg navbar-dark bg-dark d-flex align-items-center">
        <a class="navbar-brand" href="{{route('home-page')}}"><img src="../imgs/brand/dateassurance-logo-shield-home.png" class="align-top" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <!-- <a class="nav-link" style="font-weight:200;" href="https://dokkummedia.000webhostapp.com:443/index.php">home</a> -->
                    <a class="nav-link" style="font-weight:200;" href="{{ route('home-page') }}">home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="font-weight:200;" href="../index.php#about">about</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="font-weight:200;" href="../index.php#features">features</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="font-weight:200;" href="{{ route('companion-directory')}}">companion directory</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="font-weight:200;" href="#" data-toggle="modal" data-target="#searchModal">search</a>
                </li>
            </ul>
            
            <ul class="navbar-nav mr-auto" style="float:right">

                @if (Auth::check())
                    <li><a href="{{ route('member-sign-out') }}"><span style="color:#008eff;font-weight:600;"><i class="fas fa-sign-in-alt"></i> sign out</span></a></li>
                @else
                    <li><a href="{{ route('member-login-handler') }}"><span style="color:#008eff;font-weight:600;"><i class="fas fa-sign-in-alt"></i> member login</span></a></li>&nbsp;&nbsp;
                    <li><a href="../signin/da-comp-mbr_signin.php"><span style="color:#f300fe;font-weight:600;"><i class="fas fa-sign-in-alt"></i> companion login</a></li>                            
                @endif
            </ul>
        </div>
    </nav>
</section>