<?php
if ( ! function_exists('valid_email')){
	function valid_email($address){
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $address)) ? FALSE : TRUE;
		}
	}
        
//   CHECKS IF NEW USER IS AN EXISTING USER //         
if(!function_exists('exits_user')){
	function exits_user($user){
		$sql = "SELECT UserID
                        FROM ".$GLOBALS['table_prefix']."users
                        WHERE Email = '".$user."' and ProfileStatusID <> 0";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		elseif(mysql_num_rows($query)>0)
			return false;
		else return true;
		}
	}
        
//   THIS SETS UP NEW MEMBER        
if(!function_exists('new_user')){
	function new_user($data=array()){
		$sql = "INSERT into ".$GLOBALS['table_prefix']."users
                       (MembershipID
                       , RemainVIPContacts
                       , SiteID
                       , Name
                       , ProfileName
                       , Email
                       , Password
                       , GenderID
                       , HeightID
                       , Age
                       , BodyTypeID
                       , HairColorID
                       , EyeColorID
                       , EthnicityID
                       , City
                       , StateID
                       , CountryID
                       , Zipcode
                       , Phone
                       , IMessagerID
                       , IMessager
                       , SexOrID
                       , EducationID
                       , OccupationID
                       , SmokingID
                       , DrinkingID
                       , MaritalStatusID
                       , DatingInterestID
                       , HaveChildren
                       , WantChildren
                       , WillingToTravel
                       , IPaddress
                       , DatePosted
                       , DateUpdate
                       , LastEmailSent
                       , LastLogon
                       , ProfileStatusID
                       , NotificationScheduleID
                       , MatchAgeFrom
                       , MatchAgeTO
                       , MatchGenderID
                       , MatchHeightIDFrom
                       , MatchHeightIDTo
                       , MatchBodyStyleID
                       , MatchSexOrID
                       , MatchEducationID
                       , MatchCareerID
                       , MatchSmokingID
                       , MatchDrinkingID
                       , MatchMaritalStatusID
                       , MatchNumberofChild
                       , MatchBeSameLocation
                       , SubcribeList
                       , RandomVerifyID
                       , Goal
                       , Interests
                       , AboutMe
                       , AboutMyMatch
                       , PrimaryPhotoID
                       , GroupID
                       , Views
                       , IsOnline
                       , TotalRating
                       , NumberOfVote
                       , ThemeId) 
                       VALUES 
                       (1
                       , 0
                       , 2
                       , ''
                       , ''
                       , '".$data[0]."'
                       , '".$data[1]."'
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , ''
                       , 0
                       , 0
                       , ''
                       , ''
                       , 0
                       , ''
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , '".$data[3]."'
                       , '".gmdate("m-d-Y", time()+7*3600)."'
                       , '".gmdate("m-d-Y", time()+7*3600)."'
                       , '0000-00-00 00:00:00'
                       , '0000-00-00 00:00:00'
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , ''
                       , ''
                       , ''
                       , ''
                       , NULL
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 1)";
		$query = mysql_query($sql);
		$newid = mysql_insert_id();
		if(!$query)
			return 0;
		else return $newid;
		}
	}
        
if(!function_exists('random_string')){
	function random_string($len=0){
		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$str = '';
		for ($i=0; $i < $len; $i++)
			$str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
		return $str;
		}
	}

if(!function_exists('dropdownlist')){
	function dropdownlist($sql='', $val=0){
		$query = mysql_query($sql);
		if(!$query)
			echo '<option value="-1">- Error data -</option>';
		else{
			while($rows=mysql_fetch_array($query)){
				$sel = ($rows['Id']==$val)?' selected="selected"':'';
				echo '<option value="'.$rows['Id'].'" '.$sel.'>'.$rows['LName'].'</option>';
				}
			}
		}
	}
        
if(!function_exists('yourdetails')){
	function yourdetails($data=array(), $userid=0){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."users
                       SET 
                       ProfileName = '".$data[0]."'
                       , AboutMe = '".$data[1]."'
                       , GenderID = ".$data[2]."
                       , Age = ".$data[3]."
                       , CountryID = ".$data[4]."
                       , StateID = ".$data[5]."
                       , City = '".$data[6]."'
                       , Zipcode = '".$data[7]."'
                       , Phone = '".$data[8]."'
                       , OccupationID = '".$data[9]."'
                       , EducationID = ".$data[10]."
                       , EthnicityID = ".$data[11]."
                       , SexOrID = ".$data[12]."
                       , BodyTypeID = ".$data[13]."
                       , HairColorID = ".$data[14]."
                       , EyeColorID = ".$data[15]."
                       , HeightID = ".$data[16]."
                       , SmokingID = ".$data[17]."
                       , DrinkingID = ".$data[18]."
                       , DateUpdate = '".gmdate("m-d-Y", time()+7*3600)."'
                       , ProfileStatusID = 4 where UserID = ".$userid;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
if(!function_exists('yourcompdetails')){
	function yourcompdetails($data=array(), $userid=0){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."companions
                       SET 
                       ProfileName = '".$data[0]."'
                       , AboutMe = '".$data[1]."'
                       , GenderID = ".$data[2]."
                       , Age = ".$data[3]."
                       , CountryID = ".$data[4]."
                       , StateID = ".$data[5]."
                       , City = '".$data[6]."'
                       , Zipcode = '".$data[7]."'
                       , Phone = '".$data[8]."'
                       , EducationID = ".$data[9]."
                       , EthnicityID = ".$data[10]."
                       , SexOrID = ".$data[11]."
                       , Transgender = ".$data[12]." 
                       , Transsexual = ".$data[13]."
                       , Independent = ".$data[14]."    
                       , Pornstar = ".$data[15]." 
                       , Website = '".$data[16]."'
                       , WebProfile = '".$data[17]."'    
                       , Whatsapp = '".$data[18]."' 
                       , Instagram = '".$data[19]."'
                       , BodyTypeID = ".$data[20]."
                       , HeightID = ".$data[21]."
                       , HairColorID = ".$data[22]."
                       , HairLengthID = ".$data[23]."
                       , HairTypeID = ".$data[24]."
                       , EyeColorID = ".$data[25]."
                       , PiercingsID = ".$data[26]."
                       , TattoosID = ".$data[27]."
                       , PussyID = ".$data[28]."
                       , BreastsID = ".$data[29]."
                       , BreastImp = ".$data[30]."
                       , CompAssID = ".$data[31]."    
                       , SmokingID = ".$data[32]."
                       , DrinkingID = ".$data[33]."
                       , DateUpdate = '".gmdate("m-d-Y", time()+7*3600)."'
                       , ProfileStatusID = 4 where UserID = ".$userid;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
if(!function_exists('yourcompindul')){
	function yourcompindul($data=array()){
    $sql = "INSERT INTO ".$GLOBALS['table_prefix']."compindul 
        (UserID
        , CompEncLikes
        , CompEncLoc
        , CompIndulSex
        , CompIndulSexPref
        , CompIndulBJ
        , CompIndulBJPref
        , CompIndulHJ
        , CompIndulKiss
        , CompIndulKissPref
        , CompIndulLickpussy
        , CompIndulTouchpussy
        , CompIndulTouchpussyPref
        , CompIndulRim
        , CompIndulRimPref
        , CompIndulAnal
        , CompIndulAnalPref
        , CompIndulMsog
        , CompIndulTwogirl
        , CompIndulSecondcomp
        , CompIndulTwoguy
        , CompIndulVideo
        , CompIndulPhotos
        , CompIndulMassage
        , CompIndulSMBD
        , CompIndulSquirt
        , CompIndulTurnons
        , CompIndulTurnoffs)
        VALUES 
            ('$userid'
            , '$compenclikes'
            , '$compencloc'
            , '$compindulsex'
            , '$compindulsexpref'
            , '$compindulbj'
            , '$compindulbjpref'
            , '$compindulhj'
            , '$compindulkiss'
            , '$compindulkisspref'
            , '$compindullickpussy'
            , '$compindultouchpussy'
            , '$compindultouchpussypref'
            , '$compindulrim'
            , '$compindulrimpref'
            , '$compindulanal'
            , '$compindulanalpref'
            , '$compindulmsog'
            , '$compindultwogirl'
            , '$compindulsecondcomp'
            , '$compindultwoguy'
            , '$compindulvideo'
            , '$compindulphotos'
            , '$compindulmassage'
            , '$compindulsmbd'
            , '$compindulsquirt'
            , '$compindulturnons'
            , '$compindulturnoffs')";

		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
// comp prefs short

if(!function_exists('compprefsshort')){
	function yourcompprefsshort($data=array()){
		$sql = "INSERT into ".$GLOBALS['table_prefix']."compindul
                        (UserId
                        , CompEncLikes
                        , CompEncLoc
                        , CompIndulSex
                        , CompIndulSexPref
                        , CompIndulBJ
                        , CompIndulBJPref
                        , CompIndulHJ
                        , CompIndulKiss
                        , CompIndulKissPref
                        , CompIndulLickpussy
                        , CompIndulTouchpussy
                        , CompIndulTouchpussyPref
                        , CompIndulRim
                        , CompIndulRimPref
                        , CompIndulAnal
                        , CompIndulAnalPref
                        , CompIndulMsog
                        , CompIndulTwogirl
                        , CompIndulSecondcomp
                        , CompIndulTwoguy
                        , CompIndulVideo
                        , CompIndulPhotos
                        , CompIndulMassage
                        , CompIndulSMBD
                        , CompIndulSquirt
                        , CompIndulTurnons
                        , CompIndulTurnoffs) 
                        VALUES 
                        ('".$data[0]."'
                        , '".$data[1]."'
                        , '".$data[2]."'
                        , '".$data[3]."'
                        , '".$data[4]."'
                        , '".$data[5]."'
                        , '".$data[6]."'
                        , '".$data[7]."'
                        , '".$data[8]."'
                        , '".$data[9]."'
                        , '".$data[10]."'
                        , '".$data[11]."'
                        , '".$data[12]."'                            
                        , '".$data[13]."'
                        , '".$data[14]."'
                        , '".$data[15]."'
                        , '".$data[16]."'
                        , '".$data[17]."'
                        , '".$data[18]."'
                        , '".$data[19]."'
                        , '".$data[20]."'
                        , '".$data[21]."'
                        , '".$data[22]."'                            
                        , '".$data[23]."'
                        , '".$data[24]."'
                        , '".$data[25]."'
                        , '".$data[26]."'
                        , '".$data[27]."')";

		$query = mysql_query($sql);
		$newid = mysql_insert_id();
		if(!$query)
			return 0;
		else return $newid;
		}
	}        
        
if(!function_exists('yourcomppracprefs')){
	function yourcomppracprefs($data=array(), $userid=0){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."companions
                       SET 
                       CompEncLikes = '".$data[0]."'
                       where UserID = ".$userid;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}      
        
if(!function_exists('yourcompindulprefs')){
	function yourcompindulprefs($data=array(), $userid=0){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."companions
                       SET 
                       CompEncLikes = '".$data[0]."'
                       where UserID = ".$userid;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}        
        
if(!function_exists('yourmatch')){
	function yourmatch($data=array(), $userid=0){
		$sql = "update ".$GLOBALS['table_prefix']."users set MatchGenderID = ".$data[0].", MatchAgeFrom = ".$data[1].", MatchAgeTO = ".$data[2].", DatingInterestID = ".$data[3].", AboutMyMatch = '".$data[4]."', MatchMaritalStatusID = ".$data[5].", MatchReligionID = ".$data[6].", MatchEducationID = ".$data[7].", MatchBodyStyleID = ".$data[8].", MatchHeightIDFrom = ".$data[9].", MatchHeightIDTo = ".$data[10].", MatchSmokingID = ".$data[11].", MatchDrinkingID = ".$data[12].", MatchBeSameLocation = ".$data[13].", LastLogon = '".gmdate("Y-m-d H:i:s", time()+7*3600)."', DateUpdate = '".gmdate("Y-m-d", time()+7*3600)."', ProfileStatusID = 4 where UserID = ".$userid;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
if(!function_exists('UserLogin')){
	function UserLogin($info=array()){
		$sql = "SELECT UserID from ".$GLOBALS['table_prefix']."users WHERE Email = '".$info[0]."' and Password = '".$info[1]."' and ProfileStatusID <> 0";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		elseif(mysql_num_rows($query)>0){
			$row=mysql_fetch_array($query);
			return $row['UserID'];
			}
		else return -1;
		}
	}
        
// COMP LOGIN

if(!function_exists('CompLogin')){
	function CompLogin($info=array()){
		$sql = "SELECT UserID from ".$GLOBALS['table_prefix']."companions where Email = '".$info[0]."' and Password = '".$info[1]."' and ProfileStatusID <> 0";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		elseif(mysql_num_rows($query)>0){
			$row=mysql_fetch_array($query);
			return $row['UserID'];
			}
		else return -1;
		}
	}
        
if(!function_exists('GetPassword')){
	function GetPassword($user=''){
		$sql = "SELECT Password from ".$GLOBALS['table_prefix']."users where Email = '".$user."'";
		$query = mysql_query($sql);
		$row=mysql_fetch_array($query);
		return $row['Password'];
		}
	}

if(!function_exists('GetMbrPassword')){
	function GetMbrPassword($user=''){
		$sql = "SELECT Password from ".$GLOBALS['table_prefix']."users where Email = '".$user."'";
		$query = mysql_query($sql);
		$row=mysql_fetch_array($query);
		return $row['Password'];
		}
	}
        
if(!function_exists('GetCompPassword')){
	function GetCompPassword($user=''){
		$sql = "SELECT Password from ".$GLOBALS['table_prefix']."companions where Email = '".$user."'";
		$query = mysql_query($sql);
		$row=mysql_fetch_array($query);
		return $row['Password'];
		}
	}
        
if(!function_exists('GetProfileName')){
	function GetProfileName($userid=0){
		$sql = "SELECT ProfileName from ".$GLOBALS['table_prefix']."users where UserID = ".$userid;
		$query = mysql_query($sql);
		$row=mysql_fetch_array($query);
		return $row['ProfileName'];
		}
	}
        
if(!function_exists('GetCompProfileName')){
	function GetCompProfileName($userid=0){
		$sql = "SELECT ProfileName from ".$GLOBALS['table_prefix']."companions where UserID = ".$userid;
		$query = mysql_query($sql);
		$row=mysql_fetch_array($query);
		return $row['ProfileName'];
		}
	}
        
if(!function_exists('GetCompUserId')){
	function GetCompUserID($profilename=''){
		$sql = "SELECT UserID from ".$GLOBALS['table_prefix']."companions where ProfileName = ".$profilename;
		$query = mysql_query($sql);
		$row=mysql_fetch_array($query);
		return $row['UserID'];
		}
	}        
        
if(!function_exists('countMembsersPhotos')){
	function countMembsersPhotos($sqlsearch=''){
		$sql = "select u.UserID from ".$GLOBALS['table_prefix']."users as u inner join ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID and p.IsApproved = 1 where ".$sqlsearch." u.ProfileStatusID = 1 and u.GenderID IS NOT NULL AND u.PrimaryPhotoID > 0";
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}
        
if(!function_exists('countMbrsPhotos')){
	function countMbrsPhotos($sqlsearch=''){
		$sql = "SELECT u.UserID from ".$GLOBALS['table_prefix']."users as u inner join ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID and p.IsApproved = 1 where ".$sqlsearch." u.ProfileStatusID = 1 and u.GenderID IS NOT NULL AND u.PrimaryPhotoID > 0";
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}        
        
/* ADDED */        
if(!function_exists('countCompPhotos')){
	function countCompPhotos($sqlsearch=''){
            $sql = "SELECT 'PhotoID' FROM dt_photos WHERE 'UserID'=$userid";
		//$sql = "select PhotoID from ".$GLOBALS['table_prefix']."photos where UserID = ".$userid;
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	} 
        
if(!function_exists('countMemberPhotos')){
	function countMemberPhotos($sqlsearch=''){
		$sql = "SELECT u.UserID from ".$GLOBALS['table_prefix']."users as u inner join ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID and p.IsApproved = 1 where ".$sqlsearch." u.ProfileStatusID = 1 and u.GenderID IS NOT NULL AND u.PrimaryPhotoID > 0";
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}        
// this is the one to use to count photos        
if(!function_exists('getCompPhotoCount')){
	function getCompPhotoCount($userid=0){
		
		$sql = "SELECT PhotoID
                        FROM ".$GLOBALS['table_prefix']."photos 
                        WHERE UserID = ".$userid." 
                        AND IsApproved = 1 order by InsertDate desc";
                
		$query = mysql_query($sql);
		while($rows = mysql_fetch_array($query)){
			$results['id'][] = $rows['PhotoID'];
		
			}
		return mysql_num_rows($query);
		}
	}

// this is the one to use to count photos        
if(!function_exists('getMemberPhotoCount')){
	function getMemberPhotoCount($userid=0){
		
		$sql = "SELECT PhotoID
                        FROM ".$GLOBALS['table_prefix']."photos 
                        WHERE UserID = ".$userid." 
                        AND IsApproved = 1 order by InsertDate desc";
                
		$query = mysql_query($sql);
		while($rows = mysql_fetch_array($query)){
			$results['id'][] = $rows['PhotoID'];
		
			}
		return mysql_num_rows($query);
		}
	}

        
if(!function_exists('getCompReviewCount')){
	function getCompReviewCount($userid=0){
		
		$sql = "SELECT ReviewID from ".$GLOBALS['table_prefix']."compreviews where CompanionID = ".$userid." and ReviewStatus = 1";
		$query = mysql_query($sql);
		while($rows = mysql_fetch_array($query)){
			$results['id'][] = $rows['ReviewID'];
		
			}
		return mysql_num_rows($query);
		}
	}                

if(!function_exists('getCompReview')){
	function getCompReview($userid=0){
		
		$sql = "SELECT ReviewID
                       FROM
                       ".$GLOBALS['table_prefix']."compreviews where CompanionID = ".$userid." and ReviewStatus = 1";
		$query = mysql_query($sql);
		while($rows = mysql_fetch_array($query)){
			$results['id'][] = $rows['ReviewID'];
		
			}
		return mysql_num_rows($query);
		}
	}          

if(!function_exists('countMemberReviews')){
	function countMemberReviews($userid=0){
            $sql = "SELECT ReviewID FROM ".$GLOBALS['table_prefix']."compreviews WHERE ReviewerID =".$userid;
		$query = mysql_query($sql);
		while($rows = mysql_fetch_array($query)){
			$results['id'][] = $rows['ReviewID'];
		
			}
		return mysql_num_rows($query);
		}
	}         
        
if(!function_exists('getMbrReviewCount')){
	function getMbrReviewCount($userid=0){
            $sql = "SELECT ReviewID from ".$GLOBALS['table_prefix']."compreviews where ReviewerID = ".$userid." and ReviewStatus = 1";
		$query = mysql_query($sql);
		while($rows = mysql_fetch_array($query)){
			$results['id'][] = $rows['ReviewID'];
		
			}
		return mysql_num_rows($query);
		}
	}           
        
if(!function_exists('getInfoMatch')){
	function getInfoMatch($userid=0){
		$sql = "SELECT MatchAgeFrom, MatchAgeTO, MatchGenderID, MatchHeightIDFrom, MatchHeightIDTo, MatchBodyStyleID, MatchReligionID, MatchEducationID, MatchSmokingID, MatchDrinkingID, MatchMaritalStatusID, MatchBeSameLocation, CountryID, DatingInterestID from ".$GLOBALS['table_prefix']."users where UserID = ".$userid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}
if(!function_exists('Pagination')){
	function Pagination($config=array()){
		if($_SESSION['lang']=='L1'){
			$firstpage = 'first';
			$lastpage = 'last';
			}
		else{
			{
			$firstpage = 'first';
			$lastpage = 'last';
			}
			}
		$pages = 'pages:';
                $output = '';
		$rs_maxpage = $config['js_numrows_page']>0?ceil($config['js_numrows_page']/$config['per_page']):0;
		$eitherside = ($config['showeachside'] * $config['per_page']);
		$paga = (strpos($config['cururl'], '?')!==false)?'&p=':'?p=';
		if($rs_maxpage>1){
			if($config['rs_start']+1 > $eitherside){
				$page = $config['curpage'] - 1;
				$output .= '<a href="'.$config['cururl'].'"><i>'.$firstpage.'</i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$config['cururl'].$paga.$page.'"><i><<</i></a> . . . ';
				}
				$pg=1;
			for($y=0; $y<$config['js_numrows_page']; $y+=$config['per_page']){
				if(($y > ($config['rs_start'] - $eitherside)) && ($y < ($config['rs_start'] + $eitherside)))
					$output .= ($pg==$config['curpage'])?'<li><span style="color:#f300fe"><b>'.$pg.'</b></span></li>':'<li><a href="'.$config['cururl'].$paga.$pg.'">'.$pg.'</a></li>';
				$pg++;
				}
			if(($config['rs_start']+$eitherside)<$config['js_numrows_page']){
				$page = $config['curpage'] + 1;
				$output .=  ' . . . <a href="'.$config['cururl'].$paga.$page.'"><i>>></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$config['cururl'].$paga.$rs_maxpage.'"><i>'.$lastpage.'</i></a>';
				}
			}
		return $output;
		}
	}
        
if(!function_exists('getFullPersonal')){
	function getFullPersonal($userid=0){
		$sql = "SELECT u.UserID
                        , ProfileName
                        , Age
                        , AboutMe
                        , City
                        , et.".$_SESSION['lang']."Ethnicity as LEthnicity
                        , MatchAgeFrom
                        , MatchAgeTO
                        , LastLogon
                        , PrimaryPhotoID
                        , m.".$_SESSION['lang']."MaritalStatus as LMaritalStatus
                        , State
                        , Country
                        , g.".$_SESSION['lang']."Gender as LGender
                        , PhotoExtension
                        , h.HeightDescription
                        , b.".$_SESSION['lang']."BodyType as LBodyType
                        , ".$_SESSION['lang']."HairColor as LHairColor
                        , ".$_SESSION['lang']."EyeColor as LEyeColor
                        , ed.".$_SESSION['lang']."Education as LEducation
                        , OccupationID
                        , sm.".$_SESSION['lang']."Smoking as LSmoking
                        , d.".$_SESSION['lang']."Drinking as LDrinking
                        , ".$_SESSION['lang']."DatingInterest as LDatingInterest
                        , HaveChildren
                        , WantChildren
                        , WillingToTravel
                        , Goal
                        , Interests
                        , AboutMyMatch
                        , ge.".$_SESSION['lang']."Gender as MLGender
                        , edu.".$_SESSION['lang']."Education as MLEducation
                        , ma.".$_SESSION['lang']."MaritalStatus as MLMaritalStatus
                        , he.HeightDescription as MFHeightDescription
                        , hei.HeightDescription as MTHeightDescription
                        , MatchHeightIDFrom, MatchHeightIDTo
                        , bo.".$_SESSION['lang']."BodyType as MLBodyType
                        , re.".$_SESSION['lang']."Religion as MLReligion
                        , smo.".$_SESSION['lang']."Smoking as MLSmoking
                        , dr.".$_SESSION['lang']."Drinking as MLDrinking
                        , u.GenderID
                        , u.CountryID
                        , RemainVIPContacts
                        , Email
                        , Phone
                        , IsOnline
                        , u.TotalRating
                        , u.NumberOfVote 
                        FROM ".$GLOBALS['table_prefix']."users as u LEFT JOIN
                        ".$GLOBALS['table_prefix']."maritalstatus as m on u.MaritalStatusID = m.MaritalStatusID LEFT JOIN
                        ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID LEFT JOIN
                        ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID LEFT JOIN
                        ".$GLOBALS['table_prefix']."gender as g on u.GenderID = g.GenderID LEFT JOIN
                        ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID LEFT JOIN
                        ".$GLOBALS['table_prefix']."height as h on u.HeightID = h.HeightID LEFT JOIN
                        ".$GLOBALS['table_prefix']."bodytypes as b on u.BodyTypeID = b.BodyTypeID LEFT JOIN
                        ".$GLOBALS['table_prefix']."haircolors as ha on u.HairColorID = ha.HairColorID LEFT JOIN
                        ".$GLOBALS['table_prefix']."eyescolors as e on u.EyeColorID = e.EyeColorID LEFT JOIN
                        ".$GLOBALS['table_prefix']."ethnicity as et on u.EthnicityID = et.EthnicityID LEFT JOIN
                        ".$GLOBALS['table_prefix']."educations as ed on u.EducationID = ed.EducationID LEFT JOIN
                        ".$GLOBALS['table_prefix']."smoking as sm on u.SmokingID = sm.SmokingID LEFT JOIN
                        ".$GLOBALS['table_prefix']."drinking as d on u.DrinkingID = d.DrinkingID LEFT JOIN
                        ".$GLOBALS['table_prefix']."datinginterest as da on u.DatingInterestID = da.DatingInterestID LEFT JOIN
                        ".$GLOBALS['table_prefix']."gender as ge on u.MatchGenderID = ge.GenderID LEFT JOIN
                        ".$GLOBALS['table_prefix']."educations as edu on u.MatchEducationID = edu.EducationID LEFT JOIN
                        ".$GLOBALS['table_prefix']."maritalstatus as ma on u.MatchMaritalStatusID = ma.MaritalStatusID LEFT JOIN
                        ".$GLOBALS['table_prefix']."height as he on u.MatchHeightIDFrom = he.HeightID LEFT JOIN
                        ".$GLOBALS['table_prefix']."height as hei on u.MatchHeightIDTo = hei.HeightID LEFT JOIN
                        ".$GLOBALS['table_prefix']."bodytypes as bo on u.MatchBodyStyleID = bo.BodyTypeID LEFT JOIN
                        
                        ".$GLOBALS['table_prefix']."smoking as smo on u.MatchSmokingID = smo.SmokingID LEFT JOIN
                        ".$GLOBALS['table_prefix']."drinking as dr on u.MatchDrinkingID = dr.DrinkingID where u.UserID = ".$userid." and u.ProfileStatusID = 1 and u.GenderID IS NOT NULL ";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}

if(!function_exists('getFullMemberProfile')){
	function getFullMemberProfile($userid=0){
		$sql = "SELECT u.UserID
                        , ProfileName
                        , Age
                        , AboutMe
                        , City
                        , et.".$_SESSION['lang']."Ethnicity as LEthnicity
                        , MatchAgeFrom
                        , MatchAgeTO
                        , LastLogon
                        , PrimaryPhotoID
                        , m.".$_SESSION['lang']."MaritalStatus as LMaritalStatus
                        , State
                        , Country
                        , g.".$_SESSION['lang']."Gender as LGender
                        , PhotoExtension
                        , h.HeightDescription
                        , b.".$_SESSION['lang']."BodyType as LBodyType
                        , ".$_SESSION['lang']."HairColor as LHairColor
                        , ".$_SESSION['lang']."EyeColor as LEyeColor
                        , ed.".$_SESSION['lang']."Education as LEducation
                        , OccupationID
                        , sm.".$_SESSION['lang']."Smoking as LSmoking
                        , d.".$_SESSION['lang']."Drinking as LDrinking
                        , ".$_SESSION['lang']."DatingInterest as LDatingInterest
                        , HaveChildren
                        , WantChildren
                        , WillingToTravel
                        , Goal
                        , Interests
                        , AboutMyMatch
                        , ge.".$_SESSION['lang']."Gender as MLGender
                        , edu.".$_SESSION['lang']."Education as MLEducation
                        , ma.".$_SESSION['lang']."MaritalStatus as MLMaritalStatus
                        , he.HeightDescription as MFHeightDescription
                        , hei.HeightDescription as MTHeightDescription
                        , MatchHeightIDFrom, MatchHeightIDTo
                        , bo.".$_SESSION['lang']."BodyType as MLBodyType
                        , re.".$_SESSION['lang']."Religion as MLReligion
                        , smo.".$_SESSION['lang']."Smoking as MLSmoking
                        , dr.".$_SESSION['lang']."Drinking as MLDrinking
                        , u.GenderID
                        , u.CountryID
                        , RemainVIPContacts
                        , Email
                        , Phone
                        , IsOnline
                        , u.TotalRating
                        , u.NumberOfVote 
                        FROM ".$GLOBALS['table_prefix']."users as u LEFT JOIN
                        ".$GLOBALS['table_prefix']."maritalstatus as m on u.MaritalStatusID = m.MaritalStatusID LEFT JOIN
                        ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID LEFT JOIN
                        ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID LEFT JOIN
                        ".$GLOBALS['table_prefix']."gender as g on u.GenderID = g.GenderID LEFT JOIN
                        ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID LEFT JOIN
                        ".$GLOBALS['table_prefix']."height as h on u.HeightID = h.HeightID LEFT JOIN
                        ".$GLOBALS['table_prefix']."bodytypes as b on u.BodyTypeID = b.BodyTypeID LEFT JOIN
                        ".$GLOBALS['table_prefix']."haircolors as ha on u.HairColorID = ha.HairColorID LEFT JOIN
                        ".$GLOBALS['table_prefix']."eyescolors as e on u.EyeColorID = e.EyeColorID LEFT JOIN
                        ".$GLOBALS['table_prefix']."ethnicity as et on u.EthnicityID = et.EthnicityID LEFT JOIN
                        ".$GLOBALS['table_prefix']."educations as ed on u.EducationID = ed.EducationID LEFT JOIN
                        ".$GLOBALS['table_prefix']."smoking as sm on u.SmokingID = sm.SmokingID LEFT JOIN
                        ".$GLOBALS['table_prefix']."drinking as d on u.DrinkingID = d.DrinkingID LEFT JOIN
                        ".$GLOBALS['table_prefix']."datinginterest as da on u.DatingInterestID = da.DatingInterestID LEFT JOIN
                        ".$GLOBALS['table_prefix']."gender as ge on u.MatchGenderID = ge.GenderID LEFT JOIN
                        ".$GLOBALS['table_prefix']."educations as edu on u.MatchEducationID = edu.EducationID LEFT JOIN
                        ".$GLOBALS['table_prefix']."maritalstatus as ma on u.MatchMaritalStatusID = ma.MaritalStatusID LEFT JOIN
                        ".$GLOBALS['table_prefix']."height as he on u.MatchHeightIDFrom = he.HeightID LEFT JOIN
                        ".$GLOBALS['table_prefix']."height as hei on u.MatchHeightIDTo = hei.HeightID LEFT JOIN
                        ".$GLOBALS['table_prefix']."bodytypes as bo on u.MatchBodyStyleID = bo.BodyTypeID LEFT JOIN
                        
                        ".$GLOBALS['table_prefix']."smoking as smo on u.MatchSmokingID = smo.SmokingID LEFT JOIN
                        ".$GLOBALS['table_prefix']."drinking as dr on u.MatchDrinkingID = dr.DrinkingID where u.UserID = ".$userid." and u.ProfileStatusID = 1 and u.GenderID IS NOT NULL ";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}

        
if(!function_exists('getFullComp')){
	function getFullComp($userid=0){
		$sql = "SELECT u.UserID
                        , ProfileName
                        , Age
                        , AboutMe
                        , City
                        , LastLogon
                        , PrimaryPhotoID
                        , State
                        , Country
                        , g.".$_SESSION['lang']."Gender as LGender
                        , PhotoExtension
                        , h.HeightDescription
                        , et.".$_SESSION['lang']."Ethnicity as LEthnicity
                        , b.".$_SESSION['lang']."BodyType as LBodyType
                        , ".$_SESSION['lang']."HairColor as LHairColor
                        , ".$_SESSION['lang']."EyeColor as LEyeColor
                        , he.HeightDescription as MFHeightDescription
                        , u.GenderID
                        , u.CountryID
                        , Email
                        , Phone
                        , IsOnline
                        , u.TotalRating
                        , u.NumberOfVote 
                        FROM 
                        ".$GLOBALS['table_prefix']."companions as u LEFT JOIN
                        ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID LEFT JOIN
                        ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID LEFT JOIN
                        ".$GLOBALS['table_prefix']."ethnicity as et on u.EthnicityID = et.EthnicityID LEFT JOIN
                        ".$GLOBALS['table_prefix']."gender as g on u.GenderID = g.GenderID LEFT JOIN
                        ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID LEFT JOIN
                        ".$GLOBALS['table_prefix']."height as h on u.HeightID = h.HeightID LEFT JOIN
                        ".$GLOBALS['table_prefix']."bodytypes as b on u.BodyTypeID = b.BodyTypeID LEFT JOIN
                        ".$GLOBALS['table_prefix']."haircolors as ha on u.HairColorID = ha.HairColorID LEFT JOIN
                        ".$GLOBALS['table_prefix']."eyescolors as e on u.EyeColorID = e.EyeColorID LEFT JOIN
                        WHERE 
                        u.UserID = ".$userid." and u.ProfileStatusID = 1 and u.GenderID IS NOT NULL ";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}

if(!function_exists('getPartMbr')){
	function getPartMbr($userid=0){
		$sql = "SELECT u.UserID
                        , ProfileName
                        , Age
                        , AboutMe
                        , City
                        , LastLogon
                        , PrimaryPhotoID
                        , State
                        , Country
                        , DatePosted
                        , PhotoExtension
                        , b.".$_SESSION['lang']."BodyType as LBodyType
                        , ".$_SESSION['lang']."HairColor as LHairColor
                        , ".$_SESSION['lang']."EyeColor as LEyeColor
                        , ".$_SESSION['lang']."Ethnicity as LEthnicity    
                        , ".$_SESSION['lang']."Gender as LGender
                        , ".$_SESSION['lang']."SexOr as LSexOr
                        , ".$_SESSION['lang']."Smoking as LSmoking
                        , ".$_SESSION['lang']."Drinking as LDrinking
                        , u.HeightID         
                        , u.CountryID
                        , Email
                        , Phone
                        , IsOnline
                        FROM 
                        ".$GLOBALS['table_prefix']."users as u LEFT JOIN
                        ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID LEFT JOIN
                        ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID LEFT JOIN
                        ".$GLOBALS['table_prefix']."gender as g on u.GenderID = g.GenderID LEFT JOIN
                        ".$GLOBALS['table_prefix']."sexor as se on u.SexOrID = se.SexOrID LEFT JOIN    
                        ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID LEFT JOIN
                        ".$GLOBALS['table_prefix']."bodytypes as b on u.BodyTypeID = b.BodyTypeID LEFT JOIN
                        ".$GLOBALS['table_prefix']."haircolors as ha on u.HairColorID = ha.HairColorID LEFT JOIN
                        ".$GLOBALS['table_prefix']."smoking as sm on u.SmokingID = sm.SmokingID LEFT JOIN
                        ".$GLOBALS['table_prefix']."drinking as dr on u.DrinkingID = dr.DrinkingID LEFT JOIN 
                        ".$GLOBALS['table_prefix']."height as he on u.HeightID = he.HeightID LEFT JOIN        
                        ".$GLOBALS['table_prefix']."eyescolors as e on u.EyeColorID = e.EyeColorID LEFT JOIN
                        ".$GLOBALS['table_prefix']."ethnicity as r on u.EthnicityID = r.EthnicityID 
                        WHERE 
                        u.UserID = ".$userid." and u.ProfileStatusID = 1 and u.GenderID IS NOT NULL ";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}        
        
        
if(!function_exists('getPartComp')){
	function getPartComp($userid=0){
		$sql = "SELECT u.UserID
                        , ProfileName
                        , Age
                        , AboutMe
                        , City
                        , LastLogon
                        , PrimaryPhotoID
                        , State
                        , Country
                        , PhotoExtension
                        , b.".$_SESSION['lang']."BodyType as LBodyType
                        , ".$_SESSION['lang']."HairColor as LHairColor
                        , ".$_SESSION['lang']."EyeColor as LEyeColor
                        , ".$_SESSION['lang']."Ethnicity as LEthnicity    
                        , ".$_SESSION['lang']."Gender as LGender
                        , u.CountryID
                        , Email
                        , Phone
                        , IsOnline
                        , u.TotalRating
                        , u.NumberOfVote
                        , CompEmail
                        , Independent
                        , Transgender
                        , Transsexual
                        , Pornstar
                        , Website
                        , Webprofile
                        , Whatsapp
                        , Instagram
                        FROM 
                        ".$GLOBALS['table_prefix']."companions as u LEFT JOIN
                        ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID LEFT JOIN
                        ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID LEFT JOIN
                        ".$GLOBALS['table_prefix']."gender as g on u.GenderID = g.GenderID LEFT JOIN
                        ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID LEFT JOIN
                        ".$GLOBALS['table_prefix']."bodytypes as b on u.BodyTypeID = b.BodyTypeID LEFT JOIN
                        ".$GLOBALS['table_prefix']."haircolors as ha on u.HairColorID = ha.HairColorID LEFT JOIN
                        ".$GLOBALS['table_prefix']."eyescolors as e on u.EyeColorID = e.EyeColorID LEFT JOIN
                        ".$GLOBALS['table_prefix']."ethnicity as r on u.EthnicityID = r.EthnicityID 
                        WHERE 
                        u.UserID = ".$userid." and u.ProfileStatusID = 1 and u.GenderID IS NOT NULL ";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}

if(!function_exists('getPartCompProfile')){
	function getPartCompProfile($userid=0){
		$sql = "SELECT u.UserID
                        , ProfileName
                        , Age
                        , AboutMe
                        , City
                        , LastLogon
                        , PrimaryPhotoID
                        , State
                        , Country
                        , ProfileStatusID
                        , HeightID
                        , PhotoExtension
                        , b.".$_SESSION['lang']."BodyType as LBodyType
                        , ".$_SESSION['lang']."HairColor as LHairColor
                        , ".$_SESSION['lang']."HairLength as LHairLength   
                        , ".$_SESSION['lang']."EyeColor as LEyeColor
                        , ".$_SESSION['lang']."Ethnicity as LEthnicity    
                        , ".$_SESSION['lang']."Gender as LGender
                        , u.CountryID
                        , Email
                        , Phone
                        , IsOnline
                        , SmokingID
                        , DrinkingID
                        , u.TotalRating
                        , u.NumberOfVote
                        , BreastImp
                        , CompEmail
                        , ".$_SESSION['lang']."Pussy as LPussy
                        , ".$_SESSION['lang']."SexOr as LSexOr
                        , ".$_SESSION['lang']."CompBreasts as LCompBreasts
                        , ".$_SESSION['lang']."CompAss as LCompAss
                        , ".$_SESSION['lang']."Piercings as LPiercings
                        , ".$_SESSION['lang']."Tattoos as LTattoos
                        , CompEncLike
                        , Independent
                        , Transgender
                        , Transsexual
                        , Pornstar
                        , Website
                        , WebProfile
                        , Whatsapp
                        , Instagram
                        , DatePosted
                        , LastLogon
                        , Views
                        , MembershipStatusID
                        FROM 
                        ".$GLOBALS['table_prefix']."companions as u LEFT JOIN
                        ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID LEFT JOIN
                        ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID LEFT JOIN
                        ".$GLOBALS['table_prefix']."gender as g on u.GenderID = g.GenderID LEFT JOIN
                        ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID LEFT JOIN
                        ".$GLOBALS['table_prefix']."bodytypes as b on u.BodyTypeID = b.BodyTypeID LEFT JOIN
                        ".$GLOBALS['table_prefix']."haircolors as ha on u.HairColorID = ha.HairColorID LEFT JOIN
                        ".$GLOBALS['table_prefix']."hairlengths as hl on u.HairLengthID = hl.HairLengthID LEFT JOIN    
                        ".$GLOBALS['table_prefix']."eyescolors as e on u.EyeColorID = e.EyeColorID LEFT JOIN
                        ".$GLOBALS['table_prefix']."ethnicity as r on u.EthnicityID = r.EthnicityID LEFT JOIN
                        ".$GLOBALS['table_prefix']."pussy as pu on u.PussyID = pu.PussyID LEFT JOIN
                        ".$GLOBALS['table_prefix']."sexor as so on u.SexOrID = so.SexOrID LEFT JOIN
                        ".$GLOBALS['table_prefix']."compbreasts as cb on u.CompBreastsID = cb.CompBreastsID LEFT JOIN     
                        ".$GLOBALS['table_prefix']."compass as ca on u.CompAssID = ca.CompAssID LEFT JOIN
                        ".$GLOBALS['table_prefix']."piercings as pc on u.PiercingsID = pc.PiercingsID LEFT JOIN
                        ".$GLOBALS['table_prefix']."tattoos as t on u.TattoosID = t.TattoosID    
                        WHERE 
                        u.UserID = ".$userid." and u.ProfileStatusID = 1 and u.GenderID IS NOT NULL ";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}        

if(!function_exists('getCompIndul')){
	function getCompIndul($userid=0){
		$sql = "SELECT *
                        FROM 
                        ".$GLOBALS['table_prefix']."compindul    
                        WHERE 
                        UserID = ".$userid."";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}        

if(!function_exists('getCompIndulByMbr')){
	function getCompIndulByMbr($userid=0){
		$sql = "SELECT *
                        FROM 
                        ".$GLOBALS['table_prefix']."compindulbymbr    
                        WHERE 
                        UserID = ".$userid."";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}        
        
if(!function_exists('getCompDonations')){
	function getCompDonations($userid=0){
		$sql = "SELECT *
                        FROM 
                        ".$GLOBALS['table_prefix']."compdonations    
                        WHERE 
                        UserID = ".$userid."";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}     
        
if(!function_exists('getFullCompAll')){
	function getFullCompAll($userid=0){
		$sql = "select * FROM
                        ".$GLOBALS['table_prefix']."companions as u LEFT JOIN
                        ".$GLOBALS['table_prefix']."maritalstatus as m on u.MaritalStatusID = m.MaritalStatusID LEFT JOIN
                        ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID LEFT JOIN
                        ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID LEFT JOIN 
                        ".$GLOBALS['table_prefix']."gender as g on u.GenderID = g.GenderID LEFT JOIN
                        ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID LEFT JOIN 
                        ".$GLOBALS['table_prefix']."height as h on u.HeightID = h.HeightID left join
                        ".$GLOBALS['table_prefix']."bodytypes as b on u.BodyTypeID = b.BodyTypeID left join
                        ".$GLOBALS['table_prefix']."haircolors as ha on u.HairColorID = ha.HairColorID left join
                        ".$GLOBALS['table_prefix']."eyescolors as e on u.EyeColorID = e.EyeColorID left join
                        ".$GLOBALS['table_prefix']."pussy as r on u.PussyID = r.PussyID left join
                        ".$GLOBALS['table_prefix']."sexor as ed on u.SexOrID = ed.SexOrID left join 
                        ".$GLOBALS['table_prefix']."smoking as sm on u.SmokingID = sm.SmokingID left join 
                        ".$GLOBALS['table_prefix']."drinking as d on u.DrinkingID = d.DrinkingID left join 
                        ".$GLOBALS['table_prefix']."datinginterest as da on u.DatingInterestID = da.DatingInterestID left join
                        ".$GLOBALS['table_prefix']."gender as ge on u.MatchGenderID = ge.GenderID left join 
                        ".$GLOBALS['table_prefix']."educations as edu on u.MatchEducationID = edu.EducationID left join 
                        ".$GLOBALS['table_prefix']."maritalstatus as ma on u.MatchMaritalStatusID = ma.MaritalStatusID left join 
                        ".$GLOBALS['table_prefix']."height as he on u.MatchHeightIDFrom = he.HeightID left join 
                        ".$GLOBALS['table_prefix']."height as hei on u.MatchHeightIDTo = hei.HeightID left join 
                        ".$GLOBALS['table_prefix']."bodytypes as bo on u.MatchBodyStyleID = bo.BodyTypeID left join 
                        ".$GLOBALS['table_prefix']."religions as re on u.MatchReligionID = re.ReligionID left join 
                        ".$GLOBALS['table_prefix']."smoking as smo on u.MatchSmokingID = smo.SmokingID left join 
                        ".$GLOBALS['table_prefix']."drinking as dr on u.MatchDrinkingID = dr.DrinkingID 
                        WHERE u.UserID = ".$userid." and u.ProfileStatusID = 1 and u.GenderID IS NOT NULL ";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}  
        
if(!function_exists('getFullCompProfile')){
	function getFullCompProfile($userid=0){
		$sql = "select * FROM
                        ".$GLOBALS['table_prefix']."companions as u LEFT JOIN
                        ".$GLOBALS['table_prefix']."maritalstatus as m on u.MaritalStatusID = m.MaritalStatusID LEFT JOIN
                        ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID LEFT JOIN
                        ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID LEFT JOIN 
                        ".$GLOBALS['table_prefix']."gender as g on u.GenderID = g.GenderID LEFT JOIN
                        ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID LEFT JOIN 
                        ".$GLOBALS['table_prefix']."height as h on u.HeightID = h.HeightID left join
                        ".$GLOBALS['table_prefix']."bodytypes as b on u.BodyTypeID = b.BodyTypeID left join
                        ".$GLOBALS['table_prefix']."haircolors as ha on u.HairColorID = ha.HairColorID left join
                        ".$GLOBALS['table_prefix']."eyescolors as e on u.EyeColorID = e.EyeColorID left join
                        ".$GLOBALS['table_prefix']."pussy as r on u.PussyID = r.PussyID left join
                        ".$GLOBALS['table_prefix']."sexor as ed on u.SexOrID = ed.SexOrID left join 
                        ".$GLOBALS['table_prefix']."smoking as sm on u.SmokingID = sm.SmokingID left join 
                        ".$GLOBALS['table_prefix']."drinking as d on u.DrinkingID = d.DrinkingID left join 
                        ".$GLOBALS['table_prefix']."datinginterest as da on u.DatingInterestID = da.DatingInterestID left join
                        ".$GLOBALS['table_prefix']."gender as ge on u.MatchGenderID = ge.GenderID left join 
                        ".$GLOBALS['table_prefix']."educations as edu on u.MatchEducationID = edu.EducationID left join 
                        ".$GLOBALS['table_prefix']."maritalstatus as ma on u.MatchMaritalStatusID = ma.MaritalStatusID left join 
                        ".$GLOBALS['table_prefix']."height as he on u.MatchHeightIDFrom = he.HeightID left join 
                        ".$GLOBALS['table_prefix']."height as hei on u.MatchHeightIDTo = hei.HeightID left join 
                        ".$GLOBALS['table_prefix']."bodytypes as bo on u.MatchBodyStyleID = bo.BodyTypeID left join 
                        ".$GLOBALS['table_prefix']."religions as re on u.MatchReligionID = re.ReligionID left join 
                        ".$GLOBALS['table_prefix']."smoking as smo on u.MatchSmokingID = smo.SmokingID left join 
                        ".$GLOBALS['table_prefix']."drinking as dr on u.MatchDrinkingID = dr.DrinkingID 
                        WHERE u.UserID = ".$userid." and u.ProfileStatusID = 1 and u.GenderID IS NOT NULL ";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}                
        
if(!function_exists('getUserPhotos')){
	function getUserPhotos($userid=0){
		$results = array();
		$sql = "SELECT PhotoID, PhotoExtension from ".$GLOBALS['table_prefix']."photos where UserID = ".$userid." and IsApproved = 1 order by InsertDate desc";
		$query = mysql_query($sql);
		while($rows = mysql_fetch_array($query)){
			$results['id'][] = $rows['PhotoID'];
			$results['ext'][] = $rows['PhotoExtension'];
			}
		return $results;
		}
	}
if(!function_exists('getSameProfile')){
	function getSameProfile($gender=0, $country=0, $userid=0, $age=0){
		$agef = $age - 5;
		$aget = $age + 5;
		$sql = "SELECT u.UserID, ProfileName, Age, City, PrimaryPhotoID, PhotoExtension, State, Country from ".$GLOBALS['table_prefix']."users as u left join ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID left join ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID left join ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID and p.IsApproved = 1 where u.UserID <> ".$userid." and u.GenderID = ".$gender." and u.ProfileStatusID = 1 and u.CountryID = ".$country.' AND p.PhotoID IS not NULL and (Age >= '.$agef.' and Age <= '.$aget.') order by rand() limit 5';
		$query = mysql_query($sql);
		return $query;
		}
	}
if(!function_exists('getPersonalInfo')){
	function getPersonalInfo($userid=0){
		$sql = "SELECT ProfileName, AboutMe, GenderID, Age, CountryID, StateID, City, Zipcode, Interests, MaritalStatusID, Occupation, Phone, ReligionID, EducationID, BodyTypeID, HairColorID, EyeColorID, HeightID, SmokingID, DrinkingID, HaveChildren, WantChildren, WillingToTravel from ".$GLOBALS['table_prefix']."users where UserID = ".$userid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}
if(!function_exists('getPersonalMatch')){
	function getPersonalMatch($userid=0){
		$sql = "SELECT MatchGenderID, MatchAgeFrom, MatchAgeTO, DatingInterestID, AboutMyMatch, MatchMaritalStatusID, MatchReligionID, MatchEducationID, MatchBodyStyleID, MatchHeightIDFrom, MatchHeightIDTo, MatchSmokingID, MatchDrinkingID, MatchBeSameLocation from ".$GLOBALS['table_prefix']."users where UserID = ".$userid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}
if(!function_exists('ChangePassword')){
	function ChangePassword($data=array()){
		$sql = "SELECT UserID, Password from ".$GLOBALS['table_prefix']."users where UserID = ".$data[2]." and Password = '".$data[0]."'";
		$query = mysql_query($sql);
		if(!$query)
			return -1;
		elseif(mysql_num_rows($query)>0){
			$uppass = "update ".$GLOBALS['table_prefix']."users set Password = '".$data[1]."' where UserID = ".$data[2];
			$uqery = mysql_query($uppass);
			if(!$uqery)
				return -1;
			else return 1;
			}
		else return 0;
		}
	}
        
if(!function_exists('settingsAccount')){
	function settingsAccount($data=array()){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."users set ProfileStatusID = ".$data[0].", NotificationScheduleID = ".$data[1]." where UserID = ".$data[2];
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
if(!function_exists('getSettingsAccount')){
	function getSettingsAccount($user=0){
		$sql = "SELECT u.ProfileStatusID, NotificationScheduleID, ".$_SESSION['lang']."ProfileStatus as LProfileStatus, ".$_SESSION['lang']."NOTIFY as LNOTIFY from ".$GLOBALS['table_prefix']."users as u left join ".$GLOBALS['table_prefix']."profilestatus as p on u.ProfileStatusID = p.ProfileStatusID left join ".$GLOBALS['table_prefix']."notifyemail as n on u.NotificationScheduleID = n.Id where UserID = ".$user;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}
        
// sets primary photo for member clients        
if(!function_exists('setPrimaryPhoto')){
	function setPrimaryPhoto($data=array()){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."users set PrimaryPhotoID = ".$data[1]." where UserID = ".$data[0];
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}

// sets primary photo for companion clients        
if(!function_exists('setCompPrimaryPhoto')){
	function setCompPrimaryPhoto($data=array()){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."companions set PrimaryPhotoID = ".$data[1]." where UserID = ".$data[0];
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}        
        
// gets primary photo for member clients           
if(!function_exists('getPrimaryPhoto')){
	function getPrimaryPhoto($userid=0){
		$img='';
		$sql = "SELECT u.PrimaryPhotoID, PhotoExtension from ".$GLOBALS['table_prefix']."users as u left join ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID where u.UserID = ".$userid.' and u.PrimaryPhotoID > 0';
		$query = mysql_query($sql);
		if(!$query)
			return $img;
		elseif(mysql_num_rows($query)>0){
			$row = mysql_fetch_array($query);
			return $userid.'u'.$row['PrimaryPhotoID'].'.'.$row['PhotoExtension'];
			}
		else return $img;
		}
	}
        
// gets primary photo for member clients           
if(!function_exists('getCompPrimaryPhoto')){
	function getCompPrimaryPhoto($user=0){
		$img='';
		$sql = "SELECT u.PrimaryPhotoID, PhotoExtension from ".$GLOBALS['table_prefix']."companions as u left join ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID where u.UserID = ".$user.' and u.PrimaryPhotoID > 0';
		$query = mysql_query($sql);
		if(!$query)
			return $img;
		elseif(mysql_num_rows($query)>0){
			$row = mysql_fetch_array($query);
			return $user.'u'.$row['PrimaryPhotoID'].'.'.$row['PhotoExtension'];
			}
		else return $img;
		}
	}        

// gets primary photo for member clients           
if(!function_exists('getMemberPrimaryPhoto')){
	function getMemberPrimaryPhoto($user=0){
		$img='';
		$sql = "SELECT u.PrimaryPhotoID, PhotoExtension from ".$GLOBALS['table_prefix']."users as u left join ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID where u.UserID = ".$user.' and u.PrimaryPhotoID > 0';
		$query = mysql_query($sql);
		if(!$query)
			return $img;
		elseif(mysql_num_rows($query)>0){
			$row = mysql_fetch_array($query);
			return $user.'u'.$row['PrimaryPhotoID'].'.'.$row['PhotoExtension'];
			}
		else return $img;
		}
	}        
        
// delete photo for all clients          
if(!function_exists('delPhotos')){
	function delPhotos($data=array(), $user=0){
		$sql = "DELETE from ".$GLOBALS['table_prefix']."photos where PhotoID in (".implode(', ', $data).") and UserID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
// gets all photos for all clients          
if(!function_exists('getAllPhotos')){
	function getAllPhotos($userid=0){
		$results = array();
		$sql = "SELECT PhotoID, PhotoExtension, IsApproved from ".$GLOBALS['table_prefix']."photos where UserID = ".$userid." order by InsertDate desc";
		$query = mysql_query($sql);
		while($rows = mysql_fetch_array($query)){
			$results['id'][] = $rows['PhotoID'];
			$results['ext'][] = $rows['PhotoExtension'];
			$results['app'][] = $rows['IsApproved'];
			}
		return $results;
		}
	}
        
// adds photos for member clients          
if(!function_exists('addPhotos')){
	function addPhotos($data=array()){
		$sql = "INSERT into ".$GLOBALS['table_prefix']."photos (UserID, PhotoExtension, PhotoDescription, IsApproved, PrimaryPhoto, InsertDate, TotalRating, NumberOfVote) values (".$data[0].", '".$data[1]."', '".$data[2]."', 0, 0, '".gmdate("Y-m-d", time()-5*3600)."', 0, 0)";
		$query = mysql_query($sql);
		$id = mysql_insert_id();
		if(!$query)
			return -1;
		else return $id;
		}
	}
        
        
if(!function_exists('countSearchProfile')){
	function countSearchProfile($sqlsearch='', $only = 0){
		if($only>0)
			$sql = "SELECT u.UserID from ".$GLOBALS['table_prefix']."users as u left join ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID and p.IsApproved = 1 where ".$sqlsearch." u.ProfileStatusID = 1 and u.GenderID IS NOT NULL AND p.PhotoID IS not NULL and GroupID <> 5";
		else $sql = "select u.UserID from ".$GLOBALS['table_prefix']."users as u left join ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID and p.IsApproved = 1 where ".$sqlsearch." u.ProfileStatusID = 1 and u.GenderID IS NOT NULL and GroupID <> 5";
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}
        
        
if(!function_exists('saveSearch')){
	function saveSearch($query='', $user = 0, $name = ''){
		$sql = "insert into ".$GLOBALS['table_prefix']."savedsearch (UserID, SearchName, Query, SearchValue, DateCreated, DateProcessed, SubcriptionStatusID) values (".$user.", '".$name."', '".$query."', '".$query."', '".gmdate("Y-m-d H:i:s", time()+7*3600)."', '".gmdate("Y-m-d H:i:s", time()+7*3600)."', 0)";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('savingProfile')){
	function savingProfile($user = 0, $profile = 0){
		$slsql = 'select UserID from '.$GLOBALS['table_prefix'].'hotlists where UserID = '.$user.' and SavedUserID = '.$profile;
		$slqry = mysql_query($slsql);
		if(!$slqry)
			return -1;
		elseif(mysql_num_rows($slqry)>0)
			return 0;
		else{
			$sql = "insert into ".$GLOBALS['table_prefix']."hotlists (UserID, SavedUserID, SavedDate) values (".$user.", ".$profile.", '".gmdate("Y-m-d H:i:s", time()+7*3600)."')";
			$query = mysql_query($sql);
			if(!$query)
				return -1;
			else return 1;
			}
		}
	}
        
        
if(!function_exists('addFavoriteCompProfile')){
	function addFavoriteCompProfile($user = 0, $profile = 0){
		$slsql = 'SELECT UserID from '.$GLOBALS['table_prefix'].'favorites where UserID = '.$user.' and SavedUserID = '.$profile;
		$slqry = mysql_query($slsql);
		if(!$slqry)
			return -1;
		elseif(mysql_num_rows($slqry)>0)
			return 0;
		else{
			$sql = "INSERT into ".$GLOBALS['table_prefix']."favorites (UserID, SavedUserID, SavedDate) values (".$user.", ".$profile.", '".gmdate("Y-m-d H:i:s", time()+7*3600)."')";
			$query = mysql_query($sql);
			if(!$query)
				return -1;
			else return 1;
			}
		}
	}        
        
        
if(!function_exists('addFavoriteCompReview')){
	function addFavoriteCompReview($user = 0, $review = 0){
		$slsql = 'SELECT UserID from '.$GLOBALS['table_prefix'].'favoritereviews where UserID = '.$user.' and SavedReviewID = '.$review;
		$slqry = mysql_query($slsql);
		if(!$slqry)
			return -1;
		elseif(mysql_num_rows($slqry)>0)
			return 0;
		else{
			$sql = "INSERT into ".$GLOBALS['table_prefix']."favoritereviews (UserID, SavedReviewID, SavedDate) values (".$user.", ".$review.", '".gmdate("Y-m-d H:i:s", time()+7*3600)."')";
			$query = mysql_query($sql);
			if(!$query)
				return -1;
			else return 1;
			}
		}
	}                
        
if(!function_exists('sendSignal')){
	function sendSignal($user = 0, $profile = 0){
		$sql = "insert into ".$GLOBALS['table_prefix']."signalme (RecieverID, SenderID, SignalDate) values (".$profile.", ".$user.", '".gmdate("Y-m-d H:i:s", time()+7*3600)."')";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
if(!function_exists('countSavedProfile')){
	function countSavedProfile($user=0){
		$sql = "select UserID from ".$GLOBALS['table_prefix']."hotlists where UserID = ".$user;
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}

// counts number of comp profiles favorited         
if(!function_exists('countFavCompProfiles')){
	function countFavCompProfiles($user=0){
		$sql = "SELECT UserID from ".$GLOBALS['table_prefix']."favorites where UserID = ".$user;
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}        

        
if(!function_exists('countFavComps')){
	function countFavComps($user=0){
		$sql = "SELECT SavedUserID from ".$GLOBALS['table_prefix']."favorites where SavedUserID = ".$user;
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}        
        
if(!function_exists('countFavCompReviews')){
	function countFavCompReviews($user=0){
		$sql = "SELECT UserID from ".$GLOBALS['table_prefix']."favoritereviews where UserID = ".$user;
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}          
        
if(!function_exists('delFavoriteCompProfile')){
	function delFavoriteCompProfile($user = 0, $profile = 0){
		$sql = "DELETE from ".$GLOBALS['table_prefix']."favorites where UserID = ".$user." and SavedUserID = ".$profile;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}        
        
if(!function_exists('delSavedProfile')){
	function delSavedProfile($user = 0, $profile = 0){
		$sql = "delete from ".$GLOBALS['table_prefix']."hotlists where UserID = ".$user." and SavedUserID = ".$profile;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('countSignalTo')){
	function countSignalTo($user=0){
		$sql = "select RecieverID from ".$GLOBALS['table_prefix']."signalme as s inner join ".$GLOBALS['table_prefix']."users as u on s.SenderID = u.UserID where RecieverID = ".$user;
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}
        
        
if(!function_exists('countSignalFrom')){
	function countSignalFrom($user=0){
		$sql = "select SenderID from ".$GLOBALS['table_prefix']."signalme as s inner join ".$GLOBALS['table_prefix']."users as u on s.RecieverID = u.UserID where SenderID = ".$user;
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}
        
        
if(!function_exists('updateWhoViews')){
	function updateWhoViews($user=0, $profile = 0){
		$sql = "select Id from ".$GLOBALS['table_prefix']."whoviews where UserId = ".$user.' and ViewUserId = '.$profile;
		$query = mysql_query($sql);
		if(!$query)
			return;
		elseif(mysql_num_rows($query)>0){
			$upsql = 'update '.$GLOBALS['table_prefix'].'whoviews set ViewDate = '.gmdate("Y-m-d H:i:s", time()+7*3600).' where UserId = '.$user.' and ViewUserId = '.$profile;
			$upqry = mysql_query($sql);
			if(!$upqry)
				return;
			}
		else{
			$insql = "insert into ".$GLOBALS['table_prefix']."whoviews (UserId, ViewUserId, ViewDate, IsView) values (".$user.", ".$profile.", '".gmdate("Y-m-d H:i:s", time()+7*3600)."', 0) ";
			$inqry = mysql_query($insql);
			if(!$inqry)
				return;
			}
		}
	}
        
        
if(!function_exists('updateWhoViewsComp')){
	function updateWhoViewsComp($user=0, $profile = 0){
		$sql = "select Id from ".$GLOBALS['table_prefix']."whoviewscomp where UserId = ".$user.' and ViewUserId = '.$profile;
		$query = mysql_query($sql);
		if(!$query)
			return;
		elseif(mysql_num_rows($query)>0){
			$upsql = 'update '.$GLOBALS['table_prefix'].'whoviewscomp set ViewDate = '.gmdate("Y-m-d H:i:s", time()+7*3600).' where UserId = '.$user.' and ViewUserId = '.$profile;
			$upqry = mysql_query($sql);
			if(!$upqry)
				return;
			}
		else{
			$insql = "insert into ".$GLOBALS['table_prefix']."whoviewscomp (UserId, ViewUserId, ViewDate, IsView) values (".$user.", ".$profile.", '".gmdate("Y-m-d H:i:s", time()+7*3600)."', 0) ";
			$inqry = mysql_query($insql);
			if(!$inqry)
				return;
			}
		}
	}
        
        
if(!function_exists('countViewsProfile')){
	function countViewsProfile($user=0){
		$sql = "select Id from ".$GLOBALS['table_prefix']."whoviews as w inner join ".$GLOBALS['table_prefix']."users as u on w.UserId = u.UserID where ViewUserId = ".$user;
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}
        
        
if(!function_exists('countViewsProfileComp')){
	function countViewsProfileComp($user=0){
		$sql = "select Id from ".$GLOBALS['table_prefix']."whoviewscomp as w inner join ".$GLOBALS['table_prefix']."companions as u on w.UserId = u.UserID where ViewUserId = ".$user;
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}        
        
        
if(!function_exists('upViewsProfile')){
	function upViewsProfile($user=0){
		$sql = "update ".$GLOBALS['table_prefix']."whoviews set IsView = 1 where ViewUserId = ".$user;
		$query = mysql_query($sql);
		return true;
		}
	}
        
        
if(!function_exists('upViewsProfileComp')){
	function upViewsProfileComp($user=0){
		$sql = "update ".$GLOBALS['table_prefix']."whoviewscomp set IsView = 1 where ViewUserId = ".$user;
		$query = mysql_query($sql);
		return true;
		}
	}        
        
        
if(!function_exists('delFavoriteProfile')){
	function delFavoriteProfile($user = 0, $profile = 0){
		$sql = "delete from ".$GLOBALS['table_prefix']."signalme where SenderID = ".$user." and RecieverID = ".$profile;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('membersSendMail')){
	function membersSendMail($data=array(), $RecieverID = 0){
		$sql = "INSERT into ".$GLOBALS['table_prefix']."messages (RecieverID, SenderID, RecieverStatusID, SenderStatusID, SentOn, ReadOn, ReplyID, AttachmentUID, AttachmentExtension, Subject, Message, MessageStatus) values (".$RecieverID.", ".$data[2].", 0, 0, '".gmdate("Y-m-d H:i:s", time()+7*3600)."', NULL, 0, '', '', '".$data[0]."', '".$data[1]."', 1)";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('getIdFromProfileName')){
	function getIdFromProfileName($name=''){
		$sql = "select UserID from ".$GLOBALS['table_prefix']."users where ProfileName = '".$name."'";
		$query = mysql_query($sql);
		if(!$query)
			return -1;
		elseif(mysql_num_rows($query)>0){
			$row = mysql_fetch_array($query);
			return $row['UserID'];
			}
		else return 0;
		}
	}
        
if(!function_exists('getIdFromCompProfileName')){
	function getIdFromCompProfileName($name=''){
		$sql = "select UserID from ".$GLOBALS['table_prefix']."companions where ProfileName = '".$name."'";
		$query = mysql_query($sql);
		if(!$query)
			return -1;
		elseif(mysql_num_rows($query)>0){
			$row = mysql_fetch_array($query);
			return $row['UserID'];
			}
		else return 0;
		}
	}        
        
        
if(!function_exists('getSentMessages')){
	function getSentMessages($user=0, $limit=''){
		$sql = "select MessageID, SentOn, Subject, SUBSTRING_INDEX(REPLACE(REPLACE(Message, '.', '. '), ',', ', '),' ', 4) as Message, MessageStatus, ProfileName from ".$GLOBALS['table_prefix']."messages as m left join ".$GLOBALS['table_prefix']."users as u on m.RecieverID = u.UserID where SenderID = ".$user." and SenderStatusID = 0 order by SentOn desc ".$limit;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return $query;
		}
	}
        
        
if(!function_exists('delSentMessages')){
	function delSentMessages($mess=array(), $user=0){
		$sql = "update ".$GLOBALS['table_prefix']."messages set SenderStatusID = 1 where MessageID in (".implode(',', $mess).") and SenderID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return $query;
		}
	}
        
        
if(!function_exists('MessagesDetails')){
	function MessagesDetails($user=0, $mess=0, $type=''){
		if($type=='s')
			$sql = "select SentOn, Subject, Message, MessageStatus, ProfileName from ".$GLOBALS['table_prefix']."messages as m left join ".$GLOBALS['table_prefix']."users as u on m.RecieverID = u.UserID where MessageID = ".$mess." and SenderID = ".$user;
		elseif($type=='i'){
			$sql = "select SenderID, SentOn, Subject, Message, MessageStatus, ProfileName, Email from ".$GLOBALS['table_prefix']."messages as m left join ".$GLOBALS['table_prefix']."users as u on m.SenderID = u.UserID where MessageID = ".$mess." and RecieverID = ".$user;
			upMessageStatus($user, array($mess));
			}
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return mysql_fetch_array($query);
		}
	}
        
        
if(!function_exists('getInbox')){
	function getInbox($user=0, $limit=''){
		$sql = "select MessageID, SentOn, Subject, SUBSTRING_INDEX(REPLACE(REPLACE(Message, '.', '. '), ',', ', '),' ', 4) as Message, MessageStatus, ProfileName, SenderID, ReadOn from ".$GLOBALS['table_prefix']."messages as m inner join ".$GLOBALS['table_prefix']."users as u on m.SenderID = u.UserID where RecieverID = ".$user." and RecieverStatusID = 0 and MessageStatus = 1 order by SentOn desc ".$limit;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return $query;
		}
	}
        
        
if(!function_exists('upMessageStatus')){
	function upMessageStatus($user=0, $mess=array()){
		$sql = "update ".$GLOBALS['table_prefix']."messages set ReadOn = '".gmdate("Y-m-d H:i:s", time()+7*3600)."' where RecieverID = ".$user." and MessageID in (".implode(',', $mess).")";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		return true;
		}
	}
        
        
if(!function_exists('delInbMessages')){
	function delInbMessages($mess=array(), $user=0){
		$sql = "update ".$GLOBALS['table_prefix']."messages set RecieverStatusID = 1 where MessageID in (".implode(',', $mess).") and RecieverID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('countNewEmail')){
	function countNewEmail($user=0){
		$sql = "SELECT MessageID FROM ".$GLOBALS['table_prefix']."messages as m inner join ".$GLOBALS['table_prefix']."users as u on u.UserID = m.SenderID WHERE RecieverID = ".$user." and RecieverStatusID = 0 and MessageStatus = 1 and ReadOn IS NULL";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		else return mysql_num_rows($query);
		}
	}

if(!function_exists('countNewCompEmail')){
	function countNewCompEmail($user=0){
		$sql = "SELECT MessageID FROM ".$GLOBALS['table_prefix']."messages as m inner join ".$GLOBALS['table_prefix']."companions as u on u.UserID = m.SenderID WHERE RecieverID = ".$user." and RecieverStatusID = 0 and MessageStatus = 1 and ReadOn IS NULL";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		else return mysql_num_rows($query);
		}
	}        
        
if(!function_exists('isReplyMessages')){
	function isReplyMessages($mess=0, $user=0){
		$sql = "select MessageID from ".$GLOBALS['table_prefix']."messages where SenderID = ".$user." and ReplyID = ".$mess;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		elseif(mysql_num_rows($query)>0)
			return true;
		else return false;
		}
	}
        
        
if(!function_exists('AnswerMessage')){
	function AnswerMessage($data=array(), $user=0){
		$sql = "insert into ".$GLOBALS['table_prefix']."messages (RecieverID, SenderID, RecieverStatusID, SenderStatusID, SentOn, ReadOn, ReplyID, AttachmentUID, AttachmentExtension, Subject, Message, MessageStatus) values (".$data[3].", ".$user.", 0, 0, '".gmdate("Y-m-d H:i:s", time()+7*3600)."', NULL, ".$data[0].", '', '', '".$data[2]."', '".$data[1]."', 1)";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('countNewSignal')){
	function countNewSignal($user=0){
		$sql = "SELECT RecieverID FROM ".$GLOBALS['table_prefix']."signalme as s inner join ".$GLOBALS['table_prefix']."users as u on u.UserID = s.SenderID where RecieverID = ".$user." and IsViews = 0";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		else return mysql_num_rows($query);
		}
	}

if(!function_exists('countNewCompSignal')){
	function countNewCompSignal($user=0){
		$sql = "SELECT RecieverID FROM ".$GLOBALS['table_prefix']."signalme as s inner join ".$GLOBALS['table_prefix']."companions as u on u.UserID = s.SenderID where RecieverID = ".$user." and IsViews = 0";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		else return mysql_num_rows($query);
		}
	}        
        
if(!function_exists('getTrashEmail')){
	function getTrashEmail($user=0, $limit=''){
		$sql = "SELECT MessageID, SentOn, Subject, SUBSTRING_INDEX(REPLACE(REPLACE(Message, '.', '. '), ',', ', '),' ', 4) as Message, MessageStatus, ProfileName, ReadOn FROM ".$GLOBALS['table_prefix']."messages AS m LEFT JOIN ".$GLOBALS['table_prefix']."users AS u on m.SenderID = u.UserID WHERE RecieverID = ".$user." AND RecieverStatusID = 1 AND MessageStatus = 1 ORDER BY SentOn desc ".$limit;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return $query;
		}
	}
        
        
if(!function_exists('updTrashMessages')){
	function updTrashMessages($mess=array(), $user=0, $status=1){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."messages SET MessageStatus = ".$status." where MessageID in (".implode(',', $mess).") and RecieverID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}

        
if(!function_exists('recoveryMessages')){
	function recoveryMessages($mess=array(), $user=0){
		$sql = "update ".$GLOBALS['table_prefix']."messages set RecieverStatusID = 0 where MessageID in (".implode(',', $mess).") and RecieverID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('blockUser')){
	function blockUser($mess=0, $user=0){
		$chksql = 'select UserID from '.$GLOBALS['table_prefix'].'blockedusers where UserID = '.$user.' and BlockedUserID in (select SenderID from '.$GLOBALS['table_prefix'].'messages where MessageID = '.$mess.')';
		$chkqry = mysql_query($chksql);
		if(!$chkqry)
			return false;
		elseif(mysql_num_rows($chkqry)>0)
			return true;
		else{
			$sql = "insert into ".$GLOBALS['table_prefix']."blockedusers (UserID, BlockedUserID, DateBlock) values ($user, (select SenderID from ".$GLOBALS['table_prefix']."messages where MessageID = ".$mess."), '".gmdate("Y-m-d H:i:s", time()+7*3600)."')";
			$query = mysql_query($sql);
			if(!$query)
				return false;
			else return true;
			}
		}
	}
        
        
if(!function_exists('getBlackList')){
	function getBlackList($user=0, $limit=''){
		$sql = "select BlockedUserID, ProfileName, Age, LastLogon, City, Country, State from ".$GLOBALS['table_prefix']."blockedusers as b left join ".$GLOBALS['table_prefix']."users as u on b.BlockedUserID = u.UserID left join ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID left join ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID where b.UserID = ".$user." order by DateBlock desc ".$limit;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return $query;
		}
	}
        
        
if(!function_exists('unBlockUser')){
	function unBlockUser($user=0, $block=0){
		$sql = "delete from ".$GLOBALS['table_prefix']."blockedusers where UserID = ".$user." and BlockedUserID = ".$block;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('updFavorites')){
	function updFavorites($user=0){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."signalme set IsViews = 1 where RecieverID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('updLogon')){
	function updLogon($user=0){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."users set LastLogon = '".gmdate("Y-m-d H:i:s", time()+7*3600)."', IsOnline = 1 where UserID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('isBlockedMe')){
	function isBlockedMe($user=0, $block=0){
		$sql = "SELECT UserID FROM ".$GLOBALS['table_prefix']."blockedusers WHERE UserID = ".$user." and BlockedUserID = ".$block;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		elseif(mysql_num_rows($query)>0)
			return true;
		else return false;
		}
	}
        
        
if(!function_exists('getSaveSearch')){
	function getSaveSearch(){
		$sql = "SELECT SearchName, SearchValue FROM ".$GLOBALS['table_prefix']."savedsearch WHERE UserID = 0";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return $query;
		}
	}
        
        
if(!function_exists('chkBannedMe')){
	function chkBannedMe($user=0){
		$baned = array();
		$sql = "SELECT Reason, BanedOn, LiftBanId, ".$_SESSION['lang']."LiftBan as LLiftBan from ".$GLOBALS['table_prefix']."bannedusers as b left join ".$GLOBALS['table_prefix']."liftbaned as l on b.LiftBanId = l.Id where UserId = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return $baned;
		elseif(mysql_num_rows($query)>0){
			return mysql_fetch_array($query);
			}
		else return $baned;
		}
	}
        
        
if(!function_exists('countSavedSearch')){
	function countSavedSearch($user=0){
		$sql = "SELECT SearchID from ".$GLOBALS['table_prefix']."savedsearch where UserID = ".$user;
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}
        
        
if(!function_exists('delSavedSearch')){
	function delSavedSearch($searchid=0, $user=0){
		$sql = "delete from ".$GLOBALS['table_prefix']."savedsearch where SearchID = ".$searchid." and UserID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('updSavedSearch')){
	function updSavedSearch($data=array()){
		$sql = "update ".$GLOBALS['table_prefix']."savedsearch set SearchName = '".$data[2]."' where SearchID = ".$data[0]." and UserID = ".$data[1];
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('countEmailSent')){
	function countEmailSent($user=0, $newdate=''){
		$sql = "select MessageID from ".$GLOBALS['table_prefix']."messages where SenderID = ".$user." and date_format(SentOn, '%Y-%m-%d') = '".$newdate."'";
		$query = mysql_query($sql);
		if(!$query)
			return -1;
		else return mysql_num_rows($query);
		}
	}
        
        
if(!function_exists('neworder')){
	function neworder($data=array()){
		$sql = "insert into ".$GLOBALS['table_prefix']."orders (OrderId, UserId, PayDate, Amounts, Status, PaymentId_NL, PaymentType_NL, PriceId, ExpireDate) values ('', ".$data[0].", '".$data[2]."', ".$data[1].", 0, 0, 0, ".$data[3].", NULL)";
		$query = mysql_query($sql);
		if(!$query)
			return -1;
		else return mysql_insert_id();
		}
	}
        
        
if(!function_exists('updorder')){
	function updorder($data=array()){
		$sql = "update ".$GLOBALS['table_prefix']."orders set OrderId = '".$data[0]."', Status = ".$data[1].", PaymentId_NL = ".$data[2].", PaymentType_NL = ".$data[1]." where Id = ".$data[3]." and UserId = ".$data[4];
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('getDateorder')){
	function getDateorder($order=0){
		$sql = "select PayDate from ".$GLOBALS['table_prefix']."orders where Id = ".$order;
		$query = mysql_query($sql);
		if(!$query)
			return '';
		elseif(mysql_num_rows($query)>0){
			$row=mysql_fetch_array($query);
			return date('H:i:s d-m-Y', strtotime($row['PayDate']));
			}
		}
	}
        
        
if(!function_exists('IpAddress')){
	function IpAddress(){
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
			$ipAddress=$_SERVER['HTTP_CLIENT_IP'];
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
			$ipAddress=$_SERVER['HTTP_X_FORWARDED_FOR'];
		else
			$ipAddress=$_SERVER['REMOTE_ADDR'];
		return $ipAddress;
		}
	}
        
        
if(!function_exists('curdate')){
	function curdate($format='m-d-Y H:i:s'){
		return gmdate($format, time()+7*3600);
		}
	}
        
        
if(!function_exists('getPrices')){
	function getPrices(){
		$sql = "SELECT * from ".$GLOBALS['table_prefix']."prices ";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return $query;
		}
	}
        
        
if(!function_exists('getAmount')){
	function getAmount($priceid=0){
		$sql = "SELECT Prices from ".$GLOBALS['table_prefix']."prices where Id = ".$priceid;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		elseif(mysql_num_rows($query)>0){
			$row=mysql_fetch_array($query);
			return $row['Prices'];
			}
		}
	}
        
        
if(!function_exists('updMember')){
	function updMember($user=0){
		$sql = "update ".$GLOBALS['table_prefix']."users set RemainVIPContacts = 1 where UserID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('chkSpam')){
	function chkSpam($mail=''){
		$domain = explode('@', $mail);
		$sql = "SELECT BanEmail, BanDomain, BanDate, ReasonBaned from ".$GLOBALS['table_prefix']."filteremails where EmailAddress like '%@".$domain[1]."' and BanDomain = 1";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		elseif(mysql_num_rows($query)>0){
			return -1;
			}
		else{
			$sqlchk = "SELECT BanEmail, BanDomain, BanDate, ReasonBaned from ".$GLOBALS['table_prefix']."filteremails where EmailAddress = '".$mail."'";
			$qrychk = mysql_query($sqlchk);
			if(!$qrychk)
				return 0;
			elseif(mysql_num_rows($qrychk)>0)
				return -2;
			else return 1;
			}
		}
	}
        
        
if(!function_exists('getEcards')){
	function getEcards($user=0, $limit=''){
		$sql = "SELECT e.Id, Subjects, SentDate, ProfileName, Age, CardName, SenderId from ".$GLOBALS['table_prefix']."ecards as e inner join ".$GLOBALS['table_prefix']."users as u on e.SenderId = u.UserID left join ".$GLOBALS['table_prefix']."greetingcards as gc on e.CardId = gc.Id where RecieverId = ".$user.' order by SentDate desc '.$limit;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return $query;
		}
	}
        
        
if(!function_exists('getDetailCard')){
	function getDetailCard($user=0, $id=0){
		$sql = "SELECT e.Id, SenderId, CardId, Subjects, Contents, WidthMess, HeightMess, LeftMess, TopMess, CardName, ProfileName, Age, SentDate, Music from ".$GLOBALS['table_prefix']."ecards as e inner join ".$GLOBALS['table_prefix']."users as u on e.SenderId = u.UserID left join ".$GLOBALS['table_prefix']."greetingcards as gc on e.CardId = gc.Id where e.Id = ".$id." and RecieverId = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return $query;
		}
	}
        
        
if(!function_exists('reportabuse')){
	function reportabuse($data=array()){
		$sql = "insert into ".$GLOBALS['table_prefix']."abuseprofiles(ReportBy, ProfileId, Reason, ReprotDate, IsProcess) values ('".$data[0]."', ".$data[1].", '".$data[2]."', '".gmdate("Y-m-d H:i:s", time()+7*3600)."', 0) ";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
        
if(!function_exists('increaseView')){
	function increaseView($user=0){
		$sql = "update ".$GLOBALS['table_prefix']."users set Views = Views + 1 where UserID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        
if(!function_exists('increaseViewComp')){
	function increaseViewComp($user=0){
		$sql = "update ".$GLOBALS['table_prefix']."companions set Views = Views + 1 where UserID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}        
        
if(!function_exists('FillListMembers')){
	function FillListMembers($sqlsearch=''){
		$sql = "SELECT UserID from ".$GLOBALS['table_prefix']."users as u where ".$sqlsearch." ProfileStatusID = 1 and GenderID IS NOT NULL and MembershipID <> 3";
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}

        
if(!function_exists('FillListCompMembers')){
	function FillListCompMembers($sqlsearch=''){
		$sql = "SELECT UserID from ".$GLOBALS['table_prefix']."companions as u where ".$sqlsearch." ProfileStatusID = 1 and GenderID IS NOT NULL and MembershipID <> 3";
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}        

if(!function_exists('FillListCompMembersByLoc')){
	function FillListCompMembersByLoc($sqlsearch=''){
		$sql = "SELECT UserID from ".$GLOBALS['table_prefix']."companions as u where ".$sqlsearch." ProfileStatusID = 1 and GenderID IS NOT NULL and MembershipID <> 3 AND City LIKE '$memberlocation'";
		$query = mysql_query($sql);
		return mysql_num_rows($query);
		}
	}         
        
if(!function_exists('FullRows')){
	function FullRows($total=0, $colperrow=0, $str=''){
		$return = '';
		if(is_int($total/$colperrow))
			return $return;
		else{
			for($j=0; $j<$colperrow-($total%$colperrow); $j++)
				$return .= $str;
			return $return;
			}
		}
	}
        
        
if(!function_exists('ConfigPaypal')){
	function ConfigPaypal(){
		$return = array();
		$sql = "SELECT APIKey,APIPassword,APISignature,CurrencyCode,ReturnURL,CancelURL,ButtonImg,APIMode from ".$GLOBALS['table_prefix']."paymentprovide where Id = 1";
		$query = mysql_query($sql);
		if($query!=false && mysql_num_rows($query)>0){
			$rs = mysql_fetch_array($query);
			$return['APIKey'] = $rs['APIKey'];
			$return['APIPassword'] = $rs['APIPassword'];
			$return['APISignature'] = $rs['APISignature'];
			$return['CurrencyCode'] = $rs['CurrencyCode'];
			$return['ReturnURL'] = $rs['ReturnURL'];
			$return['CancelURL'] = $rs['CancelURL'];
			$return['ButtonImg'] = $rs['ButtonImg'];
			$return['APIMode'] = $rs['APIMode'];
			}
		return $return;
		}
	}
        
        
if(!function_exists('getItem')){
	function getItem($id=0){
		$return = array();
		$sql = "select Prices,NumofEmail,Block from ".$GLOBALS['table_prefix']."prices where Id = ".$id;
		$query = mysql_query($sql);
		if($query!=false && mysql_num_rows($query)>0){
			$rs = mysql_fetch_array($query);
			$return['Prices'] = $rs['Prices'];
			$return['NumofEmail'] = $rs['NumofEmail'];
			$return['Block'] = $rs['Block'];
			}
		return $return;
		}
	}
        
        
if(!function_exists('getThemes')){
	function getThemes($id=0){
		if($id>0)
			$sql = "select Id, ThemeName, SourceName, FileView from ".$GLOBALS['table_prefix']."themes where Id = ".$id;
		else $sql = "select Id, ThemeName, SourceName, FileView from ".$GLOBALS['table_prefix']."themes";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return $query;
		}
	}
        
        
if(!function_exists('getCurrentTheme')){
	function getCurrentTheme($userid=0){
		$sql = "select ThemeId from ".$GLOBALS['table_prefix']."users where UserID = ".$userid;
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		elseif(mysql_num_rows($query)>0){
			$row = mysql_fetch_array($query);
			return $row['ThemeId'];
			}
		else return 0;
		}
	}
        
        
if(!function_exists('getSourceThemes')){
	function getSourceThemes($id=1){
		$sql = "select SourceName from ".$GLOBALS['table_prefix']."themes where Id = ".$id;
		$query = mysql_query($sql);
		if(!$query)
			return '';
		elseif(mysql_num_rows($query)>0){
			$row = mysql_fetch_array($query);
			return $row['SourceName'];
			}
		else return '';
		}
	}
        
        
if(!function_exists('updateTheme')){
	function updateTheme($userid=0, $theme=0){
		$sql = "update ".$GLOBALS['table_prefix']."users set ThemeId = ".$theme." where UserID = ".$userid;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        

if(!function_exists('myprofile')){
	function myprofile($userid=0){
		$sql = "SELECT u.UserID, ProfileName, Age, AboutMe, City, MatchAgeFrom, MatchAgeTO, LastLogon, PrimaryPhotoID, State, Country, g.".$_SESSION['lang']."Gender as LGender, PhotoExtension, h.HeightDescription, b.".$_SESSION['lang']."BodyType as LBodyType, ".$_SESSION['lang']."HairColor as LHairColor, ".$_SESSION['lang']."EyeColor as LEyeColor, ed.".$_SESSION['lang']."Education as LEducation, Occupation, sm.".$_SESSION['lang']."Smoking as LSmoking, d.".$_SESSION['lang']."Drinking as LDrinking, HaveChildren, WantChildren, WillingToTravel, Goal, Interests, AboutMyMatch, ge.".$_SESSION['lang']."Gender as MLGender, edu.".$_SESSION['lang']."Education as MLEducation, he.HeightDescription as MFHeightDescription, hei.HeightDescription as MTHeightDescription, MatchHeightIDFrom, MatchHeightIDTo, bo.".$_SESSION['lang']."BodyType as MLBodyType, smo.".$_SESSION['lang']."Smoking as MLSmoking, dr.".$_SESSION['lang']."Drinking as MLDrinking, u.GenderID, u.CountryID, RemainVIPContacts, Email, Phone, IsOnline, u.ProfileStatusID, ps.".$_SESSION['lang']."ProfileStatus as LProfileStatus from ".$GLOBALS['table_prefix']."users as u left join ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID left join ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID left join ".$GLOBALS['table_prefix']."gender as g on u.GenderID = g.GenderID left join ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID left join ".$GLOBALS['table_prefix']."height as h on u.HeightID = h.HeightID left join ".$GLOBALS['table_prefix']."bodytypes as b on u.BodyTypeID = b.BodyTypeID left join ".$GLOBALS['table_prefix']."haircolors as ha on u.HairColorID = ha.HairColorID left join ".$GLOBALS['table_prefix']."eyescolors as e on u.EyeColorID = e.EyeColorID left join ".$GLOBALS['table_prefix']."educations as ed on u.EducationID = ed.EducationID left join ".$GLOBALS['table_prefix']."smoking as sm on u.SmokingID = sm.SmokingID left join ".$GLOBALS['table_prefix']."drinking as d on u.DrinkingID = d.DrinkingID left join ".$GLOBALS['table_prefix']."gender as ge on u.MatchGenderID = ge.GenderID left join ".$GLOBALS['table_prefix']."educations as edu on u.MatchEducationID = edu.EducationID left join ".$GLOBALS['table_prefix']."height as he on u.MatchHeightIDFrom = he.HeightID left join ".$GLOBALS['table_prefix']."height as hei on u.MatchHeightIDTo = hei.HeightID left join ".$GLOBALS['table_prefix']."bodytypes as bo on u.MatchBodyStyleID = bo.BodyTypeID left join ".$GLOBALS['table_prefix']."smoking as smo on u.MatchSmokingID = smo.SmokingID left join ".$GLOBALS['table_prefix']."drinking as dr on u.MatchDrinkingID = dr.DrinkingID left join ".$GLOBALS['table_prefix']."profilestatus as ps on u.ProfileStatusID = ps.ProfileStatusID where u.UserID = ".$userid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}        

        
if(!function_exists('myProfile')){
	function myProfile($userid=0){
		$sql = "SELECT u.UserID, ProfileName, Age, AboutMe, City, MatchAgeFrom, MatchAgeTO, LastLogon, PrimaryPhotoID, m.".$_SESSION['lang']."MaritalStatus as LMaritalStatus, State, Country, g.".$_SESSION['lang']."Gender as LGender, PhotoExtension, h.HeightDescription, b.".$_SESSION['lang']."BodyType as LBodyType, ".$_SESSION['lang']."HairColor as LHairColor, ".$_SESSION['lang']."EyeColor as LEyeColor, r.".$_SESSION['lang']."Religion as LReligion, ed.".$_SESSION['lang']."Education as LEducation, Occupation, sm.".$_SESSION['lang']."Smoking as LSmoking, d.".$_SESSION['lang']."Drinking as LDrinking, ".$_SESSION['lang']."DatingInterest as LDatingInterest, HaveChildren, WantChildren, WillingToTravel, Goal, Interests, AboutMyMatch, ge.".$_SESSION['lang']."Gender as MLGender, edu.".$_SESSION['lang']."Education as MLEducation, ma.".$_SESSION['lang']."MaritalStatus as MLMaritalStatus, he.HeightDescription as MFHeightDescription, hei.HeightDescription as MTHeightDescription, MatchHeightIDFrom, MatchHeightIDTo, bo.".$_SESSION['lang']."BodyType as MLBodyType, re.".$_SESSION['lang']."Religion as MLReligion, smo.".$_SESSION['lang']."Smoking as MLSmoking, dr.".$_SESSION['lang']."Drinking as MLDrinking, u.GenderID, u.CountryID, RemainVIPContacts, Email, Phone, IsOnline, u.ProfileStatusID, ps.".$_SESSION['lang']."ProfileStatus as LProfileStatus 
                        FROM ".$GLOBALS['table_prefix']."users as u left join ".$GLOBALS['table_prefix']."maritalstatus as m on u.MaritalStatusID = m.MaritalStatusID left join ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID left join ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID left join ".$GLOBALS['table_prefix']."gender as g on u.GenderID = g.GenderID left join ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID left join ".$GLOBALS['table_prefix']."height as h on u.HeightID = h.HeightID left join ".$GLOBALS['table_prefix']."bodytypes as b on u.BodyTypeID = b.BodyTypeID left join ".$GLOBALS['table_prefix']."haircolors as ha on u.HairColorID = ha.HairColorID left join ".$GLOBALS['table_prefix']."eyescolors as e on u.EyeColorID = e.EyeColorID left join ".$GLOBALS['table_prefix']."religions as r on u.ReligionID = r.ReligionID left join ".$GLOBALS['table_prefix']."educations as ed on u.EducationID = ed.EducationID left join ".$GLOBALS['table_prefix']."smoking as sm on u.SmokingID = sm.SmokingID left join ".$GLOBALS['table_prefix']."drinking as d on u.DrinkingID = d.DrinkingID left join ".$GLOBALS['table_prefix']."datinginterest as da on u.DatingInterestID = da.DatingInterestID left join ".$GLOBALS['table_prefix']."gender as ge on u.MatchGenderID = ge.GenderID left join ".$GLOBALS['table_prefix']."educations as edu on u.MatchEducationID = edu.EducationID left join ".$GLOBALS['table_prefix']."maritalstatus as ma on u.MatchMaritalStatusID = ma.MaritalStatusID left join ".$GLOBALS['table_prefix']."height as he on u.MatchHeightIDFrom = he.HeightID left join ".$GLOBALS['table_prefix']."height as hei on u.MatchHeightIDTo = hei.HeightID left join ".$GLOBALS['table_prefix']."bodytypes as bo on u.MatchBodyStyleID = bo.BodyTypeID left join ".$GLOBALS['table_prefix']."religions as re on u.MatchReligionID = re.ReligionID left join ".$GLOBALS['table_prefix']."smoking as smo on u.MatchSmokingID = smo.SmokingID left join ".$GLOBALS['table_prefix']."drinking as dr on u.MatchDrinkingID = dr.DrinkingID left join ".$GLOBALS['table_prefix']."profilestatus as ps on u.ProfileStatusID = ps.ProfileStatusID where u.UserID = ".$userid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}
        
        
if(!function_exists('myProfileAll')){
	function myProfileAll($userid=0){
		$sql = "SELECT u.UserID
                            , ProfileName
                            , Age
                            , AboutMe
                            , City
                            , MatchAgeFrom
                            , MatchAgeTO
                            , LastLogon
                            , PrimaryPhotoID
                            , State
                            , Country
                            , g.".$_SESSION['lang']."Gender as LGender
                            , PhotoExtension
                            , h.HeightDescription
                            , b.".$_SESSION['lang']."BodyType as LBodyType
                            , ".$_SESSION['lang']."HairColor as LHairColor
                            , ".$_SESSION['lang']."EyeColor as LEyeColor
                            , ed.".$_SESSION['lang']."Education as LEducation
                            , Occupation
                            , sm.".$_SESSION['lang']."Smoking as LSmoking
                            , d.".$_SESSION['lang']."Drinking as LDrinking
                            , HaveChildren
                            , WantChildren
                            , WillingToTravel
                            , Goal
                            , Interests
                            , AboutMyMatch
                            , ge.".$_SESSION['lang']."Gender as MLGender
                            , edu.".$_SESSION['lang']."Education as MLEducation
                            , he.HeightDescription as MFHeightDescription
                            , hei.HeightDescription as MTHeightDescription
                            , MatchHeightIDFrom
                            , MatchHeightIDTo
                            , bo.".$_SESSION['lang']."BodyType as MLBodyType
                            , smo.".$_SESSION['lang']."Smoking as MLSmoking
                            , dr.".$_SESSION['lang']."Drinking as MLDrinking
                            , u.GenderID
                            , u.CountryID
                            , RemainVIPContacts
                            , Email
                            , Phone
                            , IsOnline
                            , u.ProfileStatusID
                            , ps.".$_SESSION['lang']."ProfileStatus as LProfileStatus 
                        FROM 
                            ".$GLOBALS['table_prefix']."users as u LEFT JOIN
                            ".$GLOBALS['table_prefix']."states as s on u.StateID = s.StateID LEFT JOIN 
                            ".$GLOBALS['table_prefix']."countries as c on u.CountryID = c.CountryID LEFT JOIN
                            ".$GLOBALS['table_prefix']."gender as g on u.GenderID = g.GenderID LEFT JOIN
                            ".$GLOBALS['table_prefix']."photos as p on u.PrimaryPhotoID = p.PhotoID LEFT JOIN
                            ".$GLOBALS['table_prefix']."height as h on u.HeightID = h.HeightID LEFT JOIN
                            ".$GLOBALS['table_prefix']."bodytypes as b on u.BodyTypeID = b.BodyTypeID LEFT JOIN
                            ".$GLOBALS['table_prefix']."haircolors as ha on u.HairColorID = ha.HairColorID LEFT JOIN
                            ".$GLOBALS['table_prefix']."eyescolors as e on u.EyeColorID = e.EyeColorID LEFT JOIN
                            ".$GLOBALS['table_prefix']."educations as ed on u.EducationID = ed.EducationID LEFT JOIN
                            ".$GLOBALS['table_prefix']."smoking as sm on u.SmokingID = sm.SmokingID LEFT JOIN
                            ".$GLOBALS['table_prefix']."drinking as d on u.DrinkingID = d.DrinkingID LEFT JOIN
                            ".$GLOBALS['table_prefix']."gender as ge on u.MatchGenderID = ge.GenderID LEFT JOIN
                            ".$GLOBALS['table_prefix']."educations as edu on u.MatchEducationID = edu.EducationID LEFT JOIN
                            ".$GLOBALS['table_prefix']."height as he on u.MatchHeightIDFrom = he.HeightID LEFT JOIN
                            ".$GLOBALS['table_prefix']."height as hei on u.MatchHeightIDTo = hei.HeightID LEFT JOIN
                            ".$GLOBALS['table_prefix']."bodytypes as bo on u.MatchBodyStyleID = bo.BodyTypeID LEFT JOIN
                            ".$GLOBALS['table_prefix']."smoking as smo on u.MatchSmokingID = smo.SmokingID LEFT JOIN
                            ".$GLOBALS['table_prefix']."drinking as dr on u.MatchDrinkingID = dr.DrinkingID LEFT JOIN
                            ".$GLOBALS['table_prefix']."profilestatus as ps on u.ProfileStatusID = ps.ProfileStatusID 
                        WHERE
                            UserID = ".$userid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}        

        
if(!function_exists('myProfileAllAll')){
	function myProfileAllAll($userid=0){
		$sql = "SELECT *
                        FROM 
                            ".$GLOBALS['table_prefix']."users 
                        WHERE
                            UserID = ".$userid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}                
        
if(!function_exists('myProfileCompAll')){
	function myProfileCompAll($userid=0){
		$sql = "SELECT *
                        FROM 
                            ".$GLOBALS['table_prefix']."companions 
                        WHERE
                            UserID = ".$userid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}              
        
if(!function_exists('IsVIP')){
	function IsVIP($user=0){
		$sql = "select date_format(ExpireDate,'%Y-%m-%d') as ExpireDate from ".$GLOBALS['table_prefix']."orders where UserId = ".$user." order by ExpireDate desc limit 0,1";
		$query = mysql_query($sql);
		if($query && mysql_num_rows($query)>0){
			$row=mysql_fetch_array($query);
			$cudate = date("Y-m-d");
			if(strtotime($cudate)>strtotime($row['ExpireDate'])){
				if(isset($_SESSION['vipm'])) unset($_SESSION['vipm']);
				}
			else $_SESSION['vipm'] = true;
			}
		else{
			if(isset($_SESSION['vipm'])) unset($_SESSION['vipm']);
			}
		}
	}
        
        
if(!function_exists('UpdateVIPStatus')){
	function UpdateVIPStatus($user=0, $date=''){
		$sql = "update ".$GLOBALS['table_prefix']."users set RemainVIPContacts = 0 where UserID in (select UserId from ".$GLOBALS['table_prefix']."orders where UserId = ".$user." having max(ExpireDate) < '".$date."')";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}
        

if(!function_exists('curPageURL')){        
    function curPageURL() {
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
            $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
            } else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
                }
            return $pageURL;
                }
        }
        
//*****************************************************************************        
//************************   COMPANION FUNCTIONS    ***************************       
        
        
//    CHECKS IF NEW COMPANION IS AN EXISTING COMPANION        
if(!function_exists('exists_comp')){
	function exists_comp($user){
		$sql = "SELECT UserID
                        FROM ".$GLOBALS['table_prefix']."companions
                        WHERE Email = '".$user."' and ProfileStatusID <> 0";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		elseif(mysql_num_rows($query)>0)
			return false;
		else return true;
		}
	}        

//   INSERTS NEW RECORD IF COMPANION IS NEW        
if(!function_exists('new_comp')){
	function new_comp($data=array()){
		$sql = "INSERT into ".$GLOBALS['table_prefix']."companions
                       (MembershipID
                       , RemainVIPContacts
                       , SiteID
                       , Name
                       , ProfileName
                       , Email
                       , Password
                       , GenderID
                       , HeightID
                       , Age
                       , BodyTypeID
                       , HairColorID
                       , EyeColorID
                       , EthnicityID
                       , City
                       , StateID
                       , CountryID
                       , Zipcode
                       , Phone
                       , IMessagerID
                       , IMessager
                       , PussyID
                       , SexOrID
                       , EducationID
                       , SmokingID
                       , DrinkingID
                       , BreastsID
                       , DatingInterestID
                       , Independent
                       , Transgender
                       , Transsexual
                       , Pornstar
                       , Website
                       , Whatsapp
                       , Instagram
                       , CompEncLike
                       , WillingToTravel
                       , IPaddress
                       , DatePosted
                       , DateUpdate
                       , LastEmailSent
                       , LastLogon
                       , ProfileStatusID
                       , NotificationScheduleID
                       , MatchAgeFrom
                       , MatchAgeTO
                       , MatchGenderID
                       , MatchHeightIDFrom
                       , MatchHeightIDTo
                       , MatchBodyStyleID
                       , MatchSexOrID
                       , MatchPussyID
                       , MatchEducationID
                       , MatchCareerID
                       , MatchSmokingID
                       , MatchDrinkingID
                       , MatchBreastsID
                       , MatchNumberofChild
                       , MatchBeSameLocation
                       , SubcribeList
                       , RandomVerifyID
                       , Goal
                       , Interests
                       , AboutMe
                       , AboutMyMatch
                       , PrimaryPhotoID
                       , GroupID
                       , Views
                       , IsOnline
                       , TotalRating
                       , NumberOfVote
                       , ThemeId) 
                       VALUES 
                       (1
                       , 0
                       , 2
                       , ''
                       , ''
                       , '".$data[0]."'
                       , '".$data[1]."'
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , ''
                       , 0
                       , 0
                       , ''
                       , ''
                       , 0
                       , ''
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , ''
                       , ''
                       , ''
                       , 0
                       , 0
                       , '".$data[3]."'
                       , '".gmdate("Y-m-d", time()+7*3600)."'
                       , '".gmdate("Y-m-d", time()+7*3600)."'
                       , '0000-00-00 00:00:00'
                       , '0000-00-00 00:00:00'
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , ''
                       , ''
                       , ''
                       , ''
                       , NULL
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 1)";
		$query = mysql_query($sql);
		$newid = mysql_insert_id();
		if(!$query)
			return 0;
		else return $newid;
		}
	}        

//   CHECKS IF THERE ARE EXISTING COMP INDULGENCES //         
if(!function_exists('exists_comp_indul')){
	function exists_comp_indul($userid){
		$sql = "SELECT UserID
                        FROM ".$GLOBALS['table_prefix']."compindul
                        WHERE UserID = '".$userid."' and ProfileStatusID <> 0";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		elseif(mysql_num_rows($query)>0)
			return false;
		else return true;
		}
	}        
        
        
//   INSERTS INDULGENCES INTO COMPINDULGENCES TABLE        
if(!function_exists('new_comp_indul')){
	function new_comp_indul($data=array()){
		$sql = "INSERT into ".$GLOBALS['table_prefix']."compindul
                        (UserID
                        , CompEncLikes
                        , CompEncLoc
                        , CompIndulSex
                        , CompIndulSexPref
                        , CompIndulBJ
                        , CompIndulBJPref
                        , CompIndulHJ
                        , CompIndulKiss
                        , CompIndulKissPref
                        , CompIndulLickpussy
                        , CompInduTouchpussy
                        , CompInduTouchpussyPref
                        , CompIndulRim
                        , CompIndulRimPref
                        , CompIndulAnal
                        , CompIndulAnalPref
                        , CompIndulMsog
                        , CompIndulTwogirl
                        , CompIndulSecondcomp
                        , CompIndulTwoguy
                        , CompIndulVideo
                        , CompIndulPhotos
                        , CompIndulMassage
                        , CompIndulSMBD
                        , CompIndulSquirt
                        , CompIndulTurnons
                        , CompIndulTurnoffs 
                        VALUES 
                       (1
                       , '".$userid."'
                       , ''
                       , ''
                       , 0
                       , ''
                       , 0
                       , ''
                       , 0
                       , 0
                       , ''
                       , 0
                       , 0
                       , ''
                       , 0
                       , ''
                       , 0
                       , ''
                       , 0
                       , ''
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , ''
                       , '')";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}        
        
if(!function_exists('updCompLogon')){
	function updCompLogon($user=0){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."companions set LastLogon = '".gmdate("Y-m-d H:i:s", time()+7*3600)."', IsOnline = 1 where UserID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}        
        
        
        
//   INSERTS NEW RECORD IF COMPANION IS NEW BY MEMBER WHEN WRITING A REVIEW        
if(!function_exists('new_comp_by_mbr')){
	function new_comp_by_mbr($data=array()){
		$sql = "INSERT into ".$GLOBALS['table_prefix']."companions
                       (MembershipID
                       , RemainVIPContacts
                       , SiteID
                       , Name
                       , ProfileName
                       , Email
                       , CompEmail
                       , Password
                       , GenderID
                       , HeightID
                       , Age
                       , BodyTypeID
                       , HairColorID
                       , HairLengthID
                       , EyeColorID
                       , PiercingsID
                       , TattoosID
                       , EthnicityID
                       , City
                       , StateID
                       , CountryID
                       , Zipcode
                       , Phone
                       , IMessagerID
                       , IMessager
                       , PussyID
                       , SexOrID
                       , EducationID
                       , SmokingID
                       , DrinkingID
                       , BreastsID
                       , BreastSizeDescID
                       , BreastImp
                       , CompAssID
                       , DatingInterestID
                       , Independent
                       , Transgender
                       , Transsexual
                       , Pornstar
                       , Website
                       , WebProfile
                       , Whatsapp
                       , Instagram
                       , CompEncLike
                       , WillingToTravel
                       , IPaddress
                       , DatePosted
                       , DateUpdate
                       , LastEmailSent
                       , LastLogon
                       , ProfileStatusID
                       , NotificationScheduleID
                       , MatchAgeFrom
                       , MatchAgeTO
                       , MatchGenderID
                       , MatchHeightIDFrom
                       , MatchHeightIDTo
                       , MatchBodyStyleID
                       , MatchSexOrID
                       , MatchPussyID
                       , MatchEducationID
                       , MatchCareerID
                       , MatchSmokingID
                       , MatchDrinkingID
                       , MatchBreastsID
                       , MatchNumberofChild
                       , MatchBeSameLocation
                       , SubcribeList
                       , RandomVerifyID
                       , Goal
                       , Interests
                       , AboutMe
                       , AboutMyMatch
                       , PrimaryPhotoID
                       , GroupID
                       , Views
                       , IsOnline
                       , TotalRating
                       , NumberOfVote
                       , ThemeId) 
                       VALUES 
                       (1 
                       , 0
                       , 2
                       , ''
                       , '".$data[0]."' 
                       , '".$data[30]."'
                       , '".$data[32]."'    
                       , ''
                       , '".$data[1]."'
                       , '".$data[18]."'
                       , '".$data[2]."'
                       , '".$data[17]."'
                       , '".$data[19]."'
                       , '".$data[20]."'
                       , 0
                       , '".$data[21]."'
                       , '".$data[22]."'
                       , '".$data[6]."'
                       , '".$data[5]."'
                       , '".$data[4]."'
                       , '".$data[3]."'
                       , ''
                       , '".$data[12]."'
                       , 0
                       , ''
                       , '".$data[23]."'
                       , '".$data[7]."'
                       , 0
                       , '".$data[27]."'
                       , '".$data[28]."'
                       , 0
                       , '".$data[24]."'
                       , '".$data[25]."'
                       , '".$data[26]."'
                       , 0
                       , '".$data[10]."'
                       , '".$data[9]."'
                       , '".$data[8]."'
                       , '".$data[11]."'
                       , '".$data[13]."'
                       , '".$data[14]."'
                       , '".$data[15]."'
                       , '".$data[16]."'
                       , 0
                       , 0
                       , '".$data[29]."'
                       , '".gmdate("Y-m-d", time()+7*3600)."'
                       , '".gmdate("Y-m-d", time()+7*3600)."'
                       , '0000-00-00 00:00:00'
                       , '0000-00-00 00:00:00'
                       , '".$data[31]."'
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , ''
                       , ''
                       , ''
                       , ''
                       , NULL
                       , 0
                       , 0
                       , 0
                       , 0
                       , 0
                       , 1)";
		$query = mysql_query($sql);
		$newid = mysql_insert_id();
		if(!$query)
			return 0;
		else return $newid;
		}
	}             
        
        
//   INSERTS NEW RECORD IF COMPANION IS NEW BY MEMBER WHEN WRITING A REVIEW        
if(!function_exists('new_comp_indul_by_mbr')){
	function new_comp_indul_by_mbr($data=array()){
		$sql = "INSERT into ".$GLOBALS['table_prefix']."compindulbymbr
                       (UserID  
                        , CompEncLikes 
                        , CompEncLoc 
                        , CompIndulSex 
                        , CompIndulSexPref 
                        , CompIndulBJ 
                        , CompIndulBJPref 
                        , CompIndulHJ 
                        , CompIndulKiss 
                        , CompIndulKissPref 
                        , CompIndulLickpussy 
                        , CompIndulTouchpussy 
                        , CompIndulTouchpussyPref 
                        , CompIndulRim 
                        , CompIndulRimPref 
                        , CompIndulAnal 
                        , CompIndulAnalPref 
                        , CompIndulMsog 
                        , CompIndulTwogirl 
                        , CompIndulSecondcomp 
                        , CompIndulTwoguy 
                        , CompIndulVideo 
                        , CompIndulPhotos 
                        , CompIndulMassage 
                        , CompIndulSMBD 
                        , CompIndulSquirt 
                        , CompIndulTurnons 
                        , CompIndulTurnoffs) 
                       VALUES 
                       ( '".$data[0]."'
                       , 0
                       , 0
                       , '".$data[1]."'
                       , '".$data[2]."' 
                       , '".$data[3]."'
                       , '".$data[4]."'
                       , '".$data[5]."'
                       , '".$data[6]."'
                       , '".$data[7]."'
                       , '".$data[8]."'
                       , '".$data[9]."'
                       , '".$data[10]."'
                       , '".$data[11]."'
                       , '".$data[12]."'
                       , '".$data[13]."'
                       , '".$data[14]."'
                       , '".$data[15]."'
                       , '".$data[16]."'
                       , '".$data[17]."'
                       , '".$data[18]."'
                       , '".$data[19]."'
                       , '".$data[20]."'
                       , '".$data[21]."'
                       , '".$data[22]."'
                       , '".$data[23]."'
                       , 0
                       , 0)";
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}        
           
                    
//   INSERTS NEW RECORD IN REVIEWS TABLE BY MEMBER WHEN WRITING A REVIEW        
if(!function_exists('new_comp_review_by_mbr')){
	function new_comp_review_by_mbr($data=array()){
		$sql = "INSERT into ".$GLOBALS['table_prefix']."compreviews
                       (ReviewerID  
                        , ReviewerStatusID 
                        , CompanionID
                        , EncCountryID
                        , EncStateID
                        , EncCity
                        , EncDate
                        , EncLocationID
                        , EncTypeID
                        , CompLooksID 
                        , CompSmokeID 
                        , CompAttitudeID 
                        , EncAtmosphereID 
                        , CompPerformanceID 
                        , CompLocationRating 
                        , IndulgeTrue 
                        , CompPhotoTrue 
                        , CompOntimeTrue 
                        , ReviewHeadline 
                        , GenRevSummary 
                        , SexyRevDetails 
                        , SubmittedOn 
                        , ReviewStatus) 
                       VALUES 
                       ( '".$data[0]."'
                       , '".$data[1]."'
                       , '".$data[2]."' 
                       , '".$data[3]."'
                       , '".$data[4]."'
                       , '".$data[5]."'
                       , '".$data[6]."'
                       , '".$data[7]."'
                       , '".$data[8]."'
                       , '".$data[9]."'
                       , '".$data[10]."'
                       , '".$data[11]."'
                       , '".$data[12]."'
                       , '".$data[13]."'
                       , '".$data[14]."'
                       , '".$data[15]."'
                       , '".$data[16]."'
                       , '".$data[17]."'
                       , '".$data[18]."'
                       , '".$data[19]."'
                       , '".$data[20]."'
                       , '".$data[21]."'
                       , '".$data[22]."'
                       , '".$data[23]."'
                       , '".$data[24]."')";
		$query = mysql_query($sql);
		$newreviewid = mysql_insert_id();
		if(!$query)
			return 0;
		else return $newreviewid;
		}
	}                     

        
if(!function_exists('getCompReviewList')){
	function getCompReviewList($userid=0){
		$sql = "SELECT ReviewID
		 , ReviewerID
		 , EncStateID
		 , EncCity
		 , EncDate
		 , EncDurationID
		 , EncDonation
		 , EncLocationID
		 , EncTypeID
		 , CompLooksID
		 , CompPerformanceID
		 , ReviewHeadline
		FROM
                ".$GLOBALS['table_prefix']."compreviews
                WHERE CompanionID = ".$userid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}  	

        
if(!function_exists('getFullCompReview')){
	function getFullCompReview($userid=0, $reviewid=0){
		
		$sql = "SELECT *
                       FROM
                       ".$GLOBALS['table_prefix']."compreviews WHERE
                        CompanionID = ".$userid." and ReviewID = ".$reviewid." and ReviewStatus = 1";
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	} 

        
if(!function_exists('GetMemberLocation')){
	function GetMemberLocation($userid=0){
		$sql = "SELECT City, StateID from ".$GLOBALS['table_prefix']."users where UserID = ".$userid;
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		return $row['City'];
		}
	}        

if(!function_exists('GetCompMemberLocation')){
	function GetCompMemberLocation($userid=0){
		$sql = "SELECT City, StateID from ".$GLOBALS['table_prefix']."companions where UserID = ".$userid;
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		return $row['City'];
		}
	}        
                
        
if(!function_exists('GetMemberLoc')){
	function GetMemberLoc($userid=0){
		$sql = "Select City from ".$GLOBALS['table_prefix']."users where UserID = ".$userid;
		$query = mysql_query($sql);
		$row=mysql_fetch_array($query);
		return $row['City'];
		}
	} 
                
        
if (!function_exists('GetTimes')){        
    function GetTimes( $default = '19:00', $interval = '+30 minutes' ) {

    $output = '';

    $current = strtotime( '00:00' );
    $end = strtotime( '23:59' );

    while( $current <= $end ) {
        $time = date( 'H:i', $current );
        $sel = ( $time == $default ) ? ' selected' : '';

        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h:i A', $current ) .'</option>';
        $current = strtotime( $interval, $current );
    }

    return $output;
    }
}   


function detect_city($ip) {
        
        $default = 'UNKNOWN';

        if (!is_string($ip) || strlen($ip) < 1 || $ip == '127.0.0.1' || $ip == 'localhost')
            $ip = '8.8.8.8';

        $curlopt_useragent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)';
        
        $url = 'http://ipinfodb.com/ip_locator.php?ip=' . urlencode($ip);
        $ch = curl_init();
        
        $curl_opt = array(
            CURLOPT_FOLLOWLOCATION  => 1,
            CURLOPT_HEADER      => 0,
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_USERAGENT   => $curlopt_useragent,
            CURLOPT_URL       => $url,
            CURLOPT_TIMEOUT         => 1,
            CURLOPT_REFERER         => 'http://' . $_SERVER['HTTP_HOST'],
        );
        
        curl_setopt_array($ch, $curl_opt);
        
        $content = curl_exec($ch);
        
        if (!is_null($curl_info)) {
            $curl_info = curl_getinfo($ch);
        }
        
        curl_close($ch);
        
        if ( preg_match('{<li>City : ([^<]*)</li>}i', $content, $regs) )  {
            $city = $regs[1];
        }
        if ( preg_match('{<li>State/Province : ([^<]*)</li>}i', $content, $regs) )  {
            $state = $regs[1];
        }

        if( $city!='' && $state!='' ){
          $location = $city . ', ' . $state;
          return $location;
        }else{
          return $default; 
        }
        
}

if(!function_exists('countOkayReqs')){
	function countOkayReqs($user=0){
		$sql = "SELECT OkayReqID FROM ".$GLOBALS['table_prefix']."okayrequests WHERE RequesterID = ".$user." AND OkayReqStatus < 2";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		else return mysql_num_rows($query);
		}
	}

if(!function_exists('countCompOkayReqs')){
	function countCompOkayReqs($user=0){
		$sql = "SELECT OkayReqID FROM ".$GLOBALS['table_prefix']."okayrequests WHERE CompanionID = ".$user." AND OkayReqStatus < 2";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		else return mysql_num_rows($query);
		}
	}
        
if(!function_exists('countOkays')){
	function countOkays($user=0){
		$sql = "SELECT OkayReqID FROM ".$GLOBALS['table_prefix']."okayrequests WHERE RequesterID = ".$user." AND OkayReqStatus = 2";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		else return mysql_num_rows($query);
		}
	}     
        
if(!function_exists('countCompOkays')){
	function countCompOkays($user=0){
		$sql = "SELECT OkayReqID FROM ".$GLOBALS['table_prefix']."okayrequests WHERE CompanionID = ".$user." AND OkayReqStatus = 1";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		else return mysql_num_rows($query);
		}
	}      
        
if(!function_exists('countMemberOkays')){
	function countMemberOkays($user=0){
		$sql = "SELECT OkayReqID FROM ".$GLOBALS['table_prefix']."okayrequests WHERE RequesterID = ".$user." AND OkayReqStatus = 2";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		else return mysql_num_rows($query);
		}
	}              
        
if(!function_exists('countIndivCompReviews')){
	function countIndivCompReviews($user=0){
		$sql = "SELECT ReviewID FROM ".$GLOBALS['table_prefix']."compreviews WHERE CompanionID = ".$user." AND ReviewStatus = 1";
		$query = mysql_query($sql);
		if(!$query)
			return 0;
		else return mysql_num_rows($query);
		}
	}                     
        
if(!function_exists('getOkayReqListShort')){
	function getOkayReqListShort($userid=0){
		$sql = "SELECT *
		FROM
                ".$GLOBALS['table_prefix']."okayrequests
                WHERE RequesterID = ".$userid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}  	
        
if(!function_exists('getOkayRequest')){
	function getOkayRequest($okreqid=0){
		$sql = "SELECT *
		FROM
                ".$GLOBALS['table_prefix']."okayrequests
                WHERE 
                OkayReqID = ".$okreqid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}  
        
if(!function_exists('getEncRequest')){
	function getEncRequest($encid=0){
		$sql = "SELECT *
		FROM
                ".$GLOBALS['table_prefix']."encrequests
                WHERE 
                EncReqID = ".$encid;
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
		}
	}  	        

if(!function_exists('getCountry')){
	function getCountry($countryid=0){
		$sql = "SELECT Country
		FROM
                ".$GLOBALS['table_prefix']."countries
                WHERE 
                CountryID = ".$countryid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['Country'];
		}
	}      
        
if(!function_exists('getState')){
	function getState($countryid=0,$stateid=0){
		$sql = "SELECT State
		FROM
                ".$GLOBALS['table_prefix']."states
                WHERE 
                CountryID = ".$countryid." AND StateID = ".$stateid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['State'];
		}
	}          
        
if(!function_exists('getEncType')){
	function getEncType($enctypeid=0){
		$sql = "SELECT EncTypeDesc
		FROM
                ".$GLOBALS['table_prefix']."enctypes
                WHERE 
                EncTypeID = ".$enctypeid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['EncTypeDesc'];
		}
	}   

if(!function_exists('getEncLocation')){
	function getEncLocation($enclocationid=0){
		$sql = "SELECT EncLocationDesc
		FROM
               ".$GLOBALS['table_prefix']."enclocations
                WHERE 
                EncLocationID = ".$enclocationid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['EncLocationDesc'];
		}
	}            
        
if(!function_exists('getGender')){
	function getGender($genderid=0){
		$sql = "SELECT L1Gender
		FROM
                ".$GLOBALS['table_prefix']."gender
                WHERE 
                GenderID = ".$genderid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['L1Gender'];
		}
	}    
        
if(!function_exists('getSexOr')){
	function getSexOr($sexorid=0){
		$sql = "SELECT L1SexOr
		FROM
                ".$GLOBALS['table_prefix']."sexor
                WHERE 
                SexOrID = ".$sexorid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['L1SexOr'];
		}
	}            
        
if(!function_exists('getBodyType')){
	function getBodyType($bodytypeid=0){
		$sql = "SELECT L1BodyType
		FROM
                ".$GLOBALS['table_prefix']."bodytypes
                WHERE 
                BodyTypeID = ".$bodytypeid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['L1BodyType'];
		}
	}     
        
if(!function_exists('getHairColor')){
	function getHairColor($haircolorid=0){
		$sql = "SELECT L1HairColor
		FROM
                ".$GLOBALS['table_prefix']."haircolors
                WHERE 
                HairColorID = ".$haircolorid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['L1HairColor'];
		}
	}       
        
if(!function_exists('getHeight')){
	function getHeight($heightid=0){
		$sql = "SELECT HeightDescription
		FROM
                ".$GLOBALS['table_prefix']."height
                WHERE 
                HeightID = ".$heightid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['HeightDescription'];
		}
	}        
        
if(!function_exists('getEyeColor')){
	function getEyeColor($eyecolorid=0){
		$sql = "SELECT L1EyeColor
		FROM
                ".$GLOBALS['table_prefix']."eyescolors
                WHERE 
                EyeColorID = ".$eyecolorid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['L1EyeColor'];
		}
	}      
        
if(!function_exists('getSmoking')){
	function getSmoking($smokingid=0){
		$sql = "SELECT L1Smoking
		FROM
                ".$GLOBALS['table_prefix']."smoking
                WHERE 
                SmokingID = ".$smokingid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['L1Smoking'];
		}
	}      
        
if(!function_exists('getDrinking')){
	function getDrinking($drinkingid=0){
		$sql = "SELECT L1Drinking
		FROM
                ".$GLOBALS['table_prefix']."drinking
                WHERE 
                DrinkingID = ".$drinkingid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['L1Drinking'];
		}
	}            
        
if(!function_exists('getEthnicity')){
	function getEthnicity($ethnicityid=0){
		$sql = "SELECT L1Ethnicity
		FROM
                ".$GLOBALS['table_prefix']."ethnicity
                WHERE 
                EthnicityID = ".$ethnicityid;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['L1Ethnicity'];
		}  
        }
        
   if(!function_exists('updMbrMembershipStatus')){
	function updMbrMembershipStatus($user=0){
		$sql = "UPDATE ".$GLOBALS['table_prefix']."users SET MembershipStatusID = ".$membershipstatusid.", CxlPauseReasonID = ".$cxlreason.", CxlPauseConfirm = ".$cxlconfirm." WHERE UserID = ".$user;
		$query = mysql_query($sql);
		if(!$query)
			return false;
		else return true;
		}
	}     
        
   if(!function_exists('getMbrMembershipStatus')){
	function getMbrMembershipStatus($user=0){
		$sql = "SELECT MembershipStatusID FROM ".$GLOBALS['table_prefix']."users
                WHERE 
                UserID = ".$user;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['MembershipStatusID'];
                }
		
	} 

   if(!function_exists('getCompMembershipStatus')){
	function getCompMembershipStatus($user=0){
		$sql = "SELECT MembershipStatusID FROM ".$GLOBALS['table_prefix']."companions
                WHERE 
                UserID = ".$user;
		$query = mysql_query($sql);
		$row = mysql_fetch_array($query);
		return $row['MembershipStatusID'];
                }
		
	}         