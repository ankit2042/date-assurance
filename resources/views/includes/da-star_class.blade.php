<?php 

class star_class
{
	function show_star($star)
	{
	$star;
	list($int, $parse) = split('[.]', $star);
	$int;
	$parse;
	for ($i = 1; $i <= $int; $i++) 
	{
		$star_view .= '<i class="far fa-star" style="color:#f300fe"></i>';
	}
	if($parse==5)
		{
		$star_view .= '<i class="far fa-star-half" style="color:#f300fe"></i>';
		$dim_star = 4-$int;
		}
	else{
		$star_view .= "";
		$dim_star = 5-$int;}
	$dim_star;
	if($dim_star!="")
	{
	for ($i = 1; $i <= $dim_star; $i++) 
	{
		$star_view .= '<i class="far fa-star" style="color:#fff"></i>';
	}
	}
	return $star_view;
	}
	
}


class star_class_lg
{
	function show_star_lg($star_lg)
	{
	$star_lg;
	list($int, $parse) = split('[.]', $star_lg);
	$int;
	$parse;
	for ($i = 1; $i <= $int; $i++) 
	{
		$star_view_lg .= '<i class="fas fa-star fa-2x" style="color:#f300fe"></i>';
	}
	if($parse==5)
		{
		$star_view_lg .= '<i class="fas fa-star-half fa-2x" style="color:#f300fe"></i>';
		$dim_star_lg = 4-$int;
		}
	else{
		$star_view_lg .= "";
		$dim_star_lg = 5-$int;}
	$dim_star_lg;
	if($dim_star_lg!="")
	{
	for ($i = 1; $i <= $dim_star_lg; $i++) 
	{
		$star_view_lg .= '<i class="fas fa-star fa-2x" style="color:#fff"></i>';
	}
	}
	return $star_view_lg;
	}
	
}
?>