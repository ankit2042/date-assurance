<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
            <title><?php echo $title;?></title>
            <?php
                if(isset($_SESSION['memberid'])){
                $theme = getSourceThemes(getCurrentTheme($_SESSION['memberid']));
                $sourcetheme = (!empty($theme))?$theme:getSourceThemes(3);
                }
                else $sourcetheme = getSourceThemes(3);
            ?>
            <link rel="stylesheet" href="css/bootstrap.css"> 
            
            <link type="text/css" rel="stylesheet" href="css/style2.css" />
            <link type="text/css" rel="stylesheet" media="all" href="css/chat.css" />
            <link type="text/css" rel="stylesheet" media="all" href="css/screen.css" />
            <link href="<?php echo $base_url.'fuploads/'.$logo;?>" rel="shortcut icon" type="image/x-icon" />
            
     <!-- BEGIN ANIMATION EFFECT CSS -->
        <link rel="stylesheet" href="css/animation.css">
    <!-- END ANIMATION EFFECT CSS -->           

            <script language="javascript" type="text/javascript" src="js/jquery.min.js"></script>
            <script language="javascript" type="text/javascript" src="js/styles.js"></script>
            <script type="text/javascript" src="js/chat.js"></script>
            <input type="hidden" id="urlchat" value="chat/chat.php" />

<!-- bgg added -->

            <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,500,600" rel="stylesheet">

<!-- end bgg added -->

    </head>
    
    <?php
        $alerin = '';
        if(isset($_SESSION['memberid'])){
	$banned = chkBannedMe($_SESSION['memberid']);
	if(count($banned)>0){
		unset($_SESSION['memberid']);
		header('Location: '.$base_url.'signin.php');
		exit();
		}
	$mninbox = countNewEmail($_SESSION['memberid']);
	$mnsignal = countNewSignal($_SESSION['memberid']);
	if($mninbox>0 && $mnsignal>0)
		$alerin = 'onload="AnimationSignal(); startAnimation()"';
	elseif($mninbox>0)
		$alerin = 'onload="startAnimation()"';
	elseif($mnsignal>0)
		$alerin = 'onload="AnimationSignal()"';
	else $alerin = '';
	}
        $temp = str_replace('&', '%26',$pageURL.$_SERVER['REQUEST_URI']);
    ?>

<body <?php echo $alerin;?> data-spy="scroll" data-offset="25">
    <div class="animationload">
        <div class="loader">Loading...</div>            
    </div> <!-- End Preloader -->
    
    <div id="header"> <!-- refers to #header -->
        <div class="maincontent">
            <div class="headleft">            
                <table border="0" width="100%" border="1">
                    <tr>
                        <th rowspan="2" width="90"><a href="<?php echo $base_url;?>" ><img src="<?php echo $base_url.'fuploads/'.$logo;?>" border="0" style="width:88px; height:70px;"</a></th>
                        <td class="li-left-bottom"><br><a href="<?php echo $base_url;?>" ><span class="slogo1">hello </span><span class="slogo2">world</span><span class="superscript-white">TM</span></a></td>
                    </tr>
                    <tr style="vertical-align:top">
                        <td colspan="2"><span class="slogo-tag">the social network for safe &AMP; discreet world</span></td>
                    </tr>
                </table>  
            </div>
        <div class="headright">

        <?php
	if(isset($_SESSION['memberid']) && isset($_SESSION['memberemail'])){
	?>
            <p class="bottright" style="margin-top:20px;"><span class="rightspan"><?php echo $welcome;?> <i><?php echo $_SESSION['memberemail'];?></i></span>

        <?php
		if($mninbox>0)
            echo '<a href="'.$base_url.'members/mail.php" title="'.$mninbox.'&nbsp;'.$newemail.'"><span id="animation"><img src="'.$base_url.'imgs/ipnew1.png" border="0" title="'.$mninbox.'&nbsp;'.$newemail.'"/><img src="'.$base_url.'imgs/oldmail.gif" border="0" style="display:none" title="'.$mninbox.'&nbsp;'.$newemail.'"/></span><span class="rightspan"><sup>&nbsp;'.$mninbox.'</sup></span></a>';
		else echo '<a href="'.$base_url.'members/mail.php" title="'.$mninbox.'&nbsp;'.$newemail.'"><span id="animation" class="rightspan"><img src="'.$base_url.'imgs/oldmail.gif" border="0" title="'.$mninbox.'&nbsp;'.$newemail.'"/></span>';
                if($mnsignal>0)
            echo '<a href="'.$base_url.'members/vaforitesyou.php" title="'.$mnsignal.'&nbsp;'.$whowant.'"><span id="signal"><img src="'.$base_url.'imgs/signal1.png" border="0" /><img src="'.$base_url.'imgs/wireless_signal_icon.png" border="0" style="display:none"/></span><span class="rightspan"><sup>&nbsp;'.$mnsignal.'</sup></span></a>';
		else echo '<a href="'.$base_url.'members/vaforitesyou.php" title="'.$mnsignal.'&nbsp;'.$whowant.'"><span id="signal" class="rightspan"><img src="'.$base_url.'imgs/wireless_signal_icon.png" border="0" /></span></a>';
	?>
            <span class="rightspan">|</span><a href="<?php echo $base_url;?>signout.php"><?php echo $signout;?></a></p>

        <?php
		}
		else{
	?>
            <p class="bottright" style="margin-top:14px;"><a href="<?php echo $base_url;?>signup_cli.php"><?php echo $signup;?></a><span class="justspan">|</span><a href="<?php echo $base_url;?>signin.php"><?php echo $signin;?></a><span class="justspan">|</span><a href="<?php echo $base_url;?>help.php"><?php echo $help;?></a></p>
        <?php }?>
         </div>    
        </div>
        <p class="linehead">&nbsp;</p> 
    </div>
    