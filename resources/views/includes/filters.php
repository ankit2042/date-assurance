<script language="javascript">
function onchangeSearch(){
	var str = '?g='+document.getElementById('slgender').value+'&a='+document.getElementById('slage').value+'&c='+document.getElementById('slcountry').value;
	window.location.href='<?php echo $base_url;?>index.php'+str;
	}
</script>
<div id="filter">
    <p>Search Providers: </font><select id="slgender" onchange="onchangeSearch()"><option value="0">- - <?php echo $gender;?> - -</option>
    		<?php
			$sel = isset($_GET['g'])?$_GET['g']:0;
			$sqlcountry = 'select GenderID as Id, '.$_SESSION['lang'].'Gender as LName from '.$table_prefix.'gender order by TopSorted desc, LName asc';
			dropdownlist($sqlcountry, $sel);
			?>
    </select><select id="slage" onchange="onchangeSearch()"><option value="0">- - <?php echo $age;?> - -</option>
    	<option value="1" <?php echo (isset($_GET['a']) && $_GET['a']==1)?' selected="selected"':'';?> >18 <?php echo $to;?> 25</option>
        <option value="2" <?php echo (isset($_GET['a']) && $_GET['a']==2)?' selected="selected"':'';?> >25 <?php echo $to;?> 35</option>
        <option value="3" <?php echo (isset($_GET['a']) && $_GET['a']==3)?' selected="selected"':'';?> >35 <?php echo $to;?> 45</option>
        <option value="4" <?php echo (isset($_GET['a']) && $_GET['a']==4)?' selected="selected"':'';?> >45 <?php echo $to;?> 55</option>
        <option value="5" <?php echo (isset($_GET['a']) && $_GET['a']==5)?' selected="selected"':'';?> >55 <?php echo $to;?> 65</option>
        <option value="6" <?php echo (isset($_GET['a']) && $_GET['a']==6)?' selected="selected"':'';?> >65 <?php echo $to;?> 75</option>
    </select>
    <select id="slcountry" onchange="onchangeSearch()"><option value="0">- - <?php echo $country;?> - -</option>
    	<?php
		$sel = isset($_GET['c'])?$_GET['c']:0;
		$sqlcountry = 'select CountryID as Id, Country as LName from '.$table_prefix.'countries order by TopSorted desc, LName asc';
		dropdownlist($sqlcountry, $sel);
		?>
    </select></p>
</div>