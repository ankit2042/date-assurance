<?php

#output buffer
ob_start();

#session start
session_start();

#report all PHP errors
error_reporting(E_ALL);

#set to zero to not display errors
ini_set("display_errors", 1); 

$hostname = "localhost";
$database = "id15191600_da_db";
$username = "id15191600_admin";
$password = "K0aLa7&76^67&7";
$port = "8889";
$table_prefix = 'dt_';

if(file_exists('./install/install.php')){
	if(isset($_SESSION['completed']) && intval($_SESSION['completed'])==1){
		header('Location: '.$base_url.'install/install.php?m='.urlencode('You have completed the installation. Please manually remove the /install folder to operate your site.'));
		exit();
		}
	else{
		header('Location: '.$base_url.'install/install.php');
		exit();
		}
	}
elseif(isset($_SESSION['completed'])){
	unset($_SESSION['completed']);
	}


$pageURL = 'http';

if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on")
	$pageURL .= "s";
$pageURL .= "://";
if ($_SERVER["SERVER_PORT"] != "80")
	$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
else
	$pageURL .= $_SERVER["SERVER_NAME"];
if(is_dir("da-css")){
	$base_url = substr(dirname($_SERVER["SCRIPT_NAME"]), -1)=='/'?$pageURL.dirname($_SERVER["SCRIPT_NAME"]):$pageURL.dirname($_SERVER["SCRIPT_NAME"])."/";
	require_once '../languages/en.php';
	}
else{
	$base_url = substr(dirname(dirname($_SERVER["SCRIPT_NAME"])), -1)=='/'?$pageURL.dirname(dirname($_SERVER["SCRIPT_NAME"])):$pageURL.dirname(dirname($_SERVER["SCRIPT_NAME"]))."/";
	if(file_exists('../languages/en.php'))
		require_once '../languages/en.php';
	elseif(file_exists('../languages/en.php'))
		require_once '../languages/en.php';
	}
ob_end_clean();