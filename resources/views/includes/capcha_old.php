<?php
session_start();
$string_length=6;
$rand_string='';
$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
for ($i=0; $i < $string_length; $i++)
	$rand_string .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
$width=89;
$height=24;
$img=imagecreatetruecolor($width, $height);
$black=imagecolorallocate($img, 255, 255, 255);
$gray=imagecolorallocate($img, 59, 89, 152);
imagefilledrectangle($img, 0, 0, $width, $height, $gray);
putenv('GDFONTPATH=' . realpath('.'));
$font='arial.TTF';
$font_size=14;
$y_value=($height/2)+($font_size/2);
$x_value=($width-($string_length*$font_size))/2+4;
imagettftext($img, $font_size, 0, $x_value, $y_value, $black, $font, $rand_string);
$_SESSION['encoded_captcha']=$rand_string;
header("Content-Type: image/png");
imagepng($img);
?>