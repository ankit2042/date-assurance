<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
            <title><?php echo $title;?></title>
            <?php
                if(isset($_SESSION['memberid'])){
                $theme = getSourceThemes(getCurrentTheme($_SESSION['memberid']));
                $sourcetheme = (!empty($theme))?$theme:getSourceThemes(3);
                }
                else $sourcetheme = getSourceThemes(3);
            ?>

            <link type="text/css" rel="stylesheet" href="css/style2_signin.css" /> 
<!--            <link type="text/css" rel="stylesheet" media="all" href="css/chat.css" /> -->
<!--            <link type="text/css" rel="stylesheet" media="all" href="css/screen.css" /> -->
            <link href="<?php echo $base_url.'fuploads/'.$logo;?>" rel="shortcut icon" type="image/x-icon" />
            
    <!-- BEGIN ANIMATION EFFECT CSS -->

<!--            <link rel="stylesheet" href="css/animation.css"> -->

    <!-- END ANIMATION EFFECT CSS -->           

            <script language="javascript" type="text/javascript" src="js/jquery.min.js"></script>
            <script language="javascript" type="text/javascript" src="js/styles.js"></script>
            <script type="text/javascript" src="js/chat.js"></script>
<!--            <input type="hidden" id="urlchat" value="chat/chat.php" /> -->

    <!-- bgg added -->

            <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,500,600" rel="stylesheet">

    <!-- end bgg added -->                        
            
    <!-- Bootstrap -->
            <link rel="stylesheet" href="css/bootstrap.css">
    
    <!-- BEGIN GOOGLE FONT -->
            <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,300' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=Oswald:400,500,600,700,800,300' rel='stylesheet' type='text/css'>
    <!-- END GOOGLE FONT -->

    <!-- BEGIN OWL CAROUSEL SLIDER -->
            <link rel="stylesheet" href="css/owl.carousel.css">
    <!-- END OWL CAROUSEL SLIDER -->    

    <!-- BEGIN FONT AWESOME ICONS -->
            <link rel="stylesheet" href="css/font-awesome.css">
    <!-- END FONT AWESOME ICONS -->

    <!-- BEGIN PRETTY PHOTO LIGHTBOX -->
            <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- END PRETTY PHOTO LIGHTBOX -->
    
    <!-- BEGIN ANIMATION EFFECT CSS -->
            <link rel="stylesheet" href="css/animation.css">
    <!-- END ANIMATION EFFECT CSS -->
    
    <!-- Main Stylesheet CSS -->
            <link rel="stylesheet" href="css/style.css">

    <!--        <link rel="stylesheet" href="css/srstyle.css"> -->
                           
            <link href="css/age-verification.css" rel="stylesheet">
    
            <script src="js/age-verification.js"></script>     
        
            <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" /> 

    </head>
    
    <?php
        $alerin = '';
        if(isset($_SESSION['memberid'])){
	$banned = chkBannedMe($_SESSION['memberid']);
	if(count($banned)>0){
		unset($_SESSION['memberid']);
		header('Location: '.$base_url.'cli_signin.php');
		exit();
		}
	$mninbox = countNewEmail($_SESSION['memberid']);
	$mnsignal = countNewSignal($_SESSION['memberid']);
	if($mninbox>0 && $mnsignal>0)
		$alerin = 'onload="AnimationSignal(); startAnimation()"';
	elseif($mninbox>0)
		$alerin = 'onload="startAnimation()"';
	elseif($mnsignal>0)
		$alerin = 'onload="AnimationSignal()"';
	else $alerin = '';
	}
        $temp = str_replace('&', '%26',$pageURL.$_SERVER['REQUEST_URI']);
    ?>    
        
<body <?php echo $alerin;?> data-spy="scroll" data-offset="25">
        <div class="animationload">
            <div class="loader">Loading...</div>            
        </div> <!-- End Preloader -->
    
       