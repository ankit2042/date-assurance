    <!--/ CONTACT SECTION -->  
   <section id="contact" class="contact-wrapper">
        <div class="title text-center">
                <h2>Contact Us</h2>
                <p>Want to learn more? Have a concern with your account? REACH US HERE</p>
                <hr>
        </div><!-- end title -->
        <div class="contact_tab text-center">
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="tab1">
                    <div class="container">
                        <div id="message"></div>
                        <form id="contactform" action="contact.php" name="contactform" method="post" data-scroll-reveal="enter from the bottom after 0.4s">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">                                    
                                <input type="text" name="name" id="name" class="form-control" placeholder="Name"> 
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <input type="text" name="email" id="email" class="form-control" placeholder="Email Address"> 
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject">                                    </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <textarea class="form-control" name="comments" id="comments" rows="6" placeholder="Message"></textarea>                                </div>
                            <div class="text-center">
                                <button type="submit" value="SEND" id="submit" class="btn btn-lg btn-contact btn-primary">SUBMIT</button>
                            </div>
                        </form> <!-- End Form -->
                    </div> <!-- End Container -->
                </div><!-- End Tab Pane -->    
            </div><!-- /end my tab content -->  
        </div><!-- /contact_tab -->              
    </section><!--/ Contact End -->  