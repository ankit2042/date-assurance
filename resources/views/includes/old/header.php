<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>confidate(tm) - home</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <!-- BEGIN GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Lato:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">

    <!-- BEGIN PRETTY PHOTO LIGHTBOX -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    
    <!-- BEGIN ANIMATION EFFECT CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    
    <!-- Main Stylesheet CSS -->
    <link rel="stylesheet" href="css/c-d-style_redo.css">
    
<!--    <link href="css/age-verification.css" rel="stylesheet"> 
    
    <script src="js/age-verification.js"></script> -->
    
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
        
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/c-d_settings.css" media="screen">    
    
</head>
<body data-spy="scroll" data-offset="25">
    <div class="animationload">
        <div class="loader">Loading...</div>            
    </div> <!-- End Preloader -->
           
    <?php
        $alerin = '';
        if(isset($_SESSION['memberid'])){
	$banned = chkBannedMe($_SESSION['memberid']);
	if(count($banned)>0){
		unset($_SESSION['memberid']);
		header('Location: '.$base_url.'signin.php');
		exit();
		}
	$mninbox = countNewEmail($_SESSION['memberid']);
	$mnsignal = countNewSignal($_SESSION['memberid']);
	if($mninbox>0 && $mnsignal>0)
		$alerin = 'onload="AnimationSignal(); startAnimation()"';
	elseif($mninbox>0)
		$alerin = 'onload="startAnimation()"';
	elseif($mnsignal>0)
		$alerin = 'onload="AnimationSignal()"';
	else $alerin = '';
	}
        $temp = str_replace('&', '%26',$pageURL.$_SERVER['REQUEST_URI']);
    ?>

<body <?php echo $alerin;?> data-spy="scroll" data-offset="25">
    <div class="animationload">
        <div class="loader">Loading...</div>            
    </div> <!-- End Preloader -->
    
    <div id="header"> <!-- refers to #header -->
        <div class="maincontent">
            <div class="headleft">            
                <table border="0" width="100%" border="1">
                    <tr>
                        <th rowspan="2" width="90"><a href="<?php echo $base_url;?>" ><img src="<?php echo $base_url.'fuploads/'.$logo;?>" border="0" style="width:88px; height:70px;"</a></th>
                        <td class="li-left-bottom"><br><a href="<?php echo $base_url;?>" ><span class="slogo1">hello </span><span class="slogo2">world</span><span class="superscript-white">TM</span></a></td>
                    </tr>
                    <tr style="vertical-align:top">
                        <td colspan="2"><span class="slogo-tag">the social network for safe &AMP; discreet world</span></td>
                    </tr>
                </table>  
            </div>
        <div class="headright">

        <?php
	if(isset($_SESSION['memberid']) && isset($_SESSION['memberemail'])){
	?>
            <p class="bottright" style="margin-top:20px;"><span class="rightspan"><?php echo $welcome;?> <i><?php echo $_SESSION['memberemail'];?></i></span>

        <?php
		if($mninbox>0)
            echo '<a href="'.$base_url.'members/mail.php" title="'.$mninbox.'&nbsp;'.$newemail.'"><span id="animation"><img src="'.$base_url.'imgs/ipnew1.png" border="0" title="'.$mninbox.'&nbsp;'.$newemail.'"/><img src="'.$base_url.'imgs/oldmail.gif" border="0" style="display:none" title="'.$mninbox.'&nbsp;'.$newemail.'"/></span><span class="rightspan"><sup>&nbsp;'.$mninbox.'</sup></span></a>';
		else echo '<a href="'.$base_url.'members/mail.php" title="'.$mninbox.'&nbsp;'.$newemail.'"><span id="animation" class="rightspan"><img src="'.$base_url.'imgs/oldmail.gif" border="0" title="'.$mninbox.'&nbsp;'.$newemail.'"/></span>';
                if($mnsignal>0)
            echo '<a href="'.$base_url.'members/vaforitesyou.php" title="'.$mnsignal.'&nbsp;'.$whowant.'"><span id="signal"><img src="'.$base_url.'imgs/signal1.png" border="0" /><img src="'.$base_url.'imgs/wireless_signal_icon.png" border="0" style="display:none"/></span><span class="rightspan"><sup>&nbsp;'.$mnsignal.'</sup></span></a>';
		else echo '<a href="'.$base_url.'members/vaforitesyou.php" title="'.$mnsignal.'&nbsp;'.$whowant.'"><span id="signal" class="rightspan"><img src="'.$base_url.'imgs/wireless_signal_icon.png" border="0" /></span></a>';
	?>
            <span class="rightspan">|</span><a href="<?php echo $base_url;?>signout.php"><?php echo $signout;?></a></p>

        <?php
		}
		else{
	?>
            <p class="bottright" style="margin-top:14px;"><a href="<?php echo $base_url;?>signup_cli.php"><?php echo $signup;?></a><span class="justspan">|</span><a href="<?php echo $base_url;?>signin.php"><?php echo $signin;?></a><span class="justspan">|</span><a href="<?php echo $base_url;?>help.php"><?php echo $help;?></a></p>
        <?php }?>
         </div>    
        </div>
        <p class="linehead">&nbsp;</p> 
    </div>
    