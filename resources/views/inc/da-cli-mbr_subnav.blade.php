<!--- START SUB NAV --->
<style>

    /* Dropdown Button */
.dropdowntitle {
    color: #696969;
    padding: 10px;
    font-size: 14px;
    font-weight: 400;
    border: none;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
    position: relative;
    display: inline-block;
    float: none;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 180px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}
/*view-cancel-membership*/
/* Links inside the dropdown */
.dropdown-content a {
    color: #0094ff;
    padding: 6px 10px;
    font-size:12px;
    font-weight:400;
    text-decoration: none;
    display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {
    background-color: #ddd;
    color: #813ee8;
    text-decoration: none;
}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}

.site-sub-nav {
    padding: 10px 0 0 0;
    float: none;
    text-align: center;
}
</style>


<!--- START COMP SUB NAV --->

<div class="row" style="background-color:#f6f6f6;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="site-sub-nav">
            <ul class="list-inline">
                
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <a class="dropdowntitle" style="font-weight:500;font-size:16px;color:#0094ff;"><i class="fas fa-home"></i>&nbsp;my&nbsp;<span style="font-weight:300;color:#0094ff;font-size:17px;">d</span><span style="font-weight:700;color:#f300fe;">a</span><span style="font-weight:300;color:#f300fe;">&trade;</span>&nbsp;<i class="fas fa-caret-down"></i>&nbsp;</a>
                        <div class="dropdown-content">
                            
                            <a style="color:#0094ff;margin:15px;font-size:15px" href="{{-- route('') --}}">my dashboard</a>
                            
                            <span style="font-size: 16px;font-weight:600;margin-left:10px;">membership settings</span>
                                <a style="font-size: 15px;color:#0094ff;margin-left:15px;" href="{{ route('membership-setting.my-profile')}}">my profile</a>
                                <a style="font-size: 15px;color:#0094ff;margin-left:15px;" href="{{route('membership-setting.edit-profile',['id'=> Auth::user()->id])}}">update my profile</a>
                                <a style="font-size: 15px;color:#0094ff;margin-left:15px;" href="{{ route('view-profile-setting')}}">hide/show/delete my profile</a>
                                <a style="font-size: 15px;color:#0094ff;margin-left:15px;" href="{{route('change-password')}}">change password</a>
                            <br>
                            <span style="font-size: 16px;font-weight:600;margin-left:10px;">&nbsp;photos</span>
                                <a style="font-size: 15px;color:#0094ff;margin-left:15px;" href="{{ route('photo-upload')}}">upload photos</a>
                                <a style="font-size: 15px;color:#0094ff;margin-left:15px;" href="{{ route('photo-delete')}}">delete photos</a>
                                <a style="font-size: 15px;color:#0094ff;margin-left:15px;" href="{{ route('primary-photo')}}">set primary photo</a>
                            <br>
                            <span style="font-size: 16px;font-weight:600;;margin-left:10px;">&nbsp;member support</span>
                                <a style="font-size: 15px;color:#0094ff;margin-left:15px;" href="{{ route('view-cancel-membership')}}">cancel my membership</a>
                                <a style="font-size: 15px;color:#0094ff;margin-left:15px;" href="{{ route('help') }}">help</a>
                                <a style="font-size: 15px;color:#0094ff;margin-left:15px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_ms_contact_support.php">contact</a>
                            <br>    
                                <a style="font-size: 18px;color:#0094ff;font-weight:400;margin-left:15px;margin-bottom:15px;" href="<?php //echo $base_url;?>signin/da-cli-mbr_signout.php"><i class="fas fa-sign-out-alt"></i>&nbsp;Signout</a>
                        </div>
                    </div>
                </li>
                
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <a class="dropdowntitle" style="font-weight:500;font-size:16px;">companions&nbsp;<i class="fas fa-caret-down"></i>&nbsp;</a>
                        <div class="dropdown-content">
                          <a style="font-size:15px;color:#0094ff;margin-left:10px;margin-top:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_browse_comp_profiles.php"><?php //echo $brws_comp_profiles;?></a>
                          <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_comp_dir.php">companion directory</a>
                          <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_my_fav_comps.php">my favorite company<?php //echo $myfavoritecomps;?></a>
                          <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_comps_favd_me.php">companions who favorited me</a>
                          <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_search_comp_profiles.php">search companion profiles</a>
                          <a style="font-size:15px;color:#0094ff;margin-left:10px;margin-bottom:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_saved_searches.php">my saved searches</a>
                        </div>
                    </div>
                </li>
                
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <a class="dropdowntitle" style="font-weight:500;font-size:16px;">reviews &amp; ratings&nbsp;<i class="fas fa-caret-down"></i>&nbsp;</a>
                        <div class="dropdown-content">
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;margin-top:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_reviews_and_ratings.php">reviews &amp; ratings</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_browse_comp_reviews.php">browse reviews</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_submit_comp_review.php">submit a companion review</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_my_reviews.php">my reviews</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_my_fav_comp_reviews.php">my favorite reviews</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;margin-bottom:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_search_comp_reviews.php">search reviews</a>
                        </div>
                    </div>
                </li>
                
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <?php 
                            /*if($mninbox>0)
                                echo '<a style="font-weight:500;font-size:16px;" class="dropdowntitle with-alert" title="'.$mninbox.'&nbsp;'.$newemail.'">private messages &amp; requests<span class="badge" style="background-color:#813ee8;">'.$mninbox.'</span><i class="fas fa-caret-down"></i>&nbsp;</a>';
                                    else echo '<a style="font-weight:500;font-size:16px;" class="dropdowntitle with-alert" title="'.$mninbox.'&nbsp;'.$newemail.'">private messages &amp; requests<span class="badge" style="background-color:#0094ff;">0</span><i class="fas fa-caret-down"></i>&nbsp;</a>';  */                                         
                        ?> 
                        <div class="dropdown-content">
                            <?php 
                           /* if($mninbox>0)
                                echo '<a style="font-size:15px;color:#0094ff;margin-left:10px;margin-top:10px;" href="/mbrs/da-cli-mbr_mc_mailbox.php">my private messages<span class="badge" style="background-color:#813ee8;">'.$mninbox.'</span>&nbsp;</a>';
                                    else echo '<a style="font-size:15px;color:#0094ff;margin-left:10px;margin-top:10px;" href="/mbrs/da-cli-mbr_mc_mailbox.php">my private messages<span class="badge" style="background-color:#0094ff;">0</span>&nbsp;</a>';*/                                           
                            ?> 
                            
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_mc_compose.php">send a private message</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_mc_make_enc_request.php">make an encounter request</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_mc_view_all_enc_requests.php">view all my encounter requests</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_mc_make_okay_request.php">request an okay from a companion</a>
                            <a style="font-size:15px;margin-left:10px;margin-bottom:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_mc_view_all_okay_requests.php">view all my okay requests</a>
                        </div>
                    </div>
                </li>
                
            </ul>
        </div>
    </div>
</div>
<!--- END SUB NAV --->