                            
                                 <div class="card-lftside">
                                    <div class="card-header"><i class="far fa-comment"></i>&nbsp;private messages</div>
                                    <div class="card-body">
                                        <div class="list-group list-group-flush">
                                            <a class="list-group-item list-group-item-active" href="#">
                                                <div class="media">
                                                    <img class="d-flex mr-3 img-circle img-thumbnail-profile" src="../imgs/pro-profiles/c-d-profile1.jpg" width="50" height="50" alt="">
                                                    <div class="media-middle">
                                                        <span class="bold-text">pro-amy from Washington DC</span> added you to her client list<br>
                                                        <div class="small-text">today at 5:43 PM - 5m ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="list-group-item list-group-item-active" href="#">
                                                <div class="media">
                                                    <img class="d-flex mr-3 img-circle img-thumbnail-profile" src="../imgs/pro-profiles/c-d-profile2.jpg" width="50" height="50" alt="">
                                                    <div class="media-middle">
                                                        <span class="bold-text">pro-amy from Washington DC</span> sent you a new message.<br>
                                                        <div class="small-text">today at 5:43 PM - 5m ago</div>
                                                    </div>
                                                </div>
                                            </a>                                            
                                            <a class="list-group-item list-group-item-active" href="#">
                                                <div class="media">
                                                    <img class="d-flex mr-3 img-circle img-thumbnail-profile" src="../imgs/pro-profiles/c-d-profile3.jpg" width="50" height="50" alt="">
                                                    <div class="media-middle">
                                                        <span class="bold-text">pro-amy from Washington DC</span> sent you a new message.
                                                        <div class="small-text">today at 5:43 PM - 5m ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <br>
                                        <div class="pull-right"><a href="#">view all private messages</a></div>
                                    </div>
                                    <div class="card-header"><i class="fab fa-hotjar"></i>&nbsp;hot new reviews</div>
                                    <div class="card-body">
                                        <div class="list-group list-group-flush">
                                            <a class="list-group-item list-group-item-active" href="#">
                                                <div class="media">
                                                    <img class="d-flex mr-3 img-circle img-thumbnail-profile" src="../imgs/pro-profiles/c-d-profile1.jpg" width="50" height="50" alt="">
                                                    <div class="media-middle">
                                                        <span class="bold-text">randy_boy</span> reviewed <span class="bold-text">pro-amy from Los Angeles.</span><br>
                                                        <span class="bold-text-purple">4.5/5</span>
                                                        <div class="small-text">today at 5:43 PM - 5m ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="list-group-item list-group-item-active" href="#">
                                                <div class="media">
                                                    <img class="d-flex mr-3 img-circle img-thumbnail-profile" src="../imgs/pro-profiles/c-d-profile2.jpg" width="50" height="50" alt="">
                                                    <div class="media-middle">
                                                        <span class="bold-text">david</span> reviewed <span class="bold-text">sandy bottom from Washington DC.</span><br>
                                                        <span class="bold-text-purple">4.8/5</span>
                                                        <div class="small-text">today at 5:43 PM - 5m ago</div>
                                                    </div>
                                                </div>
                                            </a>                                            
                                            <a class="list-group-item list-group-item-active" href="#">
                                                <div class="media">
                                                    <img class="d-flex mr-3 img-circle img-thumbnail-profile" src="../imgs/pro-profiles/c-d-profile3.jpg" width="50" height="50" alt="">
                                                    <div class="media-middle">
                                                        <span class="bold-text">steve-o</span> reviewed <span class="bold-text">pro-amy from Philadelphia, PA.</span><br>
                                                        <span class="bold-text-purple">4.2/5</span>
                                                        <div class="small-text">today at 5:43 PM - 5m ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <br>
                                        <div class="pull-right"><a href="#">view all hot new reviews</a></div>
                                    </div>
                                    <div class="card-header"><i class="fas fa-link"></i>&nbsp;quick links</div>
                                    <div class="card-body">
                                        <div class="left-link"><a href="listMyReviews.asp"><span>my reviews</span></a></div>
                                        <div class="left-link"><a href="myfavoriteReviews.asp"><span>my favorite reviews</span></a></div>
                                        <div class="left-link"><a href="myfavorites.asp"><span>my favorite providers</span></a></div>
                                        <div class="left-link"><a href="myfavorites.asp"><span>my profile</span></a></div>
                                        <div class="left-link"><a href="account_manager/myPosts.asp"><span>my membership settings</span></a></div>
                                        <div class="left-link"><a href="account_manager/feedback.asp"><span>my feedback</span></a></div>
                                        <div class="left-link"><a href="account_manager/problemReports.asp"><span>manage my account</span></a></div>
                                        <div class="left-link"><a href="account_manager/myWhiteList.asp"><span>my referrals</span></a></div>
                                        <div class="left-link"><a href="discussion-boards/newbie-33/ters-instruction-manual-75528"><span>member support</span></a></div>
                                    </div>                                            
                                    <div class="card-header"><i class="far fa-newspaper"></i>&nbsp;free newsletter</div>
                                    <div class="card-body">
                                        <p class="card-text">sign up for a free dateassure&trade; email newsletters every quarter.</p>
                                        <div class="da-form newsub-form form-horizontal">
                                            <form name="SubscribeNews" action="submit" method="get">
                                                <div class="form-group">
                                                    <label class="form-group-label">enter an email address</label>
                                                    <input class="form-control" name="SubscribeEmail" value="" type="text">
                                                </div>
                                                <div class="newsub-form-actions">
                                                    <button type="submit" class="btn btn-cli-grad">subscribe</button>
                                                </div>
                                            </form>
                                        </div> 
                                    </div>
                                    <div class="card-header"><i class="far fa-question-circle"></i>&nbsp;things you should know</div>
                                    <div class="card-body">
                                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                    </div>
                                </div><!----><!---->
                            </div><!----><!---->