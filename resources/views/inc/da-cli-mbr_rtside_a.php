                <div class="right-container visible-lg">
    
        <!-- begin "write a review" button -->            
            
                    <a href="<?php echo $base_url;?>mbrs/cli-submitReview.asp" class="action-write">
                        <i class="far fa-edit"></i>
                        write a review
                    </a><!-- end "write a review" button -->  
                    
            <!-- begin "site statistics side-bar" -->
	
                    <?php 
                        require_once "../inc/modules/da-stats_sidebar.php";
                    ?>    
            
            <!-- end "site statistics side-bar" -->
    
            <!-- begin "site-promo-side-bar" -->
    
                    <div class="banner-container alt">
                        <a href="vip/buyVIP.asp?utm_source=tersite&amp;utm_medium=banner&amp;utm_campaign=vip&amp;utm_content=d1_58&amp;utm_term=i1_2" class="banner-link">
                            <img src="../da-ter-based-files/BecomeVIPbanner-v1.jpg" class="banner-image" data-rjs="2">
                                <span class="banner-title"></span>
                                <span class="banner-text">Never settle for less, get the best!</span>
                                <span class="banner-call">Upgrade</span>
                        </a>
                    </div><!-- end "site-promo-side-bar" -->                    
                </div>