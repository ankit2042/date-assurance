
                                    
                                    <div class="section-subtitle-left" style="font-size:34px;"><span style="color:#f300fe;"><i class="far fa-sad-tear"></i></span>&nbsp;we're sorry to see you go!</div>

                                    <br>
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            
                                            <span style="font-size:16px;">while breaking up is hard, we want to make this as easy and painless as possible</span>
                                    
                                            <br>
                                            
                                            <br>
                                            
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="section-subtitle-left">
                                                        <span style="font-size:26px;">we offer</span><span class="fa-layers fa-2x fa-fw"><i class="fas fa-circle" style="color:#813ee8"></i><span class="fa-layers-text fa-inverse" style="font-size:24px;font-weight:900">2</span>
                                                        </span><span style="font-size:26px;">options for you: you can either <span style="color:#813ee8;">pause</span> or <span style="color:#813ee8;">cancel</span> your membership</span>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <br>
                                            
                                            <br> 
                                            
                                            <div class="card" style="background:#e3f3ff;">
                                                <div class="card-body">
                                                    <span style="font-size:18px;font-weight:400;color:#813ee8;">are you sure you want to go?</span>&nbsp;<span style="font-size:14px;color:#595959;font-weight:300">was it something we said? <a href="../mbrs/da-cli-mbr_feedback.php">tell us</a></span>
                                                        
                                                    <br>
                                                    
                                                    <br>
                                                        
                                                    <span style="font-size:16px;">please be careful when deciding to cancel your membership. memberships that are cancelled will lose access to any reviews, photos, personal contacts, private messages and more created through the website and cannot be restored.</span>
                                                    
                                                </div>
                                            </div>
                                            
                                            <br>
                                                    
                                            <div class="row">
                                                <div class="col-lg-6">        
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="section-subtitle-left">
                                                                        <span style="font-size:26px;">option</span><span class="fa-layers fa-2x fa-fw"><i class="fas fa-circle" style="color:#0094ff"></i><span class="fa-layers-text fa-inverse" style="font-size:24px;font-weight:900">1</span>
                                                                        </span><span style="font-size:26px;"> pause your membership</span>
                                                                    </div>

                                                                    <hr>
                                                                    
                                                                    <div class="row">

                                                                        <div class="col-lg-1">&nbsp;</div>

                                                                        <div class="col-lg-10">

                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input" type="radio" name="rdpausembrship" value="1">
                                                                                <label class="form-check-label" for="rdpausembrship" style="font-weight:400;font-size:14px;">pause my membership</label>
                                                                            </div>

                                                                            <br>

                                                                            <br>

                                                                            <span style="font-size:14px;font-weight:300;">(you can pause your membership for up to 6 months and your profile data will be preserved for future retrieval. unless you renew, after 6 months all profile data will be deleted.)</span>

                                                                        </div>
                                                                        
                                                                        <div class="col-lg-1">&nbsp;</div>
                                                                        
                                                                    </div>
                                                                    
                                                                    <br>
                                                                    
                                                                    <div class="row">
                                                                        <div class="col-lg-12 align-center">
                                                                            <p style="text-align:center;">
                                                                                <button data-toggle="modal" data-target="#pauseMembershipModal" class="btn btn-pro-cli"> next >> </button>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>    

                                                <div class="col-lg-6">        
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="section-subtitle-left">
                                                                        <span style="font-size:26px;">option</span><span class="fa-layers fa-2x fa-fw"><i class="fas fa-circle" style="color:#0094ff"></i><span class="fa-layers-text fa-inverse" style="font-size:24px;font-weight:900">2</span>
                                                                        </span><span style="font-size:26px;"> cancel your membership</span>
                                                                    </div>

                                                                    <hr>
                                                                    
                                                                    <div class="row">

                                                                        <div class="col-lg-1">&nbsp;</div>

                                                                        <div class="col-lg-10">

                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input" type="radio" name="rdcancelmbrship" value="1">
                                                                                <label class="form-check-label" for="rdcancelmbrship" style="font-weight:400;font-size:14px;">cancel my membership</label>
                                                                            </div>

                                                                            <br>

                                                                            <br>

                                                                            <span style="font-size:14px;font-weight:300;">(your profile data will be preserved for 30 days after your cancellation date.
                                                                                
                                                                                <br>
                                                                                
                                                                                unless you renew, after 30 days all profile data will be deleted.)
                                                                            
                                                                            </span>

                                                                        </div>
                                                                        
                                                                        <div class="col-lg-1">&nbsp;</div>
                                                                        
                                                                    </div>
                                                                    
                                                                    <br>
                                                                    
                                                                    <div class="row">
                                                                        <div class="col-lg-12 align-center">
                                                                            <p style="text-align:center;">
                                                                                <button data-toggle="modal" data-target="#cancelMembershipModal" class="btn btn-cli-pro"> next >> </button>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
