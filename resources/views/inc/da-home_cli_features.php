<!--/ ABOUT SECTION --> 

<a name="cli_features"></a>            
        
    <section id="features" class="white-wrapper">
        <div class="container">
            <div class="title text-center">
                <h2>Features for Client Members</h2>
                <p>READ REVIEWS, POWERFUL SEARCH TOOLS, RATE YOUR DATE, POST REVIEWS, SECURE COMMUNICATIONS WITH COMPANIONS</p>
            </div><!-- end title -->
        
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><i class="far fa-comments fa-3x alignleft" style="color:#fd7bcb;"></i></div>
                        <h3 style="color:#8dc3fe;">Secure Communications with Companions</h3>
                        <p>With dateassure's internal messaging system, you can be confident no one is going to stumble upon your texts or emails when you reach out to verified Companions.</p>
                    </div>
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><i class="far fa-check-circle fa-3x alignleft" style="color:#fd7bcb;"></i></div>
                        <h3>VERIFIED Companions</h3>
                        <p>We make every effort to verify Companions that join our site or that are reviewed or appearing 
                            in the photographs on our site are the individual providing adult companionship. </p>
                    </div>
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><i class="fas fa-search fa-3x alignleft" style="color:#fd7bcb;"></i></div>
                        <h3>POWERFUL SEARCH TOOLS</h3>
                        <p>Search from thousands of Companions for your favorite attributes or special indulgences.</p>
                    </div>
                </div>
            </div> <!-- end row 1 -->
        
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><i class="fas fa-mobile-alt fa-3x alignleft" style="color:#fd7bcb;"></i></div>
                        <h3>COMPLETELY MOBILE-FRIENDLY</h3>
                        <p>No computer? Out on the road? No problem - easily search, review, and connect with your favorite Companions all 
                            from your smartphone.</p>
                    </div>
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><i class="far fa-thumbs-up fa-3x alignleft" style="color:#fd7bcb;"></i></div>
                        <h3>REVIEWS</h3>
                        <p>Read reviews written by fellow client members' experiences with Companions or write your own. The more 
                            reviews, the more valuable the site is to all its members. Get 5 membership credits for every review. 
                            100 credits gets you a free month!</p>
                    </div>
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><i class="fas fa-users fa-3x alignleft" style="color:#fd7bcb;"></i></div>
                        <h3>SOCIAL MEDIA</h3>
                        <p>Many Companions are active on Twitter, Instagram, etc. and we provide a Twitter feed to allow followers 
                            a single place to keep up on all activity.</p>
                    </div>
                </div>
            </div> <!-- end row 2 -->
            <div class="text-center">
                <a href="#pricing" class="btn-lg btn-pro-signup">SIGN ME UP AS A CLIENT!</a>
            </div>
        </div>
    </section>
