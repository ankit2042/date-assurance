    <!--/ PROVIDER WEBSITE FEATURE SECTION -->  
    <section id="featured_parallax" class="parallax" style="background-image: url('imgs/home/couple_happy1.jpg');" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="20">
        <div class="overlay">
            <div class="container">
                <div class="featured-box" data-effect="slide-bottom">
                    <h3>COMING SOON...COMPANIONS WILL GET A FREE CUSTOMIZABLE WEBSITE <br> & EMAIL/SCHEDULE MANAGEMENT SYSTEM</h3>
                    <img class="img-respnsive" src="imgs/promo/banner2.png" alt="">
                </div>
            </div><!-- end container -->
        </div><!-- end overlay -->
    </section><!--/ Featured Parallex -->  