<section class="top-nav">
<nav class="navbar navbar-fixed-top navbar-expand-lg navbar-dark bg-dark d-flex align-items-center">
    
    <a class="navbar-brand" href="<?php echo $base_url;?>index.php"><img src="../imgs/brand/dateassurance-logo-shield-home.png" class="align-top" alt=""></a>
    
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" style="font-weight:200;" href="<?php echo $base_url;?>index.php">home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="font-weight:200;" href="<?php echo $base_url;?>index.php#about">about</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="font-weight:200;" href="<?php echo $base_url;?>index.php#features">features</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="font-weight:200;" href="<?php echo $base_url;?>mbrs/da-cli-mbr_comp_dir.php">companion directory</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="font-weight:200;" href="#" data-toggle="modal" data-target="#searchModal">search</a>
            </li>
        </ul>
        
        <ul class="navbar-nav mr-auto" style="float:right">

            <li class="nav-item">
                <a class="nav-link" href="<?php echo $base_url;?>signin/da-cli-mbr_signin.php"><span style="font-weight:200;font-size:15px;color:#0094ff;"><i class="fas fa-sign-in-alt"></i>&nbsp;member login</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo $base_url;?>signin/da-comp-mbr_signin.php"><span style="font-weight:200;font-size:15px;color:#f300fe;"><i class="fas fa-sign-in-alt"></i>&nbsp;companion login</span></a>
            </li>
           
        </ul>
    </div>

</nav>
</section>
