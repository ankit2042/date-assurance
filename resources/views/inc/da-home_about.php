<!--/ ABOUT SECTION --> 

<a name="about"></a>    
    
    <section id="about" class="white-wrapper">
        <div class="container">
            <div class="title text-center">
                <h2>about the site</h2>
                <p>DATEASSURE&trade; CAN HELP KEEP YOU SAFE</p>
            </div><!-- end title -->
        
            <div class="row text-center">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-box">
                        <div class="about-border"> <i class="fas fa-3x fa-inbox aligncenter" style="color:#8dc3fe;"></i></div>
                        <h3>MESSAGE CENTER</h3>
                        <p>Allows for near real-time secure chat with providers. It also lets you see when others are typing, when messages are delivered, and when they are read.</p>
                    </div>
                </div><!-- end column -->
            
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-box">
                        <div class="about-border"> <i class="fab fa-3x fa-sistrix aligncenter" style="color:#8dc3fe;"></i></div>
                        <h3>POWERFUL SEARCH CAPABILITIES</h3>
                        <p>Quisque est enim lacinia lobortis da viverra interdum, quam. In sagittis, eros faucibus ullamcorper nibh dolor</p>
                    </div>
                </div><!-- end column -->
            
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="about-box">
                                <div class="about-border"> <i class="far fa-3x fa-check-circle aligncenter" style="color:#8dc3fe;"></i></div>
                                <h3>SIMPLE & EASY TO USE ON YOUR MOBILE AS WELL</h3>
                                <p>Quisque est enim lacinia lobortis da viverra interdum, quam. In sagittis, eros faucibus ullamcorper nibh dolor</p>
                        </div>
                    </div><!-- end column -->
            
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="about-box">
                                <div class="about-border"> <i class="fas fa-user-secret fa-3x aligncenter" style="color:#8dc3fe;"></i></div>
                                <h3>MILITARY GRADE ENCRYPTION</h3>
                                <p>Approved by the National Security Agency (NSA) to protect information at a “Top Secret” level. It is now used by governments, militaries, banks and other organizations to protect sensitive data.</p>
                        </div>
                    </div><!-- end column -->
            </div><!-- end row -->
            <div class="text-center">
                <a href="#pricing" class="btn-lg btn-pro-signup">SIGN ME UP!</a>
            </div> 
        </div><!-- end container -->

    </section><!-- End About Section -->
