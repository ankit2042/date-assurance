
<!--	<style>
		* {font-family: 'Lato',sans-serif; color:#fff;size:6px} 
		a, h1, h2, h3, h4, h5 {text-align:center; color:#eee;}
		body {background:#ffffff;}
	</style> -->

    <!-- font awesome -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
  
    <!-- jQuery -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>  
  
    <!-- nanoGALLERY CSS files -->
                                                          
	<link href="da-themes/nano-themes/clean/nanogallery_clean.css" rel="stylesheet" type="text/css">

    <!-- nanoGALLERY javascript  -->
                                                                                                                 
	<script type="text/javascript" src="da-js/nano-js/jquery.nanogallery.js"></script>

  
	<script>
		$(document).ready(function () {
    

      // ##################################################################################################################
			// ##### Multi-level navigation (API method) #####
      // ##################################################################################################################
      
      // you define the albumID when you make an ID with kind:'album'
                    var contentGalleryMLN=[
			{ ID:1, kind:'album', src: 'pro-amy1.jpg', srct: 'pro-amy1.jpg', title: 'pro-amy', 
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><br>houston, tx', },
			{ ID:2, kind:'album', src: 'pro-milan_millions1.jpg', srct: 'pro-milan_millions1.jpg', title: 'milan_millions', 
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star-half"></i><br>san diego, ca',},
                        { ID:3, kind:'album', src: 'pro-sandy1.jpg', srct: 'pro-sandy1.jpg', title: 'blondie69', 
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star-half"></i><br>dallas, tx', },
			{ ID:4, kind:'album', src: 'pro-amy1.jpg', srct: 'pro-amy1.jpg', title: 'pro-amy', 
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><br>new orleans, la', },
			{ ID:5, kind:'album',src: 'pro-milan_millions1.jpg', srct: 'pro-milan_millions1.jpg', title: 'stormy weather',
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><br>new orleans, la',},
                        { ID:6, kind:'album',src: 'pro-sandy1.jpg', srct: 'pro-sandy1.jpg', title: 'blondie69', 
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><br>new orleans, la',
                        },
			{   
                            src: 'image_03.jpg', 
                            srct: 'image_03ts.jpg', 
                            title: 'album 2', 
                            albumID:103, 
                            kind:'album' 
                        },
			{ src: 'pro-amy1.jpg',	// image url
                            srct: 'pro-amy1.jpg',	// thumbnail url
                            title: 'pro-amy', 		// thumbnail title
                            albumID:101,
                            kind:'album',
                        },
			{   
                            src: 'image_03.jpg', srct: 'image_03ts.jpg', title: 'image 1a', ID:23, albumID:3	
                        },
			{   
                            src: 'image_02.jpg', srct: 'image_02ts.jpg', title: 'image 1b', ID:24, albumID:3	
                        },
			{   
                            src: 'image_01.jpg', srct: 'image_01ts.jpg', title: 'image 1c', ID:25, albumID:2	
                        },
			{   
                            src: 'image_03.jpg', srct: 'image_03ts.jpg', title: 'image 1d', ID:26, albumID:1	
                        },
			{   
                            src: 'image_03.jpg', srct: 'image_03ts.jpg', title: 'image 1e', ID:27, albumID:1 
                        },
			{   
                            src: 'image_03.jpg', srct: 'image_03ts.jpg', title: 'image 1f', ID:28, albumID:1	
                        },
			
        ];
			
            jQuery("#nanoGalleryMLN").nanoGallery({thumbnailWidth:90,thumbnailHeight:120,
                    items:contentGalleryMLN,
            // paginationMaxItemsPerPage:3,
                        paginationSwipe:true,
                        thumbnailAlignment: 'center',
                        thumbnailDisplayTransition:'flipUp',
                        thumbnailLabel:     { position: 'overImageOnBottom', hideIcons: true },
                        displayDescription:true,
                        paginationMaxLinesPerPage:1,
                        paginationDots : true,
                        paginationVisiblePages: 10,
			thumbnailHoverEffect:'imageScale150',
                        viewerDisplayLogo:true,
			useTags:false,
                        locationHash:false,
                        breadcrumbAutoHideTopLevel:true,
                        galleryToolbarHideIcons:true,
                        maxItemsPerLine:4,
                        locationHash: false,
                        theme:'clean',
                        itemsBaseURL:'../imgs/pro-profiles/'
			});
			
      jQuery('#btnPaginationCount').on('click', function() {
        alert(jQuery('#nanoGalleryMLN').nanoGallery('paginationCountPages'));
      });
      jQuery('#btnPaginationNext').on('click', function() {
        jQuery('#nanoGalleryMLN').nanoGallery('paginationNextPage');
      });
      jQuery('#btnPaginationPrevious').on('click', function() {
        jQuery('#nanoGalleryMLN').nanoGallery('paginationPreviousPage');
      });
      jQuery('#btnPaginationGoto').on('click', function() {
        jQuery('#nanoGalleryMLN').nanoGallery('paginationGotoPage',2);
      });

	
		});
	</script>


	<div id="nanoGalleryMLN"></div>
