<!--/HEADER SECTION -->

<div class="header"> 
<nav class="navbar navbar-fixed-top navbar-dark bg-dark d-flex align-items-center">
    <div class="navbar-header col-lg-2 col-md-4 col-sm-4 col-xs-5">
        <a class="navbar-brand" href="<?php echo $base_url;?>da-index.php"><img src="<?php echo $base_url;?>imgs/brand/dateassurance-logo-shield-home.png" class="align-top" alt=""></a>  
    </div>
    
        <ul class="navbar-nav mr-auto col-lg-7 col-md-4 col-sm-4 hidden-xs-down">
            <li class="nav-item">
                &nbsp;
            </li>
        </ul>

        <?php
            if(isset($_SESSION['memberid']) && isset($_SESSION['memberemail'])){
        ?>        
        
        <ul class="navbar-nav mr-auto col-lg-3 col-md-4 col-sm-4 col-xs-7" style="font-weight:500;font-size:15px;text-align:right;float:right;">

            <li class="nav-item" style="float:right">
            <a href="<?php echo $base_url;?>mbrs/da-cli-mbr_signout.php" id="navbarDropdownMbr"><span style="color:#0094ff;">member <?php echo $_SESSION['memberid'];?>&nbsp;<i class="fas fa-sign-out-alt"></i>&nbsp;<?php echo $signout;?></span></a>
            <span style="font-size:10px" class="form-text text-muted">for your own security<br>please sign out when finished</span>
            </li>
            
        </ul>
    

</nav>
</div>
    <?php
        }
        else{
    ?>
                            

    <?php
    }
    ?>


    <!-- end da-cli-mbr_topnav  -->



