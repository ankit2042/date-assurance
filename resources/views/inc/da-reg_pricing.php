
<a name="pricing"></a> 

<!--PRICING SECTION  -->
<br>
<br>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-12">
             <div class="card-container manual-flip">
                <div class="flipcard">
                    
<!-- begin FRONT OF SILVER CARD -->                                        
                    <div class="front">
                        <div class="cover">
                            <img src="../imgs/site/sexy-bed-woman799088.png" class="blur">
                        </div>
                        <div class="user">
                        <!--    <a onclick="rotateCard(this)"> --><img class="img-circle" src="../imgs/site/purple-fill.png">
                            <p id="price-silver">
                                <sup>$</sup><?php/*
                                                $sql = 'select Prices from '.$table_prefix.Prices' where 'Id' = 1;
                                                    silverPrice = $sql
                                                    echo (silverPrice); */
							
                                            ?>24
                            </p>
                            <p id="price-dur">
                                30 days
                            </p>
                            </a>
                        </div>
                        <div class="content">
                            <div class="main" style="text-align:center">
                                <div class="promo-title">
                                    <i class="fas fa-star"></i><br>
                                    <br>
                                    VIP<br>
                                    SILVER PLAN
                                </div>
                                <h6>30 days of access<br>
                                    (billing recurs every 30 days)</h6>
                                <br>
                                <span style="line-height:20px;">
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;search companion profiles<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;view companion ratings<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;browse companion gallery<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;view companion reviews<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;write companion reviews<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;secure communication with companions<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;secure communication with other members<br>
                                </span>
                            </div>
                            <div class="footer">
                                <button class="btn btn-pro-cli" onclick="rotateCard(this)">
                                    <i class="fas fa-unlock-alt"></i> sign up for vip silver
                                </button>
                            </div>
                        </div>
                    </div> <!-- end front panel -->
                    
                    <div class="back">
                        <div class="header">
                            <div class="headline">subscription payment</div>
                        </div>
                        <div class="content">
                            <div class="main">
                        <?php
                            require_once '../inc/modules/da-price-silver.php';
                        ?>
                            </div>
                        </div>
                    </div> <!-- end back panel -->
                </div> <!-- end card -->
            </div> <!-- end card-container -->
        </div> <!-- end col sm 3 -->

        <div class="col-lg-4 col-md-6 col-sm-12">
             <div class="card-container manual-flip">
                <div class="flipcard">
                    
<!-- begin FRONT OF GOLD CARD -->                                        
                    <div class="front">
                        <div class="cover">
                            <img src="../imgs/site/sexy-bed-woman799095.png" class="blur">
                        </div>
                        <div class="user">
                            <img class="img-circle" src="../imgs/site/purple-fill.png">
                            <p id="price-gold">
                                <sup>$</sup>59
                            </p>
                            <p id="price-dur">
                                90 days
                            </p>
                        </div>
                        <div class="content">
                            <div class="main" style="text-align:center">
                                <div class="promo-title">
                                    <i class="fas fa-star"></i><i class="fas fa-star"></i><br>
                                    <br>
                                    VIP<br>
                                    GOLD PLAN
                                </div>
                                <h6>90 days of access<br>
                                    (billing recurs every 90 days)</h6>
                                <br>
                                <span style="line-height:20px;">
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;search companion profiles<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;view companion ratings<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;browse companion gallery<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;view companion reviews<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;write companion reviews<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;secure communication with companions<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;secure communication with other members<br>
                                </span>
                            </div>
                            <div class="footer">
                                <button class="btn btn-pro-cli" onclick="rotateCard(this)">
                                    <i class="fas fa-unlock-alt"></i> sign up for vip gold
                                </button>
                            </div>
                        </div>
                    </div> <!-- end front panel -->
                    
                    <div class="back">
                        <div class="header">
                            <div class="headline">subscription payment</div>
                        </div>
                        <div class="content">
                            <div class="main">
                        <?php
                            require_once '../inc/modules/da-price-gold.php';
                        ?>
                            </div>
                        </div>
                    </div> <!-- end back panel -->
                </div> <!-- end card -->
            </div> <!-- end card-container -->
        </div> <!-- end col sm 3 -->
        
        
        <div class="col-lg-4 col-md-6 col-sm-12">
             <div class="card-container manual-flip">
                <div class="flipcard">
                    
<!-- begin FRONT OF PLATINUM CARD -->                                        
                    <div class="front">
                        <div class="cover">
                            <img src="../imgs/site/sexy-bed-woman799099.png" class="blur">
                        </div>
                        <div class="user">
                            <img class="img-circle" src="../imgs/site/purple-fill.png">
                            <p id="price-plat">
                                <sup>$</sup>159
                            </p>
                            <p id="price-dur">
                                365 days
                            </p>
                        </div>
                        <div class="content">
                            <div class="main" style="text-align:center">
                                <div class="promo-title">
                                    <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><br>
                                    <br>
                                    VIP<br>
                                    PLATINUM PLAN
                                </div>
                                <h6>365 days of access<br>
                                    (billing recurs annually)</h6>
                                <br>
                                <span style="line-height:20px;">
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;search companion profiles<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;view companion ratings<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;browse companion gallery<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;view companion reviews<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;write companion reviews<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;secure communication with companions<br>
                                    <i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;secure communication with other members<br>
                                </span>
                            </div>
                            <div class="footer">
                                <button class="btn btn-pro-cli" onclick="rotateCard(this)">
                                    <i class="fas fa-unlock-alt"></i> sign up for vip platinum
                                </button>
                            </div>
                        </div>
                    </div> <!-- end front panel -->
                    
                    <div class="back">
                        <div class="header">
                            <div class="headline">subscription payment</div>
                        </div>
                        <div class="content">
                            <div class="main">
                        <?php
                            require_once '../inc/modules/da-price-plat.php';
                        ?>
                            </div>
                        </div>
                    </div> <!-- end back panel -->
                </div> <!-- end card -->
            </div> <!-- end card-container -->
        </div> <!-- end col sm 3 -->
    </div>


  
<!--
    <div class="row text-center">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" data-effect="helix">
            <div class="pricing-box">
                <span class="hideme"><i class="fas fa-star fa-2x"></i></span>
                <div class="card-header"><h3>VIP - Awesome!</h3></div>
                            <div class="price">
                                <p class="price-value">$24</p>
                                <p class="price-month">for 30 days (recurring)</p>
                            </div>
                            <div class="text-center">
                                <a href="c-d-cli-signup-2.php" class="btn-lg btn-cli-signup">SIGN UP NOW!</a><br><br>
                            </div>                            
                            <ul class="pricing clearfix">
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Search Companion Profiles</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Companion Ratings</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Browse Companion Gallery</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Companion Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Write Companion Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Companions</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Other Members</li>
                            </ul>
                            <div class="text-center">
                                <a href="c-d-cli-signup-2.php" class="btn-lg btn-cli-signup">SIGN UP NOW!</a>
                            </div>
                </div><!-- Pricing Box -->
<!--            </div><!-- Column 2 -->
<!--        </div>      
            
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" data-effect="helix">
                        <div class="pricing-box">
                               <span class="hideme"><i class="fas fa-star fa-2x"></i></span>
                                <div class="title"><h3>VIP - Great Deal!</h3></div>
                                <div class="price">
                                    <p class="price-value">$59</p>
                                    <p class="price-month">for 90 days (recurring)</p>
                                </div>
                                <div class="text-center">
                                    <a href="c-d-cli-signup-3.php" class="btn-lg btn-cli-signup">protect me now!</a><br><br>
                                </div>
                            <ul class="pricing clearfix">
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Search Companion Profiles</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Companion Ratings</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Browse Companion Gallery</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Companion Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Write Companion Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Companions</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Other Members</li>
                            </ul>
                            <div class="text-center">
                                <a href="c-d-cli-signup-3.php" class="btn-lg btn-cli-signup">protect me now!</a>
                            </div>
                           
<!--                        </div><!-- Pricing Box -->
<!--                    </div><!-- Column 3 -->
            
<!--                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" data-effect="helix">
                        <div class="pricing-box">
                              <span class="hideme"><i class="fas fa-star fa-2x"></i></span>
                            <div class="title"><h3>VIP - Best Value!</h3></div>
                             <div class="price">
                                <p class="price-value">$169</p>
                                <p class="price-month">Annually</p>
                            </div>
                            <div class="text-center">
                                <a href="c-d-cli-signup-4.php" class="btn-lg btn-cli-signup">get me this deal!</a><br><br>
                            </div>                                                        
                            <ul class="pricing clearfix">
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Search Companion Profiles</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Companion Ratings</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Browse Companion Gallery</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Companion Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Write Companion Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Companions</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Other Members</li>
                            </ul>
                            <div class="text-center">
                                <a href="c-d-cli-signup-4.php" class="btn-lg btn-cli-signup">get me this deal!</a>
                            </div>                            
<!--                        </div><!-- Pricing Box -->
<!--                    </div><!-- Column 4 -->
<!--            </div><!-- end row -->
<!--        </div><!-- end container -->
<!--    </section><!-- End Pricing -->
    <!--/ END PRICING SECTION  -->
    
    <script type="text/javascript">
    $().ready(function(){
        $('[rel="tooltip"]').tooltip();

    });

    function rotateCard(btn){
        var $card = $(btn).closest('.card-container');
        console.log($card);
        if($card.hasClass('hover')){
            $card.removeClass('hover');
        } else {
            $card.addClass('hover');
        }
    }
</script>
