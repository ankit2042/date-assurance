<!--/ CONTACT SECTION --> 

<a name="contact"></a>    
    
<section id="contact" class="gray-wrapper">
    <div class="title text-center">
        <h2>Contact Us</h2>
        <p>Want to learn more? Have a concern with your account? REACH US HERE</p>
    </div><!-- end title -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div style="float:none">
                    <form id="contactform" action="contact.php" name="contactform" method="post">
                        <div>                                    
                                <input type="text" name="name" id="name" class="form-control" placeholder="Name"> 
                        </div>
                        <div>
                                <input type="text" name="email" id="email" class="form-control" placeholder="Email Address"> 
                            </div>
                            <div>
                                <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject">                                    </div>
                            <div class="clearfix"></div>
                            <div>
                                <textarea class="form-control" name="comments" id="comments" rows="6" placeholder="Message"></textarea>                                </div>
                            <div class="text-center">
                                <button type="submit" value="SEND" id="submit" class="btn-lg btn-contact">SUBMIT</button>
                            </div>
                    </form><!-- End Form -->
                </div>
            </div>
        </div>
    </div> <!-- End Container -->
<!-- End Tab Pane -->    
<!-- /end my tab content -->  
<!-- /contact_tab -->              
    </section><!--/ Contact End -->  