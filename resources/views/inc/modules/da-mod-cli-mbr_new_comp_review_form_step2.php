<script type="text/javascript">
    $.ter.TimezoneUtils.setTzOffsetCookieIfAbsent();

    $(window).on("load", (function () {
        
    }));

    $(function() {
        
    });
    //-->
</script>


<script language="javascript">
    function showDraftWarning() {
        bootbox.dialog({
            className: ' modal-alert warning',
            title: "<button type='button' class='sr-only close-x' data-dismiss='modal'>Close</button><i class='icon-warning'></i>",
            message: '<h5 class="modal-alert-subtitle">Warning!</h5>' +
            '<p class="modal-alert-text">It looks like you already started writing a review for a different provider. You would need to finish that review first or delete the draft prior to reviewing another provider.</p><button class="btn btn-primary" data-dismiss="modal">Finish Review</button>',
            onEscape: true,
            backdrop: true
        });
    }
</script>


        <div class="js-draft-autosave-message warning-block" hidden="hidden"></div>
                                                
        <form action="../mbrs/da-cli-mbr_exst_comp_review_submitted.php" method="POST" id="reviewDetailsForm" name="form1" class="js-autosave js-submit-form" novalidate="novalidate">
            <input type="hidden" name="ReviewerStatusID" value="1">
            <input type="hidden" name="ReviewerID" value="<?php echo $_SESSION['memberid'];?>">
            <input type="hidden" name="CompanionID" value="<?php echo $person['UserID'];?>">
            <input type="hidden" name="SubmittedOn" value="<?php date_default_timezone_set("America/New_York"); echo date("Y-m-d h:i:sa");?>">
            <input type="hidden" name="ReviewStatus" value="0">                           

            <script language="javascript">
                $(document).ready(function(){
                    $('#EncCountryID').change(function() {
                        var valchange = this.value;
                            $('#EncStateID').load('../includes/loadstates.php?countryId=' + valchange);
                        });
                    });
            </script>                                                        

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <p>here is the information you have submitted:</p>

                    <div>Companion's Email: <b><?php echo $_POST['email']?></b></div>

                    <div>Companion's CountryID: <b><?php echo $_POST['slCountry']?></b></div>

                    <div>Companion's StateID: <b><?php echo $_POST['slState']?></b></div>

                    <div>Companion's City: <b><?php echo $_POST['txtcity']?></b></div>

                    <div>Companion's Ethnicity <b><?php echo $_POST['slEthnicity']?></b></div>

                    <div>Companions' sexual orientation: <b><?php echo $_POST['slSexOr']?></b></div>

                    <div>Is companion a transgender: <b><?php echo $_POST['rdTransgender']?></b></div>

                    <div>Is companion a transsexual: <b><?php echo $_POST['rdTranssexual']?></b></div>

                    <div>Companion's independent: <b><?php echo $_POST['rdIndep']?></b></div>

                    <div>Companion's pornstar: <b><?php echo $_POST['phone']?></b></div>
                    <div>Companion's website: <b><?php echo $_POST['compwebsite']?></b></div>
                    <div>Companion's web profile: <b><?php echo $_POST['compwebprof']?></b></div>
                    <div>Companion's whats app: <b><?php echo $_POST['compwhatsapp']?></b></div>
                    <div>Companion's instagram: <b><?php echo $_POST['compinstagram']?></b></div>

                    <div>Companion's bodytype: <b><?php echo $_POST['slBodyType']?></b></div>
                    <div>Companion's web profile: <b><?php echo $_POST['slHeight']?></b></div>
                    <div>Companion's whats app: <b><?php echo $_POST['slHairColor']?></b></div>
                    <div>Companion's instagram: <b><?php echo $_POST['slHairLength']?></b></div>
                    <div>Does Companion Smoke: <b><?php echo $_POST['slPiercings']?></b></div>

                    <div>Companion's bodytype: <b><?php echo $_POST['slTattoos']?></b></div>
                    <div>Companion's web profile: <b><?php echo $_POST['slPussy']?></b></div>
                    <div>Companion's whats app: <b><?php echo $_POST['slBreastSizeDesc']?></b></div>
                    <div>Companion's instagram: <b><?php echo $_POST['rdBreastImp']?></b></div>
                    <div>Does Companion Smoke: <b><?php echo $_POST['slCompAss']?></b></div>

                    <div>Companion's instagram: <b><?php echo $_POST['slSmoking']?></b></div>
                    <div>Does Companion Smoke: <b><?php echo $_POST['slDrinking']?></b></div>

<?php
$link = mysqli_connect("localhost", "root", "root", "cd_db");
 
// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
 
// Attempt insert query execution

                            $reviewerid = ($_POST["ReviewerID"]);
                            $reviewerstatusid = ($_POST["ReviewerStatusID"]);
                            $companionid = ($_POST["CompanionID"]);
                            $enccountryid = ($_POST["EncCountryID"]);
                            
                            $encstateid = ($_POST["EncStateID"]);
                            $enccity = ($_POST["EncCity"]);
                            $encmonth = ($_POST["EncMonth"]);
                            $encyear = ($_POST["EncYear"]);
                            $enclocationid = ($_POST["EncLocationID"]);
                            
                            $enctypeid = ($_POST["EncTypeID"]);
                            $complooksid = ($_POST["CompLooksID"]);
                            $compsmokeid = ($_POST["CompSmokeID"]);
                            $compattitudeid = ($_POST["CompAttitudeID"]);
                            
                            $encatmosphereid = ($_POST["EncAtmosphereID"]);
                            $compperformance = ($_POST["CompPerformanceID"]);
//                            $complocationrating = ($_POST["CompLocationRating"]);
                            $indulgetrue = ($_POST["IndulgeTrue"]);
                            $compphototrue = ($_POST["CompPhotoTrue"]);
                            
                            $compontime = ($_POST["CompOntimeTrue"]);
                            $reviewheadline = ($_POST["ReviewHeadline"]);
                            $genrevsummary = ($_POST['GenRevSummary']);
                            $sexyrevdetails = ($_POST['SexyRevDetails']);
                            $reviewstatus = ($_POST['ReviewStatus']);
                            $submittedon = ($_POST["SubmittedOn"]);

$sql = "INSERT INTO dt_compreviews (ReviewerID, ReviewerStatusID, CompanionID, EncCountryID, EncStateID, EncCity, EncMonth, EncYear, EncLocationID, EncTypeID, CompLooksID, CompSmokeID, CompAttitudeID, EncAtmosphereID, CompPerformanceID, IndulgeTrue, CompPhotoTrue, CompOntimeTrue, ReviewHeadline, GenRevSummary, SexyRevDetails, ReviewStatus, SubmittedOn)
    VALUES ('$reviewerid', '$reviewerstatusid', '$companionid', '$enccountryid', '$encstateid', '$enccity', '$encmonth', '$encyear', '$enclocationid', '$enctypeid', '$complooksid', '$compsmokeid', '$compattitudeid', '$encatmosphereid', '$compperformance', '$indulgetrue', '$compphototrue', '$compontime', '$reviewheadline', '$genrevsummary', '$sexyrevdetails', '$reviewstatus', '$submittedon')";

if(mysqli_query($link, $sql)){
    echo "Records inserted successfully.";
} else{
    echo "ERROR: was not able to execute $sql. " . mysqli_error($link);
}
 
// Close connection
mysqli_close($link);                     
                      
?>
                    
                    
                    
                    
                <h5 style="color:#813ee8;">details about your encounter</h5>
                <hr style="color:#813ee8;">

                <div class="row">

                    <div class="col-lg-1">&nbsp;</div>

                    <div class="col-lg-10">

<!-- begin ENCOUNTER COUNTRY -->                                                        
                        <div class="form-group row">
                            <label for="enccountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter country: </label>

                            <div class="col-sm-6">
                                <select class="bootstrap-select form-control" name="EncCountryID" id="EncCountryID">
                                <option value="0"><?php echo $select;?></option>

                                <?php
                                    $sel = isset($_POST['EncCountryID'])?$_POST['EncCountryID']:0;
                                    $sqlcountry = 'select CountryID as Id, Country as LName from '.$table_prefix.'countries order by TopSorted desc, LName asc';
                                        dropdownlist($sqlcountry, $sel);
                                ?>

                                </select>
                            </div>
                        </div>
<!-- end ENCOUNTER COUNTRY -->

<!-- begin ENCOUNTER STATE -->
                        <div class="form-group row">
                            <label for="enccountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter state: </label>

                            <div class="col-sm-6">
                                <select class="bootstrap-select form-control" name="EncStateID" id="EncStateID">

                                <?php
                                    if(isset($_POST['EncCountryID']) && $_POST['EncCountryID']>0){
                                        $sql = 'select StateID as Id, State as LName from '.$table_prefix.'states where CountryID = '.intval($_POST['EncCountryID']).' order by TopSorted desc, LName asc';
                                            dropdownlist($sql, $_POST['EncStateID']);
                                            }
                                        else{
                                        echo '<option value="0">'.$select.'</option>';
                                    }
                                ?>

                                </select>
                            </div>
                        </div>                                                                
<!-- end ENCOUNTER STATE -->

<!-- begin ENCOUNTER CITY -->
                        <div class="form-group row">
                            <label for="enccity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter city: </label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control js-websitegroup-validation" name="EncCity" value="" maxlength="100" required>
                            </div>
                        </div>
<!-- end ENCOUNTER CITY -->

<!-- begin ENCOUNTER DATE -->
                        <div class="form-group row">
                            <label for="encdate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">approximately when was your encounter: </label>                                                    

                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker2').datetimepicker({
                                        format: 'MM/YYYY'
                                    });
                                });
                            </script>

                            <div class="input-group date col-sm-5" id="datetimepicker2" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2" name="EncDate"/>
                                <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                </div>
                            </div>
                        </div>
<!-- end ENCOUNTER DATE -->

<!-- begin ENCOUNTER DURATION -->
                        <div class="form-group row">
                            <label for="encloc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter duration: </label>
                            <div class="col-sm-4">
                                <select class="bootstrap-select form-control" id="EncDuration" name="slEncDuration" title="" tabindex="-98">
                                    <option value="" label="- select one -"></option>
                                    <option value="30">30 mins</option>
                                    <option value="60">60 mins</option>
                                    <option value="90">90 mins</option>
                                    <option value="2">2 hrs</option>
                                    <option value="3">3 hrs</option>
                                    <option value="4">4 hrs</option>
                                    <option value="overnight">overnight</option>
                                    <option value="other">other</option>
                                </select>
                            </div>
                        </div>
<!-- end ENCOUNTER DURATION -->                            

<!-- begin ENCOUNTER DONATION -->
                        <div class="form-group row">
                            <label for="encDonation" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">donation: </label>
                            <div class="col-sm-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                    <input type="text" class="form-control js-websitegroup-validation" name="txtEncDonation" value="" maxlength="100" required>
                                </div>
                            </div>
                        </div>
<!-- end ENCOUNTER DONATION -->

<!-- begin ENCOUNTER LOCATION -->
                        <div class="form-group row">
                            <label for="encloc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter location: </label>
                            <div class="col-sm-5">
                                <select class="bootstrap-select form-control" id="EnLocationID" name="slEncLocationID" title="" tabindex="-98">
                                    <option value="" label="- select one -"></option>
                                    <option value="1">her place</option>
                                    <option value="2">her hotel</option>
                                    <option value="3">my place</option>
                                    <option value="4">my hotel</option>
                                    <option value="5">other</option>
                                </select>
                            </div>
                        </div>    
<!-- end ENCOUNTER LOCATION -->

                    </div>

                <div class="col-lg-1">&nbsp;</div>

            </div>

            <h5 style="color:#813ee8;">companion's indulgences offered or that you enjoyed</h5>
            <hr style="color:#813ee8;">

            <div class="row">
                <div class="col-lg-1">&nbsp;</div>

                <div class="col-lg-10">

                    <fieldset class="form-group">

<!-- begin ENCOUNTER INDULGENCES - SEX -->                                
                        <div class="row">
                            <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">sex: </legend>

                            <?php
                                if(isset($_POST['rdCompIndulSex'])){
                                    if($_POST['rdCompIndulSex']==1){
                                        $compindulsex1 = ' checked="checked"';
                                        $compindulsex2 = '';
                                        $compindulsex3 = '';
                                        }
                                        elseif($_POST['rdCompIndulSex']==2){
                                            $compindulsex1 = '';
                                            $compindulsex2 = ' checked="checked"';
                                            $compindulsex3 = '';
                                            }
                                            elseif($_POST['rdCompIndulSex']==3){
                                                $compindulsex1 = '';
                                                $compindulsex2 = '';
                                                $compindulsex3 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindulsex1 = ' checked="checked"';
                                                $compindulsex2 = '';
                                                $compindulsex3 = '';
                                                }
                            ?>

                            <div class="col-sm-8">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" <?php echo $compindulsex1;?> name="rdCompIndulSex" value="1">
                                    <label class="form-check-label" for="rdCompIndulSex" style="font-weight:300;font-size:14px;">yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" <?php echo $compindulsex2;?> name="rdCompIndulSex" value="2">
                                    <label class="form-check-label" for="rdCompIndulSex" style="font-weight:300;font-size:14px;">no</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" <?php echo $compindulsex3;?> name="rdCompIndulSex" value="3">
                                    <label class="form-check-label" for="rdCompIndulSex" style="font-weight:300;font-size:14px;">don't know</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">&nbsp;</div>
                            <div class="col-sm-8">
                                <small id="compsexHelpBlock" class="form-text text-muted">
                                if you selected &quot;yes&quot; above, please check all that apply below.
                                </small>
                            </div>
                        </div>

                    </fieldset> 
<!-- end ENCOUNTER INDULGENCES - SEX -->

<!-- begin ENCOUNTER INDULGENCES - SEXTYPE -->
                    <div class="form-group row">
                        <legend class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</legend>
                        <div class="col-sm-4">
                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" name="chCompIndulSexPref[]" value="cfs">
                                <label class="form-check-label" for="chCompIndulSexPref">
                                    CFS = covered full service sex (sex with condom)
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" name="chCompIndulSexPref[]" value="gfe">
                                <label class="form-check-label" for="chCompIndulSexPref">
                                    GFE = girlfriend experience
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                        <div class="col-sm-4">
                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" name="chCompIndulSexPref[]" value="pse">
                                <label class="form-check-label" for="chCompIndulSexPref">
                                    PSE = porn star experience
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" name="chCompIndulSexPref[]" value="nr">
                                <label class="form-check-label" for="chCompIndulSexPref">
                                    NR = no rush
                                </label>
                            </div>
                        </div>
                    </div>
<!-- end ENCOUNTER INDULGENCES - SEXTYPE -->

                    <hr>

<!-- begin ENCOUNTER INDULGENCE BJ -->                            
                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">bj: </legend>

                            <?php
                                if(isset($_POST['rdCompIndulBJ'])){
                                    if($_POST['rdCompIndulBJ']==1){
                                        $compindulbj1 = ' checked="checked"';
                                        $compindulbj2 = '';
                                        $compindulbj3 = '';
                                        }
                                        elseif($_POST['rdCompIndulBJ']==2){
                                            $compindulbj1 = '';
                                            $compindulbj2 = ' checked="checked"';
                                            $compindulbj3 = '';
                                            }
                                            elseif($_POST['rdCompIndulBJ']==3){
                                                $compindulbj1 = '';
                                                $compindulbj2 = '';
                                                $compindulbj3 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindulbj1 = ' checked="checked"';
                                                $compindulbj2 = '';
                                                $compindulbj3 = '';
                                                }
                            ?>

                            <div class="col-sm-8">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" <?php echo $compindulbj1;?> name="rdCompIndulBJ" value="1">
                                    <label class="form-check-label" for="rdCompIndulBJ" style="font-weight:400;font-size:14px;">yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" <?php echo $compindulbj2;?> name="rdCompIndulBJ" value="2">
                                    <label class="form-check-label" for="rdCompIndulBJ" style="font-weight:400;font-size:14px;">no</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" <?php echo $compindulbj3;?> name="rdCompIndulBJ" value="3">
                                    <label class="form-check-label" for="rdCompIndulBJ" style="font-weight:400;font-size:14px;">don't know</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">&nbsp;</div>
                            <div class="col-sm-8">
                                <small id="bjHelpBlock" class="form-text text-muted">
                                if you selected &quot;yes&quot; above, please check all that apply below.
                                </small>
                            </div>
                        </div>                                        
<!-- end ENCOUNTER INDULGENCE BJ -->

<!-- begin ENCOUNTER INDULGENCE BJ PREFS -->  
                    </fieldset>

                        <div class="form-group row">
                            <div class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbj">
                                    <label class="form-check-label" for="chCompIndulBJPref">
                                        BBBJ = bare back blow job = bj without condom
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbjtc">
                                    <label class="form-check-label" for="chCompIndulBJPref">
                                        BBBJTC = bare back blow job to completion
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbjtcim">
                                    <label class="form-check-label" for="chCompIndulBJPref">
                                        BBBJTCIM = bare back blow job to completion, cum in mouth
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbjtcnqns">
                                    <label class="form-check-label" for="chCompIndulBJPref">
                                        BBBJTCNQNS = bare back blow job to completion, no quit, no spit
                                    </label>
                                </div>
                            </div>
                        </div>                                    

                        <div class="form-group row">
                            <div class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbjtcws">
                                    <label class="form-check-label" for="chCompIndulBJPref">
                                        BBBJTCWS = bare back blow job to completion with swallow
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbjwf">
                                    <label class="form-check-label" for="chCompIndulBJPref">
                                        BBBJWF = bare back blow job with facial
                                    </label>
                                </div>
                            </div>
                        </div> 

                        <div class="form-group row">
                            <div class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="cbj">
                                    <label class="form-check-label" for="chCompIndulBJPref">
                                        CBJ = covered blow job = BJ with condom
                                    </label>
                                </div>
                            </div>
                        </div> 
<!-- end ENCOUNTER INDULGENCE BJ PREFS -->

                        <hr>

<!-- begin ENCOUNTER INDULGENCE HJ PREFS -->                            
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">hj: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulHJ'])){
                                        if($_POST['rdCompIndulHJ']==1){
                                            $compindulhj1 = ' checked="checked"';
                                            $compindulhj2 = '';
                                            $compindulhj3 = '';
                                            }
                                        elseif($_POST['rdCompIndulHJ']==2){
                                            $compindulhj1 = '';
                                            $compindulhj2 = ' checked="checked"';
                                            $compindulhj3 = '';
                                            }
                                            elseif($_POST['rdCompIndulHJ']==3){
                                                $compindulhj1 = '';
                                                $compindulhj2 = '';
                                                $compindulhj3 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindulhj1 = ' checked="checked"';
                                                $compindulhj2 = '';
                                                $compindulhj3 = '';
                                                }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulhj1;?> name="rdCompIndulHJ" value="1">
                                        <label class="form-check-label" for="rdCompIndulHJ" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulhj2;?> name="rdCompIndulHJ" value="2">
                                        <label class="form-check-label" for="rdCompIndulHJ" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulhj3;?> name="rdCompIndulHJ" value="3">
                                        <label class="form-check-label" for="rdCompIndulHJ" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                </div>
                            </div>

                        </fieldset>    
<!-- begin ENCOUNTER INDULGENCE HJ PREFS --> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE KISSING PREFS --> 
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">kissing: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulKiss'])){
                                        if($_POST['rdCompIndulKiss']==1){
                                            $compindulkiss1 = ' checked="checked"';
                                            $compindulkiss2 = '';
                                            $compindulkiss3 = '';
                                            }
                                        elseif($_POST['rdCompIndulKiss']==2){
                                            $compindulkiss1 = '';
                                            $compindulkiss2 = ' checked="checked"';
                                            $compindulkiss3 = '';
                                            }
                                            elseif($_POST['rdCompIndulKiss']==3){
                                            $compindulkiss1 = '';
                                            $compindulkiss2 = '';
                                            $compindulkiss3 = ' checked="checked"';
                                                }
                                            }
                                                else{
                                                    $compindulkiss1 = ' checked="checked"';
                                                    $compindulkiss2 = '';
                                                    $compindulkiss3 = '';
                                                    }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulkiss1;?> name="rdCompIndulKiss" value="1">
                                        <label class="form-check-label" for="rdCompIndulKiss" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulkiss2;?> name="rdCompIndulKiss" value="2">
                                        <label class="form-check-label" for="rdCompIndulKiss" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulkiss3;?> name="rdCompIndulKiss" value="3">
                                        <label class="form-check-label" for="rdCompIndulKiss" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">&nbsp;</div>
                                <div class="col-sm-8">
                                    <small id="kissHelpBlock" class="form-text text-muted">
                                    if you selected &quot;yes&quot; above, please check all that apply below.
                                    </small>
                                </div>
                            </div>                                        

                        </fieldset>

                        <div class="form-group row">
                            <div class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulKissPref[]" value="dfk">
                                    <label class="form-check-label" for="chCompIndulKissPref">
                                        DFK = deep french kissing, open mouth with tongue
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulKissPref[]" value="fk">
                                    <label class="form-check-label" for="chCompIndulKissPref">
                                        FK = french kiss = kissing with tongue insertion
                                    </label>
                                </div>
                            </div>
                        </div>                                                                        

                        <div class="form-group row">
                            <div class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulKissPref[]" value="lk">
                                    <label class="form-check-label" for="chCompIndulKissPref">
                                        LK = light kissing, closed mouth
                                    </label>
                                </div>
                            </div>
                        </div>
<!-- end ENCOUNTER INDULGENCE KISSING PREFS -->

                        <hr>

<!-- begin ENCOUNTER INDULGENCE LICK PUSSY PREFS -->                            
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">lick pussy: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulLickpussy'])){
                                        if($_POST['rdCompIndulLickpussy']==1){
                                            $compindullickpussy1 = ' checked="checked"';
                                            $compindullickpussy2 = '';
                                            $compindullickpussy3 = '';
                                            }
                                        elseif($_POST['rdCompIndulLickpussy']==2){
                                            $compindullickpussy1 = '';
                                            $compindullickpussy2 = ' checked="checked"';
                                            $compindullickpussy3 = '';
                                            }
                                            elseif($_POST['rdCompIndulLickpussy']==3){
                                            $compindullickpussy1 = '';
                                            $compindullickpussy2 = '';
                                            $compindullickpussy3 = ' checked="checked"';
                                                }
                                            }
                                                else{
                                                    $compindullickpussy1 = ' checked="checked"';
                                                    $compindullickpussy2 = '';
                                                    $compindullickpussy3 = '';
                                                    }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindullickpussy1;?> name="rdCompIndulLickpussy" value="1">
                                        <label class="form-check-label" for="rdCompIndulLickpussy" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindullickpussy2;?> name="rdCompIndulLickpussy" value="2">
                                        <label class="form-check-label" for="rdCompIndulLickpussy" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindullickpussy3;?> name="rdCompIndulLickpussy" value="3">
                                        <label class="form-check-label" for="rdCompIndulLickpussy" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>  
<!-- end ENCOUNTER INDULGENCE LICK PUSSY PREFS -->   

                        <hr>

<!-- begin ENCOUNTER INDULGENCE TOUCH PUSSY -->                               
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">touch pussy: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulTouchpussy'])){
                                        if($_POST['rdCompIndulTouchpussy']==1){
                                            $compindultouchpussy1 = ' checked="checked"';
                                            $compindultouchpussy2 = '';
                                            $compindultouchpussy3 = '';
                                            }
                                        elseif($_POST['rdCompIndulTouchpussy']==2){
                                            $compindultouchpussy1 = '';
                                            $compindultouchpussy2 = ' checked="checked"';
                                            $compindultouchpussy3 = '';
                                            }
                                            elseif($_POST['rdCompIndulTouchpussy']==3){
                                            $compindultouchpussy1 = '';
                                            $compindultouchpussy2 = '';
                                            $compindultouchpussy3 = ' checked="checked"';
                                                    }
                                                }
                                                else{
                                                    $compindultouchpussy1 = ' checked="checked"';
                                                    $compindultouchpussy2 = '';
                                                    $compindultouchpussy3 = '';
                                                    }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindultouchpussy1;?> name="rdCompIndulTouchpussy" value="1">
                                        <label class="form-check-label" for="rdCompIndulTouchpussy" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindultouchpussy2;?> name="rdCompIndulTouchpussy" value="2">
                                        <label class="form-check-label" for="rdCompIndulTouchpussy" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindultouchpussy3;?> name="rdCompIndulTouchpussy" value="3">
                                        <label class="form-check-label" for="rdCompIndulTouchpussy" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">&nbsp;</div>
                                <div class="col-sm-8">
                                    <small id="touchpussyHelpBlock" class="form-text text-muted">
                                    if you selected &quot;yes&quot; above, please check all that apply below.
                                    </small>
                                </div>
                            </div>                                        

                        </fieldset>
<!-- end ENCOUNTER INDULGENCE TOUCH PUSSY -->

<!-- begin ENCOUNTER INDULGENCE TOUCH PUSSY PREFS -->                                   
                        <div class="form-group row">
                            <div class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulTouchpussyPref[]" value="fiv">
                                    <label class="form-check-label" for="chCompIndulTouchpussyPref">
                                        FIV = finger in vagina
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulTouchpussyPref[]" value="fov">
                                    <label class="form-check-label" for="chCompIndulTouchpussyPref">
                                        FOV = finger outside vagina
                                    </label>
                                </div>
                            </div>
                        </div>                                                                        
<!-- end ENCOUNTER INDULGENCE TOUCH PUSSY PREFS --> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE RIMMING -->                                  
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-5 pt-0" style="font-weight:400;font-size:14px;">rimming: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulRim'])){
                                        if($_POST['rdCompIndulRim']==1){
                                            $compindulrim1 = ' checked="checked"';
                                            $compindulrim2 = '';
                                            $compindulrim3 = '';
                                            }
                                        elseif($_POST['rdCompIndulRim']==2){
                                            $compindulrim1 = '';
                                            $compindulrim2 = ' checked="checked"';
                                            $compindulrim3 = '';
                                            }
                                            elseif($_POST['rdCompIndulRim']==3){
                                            $compindulrim1 = '';
                                            $compindulrim2 = ' checked="checked"';
                                            $compindulrim3 = '';
                                                }
                                            }
                                                else{
                                                    $compindulrim1 = ' checked="checked"';
                                                    $compindulrim2 = '';
                                                    $compindulrim3 = '';
                                                    }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulrim1;?> name="rdCompIndulRim" value="1">
                                        <label class="form-check-label" for="rdCompIndulRim" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulrim2;?> name="rdCompIndulRim" value="2">
                                        <label class="form-check-label" for="rdCompIndulRim" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulrim3;?> name="rdCompIndulRim" value="2">
                                        <label class="form-check-label" for="rdCompIndulRim" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">&nbsp;</div>
                                <div class="col-sm-8">
                                    <small id="rimHelpBlock" class="form-text text-muted">
                                    if you selected &quot;yes&quot; above, please check all that apply below.
                                    </small>
                                </div>
                            </div>                                        

                        </fieldset>
<!-- end ENCOUNTER INDULGENCE RIMMING -->   

<!-- begin ENCOUNTER INDULGENCE RIMMING PREFS -->
                        <div class="form-group row">
                            <div class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulRimPref[]" value="rimr">
                                    <label class="form-check-label" for="chCompIndulRimPref">
                                        RIMR = receive analingus
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulRimPref[]" value="rimg">
                                    <label class="form-check-label" for="chCompIndulRimPref">
                                        RIMG = give analingus
                                    </label>
                                </div>
                            </div>
                        </div>      
<!-- end ENCOUNTER INDULGENCE RIMMING PREFS --> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE ANAL --> 
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">anal: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulAnal'])){
                                        if($_POST['rdCompIndulAnal']==1){
                                            $compindulanal1 = ' checked="checked"';
                                            $compindulanal2 = '';
                                            $compindulanal3 = '';
                                            }
                                        elseif($_POST['rdCompIndulAnal']==2){
                                            $compindulanal1 = '';
                                            $compindulanal2 = ' checked="checked"';
                                            $compindulanal3 = '';
                                            }
                                            elseif($_POST['rdCompIndulAnal']==2){
                                            $compindulanal1 = '';
                                            $compindulanal2 = '';
                                            $compindulanal3 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindulanal1 = ' checked="checked"';
                                                $compindulanal2 = '';
                                                $compindulanal3 = '';
                                                }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulanal1;?> name="rdCompIndulAnal" value="1">
                                        <label class="form-check-label" for="rdCompIndulAnal" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulanal2;?> name="rdCompIndulAnal" value="2">
                                        <label class="form-check-label" for="rdCompIndulAnal" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulanal3;?> name="rdCompIndulAnal" value="3">
                                        <label class="form-check-label" for="rdCompIndulAnal" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">&nbsp;</div>
                                <div class="col-sm-8">
                                    <small id="analHelpBlock" class="form-text text-muted">
                                    if you selected &quot;yes&quot; above, please check all that apply below.
                                    </small>
                                </div>
                            </div>                                        

                        </fieldset>
<!-- end ENCOUNTER INDULGENCE ANAL --> 

<!-- begin ENCOUNTER INDULGENCE ANAL PREFS -->                                 
                        <div class="form-group row">
                            <div class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulAnalPref[]" value="ddp">
                                    <label class="form-check-label" for="chCompIndulAnalPref">
                                        DDP = double digit penetration (pussy and ass)
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulAnalPref[]" value="dp">
                                    <label class="form-check-label" for="chCompIndulAnalPref">
                                        DP = double penetration, two guys on one girl
                                    </label>
                                </div>
                            </div>
                        </div>                                              

                        <div class="form-group row">
                            <div class="col-form-label col-sm-4 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                            <div class="col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="chCompIndulAnalPref[]" value="grk">
                                    <label class="form-check-label" for="chCompIndulAnalPref">
                                        GRK = greek (anal sex, back door)
                                    </label>
                                </div>
                            </div>
                        </div> 
<!-- end ENCOUNTER INDULGENCE ANAL PREFS--> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE MSOG -->                                 
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">multiple pops: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulMsog'])){
                                        if($_POST['rdCompIndulMsog']==1){
                                            $compindulmsog1 = ' checked="checked"';
                                            $compindulmsog2 = '';
                                            $compindulmsog3 = '';
                                            }
                                        elseif($_POST['rdCompIndulMsog']==2){
                                            $compindulmsog1 = '';
                                            $compindulmsog2 = ' checked="checked"';
                                            $compindulmsog3 = '';
                                            }
                                            elseif($_POST['rdCompIndulMsog']==3){
                                            $compindulmsog1 = '';
                                            $compindulmsog2 = '';
                                            $compindulmsog3 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindulmsog1 = ' checked="checked"';
                                                $compindulmsog2 = '';
                                                $compindulmsog3 = '';
                                                }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulmsog1;?> name="rdCompIndulMsog" value="1">
                                        <label class="form-check-label" for="rdCompIndulMsog" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulmsog2;?> name="rdCompIndulMsog" value="2">
                                        <label class="form-check-label" for="rdCompIndulMsog" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulmsog3;?> name="rdCompIndulMsog" value="3">
                                        <label class="form-check-label" for="rdCompIndulMsog" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
<!-- end ENCOUNTER INDULGENCE MSOG --> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE TWOGIRL --> 
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">two girl action: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulTwogirl'])){
                                        if($_POST['rdCompIndulTwogirl']==1){
                                            $compindultwogirl1 = ' checked="checked"';
                                            $compindultwogirl2 = '';
                                            $compindultwogirl2 = '';
                                            }
                                        elseif($_POST['rdCompIndulTwogirl']==2){
                                            $compindultwogirl1 = '';
                                            $compindultwogirl2 = ' checked="checked"';
                                            $compindultwogirl3 = '';
                                            }
                                            elseif($_POST['rdCompIndulTwogirl']==3){
                                            $compindultwogirl1 = '';
                                            $compindultwogirl2 = '';
                                            $compindultwogirl3 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindultwogirl1 = ' checked="checked"';
                                                $compindultwogirl2 = '';
                                                $compindultwogirl3 = '';
                                                }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindultwogirl1;?> name="rdCompIndulTwogirl" value="1">
                                        <label class="form-check-label" for="rdCompIndulTwogirl" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindultwogirl2;?> name="rdCompIndulTwogirl" value="2">
                                        <label class="form-check-label" for="rdCompIndulTwogirl" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindultwogirl3;?> name="rdCompIndulTwogirl" value="3">
                                        <label class="form-check-label" for="rdCompIndulTwogirl" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
<!-- end ENCOUNTER INDULGENCE TWOGIRL --> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE SECOND COMP --> 
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">bring second companion: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulSecondcomp'])){
                                        if($_POST['rdCompIndulSecondcomp']==1){
                                            $compindulsecondcomp1 = ' checked="checked"';
                                            $compindulsecondcomp2 = '';
                                            $compindulsecondcomp3 = '';
                                            }
                                        elseif($_POST['rdCompIndulSecondcomp']==2){
                                            $compindulsecondcomp1 = '';
                                            $compindulsecondcomp2 = ' checked="checked"';
                                            $compindulsecondcomp3 = '';
                                            }
                                            elseif($_POST['rdCompIndulSecondcomp']==3){
                                            $compindulsecondcomp1 = '';
                                            $compindulsecondcomp2 = '';
                                            $compindulsecondcomp3 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindulsecondcomp1 = ' checked="checked"';
                                                $compindulsecondcomp2 = '';
                                                $compindulsecondcomp3 = '';
                                                }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulsecondcomp1;?> name="rdCompIndulSecondcomp" value="1">
                                        <label class="form-check-label" for="rdCompIndulSecondcomp" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulsecondcomp2;?> name="rdCompIndulSecondcomp" value="2">
                                        <label class="form-check-label" for="rdCompIndulSecondcomp" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulsecondcomp3;?> name="rdCompIndulSecondcomp" value="3">
                                        <label class="form-check-label" for="rdCompIndulSecondcomp" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
<!-- end ENCOUNTER INDULGENCE SECOND COMP --> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE TWOGUY -->                                 
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">two guys at one time: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulTwoguy'])){
                                        if($_POST['rdCompIndulTwoguy']==1){
                                            $compindultwoguy1 = ' checked="checked"';
                                            $compindultwoguy2 = '';
                                            $compindultwoguy3 = '';
                                            }
                                        elseif($_POST['rdCompIndulTwoguy']==2){
                                            $compindultwoguy1 = '';
                                            $compindultwoguy2 = ' checked="checked"';
                                            $compindultwoguy3 = '';
                                            }
                                        }
                                        elseif($_POST['rdCompIndulTwoguy']==3){
                                            $compindultwoguy1 = '';
                                            $compindultwoguy2 = '';
                                            $compindultwoguy3 = ' checked="checked"';
                                            }

                                        else{
                                            $compindultwoguy1 = ' checked="checked"';
                                            $compindultwoguy2 = '';
                                            $compindultwoguy3 = '';
                                            }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindultwoguy1;?> name="rdCompIndulTwoguy" value="1">
                                        <label class="form-check-label" for="rdCompIndulTwoguy" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindultwoguy2;?> name="rdCompIndulTwoguy" value="2">
                                        <label class="form-check-label" for="rdCompIndulTwoguy" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindultwoguy2;?> name="rdCompIndulTwoguy" value="3">
                                        <label class="form-check-label" for="rdCompIndulTwoguy" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
<!-- end ENCOUNTER INDULGENCE TWOGUY --> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE VIDEO --> 
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">video with permission: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulVideo'])){
                                        if($_POST['rdCompIndulVideo']==1){
                                            $compindulvideo1 = ' checked="checked"';
                                            $compindulvideo2 = '';
                                            $compindulvideo2 = '';
                                            }
                                        elseif($_POST['rdCompIndulVideo']==2){
                                            $compindulvideo1 = '';
                                            $compindulvideo2 = ' checked="checked"';
                                            $compindulvideo2 = '';
                                            }
                                            elseif($_POST['rdCompIndulVideo']==3){
                                            $compindulvideo1 = '';
                                            $compindulvideo2 = '';
                                            $compindulvideo2 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindulvideo1 = ' checked="checked"';
                                                $compindulvideo2 = '';
                                                $compindulvideo2 = '';
                                                }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulvideo1;?> name="rdCompIndulVideo" value="1">
                                        <label class="form-check-label" for="rdCompIndulVideo" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulvideo2;?> name="rdCompIndulVideo" value="2">
                                        <label class="form-check-label" for="rdCompIndulVideo" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulvideo3;?> name="rdCompIndulVideo" value="3">
                                        <label class="form-check-label" for="rdCompIndulVideo" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
<!-- end ENCOUNTER INDULGENCE VIDEO --> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE PHOTOS --> 
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">photos with permission: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulPhotos'])){
                                        if($_POST['rdCompIndulPhotos']==1){
                                            $compindulphotos1 = ' checked="checked"';
                                            $compindulphotos2 = '';
                                            $compindulphotos3 = '';
                                            }
                                        elseif($_POST['rdCompIndulPhotos']==2){
                                            $compindulphotos1 = '';
                                            $compindulphotos2 = ' checked="checked"';
                                            $compindulphotos3 = '';
                                            }
                                            elseif($_POST['rdCompIndulPhotos']==3){
                                            $compindulphotos1 = '';
                                            $compindulphotos2 = '';
                                            $compindulphotos3 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindulphotos1 = ' checked="checked"';
                                                $compindulphotos2 = '';
                                                $compindulphotos3 = '';
                                                }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulphotos1;?> name="rdCompIndulPhotos" value="1">
                                        <label class="form-check-label" for="rdCompIndulPhotos" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulphotos2;?> name="rdCompIndulPhotos" value="2">
                                        <label class="form-check-label" for="rdCompIndulPhotos" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulphotos3;?> name="rdCompIndulPhotos" value="3">
                                        <label class="form-check-label" for="rdCompIndulPhotos" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
<!-- end ENCOUNTER INDULGENCE PHOTOS --> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE MASSAGE -->                                 
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">massage: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulMassage'])){
                                        if($_POST['rdCompIndulMassage']==1){
                                            $compindulmassage1 = ' checked="checked"';
                                            $compindulmassage2 = '';
                                            $compindulmassage3 = '';
                                            }
                                        elseif($_POST['rdCompIndulMassage']==2){
                                            $compindulmassage1 = '';
                                            $compindulmassage2 = ' checked="checked"';
                                            $compindulmassage3 = '';
                                            }
                                            elseif($_POST['rdCompIndulMassage']==3){
                                            $compindulmassage1 = '';
                                            $compindulmassage2 = '';
                                            $compindulmassage3 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindulmassage1 = ' checked="checked"';
                                                $compindulmassage2 = '';
                                                $compindulmassage3 = '';
                                                }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulmassage1;?> name="rdCompIndulMassage" value="1">
                                        <label class="form-check-label" for="rdCompIndulMassage" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulmassage2;?> name="rdCompIndulMassage" value="2">
                                        <label class="form-check-label" for="rdCompIndulMassage" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                     <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulmassage3;?> name="rdCompIndulMassage" value="3">
                                        <label class="form-check-label" for="rdCompIndulMassage" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
<!-- end ENCOUNTER INDULGENCE MASSAGE --> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE SMBD --> 
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">s &amp; m/b &amp; d: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulSMBD'])){
                                        if($_POST['rdCompIndulSMBD']==1){
                                            $compindulsmbd1 = ' checked="checked"';
                                            $compindulsmbd2 = '';
                                            $compindulsmbd3 = '';
                                            }
                                        elseif($_POST['rdCompIndulSMBD']==2){
                                            $compindulsmbd1 = '';
                                            $compindulsmbd2 = ' checked="checked"';
                                            $compindulsmbd3 = '';
                                            }
                                            elseif($_POST['rdCompIndulSMBD']==3){
                                            $compindulsmbd1 = '';
                                            $compindulsmbd2 = '';
                                            $compindulsmbd3 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindulsmbd1 = ' checked="checked"';
                                                $compindulsmbd2 = '';
                                                $compindulsmbd3 = '';
                                                }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulsmbd1;?> name="rdCompIndulSMBD" value="1">
                                        <label class="form-check-label" for="rdCompIndulSMBD" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulsmbd2;?> name="rdCompIndulSMBD" value="2">
                                        <label class="form-check-label" for="rdCompIndulSMBD" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulsmbd3;?> name="rdCompIndulSMBD" value="3">
                                        <label class="form-check-label" for="rdCompIndulSMBD" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
<!-- begin ENCOUNTER INDULGENCE SMBD --> 

                        <hr>

<!-- begin ENCOUNTER INDULGENCE SQUIRT --> 
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">squirt: </legend>

                                <?php
                                    if(isset($_POST['rdCompIndulSquirt'])){
                                        if($_POST['rdCompIndulSquirt']==1){
                                            $compindulsquirt1 = ' checked="checked"';
                                            $compindulsquirt2 = '';
                                            $compindulsquirt3 = '';
                                            }
                                        elseif($_POST['rdCompIndulSquirt']==2){
                                            $compindulsquirt1 = '';
                                            $compindulsquirt2 = ' checked="checked"';
                                            $compindulsquirt3 = '';
                                            }
                                            elseif($_POST['rdCompIndulSquirt']==3){
                                            $compindulsquirt1 = '';
                                            $compindulsquirt2 = '';
                                            $compindulsquirt3 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compindulsquirt1 = ' checked="checked"';
                                                $compindulsquirt2 = '';
                                                $compindulsquirt3 = '';
                                                }
                                ?>

                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulsquirt1;?> name="rdCompIndulSquirt" value="1">
                                        <label class="form-check-label" for="rdCompIndulSquirt" style="font-weight:400;font-size:14px;">yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulsquirt2;?> name="rdCompIndulSquirt" value="2">
                                        <label class="form-check-label" for="rdCompIndulSquirt" style="font-weight:400;font-size:14px;">no</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" <?php echo $compindulsquirt3;?> name="rdCompIndulSquirt" value="3">
                                        <label class="form-check-label" for="rdCompIndulSquirt" style="font-weight:400;font-size:14px;">don't know</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
<!-- end ENCOUNTER INDULGENCE SQUIRT --> 

                    </div>

                    <div class="col-lg-1">&nbsp;</div>

                </div>

<!-- begin ENCOUNTER ATMOSPHERE -->   

                <h5 style="color:#813ee8;">encounter atmosphere</h5>
                <hr>

                <div class="row">
                    <div class="col-lg-1">&nbsp;</div>

                    <div class="col-lg-10">

<!-- begin ENCOUNTER TYPE -->                                   
                        <div class="form-group row">
                            <label for="encdate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">type of encounter: </label>                                                    
                            <div class="col-sm-6">   

                                <select class="bootstrap-select form-control" name="EncTypeID" title="" tabindex="-98">
                                    <option value="" label="<?php echo $select;?>"></option>
                                    <option value="1">gfe - girlfriend exp</option>
                                    <option value="2">pse - pornstar exp</option>
                                    <option value="3">erotic massage - body rub</option> 
                                    <option value="4">blow job</option>
                                    <option value="5">body slide - companion slid their naked body along your body</option>
                                    <option value="6">hand job</option>
                                    <option value="7">anal sex (also known as greek)</option>
                                    <option value="8">tie &amp; tease - companion gently tied your hands and/or feet</option>
                                    <option value="9">fantasy - companion dressed up in a costume and /or role-play</option>
                                    <option value="10">b &amp; d - being whipped, flogged, caned, tied up, told what to do</option>
                                    <option value="11">s &amp; m - a more extreme form of b&amp;d</option>                                   
                                </select>

                            </div>
                        </div>
<!-- end ENCOUNTER TYPE -->                                   

<!-- begin COMPANIONS OVERALL LOOKS -->                                                
                        <div class="form-group row">
                            <label for="encdate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's overall looks: </label>
                            <div class="col-sm-6"> 

                                <select class="bootstrap-select form-control" name="CompLooksID" title="" tabindex="-98">
                                    <option value="" label="<?php echo $select;?>"></option>
                                    <option value="1">1 - i was frightened</option>
                                    <option value="2">2 - needed a blindfold</option>
                                    <option value="3">3 - ugly</option>
                                    <option value="4">4 - ok if i was drunk</option>
                                    <option value="5">5 - average</option>
                                    <option value="6">6 - pretty</option>
                                    <option value="7">7 - very attractive</option>
                                    <option value="8">8 - super hot</option>
                                    <option value="9">9 - could be a model</option>
                                    <option value="10">10 - i was in awe</option>
                                </select>

                            </div>
                        </div>
<!-- end COMPANIONS OVERALL LOOKS -->                                                

<!-- begin COMPANION SMOKES? -->
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">does companion smoke: </label>
                            <div class="col-sm-6"> 

                                <select class="bootstrap-select form-control" name="CompSmokeID" title="" tabindex="-98" required>
                                    <option value="" label="<?php echo $select;?>"></option>
                                    <option value="1">no</option>
                                    <option value="2">yes - but not during session</option>
                                    <option value="3">yes - during session</option>
                                    <option value="4">yes - thats all i could smell</option>
                                </select>

                            </div>
                        </div>
<!-- end COMPANION SMOKES -->

<!-- begin COMPANIONS ATTITUDE -->
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's attitude: </label>
                            <div class="col-sm-6">
                                <select class="bootstrap-select form-control" name="CompAttitudeID" title="" tabindex="-98">
                                    <option value="" label="<?php echo $select;?>"></option>
                                    <option value="1">apathetic - indifferent - lacked energy or concern</option>
                                    <option value="2">bitter - exhibited strong animosity</option>
                                    <option value="3">bored - gave off an air that they would rather be elsewhere</option>
                                    <option value="3">cynical - questioned sincerity and goodness of people</option>
                                    <option value="4">condescending - gave off a feeling of superiority</option>
                                    <option value="5">callous - unfeeling - insensitive</option>
                                    <option value="6">chill - super relaxed</option>
                                    <option value="7">contemplative - thoughtful, reflective</option>
                                    <option value="8">conventional - lacked spontaneity</option>
                                    <option value="9">fanciful - used their imagination</option>
                                    <option value="10">fanciful - used their imagination</option>
                                    <option value="11">gloomy - dark - sad</option>
                                    <option value="12">haughty - arrogant</option>
                                    <option value="13">hot-tempered - easily angered</option>
                                    <option value="14">intimate - very familiar</option>
                                    <option value="15">judgmental - critical</option>
                                    <option value="16">jovial - happy</option>
                                    <option value="17">matter-of-fact - not fanciful or emotional</option>
                                    <option value="18">mocking - treated with contempt</option>
                                    <option value="19">optimistic - hopeful, cheerful</option>
                                    <option value="20">patronizing - had air of condescension</option>
                                    <option value="21">pessimistic - saw the worst side of things</option>
                                    <option value="21">professional - performed like a pro</option>
                                    <option value="22">ridiculing - made fun of</option>
                                    <option value="23">sincere - without deceit or pretense; genuine</option>
                                    <option value="24">whimsical - odd, strange, fantastic; fun</option>
                                </select>
                            </div>
                        </div>
<!-- end COMPANIONS ATTITUDE -->

<!-- begin ENCOUNTER OVERALL ATMOSPHERE -->
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">overall atmosphere: </label>
                                <div class="col-sm-6">
                                    <select class="bootstrap-select form-control" id="EncAtmosphereID" name="EncAtmosphereID" title="" tabindex="-98">
                                        <option value="" label="<?php echo $select;?>"></option>
                                        <option value="1">very scary</option>
                                        <option value="2">a bit scary</option>
                                        <option value="3">extremely awkward</option>
                                        <option value="4">uptight</option>
                                        <option value="5">like a first date</option>
                                        <option value="6">chill</option>
                                        <option value="7">sexy vibe</option>
                                        <option value="8">like a party</option>
                                        <option value="9">like a workout</option>
                                    </select>
                                </div>

                            </div>
<!-- end ENCOUNTER OVERALL ATMOSPHERE -->

<!-- begin COMPANION OVERALL PERFORMANCE -->
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">overall performance: </label>
                                <div class="col-sm-6">
                                    <select class="bootstrap-select form-control" id="CompPerformanceID" name="CompPerformanceID" title="" tabindex="-98">
                                        <option value="" label="<?php echo $select;?>"></option>
                                        <option value="1">1 - a complete rip-off</option>
                                        <option value="2">2 - i wish we hadn't met</option>
                                        <option value="3">3 - not worth the effort</option>
                                        <option value="4">4 - she just laid there</option>
                                        <option value="5">5 - average</option>
                                        <option value="6">6 - nice Time</option>
                                        <option value="7">7 - good time</option>
                                        <option value="8">8 - went the extra mile</option>
                                        <option value="9">9 - we were meant to be together</option>
                                        <option value="10">10 - she blew my mind</option>
                                    </select>
                                </div>

                            </div>
<!-- end COMPANION OVERALL PERFORMANCE -->

                        </div>

                        <div class="col-lg-1">&nbsp;</div>

                    </div>

<!-- begin VERIFICATIONS -->
                    <h5 style="color:#813ee8;">verifications</h5>
                    <hr>

                    <div class="row">

                        <div class="col-lg-1">&nbsp;</div>

                        <div class="col-lg-10">

<!-- begin INDULGENCES TRUE -->
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">indulgences were as described: </legend>

                                    <?php
                                        if(isset($_POST['rdIndulTrue'])){
                                            if($_POST['rdIndulTrue']==1){
                                                $indultrue1 = ' checked="checked"';
                                                $indultrue2 = '';
                                                }
                                            elseif($_POST['rdIndulTrue']==2){
                                                $indultrue1 = '';
                                                $indultrue2 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $indultrue1 = '';
                                                $indultrue2 = ' checked="checked"';
                                                }
                                    ?>

                                    <div class="col-sm-8">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $indultrue1;?> name="rdIndulTrue" value="1">
                                            <label class="form-check-label" for="rdIndulTrue" style="font-weight:400;font-size:14px;">yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $indultrue2;?> name="rdIndulTrue" value="2">
                                            <label class="form-check-label" for="rdIndulTrue" style="font-weight:400;font-size:14px;">no</label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>    
<!-- end INDULGENCES TRUE -->                                                             

<!-- begin PHOTOS TRUE -->
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">companion looked like photos: </legend>

                                    <?php
                                        if(isset($_POST['rdCompPhotoTrue'])){
                                            if($_POST['rdCompPhotoTrue']==1){
                                                $compphototrue1 = ' checked="checked"';
                                                $compphototrue2 = '';
                                                }
                                            elseif($_POST['rdCompPhotoTrue']==2){
                                                $compphototrue1 = '';
                                                $compphototrue2 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compphototrue1 = '';
                                                $compphototrue2 = ' checked="checked"';
                                                }
                                    ?>

                                    <div class="col-sm-8">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $compphototrue1;?> name="rdCompPhotoTrue" value="1">
                                            <label class="form-check-label" for="rdCompPhotoTrue" style="font-weight:400;font-size:14px;">yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $compphototrue2;?> name="rdCompPhotoTrue" value="2">
                                            <label class="form-check-label" for="rdCompPhotoTrue" style="font-weight:400;font-size:14px;">no</label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>    
<!-- end PHOTOS TRUE -->                                                                                                    

<!-- begin COMPANION ONTIME TRUE -->
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">companion was on time: </legend>

                                    <?php
                                        if(isset($_POST['rdCompOntimeTrue'])){
                                            if($_POST['rdCompOntimeTrue']==1){
                                                $compontimetrue1 = ' checked="checked"';
                                                $compontimetrue2 = '';
                                                }
                                            elseif($_POST['rdCompOntimeTrue']==2){
                                                $compontimetrue1 = '';
                                                $compontimetrue2 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $compontimetrue1 = '';
                                                $compontimetrue2 = ' checked="checked"';
                                                }
                                    ?>

                                    <div class="col-sm-8">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $compontimetrue1;?> name="rdCompOntimeTrue" value="1">
                                            <label class="form-check-label" for="rdCompOntimeTrue" style="font-weight:400;font-size:14px;">yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $compontimetrue2;?> name="rdCompOntimeTrue" value="2">
                                            <label class="form-check-label" for="rdCompOntimeTrue" style="font-weight:400;font-size:14px;">no</label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>    
<!-- end COMPANION ONTIME TRUE -->

                        </div> 

                        <div class="col-lg-1">&nbsp;</div>

                    </div>

<!-- begin GENERAL DESCRIPTION OF ENCOUNTER -->                                
                    <h5 style="color:#813ee8;">general description of your encounter</h5>
                    <hr>

                    <div class="row">
                        <div class="col-lg-1">&nbsp;</div>

                        <div class="col-lg-10">

<!-- begin ENCOUNTER HEADLINE -->                                     
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">headline for your review:</label>
                                <div class="col-sm-8">    
                                    <input name="ReviewHeadline" value="" type="text" class="form-control" maxlength="60">
                                        <div class="textarea-feedback" style="color:#ff0000;">max 60 characters</div>
                                </div>
                            </div>
<!-- end ENCOUNTER HEADLINE --> 

                            general summary should include broad comments about your companion and your initial experience. please do not mention specific acts or donation - just tease your fellow members.

                            <br>
                            <br>

<!-- begin GENERAL SUMMARY OF ENCOUNTER -->                                     
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">general summary:</label>
                                <div class="col-sm-8">
                                    <textarea class="bootstrap-select form-control review-form-textarea js-textarea " data-limit="4000" name="GenRevSummary" id="GenRevSummary" rows="2" style="overflow: hidden; word-wrap: break-word;"></textarea>
                                    <div class="textarea-feedback" style="color:#ff0000;">characters: <span class="textarea-feedback-content js-textarea-feedback" id="len1">4000 left</span>
                                    </div>
                                </div>
                            </div>
<!-- end GENERAL SUMMARY OF ENCOUNTER -->

                        </div>

                        <div class="col-lg-1">&nbsp;</div>

                    </div>

<!-- begin DETAILED DESCRIPTION OF ENCOUNTER -->                            
                    <h5 style="color:#813ee8;">detailed description of encounter</h5>
                    <hr>

                    <div class="row">
                        <div class="col-lg-1">&nbsp;</div>

                        <div class="col-lg-10">
                            describe your companion, the experience, and whether or not you enjoyed it in graphic emotional and sexual terms. don't make this a recap of the general summary, instead, go for a blow-by-blow tell-all of your encounter from your own point of view.

<!-- begin SEXY DETAILS OF ENCOUNTER -->                                    
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">sexy details: </label>
                                <div class="col-sm-8">
                                    <textarea class="bootstrap-select form-control review-form-textarea js-textarea " data-limit="6000" name="SexyRevDetails" id="SexyRevDetails" rows="4" style="overflow: hidden; word-wrap: break-word;"></textarea>
                                    <div class="textarea-feedback" style="color:#ff0000;">characters: <span class="textarea-feedback-content js-textarea-feedback" id="len1">6000 left</span></div>
                                </div>
                            </div>
<!-- end SEXY DETAILS OF ENCOUNTER -->

                        </div>

                        <div class="col-lg-1">&nbsp;</div>
                    </div>

                    <div class="row">

                        <div class="col-lg-1">&nbsp;</div>

                        <div class="col-lg-10">

<!-- begin SUBMIT FORM -->                                    
                            <div class="form-group row">
                                <div class="col-sm-4">&nbsp;</div>
                                <div class="col-sm-8">
                                    <button class="btn btn-cli-pro" data-toggle="modal2" data-target="#submitReview">submit</button>
                                    <button type="button" formnovalidate="formnovalidate" class="btn btn-pro-cli js-save-draft-click">save draft</button>
                                </div>
                            </div>
<!-- end SUBMIT FORM -->

                        </div>

                        <div class="col-lg-1">&nbsp;</div>

                    </div>
                            
                </div>
        </form>
                        <div class="modal fade" id="submitReview" tabindex="-1" role="dialog" aria-labelledby="submitReviewLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <h5 class="modal-title" id="submitReviewLabel">Error</h5>
                                    </div>
                                    <div class="modal-body">
                                        <span class="icon-error">!</span>
                                        <p>Your review wasn't added. Please try to do it later.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
