
<?php
    if($person['PrimaryPhotoID']>0 && file_exists('../fuploads/'.$person['UserID'].'u'.$person['PrimaryPhotoID'].'.'.$person['PhotoExtension']))
        $compprimphoto = '../fuploads/'.$person['UserID'].'u'.$person['PrimaryPhotoID'].'.'.$person['PhotoExtension'];
        else $compprimphoto = '../imgs/noimage.jpg';
?>

                        <div class="mini-card">
                            <div class="card">
                                <div class="card-header"><i class="fas fa-venus"></i> <?php echo $person['ProfileName'];?>, <?php echo $person['Age'];?>, <?php echo $person['City'];?></div>
                                <div class="card-body">
                                    
                <!-- begin COMPANION PRIMARY PHOTO -->                    
                                    <div class="comp-mini-card_left">
                                        
                                            <div class="avatar xl tier_1"><!----><!---->
                                                <a href="../mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php echo $person['UserID'];?>" style="background-image: url(<?php echo $compprimphoto;?>);"></a>
                                            </div>
                                            <a href="<?php echo $base_url;?>mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php echo $person['UserID'];?>" class="comp_mini_left_thumb" style="background-image: url(<?php echo $compprimphoto;?>);">
                                            <span class="comp-mini-left_thumb_profname"><?php echo $person['ProfileName'];?></span>
                                            </a>
                                        
                                    </div>
                <!-- end COMPANION PRIMARY PHOTO -->
                
                                    <div class="comp-mini-card_right">
                                        <div class="comp-mini-card_header">
                                            
                <!-- begin COMPANION ID AND LOCATION -->                            
                                            <div class="comp-mini-card_right_header_left">
                                                <span style="color:#595959">
                                                    <span style="font-weight:600;font-size:18px;color:#f300fe;"><?php echo $person['ProfileName'];?></span><br> 
                                                    home: <span style="font-weight:400;"><?php echo $person['City'];?>, <?php echo $person['State']?></span><br>
                                                    age: <span style="font-weight:400;"><?php echo $person['Age'];?></span>&nbsp;&nbsp;ethnicity: <span style="font-weight:400;"><?php echo $person['Ethnicity'];?></span><br>
                                                    da ID: <span style="font-weight:400;"><?php echo $person['UserID'];?></span>
                                                </span>
                                            </div>
                <!-- end COMPANION ID AND LOCATION -->
                
                <!-- begin COMPANION MEMBERS STATS -->
                
                                            <div class="comp-mini-card_header_right">
                                                <i class="fas fa-pencil-alt text-pink" data-original-title="views"></i><a href="<?php echo $base_url;?>mbrs/da-cli-mbr_view_full_profile.php?id=<?php echo $person['UserID'];?>">&nbsp;<?php echo $compnumreviews;?>&nbsp;<span class="hidden-sm hidden-xs" style="font-weight:300">&nbsp;reviews</span></a><br>
                                                <i class="far fa-heart text-pink" data-original-title="favorites"></i><a href="<?php echo $base_url;?>mbrs/da-cli-mbr_view_full_profile.php?id=<?php echo $person['UserID'];?>"> 14 <span class="hidden-sm hidden-xs" style="font-weight:300">&nbsp;fans</span></a><br>
                                                <i class="far fa-star text-pink"></i><i class="far fa-star text-pink"></i><i class="far fa-star text-pink"></i><i class="far fa-star text-pink"></i><i class="far fa-star-half text-pink"></i><br>
                                                <a href="<?php echo $base_url;?>mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php echo $person['UserID'];?>"> <?php echo $person['TotalRating'];?> da rating</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div> 
                            </div>
                        </div>
