<script type="text/javascript">
    $.ter.TimezoneUtils.setTzOffsetCookieIfAbsent();

    $(window).on("load", (function () {
        
    }));

    $(function() {
        
    });
    //-->
</script>


<script language="javascript">
    function showDraftWarning() {
        bootbox.dialog({
            className: ' modal-alert warning',
            title: "<button type='button' class='sr-only close-x' data-dismiss='modal'>Close</button><i class='icon-warning'></i>",
            message: '<h5 class="modal-alert-subtitle">Warning!</h5>' +
            '<p class="modal-alert-text">It looks like you already started writing a review for a different provider. You would need to finish that review first or delete the draft prior to reviewing another provider.</p><button class="btn btn-primary" data-dismiss="modal">Finish Review</button>',
            onEscape: true,
            backdrop: true
        });
    }
</script>

        <script type="text/javascript">
            $(function () {
                var validator;

                $('.selectpicker').on('change', function () {
                    $(this).valid();
                });

                $("#CompPerformanceID").on("change", function(){
                    var selectedValue = $(this).val();
                    if (selectedValue !== '') {
                        var intValue = parseInt(selectedValue);
                        if (!isNaN(intValue) && intValue > 7) {
                            $.ter.DialogManager.showWarning('you have given a score higher than 7.  encounters with scores higher than 7 should include indulgences such as: kisses with tongue, bj without condom, more than one guy, really bi session or anal.');
                        }
                    }
                });

                var triggerAutosave = function () {
                    $(".js-autosave").trigger("ter.forceAutosave");
                };

                $(".js-submit-form").on("submit", function(e) {
                    if ($(".js-submit-form").valid()) {
                        $(".js-autosave").trigger("ter.stopAutosave");
                    }
                });

                $(".js-chemistry-rating").each( function() {
                    var rating = $(this).attr("data-rating"),
                        formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                    $(this).rateYo({
                        
                        fullStar: true,
                            totalStars: 5,
                    starWidth: "16px",
                    ratedFill: "#f300fe",
                    normalFill: "#dbe0e4",
                    precision: 2,
                    onSet: function (rating, rateYoInstance) {
                        formControl.val(rating);
                        if (formControl.val() != 0) {
                            formControl.valid();
                        };
                        triggerAutosave();
                    }
                });
            });
        
            $(".js-incalllocation-rating").each( function() {
                var rating = $(this).attr("data-rating"),
                    formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                $(this).rateYo({
                    
                    fullStar: true,
                        totalStars: 5,
                starWidth: "16px",
                ratedFill: "#f300fe",
                normalFill: "#dbe0e4",
                precision: 2,
                onSet: function (rating, rateYoInstance) {
                    formControl.val(rating);
                    if (formControl.val() != 0) {
                        formControl.valid();
                    };
                    triggerAutosave();
                }
            });
            });
        
            $(function(){
                $(".js-incalllocation-clear").on("click", function(){
                    var chemistry = $(".js-incalllocation-rating");
                    chemistry.rateYo("option", "rating", "0");
                    chemistry.parents(".form-control-wrapper").find($('.form-control')).val("");
                    $("#IncallLocationRating").val('');
                    triggerAutosave();
                });
            });


                $(".js-autosave .selectpicker").on('hidden.bs.select', function (e) {
                    triggerAutosave();
                });

                $(".js-autosave input:checkbox").on('change', function(e) {
                    triggerAutosave();
                });

                //delay for autosave in milliseconds
                var AUTO_SAVE_PERIOD = 2000;

                function timeFormatter(dateTime) {
                    var timeNow = new Date(dateTime);
                    var hours = timeNow.getHours();
                    var minutes = timeNow.getMinutes();
                    var seconds = timeNow.getSeconds();
                    var timeString = "" + ((hours > 12) ? hours - 12 : hours);
                    timeString += ((minutes < 10) ? ":0" : ":") + minutes;
                    timeString += ((seconds < 10) ? ":0" : ":") + seconds;
                    timeString += (hours >= 12) ? " PM" : " AM";

                    return timeString;
                }

                var submitDraftAutosaveCallback = function () {
                    updateAutosaveMessage();
                }

                var submitDraftAutosave = function () {
                    var form = $("#reviewDetailsForm");
                    var url = form.attr('action');
                    $("#isDraft").val("1");
                    $("#isAutosave").val("1");
                    var formData = form.serialize();
                    $("#isDraft").val("");
                    $("#isAutosave").val("");
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        success: function (data, textStatus, jqXHR) {
                            submitDraftAutosaveCallback();
                        },
                        error: function (jqXHR, status, error) {
                            if (status == 401) {
                                location.href = "/memberlaunch/login.asp?dest=/reviews/submitReviewDetails.asp?terId%3D253746";
                            }
                            //console.log(status + ": " + error);
                        }
                    });
                };

                var updateAutosaveMessage = function () {
                    var time = new Date().getTime();
                    $(".js-draft-autosave-message").show();
                    $(".js-draft-autosave-message").text("Review Draft autosaved " + timeFormatter(time));
                };

                $(".js-autosave").autoSave(function () {
                    submitDraftAutosave();
                }, AUTO_SAVE_PERIOD);

                $(".js-save-draft-click").on("click", function (event) {
                    $("#reviewDetailsForm").data('validator').cancelSubmit = true;
                    $("#reviewDetailsForm").validate().settings.ignore = "*";
                    $("#isDraft").val("1");
                    $("#reviewDetailsForm").submit();
                    return false;
                });

                
                validator = $('.js-submit-form').validate({
                    rules: {
                        Website: "required",
                        Location: "required",
                        //session atmosphere
                        Smokes: "required",
                        OverallLooks: "required",
                        Attitude: "required",
                        OverallAtmosphere: "required",
                        OverallPerformance: "required",
                        //visit details
                        GeneralDetails: "required",
                        TheJuicyDetails: "required",
                        ChemistryRating: "required"
                    },
                    messages: {
                        ChemistryRating: "Please select Chemistry star rating."
                    },
                    highlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").addClass("error");
                        $element.next(".btn-group").addClass("error");
                    },
                    unhighlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").removeClass("error");
                        $element.removeClass("error").next(".btn-group").removeClass("error");
                    },
                    ignore:"",
                    //TODO: move out to common js
                    invalidHandler: function(form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            if($(validator.errorList[0].element).is(":visible"))
                            {
                                validator.errorList[0].element.focus();
                            }
                            else
                            {
                                var elementOffset = $("#" + $(validator.errorList[0].element).attr("data-focusID")).offset().top;
                                if (elementOffset > $(window).height()){
                                    header = $(".header").height();
                                    $('html, body').animate({
                                        scrollTop: elementOffset - header - 30
                                    }, 1000);
                                }
                            }
                        }
                    }
                });
            });

        </script>

        <div class="js-draft-autosave-message warning-block" hidden="hidden"></div>

        <?php
            if(isset($error) && !empty($error))
                echo '<p style="margin:0px; padding:5px 20px"><font color="#FF0000"><i>'.$error.'</i></font></p>';
        ?>                                    
                                                
            <form action="../mbrs/da-cli-mbr_new_comp_review_pg1_submitted.php" method="POST" id="reviewDetailsForm" name="form1" class="js-autosave js-submit-form" novalidate="novalidate">


                <script language="javascript">
                    $(document).ready(function(){
                        $('#EncCountryID').change(function() {
                            var valchange = this.value;
                                $('#EncStateID').load('../includes/loadstates.php?countryId=' + valchange);
                            });
                        });
                </script>                                                        
                                                        
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <h5 style="color:#813ee8;">about your companion</h5>
                    <hr style="color:#813ee8;">

                    <div class="row">
                        <div class="col-lg-1">&nbsp;</div>

                        <div class="col-lg-10">
                            <div class="form-group row">
                                <label for="profilename" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">enter your companion's name: </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="profilename" value="<?php echo isset($_POST['profilename'])?$_POST['profilename']:'';?>" required>
                                    <small id="profileNameHelpBlock" class="form-text text-muted">
                                        please enter your companion's name between 6-20 characters. this will be your companion's profile name will be seen by members and other companions.
                                    </small>

    <!-- begin PROFILE NAME ERROR MESSAGE -->
                                <?php
                                    if(isset($err_p) && !empty($err_p))
                                        echo '<br><span style="color:#FF0000;font-size:16px;font-weight:500;font-style:italic;">'.$err_p.'</span>';
                                ?>
    <!-- end PROFILE NAME ERROR MESSAGE -->

                                </div>    
                            </div>
                            
    <!-- begin ENTER EMAIL/MEMBER ID -->                                     
                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">email (member id): </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="email" value="<?php echo isset($_POST['email'])?$_POST['email']:'';?>" required>
                                    <small id="emailHelpBlock" class="form-text text-muted">
                                        please enter a valid email address. your email address will become your member id.
                                    </small>


            <!-- begin EMAIL EMPTY/SHORT ERROR MESSAGE -->
                                <?php
                                    if(isset($err_e) && !empty($err_e))
                                        echo '<br><span style="color:#FF0000;font-size:16px;font-weight:500;font-style:italic;">'.$err_e.'</span>';
                                ?>
            <!-- end EMAIL EMPTY/SHORT ERROR MESSAGE -->

                                </div>
                            </div>
    <!-- end ENTER EMAIL/MEMBER ID -->                            
    
    <!-- begin COMP GENDER -->                        
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">companion's gender: </legend>

                                    <?php
                                        if(isset($_POST['rdGender'])){
                                            if($_POST['rdGender']==1){
                                                $gen1 = ' checked="checked"';
                                                $gen2 = '';
                                                }
                                            elseif($_POST['rdGender']==2){
                                                $gen1 = '';
                                                $gen2 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $gen1 = '';
                                                $gen2 = ' checked="checked"';
                                                }
                                    ?>

                                    <div class="col-sm-8">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $gen1;?> name="rdGender" value="1">
                                            <label class="form-check-label" for="rdGender" style="font-weight:400;font-size:14px;"><?php echo $male;?></label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $gen2;?> name="rdGender" value="2">
                                            <label class="form-check-label" for="rdGender" style="font-weight:400;font-size:14px;"><?php echo $female;?></label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
    <!-- end COMP GENDER -->

    <!-- begin COMP AGE -->
                            <div class="form-group row">
                                <label for="slAge" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's approximate age: </label>
                                <div class="col-sm-2">
                                    <select class="form-control" name="slAge">
                                    <?php
                                        for($i=18; $i<76; $i++){
                                            $val = isset($_POST['slAge'])?$_POST['slAge']:18;
                                            $sel = ($val==$i)?' selected="selected"':'';
                                                echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
                                            }
                                    ?>
                                    </select>
                                </div>
                            </div>
    <!-- end COMP AGE -->
    
    <!-- begin COMP ETHNICITY -->
                            <div class="form-group row">
                                <label for="slEthnicity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's <?php echo $ethnicity;?>: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slEthnicity" name="slEthnicity">
                                    <?php
                                        $sel = isset($_POST['slEthnicity'])?$_POST['slEthnicity']:1;
                                        $sql = 'select EthnicityID as Id, '.$_SESSION['lang'].'Ethnicity as LName from '.$table_prefix.'ethnicity order by TopSorted desc, LName asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div> 
    <!-- end COMP ETHNICITY -->
    
    <!-- begin COMP SEX OR -->
                            <div class="form-group row">
                                <label for="slSexOr" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's sexual orientation: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slSexO" name="slSexOr">
                                    <?php
                                        $sel = isset($_POST['slSexOr'])?$_POST['slSexOr']:1;
                                        $sql = 'select SexOrID as Id, '.$_SESSION['lang'].'SexOr as LName from '.$table_prefix.'sexor order by TopSorted desc, LName asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div> 
    <!-- end COMP SEX OR -->
    
    <!-- begin TRANSSEXUAL -->
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">is your companion a transsexual: </legend>

                                    <?php
                                        if(isset($_POST['rdTranssexual'])){
                                            if($_POST['rdTranssexual']==1){
                                                $transs1 = ' checked="checked"';
                                                $transs2 = '';
                                                }
                                            elseif($_POST['rdTranssexual']==2){
                                                $transs1 = '';
                                                $transs2 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $transs1 = '';
                                                $transs2 = ' checked="checked"';
                                                }
                                    ?>

                                    <div class="col-sm-6">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $transs1;?> name="rdTranssexual" value="1">
                                            <label class="form-check-label" for="rdTranssexual" style="font-weight:400;font-size:14px;">yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $transs2;?> name="rdTranssexual" value="2">
                                            <label class="form-check-label" for="rdTranssexual" style="font-weight:400;font-size:14px;">no</label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
    <!-- end TRANSSEXUAL -->

    <!-- begin TRANSGENDER -->
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">is your companion a transgender: </legend>

                                    <?php
                                        if(isset($_POST['rdTransgender'])){
                                            if($_POST['rdTransgender']==1){
                                                $transg1 = ' checked="checked"';
                                                $transg2 = '';
                                                }
                                            elseif($_POST['rdTransgender']==2){
                                                $transg1 = '';
                                                $transg2 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $transg1 = '';
                                                $transg2 = ' checked="checked"';
                                                }
                                    ?>

                                    <div class="col-sm-8">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $transg1;?> name="rdTransgender" value="1">
                                            <label class="form-check-label" for="rdTransgender" style="font-weight:400;font-size:14px;">yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $transg2;?> name="rdTransgender" value="2">
                                            <label class="form-check-label" for="rdTransgender" style="font-weight:400;font-size:14px;">no</label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset> 
    <!-- end TRANSGENDER -->

    <!-- begin INDEPENDENT -->
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">is your companion independent: </legend>

                                    <?php
                                        if(isset($_POST['rdIndep'])){
                                            if($_POST['rdIndep']==1){
                                                $indep1 = ' checked="checked"';
                                                $indep2 = '';
                                                }
                                            elseif($_POST['rdIndep']==2){
                                                $indep1 = '';
                                                $indep2 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $indep1 = ' checked="checked"';
                                                $indep2 = '';
                                                }
                                    ?>

                                    <div class="col-sm-8">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $indep1;?> name="rdIndep" value="1">
                                            <label class="form-check-label" for="rdIndep" style="font-weight:400;font-size:14px;">yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $indep2;?> name="rdIndep" value="2">
                                            <label class="form-check-label" for="rdIndep" style="font-weight:400;font-size:14px;">no</label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>    
    <!-- end INDEPENDENT -->           

    <!-- begin PORNSTAR -->
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">is your companion a porn star: </legend>

                                    <?php
                                        if(isset($_POST['rdPornstar'])){
                                            if($_POST['rdPornstar']==1){
                                                $pornstar1 = ' checked="checked"';
                                                $pornstar2 = '';
                                                }
                                            elseif($_POST['rdPornstar']==2){
                                                $pornstar1 = '';
                                                $pornstar2 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $pornstar1 = '';
                                                $pornstar2 = ' checked="checked"';
                                                }
                                    ?>

                                    <div class="col-sm-8">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $pornstar1;?> name="rdPornstar" value="1">
                                            <label class="form-check-label" for="rdPornstar" style="font-weight:400;font-size:14px;">yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $pornstar2;?> name="rdPornstar" value="2">
                                            <label class="form-check-label" for="rdPornstar" style="font-weight:400;font-size:14px;">no</label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>    
        <!-- end PORNSTAR --> 
                                    
        <!-- begin COMP COUNTRY -->
                            <div class="form-group row">
                                <label for="enccountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's location - country: </label>

                                <div class="col-sm-5">
                                    <select class="bootstrap-select form-control" name="EncCountryID" id="EncCountryID">
                                    <option value="0"><?php echo $select;?></option>

                                    <?php
                                        $sel = isset($_POST['CompCountryID'])?$_POST['CompCountryID']:0;
                                        $sqlcountry = 'select CountryID as Id, Country as LName from '.$table_prefix.'countries order by TopSorted desc, LName asc';
                                            dropdownlist($sqlcountry, $sel);
                                    ?>

                                    </select>
                                </div>
                            </div>
        <!-- end COMP COUNTRY -->

        <!-- begin COMP STATE -->
                            <div class="form-group row">
                                <label for="enccountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's location - state: </label>

                                <div class="col-sm-5">
                                    <select class="bootstrap-select form-control" name="EncStateID" id="EncStateID">

                                    <?php
                                        if(isset($_POST['CompCountryID']) && $_POST['CompCountryID']>0){
                                            $sql = 'select StateID as Id, State as LName from '.$table_prefix.'states where CountryID = '.intval($_POST['EncCountryID']).' order by TopSorted desc, LName asc';
                                                dropdownlist($sql, $_POST['CompStateID']);
                                                }
                                            else{
                                            echo '<option value="0">'.$select.'</option>';
                                        }
                                    ?>

                                    </select>
                                </div>
                            </div>                                                                
        <!-- end COMP STATE -->
        
        <!-- begin COMP CITY -->
                            <div class="form-group row">
                                <label for="enccity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's location - city: </label>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control js-websitegroup-validation" name="CompCity" value="" maxlength="100" required>
                                </div>
                            </div>
        <!-- end COMP CITY -->                                                         
                                    
                                    
    <!-- begin COMP PHONE --> 
                            <div class="form-group row">
                                <label for="phone" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's posted phone: </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" size="16" id="phone" name="phone" maxlength="16" value="<?php echo isset($_POST['phone'])?$_POST['phone']:'';?>" onkeypress="return numberPressed(event);">
                                    <small id="phoneHelpBlock" class="form-text text-muted">
                                        please post only the phone number your companion uses to communicate with you. no one from da&trade; will ever call.
                                    </small> 

                                    <script language="javascript">
                                        // Format the phone number as the user types it
                                        document.getElementById('phone').addEventListener('keyup',function(evt){
                                            var phone = document.getElementById('phone');
                                            var charCode = (evt.which) ? evt.which : evt.keyCode;
                                                phone.value = phoneFormat(phone.value);
                                            });

                                        // We need to manually format the phone number on page load
                                        document.getElementById('phone').value = phoneFormat(document.getElementById('phone').value);

                                        // A function to determine if the pressed key is an integer
                                        function numberPressed(evt){
                                            var charCode = (evt.which) ? evt.which : evt.keyCode;
                                                if(charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 36 || charCode > 40)){
                                                return false;
                                                }
                                            return true;
                                            }

                                        // A function to format text to look like a phone number
                                        function phoneFormat(input){
                                        // Strip all characters from the input except digits
                                            input = input.replace(/\D/g,'');

                                        // Trim the remaining input to ten characters, to preserve phone number format
                                            input = input.substring(0,10);

                                        // Based upon the length of the string, we add formatting as necessary
                                        var size = input.length;
                                            if(size == 0){
                                                input = input;
                                                }else if(size < 4){
                                                    input = '('+input;
                                                    }else if(size < 7){
                                                        input = '('+input.substring(0,3)+') '+input.substring(3,6);
                                                        }else{
                                                            input = '('+input.substring(0,3)+') '+input.substring(3,6)+' - '+input.substring(6,10);
                                                            }
                                                        return input; 
                                                }
                                    </script>

                                </div>
                            </div>                                       
    <!-- end COMP PHONE -->

    <!-- begin COMPANION WEBSITE -->               
                            <div class="form-group row">
                                <label for="compwebsite" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your companion's website: </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="compwebsite" name="compwebsite" value="<?php echo isset($_POST['compwebsite'])?$_POST['compwebsite']:'';?>" maxlength="250">
                                </div>
                            </div>                                     
    <!-- end COMPANION WEBSITE -->

    <!-- begin COMPANION WEB PROFILE -->               
                            <div class="form-group row">
                                <label for="compwebprof" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your companion's profile on the web: </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="compwebprof" name="compwebprof" value="<?php echo isset($_POST['compwebprof'])?$_POST['compwebprof']:'';?>" maxlength="250">
                                </div>
                            </div>                                     
    <!-- end COMPANION WEB PROFILE -->             

    <!-- begin COMPANION WHATSAPP -->                   
                            <div class="form-group row">
                                <label for="compwhatsapp" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your companion's whatsapp: </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="compwhatsapp" name="compwhatsapp" value="<?php echo isset($_POST['compwhatsapp'])?$_POST['compwhatsapp']:'';?>" maxlength="250">
                                </div>
                            </div>                               
    <!-- end COMPANION WHATSAPP -->             

    <!-- begin COMPANION INSTAGRAM -->                   
                            <div class="form-group row">
                                <label for="compinstagram" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your companion's instagram: </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="compinstagram" name="compinstagram" value="<?php echo isset($_POST['compinstagram'])?$_POST['compinstagram']:'';?>" maxlength="250">
                                </div>
                            </div>                               
    <!-- end COMPANION INSTAGRAM -->          
                                    
                        </div>
                        <div class="col-lg-1">&nbsp;</div>
                    </div>
                                    
                    <div class="row">

                        <div class="col-lg-12">
                            <br>
                            <h5 style="color:#813ee8;">please describe your companion's appearance</h5>
                            <hr style="color:#813ee8;">
                            <br>
                        </div>
                    </div>
                                
                    <div class="row">
                        <div class="col-lg-1">&nbsp;</div>

                        <div class="col-lg-10">                                    
                                    
    <!-- begin BODYTYPE -->                        
                            <div class="form-group row">
                                <label for="slBodyType" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companions's <?php echo $bodytype;?>: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slBodyType" name="slBodyType">
                                    <?php
                                        $sel = isset($_POST['slBodyType'])?$_POST['slBodyType']:1;
                                        $sql = 'select BodyTypeID as Id, '.$_SESSION['lang'].'BodyType as LName from '.$table_prefix.'bodytypes order by TopSorted desc, LName asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div>                                   
    <!-- end BODYTYPE -->

    <!-- begin HEIGHT -->
                            <div class="form-group row">
                                <label for="slHeight" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's approximate <?php echo $height;?>: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slHeight" name="slHeight">
                                        <option value="0"><?php echo $select;?></option>

                                        <?php 
                                            $sel = isset($_POST['slHeight'])?$_POST['slHeight']:358;
                                            $sql = 'select HeightID as Id, HeightDescription as LName from '.$table_prefix.'height order by HeightID asc';
                                                dropdownlist($sql, $sel);
                                        ?>

                                    </select> 
                                </div>
                            </div>
    <!-- end HEIGHT -->

    <!-- begin HAIRCOLOR -->
                            <div class="form-group row">
                                <label for="slHairColor" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's<?php echo $haircolor;?>: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slHairColor" name="slHairColor">
                                    <?php
                                        $sel = isset($_POST['slHairColor'])?$_POST['slHairColor']:1;
                                        $sql = 'select HairColorID as Id, '.$_SESSION['lang'].'HairColor as LName from '.$table_prefix.'haircolors order by TopSorted desc, LName asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div>                                   
    <!-- end HAIRCOLOR -->

    <!-- begin HAIRLENGTH -->
                            <div class="form-group row">
                                <label for="slHairLength" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's hair length: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slHairLength" name="slHairLength">
                                    <?php
                                        $sel = isset($_POST['slHairLength'])?$_POST['slHairLength']:1;
                                        $sql = 'SELECT HairLengthID as Id, '.$_SESSION['lang'].'HairLength as LName from '.$table_prefix.'hairlengths order by TopSorted desc, LName asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div>                                   
    <!-- end HAIRLENGTH -->

    <!-- begin PIERCINGS -->
                            <div class="form-group row">
                                <label for="slPiercings" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's piercings: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slPiercings" name="slPiercings">
                                    <?php
                                        $sel = isset($_POST['slPiercings'])?$_POST['slPiercings']:1;
                                        $sql = 'select PiercingTypeID as Id, '.$_SESSION['lang'].'PiercingType as LName from '.$table_prefix.'piercings order by LName asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div>                                                                       
    <!-- end PIERCINGS -->

    <!-- begin TATTOOS -->
                            <div class="form-group row">
                                <label for="slTattoos" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's tattoos: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slTattoos" name="slTattoos">
                                    <?php
                                        $sel = isset($_POST['slTattoos'])?$_POST['slTattoos']:1;
                                        $sql = 'select TattoosTypeID as Id, '.$_SESSION['lang'].'TattooType as LName from '.$table_prefix.'tattoos order by LName asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div>                                                                       
    <!-- end TATTOOS -->

    <!-- begin PUSSY GROOMING -->
                            <div class="form-group row">
                                <label for="slPussy" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's pussy grooming: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slPussy" name="slPussy">
                                    <?php
                                        $sel = isset($_POST['slPussy'])?$_POST['slPussy']:1;
                                        $sql = 'select PussyID as Id, '.$_SESSION['lang'].'Pussy as LName from '.$table_prefix.'pussy order by LName asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div>                                                                       
    <!-- end PUSSY GROOMING -->            

    <!-- begin BREASTS -->
                            <div class="form-group row">
                                <label for="slCompBreastSizeDesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's breast description: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slCompBreastSizeDesc" name="slCompBreastSizeDesc">
                                    <?php
                                        $sel = isset($_POST['slCompBreastSizeDesc'])?$_POST['slCompBreastSizeDesc']:1;
                                        $sql = 'select BreastSizeDescID as Id, '.$_SESSION['lang'].'BreastSizeDesc as LName from '.$table_prefix.'breastsizedesc order by BreastSizeDescID asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div>                                                                       
    <!-- end BREASTS -->                        

    <!-- begin BREAST IMPLANTS -->
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">companion breast implants: </legend>

                                    <?php
                                        if(isset($_POST['rdBreastImp'])){
                                            if($_POST['rdBreastImp']==1){
                                                $breastimp1 = ' checked="checked"';
                                                $breastimp2 = '';
                                                }
                                            elseif($_POST['rdBreastImp']==2){
                                                $breastimp1 = '';
                                                $breastimp2 = ' checked="checked"';
                                                }
                                            }
                                            else{
                                                $breastimp1 = ' checked="checked"';
                                                $breastimp2 = '';
                                                }
                                    ?>

                                    <div class="col-sm-6">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $breastimp1;?> name="rdBreastImp" value="1">
                                            <label class="form-check-label" for="rdBreastImp" style="font-weight:400;font-size:14px;">yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <?php echo $breastimp2;?> name="rdBreastImp" value="2">
                                            <label class="form-check-label" for="rdBreastImp" style="font-weight:400;font-size:14px;">no</label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>    
    <!-- end BREAST IMPLANTS -->             

    <!-- begin ASS -->
                            <div class="form-group row">
                                <label for="slCompAss" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's ass: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slCompAss" name="slCompAss">
                                    <?php
                                        $sel = isset($_POST['slCompAss'])?$_POST['slCompAss']:1;
                                        $sql = 'select CompAssID as Id, '.$_SESSION['lang'].'CompAss as LName from '.$table_prefix.'compass order by CompAssID asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div>                                                                       
    <!-- end ASS -->                              

                        </div>
                        <div class="col-lg-1">&nbsp;</div>

                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <h5 style="color:#813ee8;">about your companion's habits</h5>
                            <hr style="color:#813ee8;">
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-1">&nbsp;</div>

                        <div class="col-lg-10">
                            <div class="form-group row">

                                <label for="slSmoking" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">did your companion smoke: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slSmoking" name="slSmoking">
                                    <?php
                                        $sel = isset($_POST['slSmoking'])?$_POST['slSmoking']:1;
                                        $sql = 'select SmokingID as Id, '.$_SESSION['lang'].'Smoking as LName from '.$table_prefix.'smoking order by TopSorted desc, LName asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="slDrinking" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">did your companion drink: </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="slDrinking" name="slDrinking">
                                    <?php
                                        $sel = isset($_POST['slDrinking'])?$_POST['slDrinking']:1;
                                        $sql = 'select DrinkingID as Id, '.$_SESSION['lang'].'Drinking as LName from '.$table_prefix.'drinking order by TopSorted desc, LName asc';
                                            dropdownlist($sql, $sel);
                                    ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1">&nbsp;</div>

                    </div>

                    <div class="row">
                        
                        <div class="col-lg-1">&nbsp;</div>
                        <div class="col-lg-10">
                            <div class="form-group row">
                                <div class="col-sm-4">&nbsp;</div>
                                <div class="col-sm-8">
                                    <input type="submit" value="<?php echo $continue;?>" class="btn btn-cli-pro" name="smcompsignup"><input type="reset" value="<?php echo $cancel;?>" class="btn btn-pro-cli">
                                    <button type="button" formnovalidate="formnovalidate" class="btn btn-pro-cli js-save-draft-click">save draft</button>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-1">&nbsp;</div>

                    </div>
                                                
                                                <div class="modal fade" id="submitReview" tabindex="-1" role="dialog" aria-labelledby="submitReviewLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                    <h5 class="modal-title" id="submitReviewLabel">Error</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <span class="icon-error">!</span>
                                                                <p>Your review wasn't added. Please try to do it later.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                            
