<script type="text/javascript">
    $.ter.TimezoneUtils.setTzOffsetCookieIfAbsent();

    $(window).on("load", (function () {
        
    }));

    $(function() {
        
    });
    //-->
</script>


<script language="javascript">
    function showDraftWarning() {
        bootbox.dialog({
            className: ' modal-alert warning',
            title: "<button type='button' class='sr-only close-x' data-dismiss='modal'>Close</button><i class='icon-warning'></i>",
            message: '<h5 class="modal-alert-subtitle">Warning!</h5>' +
            '<p class="modal-alert-text">It looks like you already started writing a review for a different provider. You would need to finish that review first or delete the draft prior to reviewing another provider.</p><button class="btn btn-primary" data-dismiss="modal">Finish Review</button>',
            onEscape: true,
            backdrop: true
        });
    }
</script>

        <script type="text/javascript">
            $(function () {
                var validator;

                $('.selectpicker').on('change', function () {
                    $(this).valid();
                });

                $("#CompPerformanceID").on("change", function(){
                    var selectedValue = $(this).val();
                    if (selectedValue !== '') {
                        var intValue = parseInt(selectedValue);
                        if (!isNaN(intValue) && intValue > 7) {
                            $.ter.DialogManager.showWarning('you have given a score higher than 7.  encounters with scores higher than 7 should include indulgences such as: kisses with tongue, bj without condom, more than one guy, really bi session or anal.');
                        }
                    }
                });

                var triggerAutosave = function () {
                    $(".js-autosave").trigger("ter.forceAutosave");
                };

                $(".js-submit-form").on("submit", function(e) {
                    if ($(".js-submit-form").valid()) {
                        $(".js-autosave").trigger("ter.stopAutosave");
                    }
                });

                $(".js-chemistry-rating").each( function() {
                    var rating = $(this).attr("data-rating"),
                        formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                    $(this).rateYo({
                        
                        fullStar: true,
                            totalStars: 5,
                    starWidth: "16px",
                    ratedFill: "#f300fe",
                    normalFill: "#dbe0e4",
                    precision: 2,
                    onSet: function (rating, rateYoInstance) {
                        formControl.val(rating);
                        if (formControl.val() != 0) {
                            formControl.valid();
                        };
                        triggerAutosave();
                    }
                });
            });
        
            $(".js-incalllocation-rating").each( function() {
                var rating = $(this).attr("data-rating"),
                    formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                $(this).rateYo({
                    
                    fullStar: true,
                        totalStars: 5,
                starWidth: "16px",
                ratedFill: "#f300fe",
                normalFill: "#dbe0e4",
                precision: 2,
                onSet: function (rating, rateYoInstance) {
                    formControl.val(rating);
                    if (formControl.val() != 0) {
                        formControl.valid();
                    };
                    triggerAutosave();
                }
            });
            });
        
            $(function(){
                $(".js-incalllocation-clear").on("click", function(){
                    var chemistry = $(".js-incalllocation-rating");
                    chemistry.rateYo("option", "rating", "0");
                    chemistry.parents(".form-control-wrapper").find($('.form-control')).val("");
                    $("#IncallLocationRating").val('');
                    triggerAutosave();
                });
            });


                $(".js-autosave .selectpicker").on('hidden.bs.select', function (e) {
                    triggerAutosave();
                });

                $(".js-autosave input:checkbox").on('change', function(e) {
                    triggerAutosave();
                });

                //delay for autosave in milliseconds
                var AUTO_SAVE_PERIOD = 2000;

                function timeFormatter(dateTime) {
                    var timeNow = new Date(dateTime);
                    var hours = timeNow.getHours();
                    var minutes = timeNow.getMinutes();
                    var seconds = timeNow.getSeconds();
                    var timeString = "" + ((hours > 12) ? hours - 12 : hours);
                    timeString += ((minutes < 10) ? ":0" : ":") + minutes;
                    timeString += ((seconds < 10) ? ":0" : ":") + seconds;
                    timeString += (hours >= 12) ? " PM" : " AM";

                    return timeString;
                }

                var submitDraftAutosaveCallback = function () {
                    updateAutosaveMessage();
                }

                var submitDraftAutosave = function () {
                    var form = $("#reviewDetailsForm");
                    var url = form.attr('action');
                    $("#isDraft").val("1");
                    $("#isAutosave").val("1");
                    var formData = form.serialize();
                    $("#isDraft").val("");
                    $("#isAutosave").val("");
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        success: function (data, textStatus, jqXHR) {
                            submitDraftAutosaveCallback();
                        },
                        error: function (jqXHR, status, error) {
                            if (status == 401) {
                                location.href = "/memberlaunch/login.asp?dest=/reviews/submitReviewDetails.asp?terId%3D253746";
                            }
                            //console.log(status + ": " + error);
                        }
                    });
                };

                var updateAutosaveMessage = function () {
                    var time = new Date().getTime();
                    $(".js-draft-autosave-message").show();
                    $(".js-draft-autosave-message").text("Review Draft autosaved " + timeFormatter(time));
                };

                $(".js-autosave").autoSave(function () {
                    submitDraftAutosave();
                }, AUTO_SAVE_PERIOD);

                $(".js-save-draft-click").on("click", function (event) {
                    $("#reviewDetailsForm").data('validator').cancelSubmit = true;
                    $("#reviewDetailsForm").validate().settings.ignore = "*";
                    $("#isDraft").val("1");
                    $("#reviewDetailsForm").submit();
                    return false;
                });

                
                validator = $('.js-submit-form').validate({
                    rules: {
                        Website: "required",
                        Location: "required",
                        //session atmosphere
                        Smokes: "required",
                        OverallLooks: "required",
                        Attitude: "required",
                        OverallAtmosphere: "required",
                        OverallPerformance: "required",
                        //visit details
                        GeneralDetails: "required",
                        TheJuicyDetails: "required",
                        ChemistryRating: "required"
                    },
                    messages: {
                        ChemistryRating: "Please select Chemistry star rating."
                    },
                    highlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").addClass("error");
                        $element.next(".btn-group").addClass("error");
                    },
                    unhighlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").removeClass("error");
                        $element.removeClass("error").next(".btn-group").removeClass("error");
                    },
                    ignore:"",
                    //TODO: move out to common js
                    invalidHandler: function(form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            if($(validator.errorList[0].element).is(":visible"))
                            {
                                validator.errorList[0].element.focus();
                            }
                            else
                            {
                                var elementOffset = $("#" + $(validator.errorList[0].element).attr("data-focusID")).offset().top;
                                if (elementOffset > $(window).height()){
                                    header = $(".header").height();
                                    $('html, body').animate({
                                        scrollTop: elementOffset - header - 30
                                    }, 1000);
                                }
                            }
                        }
                    }
                });
            });

        </script>

                                            <div class="js-draft-autosave-message warning-block" hidden="hidden"></div>

                                            
                                                
                                                    <form action="../mbrs/simple_review_submitted.php" method="POST" id="reviewDetailsForm" name="form1" class="js-autosave js-submit-form" novalidate="novalidate">
                                                        <input type="hidden" name="ReviewerStatusID" value="1">
                                                        <input type="hidden" name="ReviewerID" value="<?php echo $_SESSION['memberid'];?>">
                                                        <input type="hidden" name="CompanionID" value="<?php echo $person['UserID'];?>">
                                                        <input type="hidden" name="SubmittedOn" value="<?php date_default_timezone_set("America/New_York"); echo date("Y-m-d h:i:sa");?>">
                                                        <input type="hidden" name="ReviewStatus" value="0">                           

                                                <script language="javascript">
                                                    $(document).ready(function(){
                                                        $('#EncCountryID').change(function() {
                                                            var valchange = this.value;
                                                                $('#EncStateID').load('../includes/loadstates.php?countryId=' + valchange);
                                                            });
                                                        });
                                                </script>                                                        
                                                        
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    
                                                    <h5 style="color:#813ee8;">encounter location</h5>
                                                    <hr style="color:#813ee8;">
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <label for="enccountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter country: </label>

                                                                <div class="col-sm-6">
                                                                    <select class="bootstrap-select form-control" name="EncCountryID" id="EncCountryID">
                                                                    <option value="0"><?php echo $select;?></option>

                                                                    <?php
                                                                        $sel = isset($_POST['EncCountryID'])?$_POST['EncCountryID']:0;
                                                                        $sqlcountry = 'select CountryID as Id, Country as LName from '.$table_prefix.'countries order by TopSorted desc, LName asc';
                                                                            dropdownlist($sqlcountry, $sel);
                                                                    ?>

                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="encstate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter state: </label>

                                                                <div class="col-sm-6">
                                                                    <select class="bootstrap-select form-control" name="EncStateID" id="EncStateID">

                                                                    <?php
                                                                        if(isset($_POST['EncCountryID']) && $_POST['EncCountryID']>0){
                                                                            $sql = 'select StateID as Id, State as LName from '.$table_prefix.'states where CountryID = '.intval($_POST['EncCountryID']).' order by TopSorted desc, LName asc';
                                                                                dropdownlist($sql, $_POST['EncStateID']);
                                                                                }
                                                                            else{
                                                                            echo '<option value="0">'.$select.'</option>';
                                                                        }
                                                                    ?>

                                                                    </select>
                                                                </div>
                                                            </div>                                                                
                                                                
                                                            <div class="form-group row">
                                                                <label for="enccity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter city: </label>

                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control js-websitegroup-validation" name="EncCity" value="" maxlength="100" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-1">&nbsp;</div>
                                                            
                                                    </div>
                                                            
                                                    <h5 style="color:#813ee8;">description of encounter with your companion</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">approximately when was your encounter: </label>                                                    
                                                                    
                                                                <script type="text/javascript">
                                                                    $(function () {
                                                                        $('#datetimepicker2').datetimepicker({
                                                                            format: 'MM/YYYY'
                                                                        });
                                                                    });
                                                                </script>
                                                                
                                                                <div class="input-group date col-sm-5" id="datetimepicker2" data-target-input="nearest">
                                                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2" name="EncDate"/>
                                                                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                                                        <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                
                                                            </div>                                                                    

                                                            <div class="form-group row">
                                                                <label for="encloc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter duration: </label>
                                                                <div class="col-sm-5">
                                                                    <select class="bootstrap-select form-control" id="EncDuration" name="EncLocationID" title="" tabindex="-98">
                                                                        <option value="" label="- select one -"></option>
                                                                        <option value="30">30 mins</option>
                                                                        <option value="60">60 mins</option>
                                                                        <option value="90">90 mins</option>
                                                                        <option value="2">2 hrs</option>
                                                                        <option value="3">3 hrs</option>
                                                                        <option value="4">4 hrs</option>
                                                                        <option value="overnight">overnight</option>
                                                                        <option value="other">other</option>
                                                                    </select>
                                                                </div>
                                                            </div>                                                            

                                                            <div class="form-group row">
                                                                <label for="encDonation" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">donation: </label>
                                                                <div class="col-sm-3">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">$</span>
                                                                        <input type="text" class="form-control js-websitegroup-validation" name="EncDonation" value="" maxlength="100" required>
                                                                    </div>
                                                                </div>
                                                            </div>                                                             
                                                            
                                                            <div class="form-group row">
                                                                <label for="encloc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter location: </label>
                                                                <div class="col-sm-6">
                                                                    <select class="bootstrap-select form-control" id="EnLocationID" name="EncLocationID" title="" tabindex="-98">
                                                                        <option value="" label="- select one -"></option>
                                                                        <option value="1">her place</option>
                                                                        <option value="2">her hotel</option>
                                                                        <option value="3">my place</option>
                                                                        <option value="4">my hotel</option>
                                                                        <option value="5">other</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">encounter atmosphere</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">type of encounter: </label>                                                    
                                                                <div class="col-sm-6">   
                                                                
                                                                    <select class="bootstrap-select form-control" name="EncTypeID" title="" tabindex="-98">
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="1">gfe - girlfriend exp</option>
                                                                        <option value="2">pse - pornstar exp</option>
                                                                        <option value="3">erotic massage - body rub</option> 
                                                                        <option value="4">blow job</option>
                                                                        <option value="5">body slide - companion slid their naked body along your body</option>
                                                                        <option value="6">hand job</option>
                                                                        <option value="7">anal sex (also known as greek)</option>
                                                                        <option value="8">tie &amp; tease - companion gently tied your hands and/or feet</option>
                                                                        <option value="9">fantasy - companion dressed up in a costume and /or role-play</option>
                                                                        <option value="10">b &amp; d - being whipped, flogged, caned, tied up, told what to do</option>
                                                                        <option value="11">s &amp; m - a more extreme form of b&amp;d</option>                                   
                                                                    </select>
                                                                
                                                                </div>
                                                            </div>
            <!-- begin OVERALL LOOKS? -->                                                
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's overall looks: </label>
                                                                <div class="col-sm-6"> 
                                                                    
                                                                    <select class="bootstrap-select form-control" name="CompLooksID" title="" tabindex="-98">
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="1">1 - i was frightened</option>
                                                                        <option value="2">2 - needed a blindfold</option>
                                                                        <option value="3">3 - ugly</option>
                                                                        <option value="4">4 - ok if i was drunk</option>
                                                                        <option value="5">5 - average</option>
                                                                        <option value="6">6 - pretty</option>
                                                                        <option value="7">7 - very attractive</option>
                                                                        <option value="8">8 - super hot</option>
                                                                        <option value="9">9 - could be a model</option>
                                                                        <option value="10">10 - i was in awe</option>
                                                                    </select>
                                                                
                                                                </div>
                                                            </div>
            <!-- end OVERALL LOOKS? -->                                                
            
            <!-- begin SMOKES? -->
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">does companion smoke: </label>
                                                                    <div class="col-sm-6"> 
                                                                        
                                                                        <select class="bootstrap-select form-control" name="CompSmokeID" title="" tabindex="-98" required>
                                                                            <option value="" label="<?php echo $select;?>"></option>
                                                                            <option value="1">no</option>
                                                                            <option value="2">yes - but not during session</option>
                                                                            <option value="3">yes - during session</option>
                                                                            <option value="4">yes - thats all i could smell</option>
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
            <!-- end SMOKES? -->

            <!-- begin COMP ATTITUDE -->
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's attitude: </label>
                                                                    <div class="col-sm-6">
                                                                        <select class="bootstrap-select form-control" name="CompAttitudeID" title="" tabindex="-98">
                                                                            <option value="" label="<?php echo $select;?>"></option>
                                                                            <option value="1">apathetic - indifferent - lacked energy or concern</option>
                                                                            <option value="2">bitter - exhibited strong animosity</option>
                                                                            <option value="3">bored - gave off an air that they would rather be elsewhere</option>
                                                                            <option value="3">cynical - questioned sincerity and goodness of people</option>
                                                                            <option value="4">condescending - gave off a feeling of superiority</option>
                                                                            <option value="5">callous - unfeeling - insensitive</option>
                                                                            <option value="6">chill - super relaxed</option>
                                                                            <option value="7">contemplative - thoughtful, reflective</option>
                                                                            <option value="8">conventional - lacked spontaneity</option>
                                                                            <option value="9">fanciful - used their imagination</option>
                                                                            <option value="10">fanciful - used their imagination</option>
                                                                            <option value="11">gloomy - dark - sad</option>
                                                                            <option value="12">haughty - arrogant</option>
                                                                            <option value="13">hot-tempered - easily angered</option>
                                                                            <option value="14">intimate - very familiar</option>
                                                                            <option value="15">judgmental - critical</option>
                                                                            <option value="16">jovial - happy</option>
                                                                            <option value="17">matter-of-fact - not fanciful or emotional</option>
                                                                            <option value="18">mocking - treated with contempt</option>
                                                                            <option value="19">optimistic - hopeful, cheerful</option>
                                                                            <option value="20">patronizing - had air of condescension</option>
                                                                            <option value="21">pessimistic - saw the worst side of things</option>
                                                                            <option value="21">professional - performed like a pro</option>
                                                                            <option value="22">ridiculing - made fun of</option>
                                                                            <option value="23">sincere - without deceit or pretense; genuine</option>
                                                                            <option value="24">whimsical - odd, strange, fantastic; fun</option>
                                                                        </select>
                                                                    </div>
                                                                    
                                                                </div>
            <!-- end COMP ATTITUDE -->
            
            <!-- begin OVERALL ATMOSPHERE -->
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">overall atmosphere: </label>
                                                                    <div class="col-sm-6">
                                                                        <select class="bootstrap-select form-control" id="EncAtmosphereID" name="EncAtmosphereID" title="" tabindex="-98">
                                                                            <option value="" label="<?php echo $select;?>"></option>
                                                                            <option value="1">very scary</option>
                                                                            <option value="2">a bit scary</option>
                                                                            <option value="3">extremely awkward</option>
                                                                            <option value="4">uptight</option>
                                                                            <option value="5">like a first date</option>
                                                                            <option value="6">chill</option>
                                                                            <option value="7">sexy vibe</option>
                                                                            <option value="8">like a party</option>
                                                                            <option value="9">like a workout</option>
                                                                        </select>
                                                                    </div>
                                                                    
                                                                </div>
            <!-- end OVERALL ATMOSPHERE -->
            
            <!-- begin OVERALL PERFORMANCE -->
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">overall performance: </label>
                                                                    <div class="col-sm-6">
                                                                        <select class="bootstrap-select form-control" id="CompPerformanceID" name="CompPerformanceID" title="" tabindex="-98">
                                                                            <option value="" label="<?php echo $select;?>"></option>
                                                                            <option value="1">1 - a complete rip-off</option>
                                                                            <option value="2">2 - i wish we hadn't met</option>
                                                                            <option value="3">3 - not worth the effort</option>
                                                                            <option value="4">4 - she just laid there</option>
                                                                            <option value="5">5 - average</option>
                                                                            <option value="6">6 - nice Time</option>
                                                                            <option value="7">7 - good time</option>
                                                                            <option value="8">8 - went the extra mile</option>
                                                                            <option value="9">9 - we were meant to be together</option>
                                                                            <option value="10">10 - she blew my mind</option>
                                                                        </select>
                                                                    </div>
                                                                    
                                                                </div>
            <!-- end OVERALL PERFORMANCE -->
                               
                                                        </div>
                                                        
                                                        <div class="col-lg-2">&nbsp;</div>
                                                        
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">verifications</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            
                                <!-- begin INDULTRUE -->
                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">indulgences were as described: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdIndulgeTrue'])){
                                                                            if($_POST['rdIndulgeTrue']==1){
                                                                                $indultrue1 = ' checked="checked"';
                                                                                $indultrue2 = '';
                                                                                }
                                                                            elseif($_POST['rdIndulgeTrue']==2){
                                                                                $indultrue1 = '';
                                                                                $indultrue2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $indultrue1 = '';
                                                                                $indultrue2 = ' checked="checked"';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-6">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $indultrue1;?> name="rdIndulgeTrue" value="1">
                                                                            <label class="form-check-label" for="rdIndulgeTrue" style="font-weight:400;font-size:14px;">yes</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $indultrue2;?> name="rdIndulgeTrue" value="2">
                                                                            <label class="form-check-label" for="rdIndulgeTrue" style="font-weight:400;font-size:14px;">no</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>    
                                    <!-- end INDULTRUE -->                                                             
                                                            
                                    <!-- begin PHOTOSTRUE -->
                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">companion looked like photos: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdCompPhotoTrue'])){
                                                                            if($_POST['rdCompPhotoTrue']==1){
                                                                                $compphototrue1 = ' checked="checked"';
                                                                                $compphototrue2 = '';
                                                                                }
                                                                            elseif($_POST['rdCompPhotoTrue']==2){
                                                                                $compphototrue1 = '';
                                                                                $compphototrue2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $compphototrue1 = '';
                                                                                $compphototrue2 = ' checked="checked"';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-6">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $compphototrue1;?> name="rdCompPhotoTrue" value="1">
                                                                            <label class="form-check-label" for="rdCompPhotoTrue" style="font-weight:400;font-size:14px;">yes</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $compphototrue2;?> name="rdCompPhotoTrue" value="2">
                                                                            <label class="form-check-label" for="rdCompPhotoTrue" style="font-weight:400;font-size:14px;">no</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>    
                                    <!-- end PHOTOSTRUE -->                                                                                                    
                                                            
                                    <!-- begin COMPONTIMETRUE -->
                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">companion was on time: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdCompOntimeTrue'])){
                                                                            if($_POST['rdCompOntimeTrue']==1){
                                                                                $compontimetrue1 = ' checked="checked"';
                                                                                $compontimetrue2 = '';
                                                                                }
                                                                            elseif($_POST['rdCompOntimeTrue']==2){
                                                                                $compontimetrue1 = '';
                                                                                $compontimetrue2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $compontimetrue1 = '';
                                                                                $compontimetrue2 = ' checked="checked"';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-8">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $compontimetrue1;?> name="rdCompOntimeTrue" value="1">
                                                                            <label class="form-check-label" for="rdCompOntimeTrue" style="font-weight:400;font-size:14px;">yes</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $compontimetrue2;?> name="rdCompOntimeTrue" value="2">
                                                                            <label class="form-check-label" for="rdCompOntimeTrue" style="font-weight:400;font-size:14px;">no</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>    
                                    <!-- end COMPONTIMETRUE -->                     
                                                            
                                                        </div>
                                                        
                                                        <div class="col-lg-2">&nbsp;</div>
                                                        
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">general description of your encounter</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-2">&nbsp;</div>
                                
                                                        <div class="col-lg-8">
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">headline for your review:</label>
                                                                <div class="col-sm-8">    
                                                                    <input name="ReviewHeadline" value="" type="text" class="form-control" maxlength="60">
                                                                        <div class="textarea-feedback" style="color:#ff0000;">max 60 characters</div>
                                                                </div>
                                                            </div>
                                                                
                                                            general summary should include broad comments about your companion and your initial experience. please do not mention specific acts or donation - just tease your fellow members.
                                                            
                                                            <br>
                                                            <br>
                                                            
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">general summary:</label>
                                                                <div class="col-sm-8">
                                                                    <textarea class="bootstrap-select form-control review-form-textarea js-textarea " data-limit="4000" name="GenRevSummary" id="GenRevSummary" rows="2" style="overflow: hidden; word-wrap: break-word;"></textarea>
                                                                    <div class="textarea-feedback" style="color:#ff0000;">characters: <span class="textarea-feedback-content js-textarea-feedback" id="len1">4000 left</span>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                        <div class="col-lg-2">&nbsp;</div>
                                                        
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">detailed description of encounter</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-2">&nbsp;</div>
                                
                                                        <div class="col-lg-8">
                                                            describe your companion, the experience, and whether or not you enjoyed it in graphic emotional and sexual terms. don't make this a recap of the general summary, instead, go for a blow-by-blow tell-all of your encounter from your own point of view.
                                                        
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">sexy details: </label>
                                                                <div class="col-sm-8">
                                                                    <textarea class="bootstrap-select form-control review-form-textarea js-textarea " data-limit="6000" name="SexyRevDetails" id="SexyRevDetails" rows="4" style="overflow: hidden; word-wrap: break-word;"></textarea>
                                                                    <div class="textarea-feedback" style="color:#ff0000;">characters: <span class="textarea-feedback-content js-textarea-feedback" id="len1">6000 left</span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-2">&nbsp;</div>
                                                    </div>
                                                
                                                    <div class="row">
                                                        <div class="col-lg-2">&nbsp;</div>
                                
                                                        <div class="col-lg-8">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">&nbsp;</div>
                                                                <div class="col-sm-8">
                                                                    <button class="btn btn-cli-pro" data-toggle="modal2" data-target="#submitReview">submit</button>
                                                                    <button type="button" formnovalidate="formnovalidate" class="btn btn-pro-cli js-save-draft-click">save draft</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-2">&nbsp;</div>
                                                    
                                                    </div>
                                                
                                                <div class="modal fade" id="submitReview" tabindex="-1" role="dialog" aria-labelledby="submitReviewLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                    <h5 class="modal-title" id="submitReviewLabel">Error</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <span class="icon-error">!</span>
                                                                <p>Your review wasn't added. Please try to do it later.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                            
