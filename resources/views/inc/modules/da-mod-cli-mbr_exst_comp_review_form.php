<script type="text/javascript">
    $.ter.TimezoneUtils.setTzOffsetCookieIfAbsent();

    $(window).on("load", (function () {
        
    }));

    $(function() {
        
    });
    //-->
</script>


<script language="javascript">
    function showDraftWarning() {
        bootbox.dialog({
            className: ' modal-alert warning',
            title: "<button type='button' class='sr-only close-x' data-dismiss='modal'>Close</button><i class='icon-warning'></i>",
            message: '<h5 class="modal-alert-subtitle">Warning!</h5>' +
            '<p class="modal-alert-text">It looks like you already started writing a review for a different provider. You would need to finish that review first or delete the draft prior to reviewing another provider.</p><button class="btn btn-primary" data-dismiss="modal">Finish Review</button>',
            onEscape: true,
            backdrop: true
        });
    }
</script>

        <script type="text/javascript">
            $(function () {
                var validator;

                $('.selectpicker').on('change', function () {
                    $(this).valid();
                });

                $("#CompPerformanceID").on("change", function(){
                    var selectedValue = $(this).val();
                    if (selectedValue !== '') {
                        var intValue = parseInt(selectedValue);
                        if (!isNaN(intValue) && intValue > 7) {
                            $.ter.DialogManager.showWarning('you have given a score higher than 7.  encounters with scores higher than 7 should include indulgences such as: kisses with tongue, bj without condom, more than one guy, really bi session or anal.');
                        }
                    }
                });

                var triggerAutosave = function () {
                    $(".js-autosave").trigger("ter.forceAutosave");
                };

                $(".js-submit-form").on("submit", function(e) {
                    if ($(".js-submit-form").valid()) {
                        $(".js-autosave").trigger("ter.stopAutosave");
                    }
                });

                $(".js-chemistry-rating").each( function() {
                    var rating = $(this).attr("data-rating"),
                        formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                    $(this).rateYo({
                        
                        fullStar: true,
                            totalStars: 5,
                    starWidth: "16px",
                    ratedFill: "#f300fe",
                    normalFill: "#dbe0e4",
                    precision: 2,
                    onSet: function (rating, rateYoInstance) {
                        formControl.val(rating);
                        if (formControl.val() != 0) {
                            formControl.valid();
                        };
                        triggerAutosave();
                    }
                });
            });
        
            $(".js-incalllocation-rating").each( function() {
                var rating = $(this).attr("data-rating"),
                    formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                $(this).rateYo({
                    
                    fullStar: true,
                        totalStars: 5,
                starWidth: "16px",
                ratedFill: "#f300fe",
                normalFill: "#dbe0e4",
                precision: 2,
                onSet: function (rating, rateYoInstance) {
                    formControl.val(rating);
                    if (formControl.val() != 0) {
                        formControl.valid();
                    };
                    triggerAutosave();
                }
            });
            });
        
            $(function(){
                $(".js-incalllocation-clear").on("click", function(){
                    var chemistry = $(".js-incalllocation-rating");
                    chemistry.rateYo("option", "rating", "0");
                    chemistry.parents(".form-control-wrapper").find($('.form-control')).val("");
                    $("#IncallLocationRating").val('');
                    triggerAutosave();
                });
            });


                $(".js-autosave .selectpicker").on('hidden.bs.select', function (e) {
                    triggerAutosave();
                });

                $(".js-autosave input:checkbox").on('change', function(e) {
                    triggerAutosave();
                });

                //delay for autosave in milliseconds
                var AUTO_SAVE_PERIOD = 2000;

                function timeFormatter(dateTime) {
                    var timeNow = new Date(dateTime);
                    var hours = timeNow.getHours();
                    var minutes = timeNow.getMinutes();
                    var seconds = timeNow.getSeconds();
                    var timeString = "" + ((hours > 12) ? hours - 12 : hours);
                    timeString += ((minutes < 10) ? ":0" : ":") + minutes;
                    timeString += ((seconds < 10) ? ":0" : ":") + seconds;
                    timeString += (hours >= 12) ? " PM" : " AM";

                    return timeString;
                }

                var submitDraftAutosaveCallback = function () {
                    updateAutosaveMessage();
                }

                var submitDraftAutosave = function () {
                    var form = $("#reviewDetailsForm");
                    var url = form.attr('action');
                    $("#isDraft").val("1");
                    $("#isAutosave").val("1");
                    var formData = form.serialize();
                    $("#isDraft").val("");
                    $("#isAutosave").val("");
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        success: function (data, textStatus, jqXHR) {
                            submitDraftAutosaveCallback();
                        },
                        error: function (jqXHR, status, error) {
                            if (status == 401) {
                                location.href = "/memberlaunch/login.asp?dest=/reviews/submitReviewDetails.asp?terId%3D253746";
                            }
                            //console.log(status + ": " + error);
                        }
                    });
                };

                var updateAutosaveMessage = function () {
                    var time = new Date().getTime();
                    $(".js-draft-autosave-message").show();
                    $(".js-draft-autosave-message").text("Review Draft autosaved " + timeFormatter(time));
                };

                $(".js-autosave").autoSave(function () {
                    submitDraftAutosave();
                }, AUTO_SAVE_PERIOD);

                $(".js-save-draft-click").on("click", function (event) {
                    $("#reviewDetailsForm").data('validator').cancelSubmit = true;
                    $("#reviewDetailsForm").validate().settings.ignore = "*";
                    $("#isDraft").val("1");
                    $("#reviewDetailsForm").submit();
                    return false;
                });

                
                validator = $('.js-submit-form').validate({
                    rules: {
                        Website: "required",
                        Location: "required",
                        //session atmosphere
                        Smokes: "required",
                        OverallLooks: "required",
                        Attitude: "required",
                        OverallAtmosphere: "required",
                        OverallPerformance: "required",
                        //visit details
                        GeneralDetails: "required",
                        TheJuicyDetails: "required",
                        ChemistryRating: "required"
                    },
                    messages: {
                        ChemistryRating: "Please select Chemistry star rating."
                    },
                    highlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").addClass("error");
                        $element.next(".btn-group").addClass("error");
                    },
                    unhighlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").removeClass("error");
                        $element.removeClass("error").next(".btn-group").removeClass("error");
                    },
                    ignore:"",
                    //TODO: move out to common js
                    invalidHandler: function(form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            if($(validator.errorList[0].element).is(":visible"))
                            {
                                validator.errorList[0].element.focus();
                            }
                            else
                            {
                                var elementOffset = $("#" + $(validator.errorList[0].element).attr("data-focusID")).offset().top;
                                if (elementOffset > $(window).height()){
                                    header = $(".header").height();
                                    $('html, body').animate({
                                        scrollTop: elementOffset - header - 30
                                    }, 1000);
                                }
                            }
                        }
                    }
                });
            });

        </script>

                                            <div class="js-draft-autosave-message warning-block" hidden="hidden"></div>

                                            <div class="da-form">
                                                <div class="review-form">
                                                    <form action="../mbrs/simple_review_submitted.php" method="POST" id="reviewDetailsForm" name="form1" class="js-autosave js-submit-form" novalidate="novalidate">
                                                        <input type="hidden" name="ReviewerStatusID" value="1">
                                                        <input type="hidden" name="ReviewerID" value="22">
                                                        <input type="hidden" name="CompanionID" value="<?php echo $person['UserID'];?>">
                                                        <input type="hidden" name="SubmittedOn" value="<?php date_default_timezone_set("America/New_York"); echo date("Y-m-d h:i:sa");?>">
                                                        <input type="hidden" name="ReviewStatus" value="0">                           

                                                <script language="javascript">
                                                    $(document).ready(function(){
                                                        $('#EncCountryID').change(function() {
                                                            var valchange = this.value;
                                                                $('#EncStateID').load('../includes/loadstates.php?countryId=' + valchange);
                                                            });
                                                        });
                                                </script>                                                        
                                                        
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    
                                                        
                                
                                                            <h5>encounter location</h5>
                                    
                                                            <div class="form-group-row">
                                                          
                                                                <div class="form-group col-xl-5 col-lg-5 col-md-5 col-sm-5">
                                                                    
                                                                    <label class="form-group-label">encounter country: </label></div>
                                                                    <div class="col-xl-4 col-lg-4 col-md-4">
                                                                    <select class="bootstrap-select form-control" name="EncCountryID" id="EncCountryID">
                                                                        <option value="0"><?php echo $select;?></option>
                                                                        
                                                                    <?php
                                                                        $sel = isset($_POST['EncCountryID'])?$_POST['EncCountryID']:0;
                                                                        $sqlcountry = 'select CountryID as Id, Country as LName from '.$table_prefix.'countries order by TopSorted desc, LName asc';
                                                                            dropdownlist($sqlcountry, $sel);
                                                                    ?>
                                                                    
                                                                    </select>
                                                                    
                                                                    <span class="required"><sup> * (required)</sup></span>
                                                                    </div>
                                                                    
                                                                </div>       

                                                                <div class="form-group">
                                                                    <label class="form-group-label">encounter state: </label>
                                                                    <select class="bootstrap-select form-control" name="EncStateID" id="EncStateID">
                                                                    <?php
                                                                        if(isset($_POST['EncCountryID']) && $_POST['EncCountryID']>0){
                                                                            $sql = 'select StateID as Id, State as LName from '.$table_prefix.'states where CountryID = '.intval($_POST['EncCountryID']).' order by TopSorted desc, LName asc';
                                                                                dropdownlist($sql, $_POST['EncStateID']);
                                                                                }
                                                                            else{
                                                                            echo '<option value="0">'.$select.'</option>';
                                                                        }
                                                                    ?>
                                                                    </select>
                                                                    
                                                                    <span class="required"><sup> * (required)</sup></span>
                                                              
                                                                </div>                                                                
                                                                
                                                                <div class="form-group">
                                                                    <label class="form-group-label">encounter city: </label>
                                                                    <input type="text" class="form-control js-websitegroup-validation" name="EncCity" value="" maxlength="100" required><span class="required"><sup> * (required)</sup></span>
                                                                </div>
                                                                
                                                                <br>
                                                            
                                                                <h5>description of encounter with your companion</h5>
                                                            
                                                                <div class="form-group">
                                                                    <label class="form-group-label ">date of encounter: </label>
                                                                    <select class="bootstrap-select form-control" style="max-width:226px;" name="EncMonth" tabindex="-98" required>
                                                                        <option value="01">january</option>
                                                                        <option value="02">february</option>
                                                                        <option value="03">march</option>
                                                                        <option value="04">april</option>
                                                                        <option value="05">may</option>
                                                                        <option value="06">june</option>
                                                                        <option value="07">july</option>
                                                                        <option value="08">august</option>
                                                                        <option value="09">september</option>
                                                                        <option value="10">october</option>
                                                                        <option value="11">november</option>
                                                                        <option value="12">december</option>
                                                                    </select>
                                                                
                                                                    <select class="bootstrap-select form-control" style="max-width:120px;" name="EncYear" tabindex="-98">
                                                                        <option value="2018">2018</option>
                                                                        <option value="2017">2017</option>
                                                                        <option value="2016">2016</option>
                                                                    </select>
                                                                    
                                                                    <span class="required"><sup> * (required)</sup></span>
                                                                    
                                                                </div>
                                                                
                                                                <div class="form-group">
                                                                    <label class="form-group-label ">encounter location: </label>
                                                                    <select class="bootstrap-select form-control" id="EnLocationID" name="EncLocationID" title="" tabindex="-98">
                                                                        <option value="" label="- select one -"></option>
                                                                        <option value="1">her place</option>
                                                                        <option value="2">her hotel</option>
                                                                        <option value="3">my place</option>
                                                                        <option value="4">my hotel</option>
                                                                        <option value="5">other</option>
                                                                    </select>
                                                                    
                                                                    <span class="required"><sup> * (required)</sup></span>
                                                                    
                                                                </div>
                                                            
                                                            </div>
                                                            
                                                        </div>
                                                
                                                        <br>
                                                
                                                        <div class="review-form-content">
                                                        
                                                            <h5>encounter atmosphere</h5>
    
                                                            <div class="form-horizontal">                               

            <!-- begin TYPE OF ENCOUNTER? -->                                                
                                                                <div class="form-group">
                                                                    <label class="form-group-label">type of encounter: </label>
                                                                
                                                                    <select class="bootstrap-select form-control" name="EncTypeID" title="" tabindex="-98">
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="1">gfe - girlfriend exp</option>
                                                                        <option value="2">pse - pornstar exp</option>
                                                                        <option value="3">erotic massage - body rub</option> 
                                                                        <option value="4">blow job</option>
                                                                        <option value="5">body slide - companion slid their naked body along your body</option>
                                                                        <option value="6">hand job</option>
                                                                        <option value="7">anal sex (also known as greek)</option>
                                                                        <option value="8">tie &amp; tease - companion gently tied your hands and/or feet</option>
                                                                        <option value="9">fantasy - companion dressed up in a costume and /or role-play</option>
                                                                        <option value="10">b &amp; d - being whipped, flogged, caned, tied up, told what to do</option>
                                                                        <option value="11">s &amp; m - a more extreme form of b&amp;d</option>                                   
                                                                    </select>
                                                                    
                                                                    <span class="required"><sup> * (required)</sup></span>
                                                                
                                                                </div>
            <!-- begin OVERALL LOOKS? -->                                                
                                                                <div class="form-group">
                                                                    <label class="form-group-label">companion's overall looks: </label>
                                                                
                                                                    <select class="bootstrap-select form-control" name="CompLooksID" title="" tabindex="-98">
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="1">1 - i was frightened</option>
                                                                        <option value="2">2 - needed a blindfold</option>
                                                                        <option value="3">3 - ugly</option>
                                                                        <option value="4">4 - ok if i was drunk</option>
                                                                        <option value="5">5 - average</option>
                                                                        <option value="6">6 - pretty</option>
                                                                        <option value="7">7 - very attractive</option>
                                                                        <option value="8">8 - super hot</option>
                                                                        <option value="9">9 - could be a model</option>
                                                                        <option value="10">10 - i was in awe</option>
                                                                    </select>
                                                                    
                                                                    <span class="required"><sup> * (required)</sup></span>
                                                                
                                                                </div>
            <!-- end OVERALL LOOKS? -->                                                
            
            <!-- begin SMOKES? -->
                                                                <div class="form-group">
                                                                    
                                                                    <label class="form-group-label">does companion smoke: </label>
                                                                    <select class="bootstrap-select form-control" name="CompSmokeID" title="" tabindex="-98" required>
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="1">no</option>
                                                                        <option value="2">yes - but not during session</option>
                                                                        <option value="3">yes - during session</option>
                                                                        <option value="4">yes - thats all i could smell</option>
                                                                    </select>
                                                                    
                                                                    <span class="required"><sup> * (required)</sup></span>
                                                                    
                                                                </div>
            <!-- end SMOKES? -->

            <!-- begin COMP ATTITUDE -->
                                                                <div class="form-group">
                                                                    <label class="form-group-label ">companion's attitude: </label>
                                                                    <select class="bootstrap-select form-control" name="CompAttitudeID" title="" tabindex="-98">
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="1">apathetic - indifferent - lacked energy or concern</option>
                                                                        <option value="2">bitter - exhibited strong animosity</option>
                                                                        <option value="3">bored - gave off an air that they would rather be elsewhere</option>
                                                                        <option value="3">cynical - questioned sincerity and goodness of people</option>
                                                                        <option value="4">condescending - gave off a feeling of superiority</option>
                                                                        <option value="5">callous - unfeeling - insensitive</option>
                                                                        <option value="6">chill - super relaxed</option>
                                                                        <option value="7">contemplative - thoughtful, reflective</option>
                                                                        <option value="8">conventional - lacked spontaneity</option>
                                                                        <option value="9">fanciful - used their imagination</option>
                                                                        <option value="10">fanciful - used their imagination</option>
                                                                        <option value="11">gloomy - dark - sad</option>
                                                                        <option value="12">haughty - arrogant</option>
                                                                        <option value="13">hot-tempered - easily angered</option>
                                                                        <option value="14">intimate - very familiar</option>
                                                                        <option value="15">judgmental - critical</option>
                                                                        <option value="16">jovial - happy</option>
                                                                        <option value="17">matter-of-fact - not fanciful or emotional</option>
                                                                        <option value="18">mocking - treated with contempt</option>
                                                                        <option value="19">optimistic - hopeful, cheerful</option>
                                                                        <option value="20">patronizing - had air of condescension</option>
                                                                        <option value="21">pessimistic - saw the worst side of things</option>
                                                                        <option value="21">professional - performed like a pro</option>
                                                                        <option value="22">ridiculing - made fun of</option>
                                                                        <option value="23">sincere - without deceit or pretense; genuine</option>
                                                                        <option value="24">whimsical - odd, strange, fantastic; fun</option>
                                                                    </select>
                                                                    
                                                                    <span class="required"><sup> * (required)</sup></span>
                                                                    
                                                                </div>
            <!-- end COMP ATTITUDE -->
            
            <!-- begin OVERALL ATMOSPHERE -->
                                                                <div class="form-group">
                                                                    <label class="form-group-label ">overall atmosphere: </label>
                                                                    <select class="bootstrap-select form-control" id="EncAtmosphereID" name="EncAtmosphereID" title="" tabindex="-98">
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="1">very scary</option>
                                                                        <option value="2">a bit scary</option>
                                                                        <option value="3">extremely awkward</option>
                                                                        <option value="4">uptight</option>
                                                                        <option value="5">like a first date</option>
                                                                        <option value="6">chill</option>
                                                                        <option value="7">sexy vibe</option>
                                                                        <option value="8">like a party</option>
                                                                        <option value="9">like a workout</option>
                                                                    </select>
                                                                    
                                                                    <span class="required"><sup> * (required)</sup></span>
                                                                    
                                                                </div>
            <!-- end OVERALL ATMOSPHERE -->
            
            <!-- begin OVERALL PERFORMANCE -->
                                                                <div class="form-group">
                                                                    <label class="form-group-label">overall performance: </label>
                                                                    <select class="bootstrap-select form-control" id="CompPerformanceID" name="CompPerformanceID" title="" tabindex="-98">
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="1">1 - a complete rip-off</option>
                                                                        <option value="2">2- i wish we hadn't met</option>
                                                                        <option value="3">3 - not worth the effort</option>
                                                                        <option value="4">4 - she just laid there</option>
                                                                        <option value="5">5 - average</option>
                                                                        <option value="6">6 - nice Time</option>
                                                                        <option value="7">7 - good time</option>
                                                                        <option value="8">8 - went the extra mile</option>
                                                                        <option value="9">9 - we were meant to be together</option>
                                                                        <option value="10">10 - she blew my mind</option>
                                                                    </select>
                                                                    
                                                                    <span class="required"><sup> * (required)</sup></span>
                                                                    
                                                                </div>
            <!-- end OVERALL PERFORMANCE -->
                                                                <br>

            
                                                            </div>
                                                        </div>
                                                        <br>
            <!-- begin VERIFICATIONS -->                                                
                                                        <div class="review-form-content">
                                                        
                                                            <h5>verifications</h5>
    
                                                            <div class="form-horizontal">              
            
                                                                <div class="form-group-radio">
                                                                
                                                                    <div class="radio">
                                                                        <label for="IndulgeTrue" class="form-group-label">indulgences were as described:</label>
                                                                        <input type="radio" id="IndulgeTrue" name="IndulgeTrue" value="1" checked>&nbsp;yes&nbsp;
                                                                        <input type="radio" id="IndulgeTrue" name="CompPhotoTrue" value="0">&nbsp;no&nbsp;                                                                            
                                                                        <span class="required"><sup> * (required)</sup></span>
                                                                    
                                                                        <br>
                                                                    
                                                                        <label for="photosAccurate" class="form-group-label">companion looked like photos:&nbsp;</label>
                                                                        <input type="radio" id="CompPhotoTrue" name="CompPhotoTrue" value="1" checked>&nbsp;yes&nbsp;
                                                                        <input type="radio" id="CompPhotoTrue" name="CompPhotoTrue" value="0">&nbsp;no&nbsp;                                                                            
                                                                        
                                                                        <span class="required"><sup> * (required)</sup></span>
                                                                    
                                                                        <br>
                                                                        
                                                                        <label for="CompOnTimeTrue" class="form-group-label">companion was on time:&nbsp;</label>
                                                                        <input type="radio" id="CompOnTimeTrue" name="CompOnTimeTrue" value="1" checked>&nbsp;yes&nbsp;
                                                                        <input type="radio" id="CompOnTimeTrue" name="CompOnTimeTrue" value="0">&nbsp;no&nbsp;
                                                                        <span class="required"><sup> * (required)</sup></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                                                            <br>
                                                            
                                                            <div class="review-form-content">
                                                        
                                                                <h5>general description of your encounter</h5>
    
                                                                <div class="form-horizontal">
                                                                    <div class="form-group">
                                                                        <label class="form-group-label ">headline for your review:</label>
                                                                        <input name="ReviewHeadline" value="" type="text" class="form-control" maxlength="60"><span class="required"><sup> * (required)</sup></span>
                                                                        <div class="textarea-feedback" style="margin-left:145px;color:#ff0000;">max 60 characters</div>
                                                                    </div>
                                                                
                                                            
                                                                    <br>
                                                            
                                                                    general summary should include broad comments about your companion and your initial experience. please do not mention specific acts or donation - just tease your fellow members.
                                                            
                                                                    <div class="form-group">
                                                                        <label class="form-group-label ">general summary:</label>
                                                                        <textarea class="bootstrap-select form-control review-form-textarea js-textarea " data-limit="4000" name="GenRevSummary" id="GenRevSummary" style="overflow: hidden; word-wrap: break-word; height: 400px;"></textarea><span class="required"><sup> * (required)</sup></span>
                                                                        <div class="textarea-feedback" style="margin-left:145px;color:#ff0000;">characters: <span class="textarea-feedback-content js-textarea-feedback" id="len1">4000 left</span>
                                                                        </div>
                                                                
                                                                    </div>
                                                            
                                                                    <br>
                                                            
                                                                    <h5>detailed description of encounter</h5>
                                                                    describe the provider, the experience, and whether or not you enjoyed it in graphic emotional and sexual terms. Don't make this space a recap of the General section. Instead, go for a blow-by-blow tell-all of your session with the provider from your own unique point of view.
                                                                    <div class="form-group">
                                                                        <label class="form-group-label ">sexy details: </label>
                                                                        <textarea class="bootstrap-select form-control review-form-textarea js-textarea " data-limit="6000" name="SexyRevDetails" id="SexyRevDetails" style="overflow: hidden; word-wrap: break-word; height: 400px;"></textarea><span class="required"><sup> * (required)</sup></span>
                                                                        <div class="textarea-feedback" style="margin-left:145px;color:#ff0000;">characters: <span class="textarea-feedback-content js-textarea-feedback" id="len1">6000 left</span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                
                                                        <br>
                                                
                                                        <div class="bottom-buttons">
                                                            <button class="btn btn-cli-pro" data-toggle="modal2" data-target="#submitReview">submit</button>
                                                            <button type="button" formnovalidate="formnovalidate" class="btn btn-pro-cli js-save-draft-click">save draft</button>
                                                        </div>
                                                
                                                <div class="modal fade" id="submitReview" tabindex="-1" role="dialog" aria-labelledby="submitReviewLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                    <h5 class="modal-title" id="submitReviewLabel">Error</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <span class="icon-error">!</span>
                                                                <p>Your review wasn't added. Please try to do it later.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                            
