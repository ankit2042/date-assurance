                            
                                 <div class="card-lftside">
                                    <div class="card-header"><i class="far fa-comment"></i>&nbsp;private messages</div>
                                    <div class="card-body">
                                        <div class="list-group list-group-flush">
                                            <a class="list-group-item list-group-item-active" href="#">
                                                <div class="media">
                                                    <img class="d-flex mr-3 img-circle img-thumbnail-profile" src="../imgs/pro-profiles/c-d-profile1.jpg" width="50" height="50" alt="">
                                                    <div class="media-middle">
                                                        <span class="bold-text">pro-amy from Washington DC</span> added you to her client list<br>
                                                        <div class="small-text">today at 5:43 PM - 5m ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="list-group-item list-group-item-active" href="#">
                                                <div class="media">
                                                    <img class="d-flex mr-3 img-circle img-thumbnail-profile" src="../imgs/pro-profiles/c-d-profile2.jpg" width="50" height="50" alt="">
                                                    <div class="media-middle">
                                                        <span class="bold-text">pro-amy from Washington DC</span> sent you a new message.<br>
                                                        <div class="small-text">today at 5:43 PM - 5m ago</div>
                                                    </div>
                                                </div>
                                            </a>                                            
                                            <a class="list-group-item list-group-item-active" href="#">
                                                <div class="media">
                                                    <img class="d-flex mr-3 img-circle img-thumbnail-profile" src="../imgs/pro-profiles/c-d-profile3.jpg" width="50" height="50" alt="">
                                                    <div class="media-middle">
                                                        <span class="bold-text">pro-amy from Washington DC</span> sent you a new message.
                                                        <div class="small-text">today at 5:43 PM - 5m ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <br>
                                        <div class="pull-right"><a href="#">view all private messages</a></div>
                                    </div>
                                    
                                    <div class="card-header"><i class="fas fa-link"></i>&nbsp;quick links</div>
                                    <div class="card-body">
                                        <div class="left-link"><a href="listMyReviews.asp"><span>reviews of me</span></a></div>
                                        <div class="left-link"><a href="myfavoriteReviews.asp"><span>my ratings of members</span></a></div>
                                        <div class="left-link"><a href="myfavorites.asp"><span>my favorite members</span></a></div>
                                        <div class="left-link"><a href="myfavorites.asp"><span>view my profile</span></a></div>
                                        <div class="left-link"><a href="account_manager/myPosts.asp"><span>my membership settings</span></a></div>
                                        <div class="left-link"><a href="account_manager/feedback.asp"><span>my feedback</span></a></div>
                                        <div class="left-link"><a href="account_manager/problemReports.asp"><span>manage my account</span></a></div>
                                        <div class="left-link"><a href="account_manager/myWhiteList.asp"><span>my referrals</span></a></div>
                                        <div class="left-link"><a href="discussion-boards/newbie-33/ters-instruction-manual-75528"><span>member support</span></a></div>
                                    </div>                                            


                                </div><!----><!---->
                            </div><!----><!---->