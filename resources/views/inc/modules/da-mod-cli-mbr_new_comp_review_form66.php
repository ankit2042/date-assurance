<?php
require_once "../includes/config.php";

//****    CHECKS IF THERE IS A LOGIN SESSION   *****/
    if(!$_SESSION['memberid'] || $_SESSION['memberid']==0){
	header('Location: ../signin/da-cli-mbr_signin.php');
	exit();
	}
    require_once "../includes/database.php";
    require_once "../includes/functions6.php";

if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['smcompdetails'])){
	if(empty($_POST['profilename']))
		$err_p = $profilenull;
	elseif(strlen($_POST['profilename'])<6)
		$err_p = $datashort;
	elseif(empty($_POST['aboutme']))
		$err_a = $aboutnull;
	elseif(strlen($_POST['aboutme'])<6)
		$err_a = $datashort;
	elseif(intval($_POST['slCountry'])==0)
		$err_co = $countrynull;
	elseif(intval($_POST['slState'])==0)
		$err_ci = $statenull;
	else{
		$data = array(
                    mysql_real_escape_string($_POST['profilename']) 
                    , mysql_real_escape_string($_POST['aboutme'])
                    , $_POST['rdGender']
                    , $_POST['slAge']
                    , $_POST['slCountry']
                    , $_POST['slState']
                    , mysql_real_escape_string($_POST['txtcity'])
                    , mysql_real_escape_string($_POST['zipcode'])
                    , mysql_real_escape_string($_POST['phone'])
                    , $_POST['slEducation']
                    , $_POST['slEthnicity']    
                    , $_POST['slSexOr']
                    , $_POST['rdTranssexual']
                    , $_POST['rdTransgender']
                    , $_POST['rdIndep']
                    , $_POST['rdPornstar']
                    , mysql_real_escape_string($_POST['compwebsite'])
                    , mysql_real_escape_string($_POST['compwebprof'])
                    , mysql_real_escape_string($_POST['compwhatsapp'])
                    , mysql_real_escape_string($_POST['compinstagram'])    
                    , $_POST['slBodyType']
                    , $_POST['slHeight']                    
                    , $_POST['slHairColor']
                    , $_POST['slHairLength']                    
                    , $_POST['slHairType']
                    , $_POST['slEyeColor']
                    , $_POST['slPiercings']
                    , $_POST['slTattoos']                        
                    , $_POST['slPussy']
                    , $_POST['slCompBreasts']
                    , $_POST['rdBreastImp']
                    , $_POST['slCompAss']    
                    , $_POST['slSmoking']
                    , $_POST['slDrinking']);
		if(!yourcompdetails($data, $_SESSION['user_temp']))      // yourdetails is what UPDATES DB
			$error = $errordata;
		else{
			mysql_close();
			header('Location: '.$base_url.'mbrs/da-da-cli-mbr_submit_new_comp_review_compose_pg2.php');
			exit();
			}
		}
	}
$title = $climbrreg;        
?>        

<!DOCTYPE html>
<html>
<head lang="en">

    <?php
        require_once "../inc/da-header-meta.php";
        
        echo "<title>".$sitename." - ".$title."</title>";
        
        require_once "../inc/da-home_header_links_scripts.php";
    ?>
    
<script src="<?php echo $base_url;?>da-ter-based-files/api.js"></script>

    <?php
        require_once "../inc/da-header-favicons.php";
    ?>        

    <link rel="stylesheet" type="text/css" href="<?php echo $base_url;?>css/da-site_style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url;?>css/da-home-nav_style.css">

</head>

<body data-spy="scroll" data-offset="25">
    <div class="animationload">
        <div class="loader">Loading...</div>            
    </div> <!-- End Preloader -->
 
    <?php
        require_once "../inc/da-home_topnav.php";
    ?>

<div class="da-margin-top">    
            
    <!-- begin "MAIN CONTENT" section -->
    
    <div class="container"><!---->
        
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="reg">
                <div class="card">
                    <div class="card-body">
                        <div class="section-title-center">companion <?php echo $title;?></div>
                                        
                        <div class="tag-line">enjoy all the mutual benefits <?php echo $sitename;?> can provide you</div>
                            <div align="center">
                                
    <!-- begin SHOW STEPS  -->                                    
                            <?php 
                                $isstep = 2;
                                require_once "../inc/modules/da-comp_step_reg1.php";
                            ?>
    <!-- end SHOW STEPS  -->
            
                            <p class="intro">step 2 - <?php echo $aboutme;?></p>

    <!-- begin GENERAL ERROR MESSAGE -->                        
                            <?php
                                if(isset($error) && !empty($error))
                                    echo '<p style="margin:0px; padding:5px 20px"><span style="color:#FF0000;font-size:18px;font-weight:500;font-style:italic;">'.$error.'</span></p>';
                            ?>
    <!-- end GENERAL ERROR MESSAGE -->                  
                            
                            </div>
                        
                            <script language="javascript">
                                $(document).ready(function(){
                                    $('#slCountry').change(function() {
                                    var valchange = this.value;
                                    $('#slState').load('../includes/loadstates.php?countryId=' + valchange);
                                    });
                                });
                            </script>
                            
                            <br>
                            
                            <form action="" method="post">

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
   
                                <h5 style="color:#813ee8;">about your companion</h5>
                                <hr style="color:#813ee8;">

                                <div class="row">
                                    <div class="col-lg-1">&nbsp;</div>
                                
                                <div class="col-lg-10">
                                    <div class="form-group row">
                                        <label for="profilename" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">create a <?php echo $profilename;?>: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="profilename" value="<?php echo isset($_POST['profilename'])?$_POST['profilename']:'';?>" required>
                                            <small id="profileNameHelpBlock" class="form-text text-muted">
                                                please enter a profile name between 6-20 characters. your profile name will be seen by members and other companions.
                                            </small>
                                            
            <!-- begin PROFILE NAME ERROR MESSAGE -->
                                        <?php
                                            if(isset($err_p) && !empty($err_p))
                                                echo '<br><span style="color:#FF0000;font-size:16px;font-weight:500;font-style:italic;">'.$err_p.'</span>';
                                        ?>
            <!-- end PROFILE NAME ERROR MESSAGE -->
            
                                        </div>    
                                    </div>
                                </div>
                                <div class="col-lg-2">&nbsp;</div>
                            </div>
    
                            <div class="row">
                                <div class="col-lg-12">
                                    <br>
                                    <h6 style="text-align:center;color:#813ee8;">tell us about yourself</h6>
                                    <br>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-lg-2">&nbsp;</div>
                                
                                <div class="col-lg-8">
                                    <div class="form-group row">
                                        <label for="aboutme" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $aboutyou;?>: </label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" id="aboutme" name="aboutme" value="<?php echo isset($_POST['aboutme'])?$_POST['aboutme']:'';?>" rows="3" required></textarea>
                                            <small id="aboutmeHelpBlock" class="form-text text-muted">
                                                please enter a brief narrative about yourself.  it can include what you like to do for fun or how you like your companions.
                                            </small>
                                        
            <!-- begin ABOUT YOU ERROR MESSAGE -->                                               
                                        <?php
                                            if(isset($err_a) && !empty($err_a))
                                                echo '<br><span style="color:#FF0000;font-size:16px;font-weight:500;font-style:italic;">'.$err_a.'</span>';
                                        ?>
            <!-- end ABOUT YOU ERROR MESSAGE -->
            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">&nbsp;</div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-2">&nbsp;</div>
                                
                                <div class="col-lg-8">
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;"><?php echo $youare;?>: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdGender'])){
                                                    if($_POST['rdGender']==1){
                                                        $gen1 = ' checked="checked"';
                                                        $gen2 = '';
                                                        }
                                                    elseif($_POST['rdGender']==2){
                                                        $gen1 = '';
                                                        $gen2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $gen1 = ' checked="checked"';
                                                        $gen2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-8">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $gen1;?> name="rdGender" value="1">
                                                    <label class="form-check-label" for="rdGender" style="font-weight:400;font-size:14px;"><?php echo $male;?></label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $gen2;?> name="rdGender" value="2">
                                                    <label class="form-check-label" for="rdGender" style="font-weight:400;font-size:14px;"><?php echo $female;?></label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-lg-2">&nbsp;</div>
                            </div>
                            
                            <div class="row">    
                                <div class="col-lg-2">&nbsp;</div>
                                
                                <div class="col-lg-8">
                                    <div class="form-group row">
                                        <label for="slAge" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $age;?>: </label>
                                        <div class="col-sm-3">
                                            <select class="form-control" name="slAge">
                                            <?php
						for($i=18; $i<76; $i++){
                                                    $val = isset($_POST['slAge'])?$_POST['slAge']:18;
                                                    $sel = ($val==$i)?' selected="selected"':'';
							echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
                                                    }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                
                                    <div class="form-group row">
                                        <label for="slCountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $country;?>: : </label>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="slCountry" id="slCountry">
                                                <option value="0"><?php echo $select;?></option>
                                            <?php
						$sel = isset($_POST['slCountry'])?$_POST['slCountry']:0;
						$sqlcountry = 'select CountryID as Id, Country as LName from '.$table_prefix.'countries order by TopSorted desc, LName asc';
                                                    dropdownlist($sqlcountry, $sel);
                                            ?>
                                            </select>
                                            
            <!-- begin COUNTRY ERROR MESSAGE -->                                                         
                                            <?php
						if(isset($err_co) && !empty($err_co))
                                                    echo '<br><span style="color:#FF0000;font-size:16px;font-weight:500;font-style:italic;">'.$err_co.'</span>';
                                            ?>
            <!-- end COUNTRY ERROR MESSAGE --> 
            
                                        </div>
            
                                    </div>
                                    

                                    <div class="form-group row">
                                        <label for="slState" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $state;?>: </label>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="slState" id="slState">
                                                <option value="0"><?php echo $select;?></option>
                                            <?php
						if(isset($_POST['slCountry']) && $_POST['slCountry']>0){
                                                    $sql = 'select StateID as Id, State as LName from '.$table_prefix.'states where CountryID = '.intval($_POST['slCountry']).' order by TopSorted desc, LName asc';
							dropdownlist($sql, $_POST['slState']);
                                                    }
                                                    else{
							echo '<option value="0">'.$select.'</option>';
							}
                                            ?>
                                            </select>
                                            
            <!-- begin STATE ERROR MESSAGE -->                                     
                                            <?php
						if(isset($err_ci) && !empty($err_ci))
							echo '<br><span style="color:#FF0000;font-size:16px;font-weight:500;font-style:italic;">'.$err_ci.'</span>';
                                            ?>
            <!-- end STATE ERROR MESSAGE -->
            
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="txtcity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $city;?>: </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="txtcity" name="txtcity" value="<?php echo isset($_POST['txtcity'])?$_POST['txtcity']:'';?>" maxlength="250" required>
                                        </div>
                                    </div>                                    

                                    <div class="form-group row">
                                        <label for="zipcode" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $zipcode;?>: </label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" size="5" name="zipcode" maxlength="5" value="<?php echo isset($_POST['zipcode'])?$_POST['zipcode']:'';?>" maxlength="12" required>
                                        </div>
                                    </div>                                

                                    <div class="form-group row">
                                        <label for="phone" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $phone;?>: </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" size="16" id="phone" name="phone" maxlength="16" value="<?php echo isset($_POST['phone'])?$_POST['phone']:'';?>" onkeypress="return numberPressed(event);">
                                            <small id="phoneHelpBlock" class="form-text text-muted">
                                                your phone number is required for features such as private messages to companions via webtext. no one from da&trade; will ever call.
                                            </small> 
                                            
                                            <script language="javascript">
                                                // Format the phone number as the user types it
                                                document.getElementById('phone').addEventListener('keyup',function(evt){
                                                    var phone = document.getElementById('phone');
                                                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                                                        phone.value = phoneFormat(phone.value);
                                                    });

                                                // We need to manually format the phone number on page load
                                                document.getElementById('phone').value = phoneFormat(document.getElementById('phone').value);

                                                // A function to determine if the pressed key is an integer
                                                function numberPressed(evt){
                                                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                                                        if(charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 36 || charCode > 40)){
                                                        return false;
                                                        }
                                                    return true;
                                                    }

                                                // A function to format text to look like a phone number
                                                function phoneFormat(input){
                                                // Strip all characters from the input except digits
                                                    input = input.replace(/\D/g,'');
        
                                                // Trim the remaining input to ten characters, to preserve phone number format
                                                    input = input.substring(0,10);

                                                // Based upon the length of the string, we add formatting as necessary
                                                var size = input.length;
                                                    if(size == 0){
                                                        input = input;
                                                        }else if(size < 4){
                                                            input = '('+input;
                                                            }else if(size < 7){
                                                                input = '('+input.substring(0,3)+') '+input.substring(3,6);
                                                                }else{
                                                                    input = '('+input.substring(0,3)+') '+input.substring(3,6)+' - '+input.substring(6,10);
                                                                    }
                                                                return input; 
                                                        }
                                            </script>
                                         
                                        </div>
                                    </div>                                
                                    
                                    <div class="form-group row">
                                        <label for="slEducation" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $education;?>: </label>
                                        <div class="col-sm-7">
                                            <select class="form-control" id="slEducation" name="slEducation">
                                            <?php
						$sel = isset($_POST['slEducation'])?$_POST['slEducation']:1;
						$sql = 'select EducationID as Id, '.$_SESSION['lang'].'Education as LName from '.$table_prefix.'educations order by TopSorted desc, LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                            <small id="educationHelpBlock" class="form-text text-muted">
                                                please select your education level. this allows companions to better know you.
                                            </small>
                                        </div>
                                    </div>                                

                                    <div class="form-group row">
                                        <label for="slEthnicity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $ethnicity;?>: </label>
                                        <div class="col-sm-7">
                                            <select class="form-control" id="slEthnicity" name="slEthnicity">
                                            <?php
						$sel = isset($_POST['slEthnicity'])?$_POST['slEthnicity']:1;
                                                $sql = 'select EthnicityID as Id, '.$_SESSION['lang'].'Ethnicity as LName from '.$table_prefix.'ethnicity order by TopSorted desc, LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>                                     

                                    <div class="form-group row">
                                        <label for="slSexOr" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">sexual orientation: </label>
                                        <div class="col-sm-7">
                                            <select class="form-control" id="slSexO" name="slSexOr">
                                            <?php
						$sel = isset($_POST['slSexOr'])?$_POST['slSexOr']:1;
                                                $sql = 'select SexOrID as Id, '.$_SESSION['lang'].'SexOr as LName from '.$table_prefix.'sexor order by TopSorted desc, LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div> 

            <!-- begin TRANSSEXUAL -->                        

                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">transsexual: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdTranssexual'])){
                                                    if($_POST['rdTranssexual']==1){
                                                        $transs1 = ' checked="checked"';
                                                        $transs2 = '';
                                                        }
                                                    elseif($_POST['rdTranssexual']==2){
                                                        $transs1 = '';
                                                        $transs2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $transs1 = '';
                                                        $transs2 = ' checked="checked"';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-8">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $transs1;?> name="rdTranssexual" value="1">
                                                    <label class="form-check-label" for="rdTranssexual" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $transs2;?> name="rdTranssexual" value="2">
                                                    <label class="form-check-label" for="rdTranssexual" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>  
            
            <!-- end TRANSSEXUAL -->
            
            <!-- begin TRANSGENDER -->

                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">transgender: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdTransgender'])){
                                                    if($_POST['rdTransgender']==1){
                                                        $transg1 = ' checked="checked"';
                                                        $transg2 = '';
                                                        }
                                                    elseif($_POST['rdTransgender']==2){
                                                        $transg1 = '';
                                                        $transg2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $transg1 = '';
                                                        $transg2 = ' checked="checked"';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-8">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $transg1;?> name="rdTransgender" value="1">
                                                    <label class="form-check-label" for="rdTransgender" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $transg2;?> name="rdTransgender" value="2">
                                                    <label class="form-check-label" for="rdTransgender" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset> 
            <!-- end TRANSGENDER -->

            <!-- begin INDEPENDENT -->
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">independent: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdIndep'])){
                                                    if($_POST['rdIndep']==1){
                                                        $indep1 = ' checked="checked"';
                                                        $indep2 = '';
                                                        }
                                                    elseif($_POST['rdIndep']==2){
                                                        $indep1 = '';
                                                        $indep2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $indep1 = ' checked="checked"';
                                                        $indep2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-8">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $indep1;?> name="rdIndep" value="1">
                                                    <label class="form-check-label" for="rdIndep" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $indep2;?> name="rdIndep" value="2">
                                                    <label class="form-check-label" for="rdIndep" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>    
            <!-- end INDEPENDENT -->           
                                                                
            <!-- begin PORNSTAR -->
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">porn star: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdPornstar'])){
                                                    if($_POST['rdPornstar']==1){
                                                        $pornstar1 = ' checked="checked"';
                                                        $pornstar2 = '';
                                                        }
                                                    elseif($_POST['rdPornstar']==2){
                                                        $pornstar1 = '';
                                                        $pornstar2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $pornstar1 = '';
                                                        $pornstar2 = ' checked="checked"';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-8">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $pornstar1;?> name="rdPornstar" value="1">
                                                    <label class="form-check-label" for="rdPornstar" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $pornstar2;?> name="rdPornstar" value="2">
                                                    <label class="form-check-label" for="rdPornstar" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>    
            <!-- end PORNSTAR --> 
            
            <!-- begin COMPANION WEBSITE -->               
                                    <div class="form-group row">
                                        <label for="compwebsite" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your website: </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="compwebsite" name="compwebsite" value="<?php echo isset($_POST['compwebsite'])?$_POST['compwebsite']:'';?>" maxlength="250">
                                        </div>
                                    </div>                                     
            <!-- end COMPANION WEBSITE -->
            
            <!-- begin COMPANION WEB PROFILE -->               
                                    <div class="form-group row">
                                        <label for="compwebprof" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your profile on the web: </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="compwebprof" name="compwebprof" value="<?php echo isset($_POST['compwebprof'])?$_POST['compwebprof']:'';?>" maxlength="250">
                                        </div>
                                    </div>                                     
            <!-- end COMPANION WEB PROFILE -->             
            
            <!-- begin COMPANION WHATSAPP -->                   
                                    <div class="form-group row">
                                        <label for="compwhatsapp" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your whatsapp: </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="compwhatsapp" name="compwhatsapp" value="<?php echo isset($_POST['compwhatsapp'])?$_POST['compwhatsapp']:'';?>" maxlength="250">
                                        </div>
                                    </div>                               
            <!-- end COMPANION WHATSAPP -->             

            <!-- begin COMPANION INSTAGRAM -->                   
                                    <div class="form-group row">
                                        <label for="compinstagram" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your instagram: </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="compinstagram" name="compinstagram" value="<?php echo isset($_POST['compinstagram'])?$_POST['compinstagram']:'';?>" maxlength="250">
                                        </div>
                                    </div>                               
            <!-- end COMPANION INSTAGRAM --> 
            
                                </div>
                                <div class="col-lg-2">&nbsp;</div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <br>
                                    <h6 style="text-align:center;color:#813ee8;">please describe your looks</h6>
                                    <br>
                                </div>
                            </div>
                                
                            <div class="row">
                                <div class="col-lg-2">&nbsp;</div>
                                <div class="col-lg-8">                                    
                                    
            <!-- begin BODYTYPE -->                        
                                    <div class="form-group row">
                                        <label for="slBodyType" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $bodytype;?>: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slBodyType" name="slBodyType">
                                            <?php
						$sel = isset($_POST['slBodyType'])?$_POST['slBodyType']:1;
						$sql = 'select BodyTypeID as Id, '.$_SESSION['lang'].'BodyType as LName from '.$table_prefix.'bodytypes order by TopSorted desc, LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>                                   
            <!-- end BODYTYPE -->
            
            <!-- begin HEIGHT -->
                                    <div class="form-group row">
                                        <label for="slHeight" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your <?php echo $height;?>: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slHeight" name="slHeight">
                                                <option value="0"><?php echo $select;?></option>

                                                <?php 
                                                    $sel = isset($_POST['slHeight'])?$_POST['slHeight']:358;
                                                    $sql = 'select HeightID as Id, HeightDescription as LName from '.$table_prefix.'height order by HeightID asc';
                                                        dropdownlist($sql, $sel);
                                                ?>
                                                
                                            </select> 
                                        </div>
                                    </div>
            <!-- end HEIGHT -->
            
            <!-- begin HAIRCOLOR -->
                                    <div class="form-group row">
                                        <label for="slHairColor" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $haircolor;?>: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slHairColor" name="slHairColor">
                                            <?php
						$sel = isset($_POST['slHairColor'])?$_POST['slHairColor']:1;
						$sql = 'select HairColorID as Id, '.$_SESSION['lang'].'HairColor as LName from '.$table_prefix.'haircolors order by TopSorted desc, LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>                                   
            <!-- end HAIRCOLOR -->

            <!-- begin HAIRLENGTH -->
                                    <div class="form-group row">
                                        <label for="slHairLength" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">hair length: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slHairLength" name="slHairLength">
                                            <?php
						$sel = isset($_POST['slHairLength'])?$_POST['slHairLength']:1;
						$sql = 'SELECT HairLengthID as Id, '.$_SESSION['lang'].'HairLength as LName from '.$table_prefix.'hairlengths order by TopSorted desc, LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>                                   
            <!-- end HAIRLENGTH -->

            <!-- begin HAIRTYPE -->
                                    <div class="form-group row">
                                        <label for="slHairType" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">hair type: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slHairType" name="slHairType">
                                            <?php
						$sel = isset($_POST['slHairType'])?$_POST['slHairType']:1;
						$sql = 'SELECT HairTypeID as Id, '.$_SESSION['lang'].'HairType as LName from '.$table_prefix.'hairtypes order by TopSorted desc, LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>                                   
            <!-- end HAIRTYPE -->

            <!-- begin EYECOLOR -->
                                    <div class="form-group row">
                                        <label for="slEyeColor" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $eyeColor;?>: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slEyeColor" name="slEyeColor">
                                            <?php
						$sel = isset($_POST['slEyeColor'])?$_POST['slEyeColor']:1;
						$sql = 'select EyeColorID as Id, '.$_SESSION['lang'].'EyeColor as LName from '.$table_prefix.'eyescolors order by TopSorted desc, LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>                                                                       
            <!-- end EYECOLOR -->
            

            <!-- begin PIERCINGS -->
                                    <div class="form-group row">
                                        <label for="slPiercings" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">piercings: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slPiercings" name="slPiercings">
                                            <?php
						$sel = isset($_POST['slPiercings'])?$_POST['slPiercings']:1;
						$sql = 'select PiercingTypeID as Id, '.$_SESSION['lang'].'PiercingType as LName from '.$table_prefix.'piercings order by LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>                                                                       
            <!-- end PIERCINGS -->

            <!-- begin TATTOOS -->
                                    <div class="form-group row">
                                        <label for="slTattoos" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">tattoos: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slTattoos" name="slTattoos">
                                            <?php
						$sel = isset($_POST['slTattoos'])?$_POST['slTattoos']:1;
						$sql = 'select TattoosTypeID as Id, '.$_SESSION['lang'].'TattooType as LName from '.$table_prefix.'tattoos order by LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>                                                                       
            <!-- end TATTOOS -->
            
            <!-- begin PUSSY GROOMING -->
                                    <div class="form-group row">
                                        <label for="slPussy" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">pussy grooming: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slPussy" name="slPussy">
                                            <?php
						$sel = isset($_POST['slPussy'])?$_POST['slPussy']:1;
						$sql = 'select PussyID as Id, '.$_SESSION['lang'].'Pussy as LName from '.$table_prefix.'pussy order by LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>                                                                       
            <!-- end PUSSY GROOMING -->            
            
            <!-- begin BREASTS -->
                                    <div class="form-group row">
                                        <label for="slCompBreasts" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">breasts size: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slCompBreasts" name="slCompBreasts">
                                            <?php
						$sel = isset($_POST['slCompBreasts'])?$_POST['slCompBreasts']:1;
						$sql = 'select CompBreastsID as Id, '.$_SESSION['lang'].'CompBreasts as LName from '.$table_prefix.'compbreasts order by CompBreastsID asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>                                                                       
            <!-- end BREASTS -->                        
            
            <!-- begin BREAST IMPLANTS -->
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">breast implants: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdBreastImp'])){
                                                    if($_POST['rdBreastImp']==1){
                                                        $breastimp1 = ' checked="checked"';
                                                        $breastimp2 = '';
                                                        }
                                                    elseif($_POST['rdBreastImp']==2){
                                                        $breastimp1 = '';
                                                        $breastimp2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $breastimp1 = ' checked="checked"';
                                                        $breastimp2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-8">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $breastimp1;?> name="rdBreastImp" value="1">
                                                    <label class="form-check-label" for="rdBreastImp" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $breastimp2;?> name="rdBreastImp" value="2">
                                                    <label class="form-check-label" for="rdBreastImp" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>    
            <!-- end BREAST IMPLANTS -->             
            
            <!-- begin ASS -->
                                    <div class="form-group row">
                                        <label for="slCompAss" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">ass: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slCompAss" name="slCompAss">
                                            <?php
						$sel = isset($_POST['slCompAss'])?$_POST['slCompAss']:1;
						$sql = 'select CompAssID as Id, '.$_SESSION['lang'].'CompAss as LName from '.$table_prefix.'compass order by CompAssID asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>                                                                       
            <!-- end ASS -->                              
            
            
            
            
            
            
 
                                </div>
                                <div class="col-lg-2">&nbsp;</div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <br>
                                    <h6 style="text-align:center;color:#813ee8;">please describe your habits</h6>
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">&nbsp;</div>
                                
                                <div class="col-lg-8">
                                    <div class="form-group row">

                                        <label for="slSmoking" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $smoking;?>: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slSmoking" name="slSmoking">
                                            <?php
						$sel = isset($_POST['slSmoking'])?$_POST['slSmoking']:1;
						$sql = 'select SmokingID as Id, '.$_SESSION['lang'].'Smoking as LName from '.$table_prefix.'smoking order by TopSorted desc, LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="slDrinking" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;"><?php echo $drinking;?>: </label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="slDrinking" name="slDrinking">
                                            <?php
						$sel = isset($_POST['slDrinking'])?$_POST['slDrinking']:1;
						$sql = 'select DrinkingID as Id, '.$_SESSION['lang'].'Drinking as LName from '.$table_prefix.'drinking order by TopSorted desc, LName asc';
                                                    dropdownlist($sql, $sel);
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                    
            <!-- begin PRIVACY -->
                                    <div class="form-group row">
                                        <label for="privacy" class="col-sm-4 col-form-label"></label>
                                        <div class="col-sm-8">
                                            we value your privacy and will not market, sell, or otherwise voluntarily disclose any of your personal information. view our <a data-toggle="modal" href="#privacyModal">privacy policy</a>.
                                        </div>
                                    </div>
            <!-- end PRIVACY -->                                                
                         
                                    <div class="form-group row">
                                        <div class="col-sm-4">&nbsp;</div>
                                        <div class="col-sm-8">
                                            <input type="submit" value="<?php echo $continue;?>" class="btn btn-cli-pro" name="smcompdetails">
                                            <input type="reset" value="<?php echo $cancel;?>" class="btn btn-pro-cli">
                                        </div>
                                    </div>
                                </form>
                                </div>
                                
                                <div class="col-lg-2">&nbsp;</div>
                                
                                </div>
                    </div> 
                </div> 
                </div> <!-- class "reg" -->
            </div>
        </div>
    </div>
</div>


<?php
    require_once "../inc/da-cli-mbr_footer.php";
    require_once "../inc/da-mbr_modals.php";
?> 

</body></html>