<div style="overflow: auto; width:100%; height:400px;">
    
    
    <div style="text-align:center">

        <a href="../mbrs/da-cli-mbr_submit_exst_comp_review_compose.php?id=<?php echo $person['UserID'];?>" class="btn btn-pro-cli"><span class="">i agree</span></a><!----><!---->

    </div>
    
<h6>guidelines for submitting a review:</h6>
<ol>
    <li>you must be a member in good-standing to submit a review.</li>
              
    <li>be fair and honest in your reviews - try not to be swayed – for good or bad – by any emotions you may feel immediately following an encounter as your feelings may change over the next day or two.  
        FAIR WARNING: We will not post any review that slams or denigrates a companion, nor will we post reviews that contain such derogatory terms such as: cunt, bitch, slut  or any other wording that we 
        deem inappropriate or hurtful.   da&trade; has created a platform where members can help other members find exactly what they are seeking – or warn someone away from a companion that doesn’t look, 
        act or perform as described.</li>

    <li>members appreciate being warned about ROB’s, bait &amp; switch companions, or dangerous or dirty situations.  Some members are sensitive to dirt, animals, noise or smoking, so be sure to mention 
        anything of note in your review.</li>

    <li>the companion being reviewed must have a current posting on the web that includes their name and at least one type of the following contact information: phone number, pager, email address or 
        reservation form.</li>

    <li>a phone number isn't required for posting.  if the companion doesn't wish to give out their number, please respect her decision.  type 555-555-1212 in the phone number field instead of a real number.</li>

    <li>you must also enter the companion’s web address in the "web site" field, along with the companion's site name.  if she posts on an adult mall, a link that takes us directly to her ad will speed up your 
        review process.  to make the link, find the picture of the companion at the mall site and right click (or SHIFT+CLICK) on their photo.  a pop up menu will appear. choose the option to “Open in a new window.” 
        the direct link will appear in the address bar above. copy and paste that address into your review.  remember, names of escort agencies or newspapers aren’t good enough.  Classified ads are also unacceptable.  
        Only URL or web links will pass muster.</li>

    <li>massage parlors reviews are not accepted</li>

    <li>in order to keep our overall performance ratings consistent, we set up the following system: a companion is only eligible to earn up to a 4, unless she performs one or some of the following during a session: 
        kisses with tongue, uncovered blow job, is really bi, anal sex, or more than one guy. for each of these indulgences that are performed, her potential max score is raised by one point, with 10 as an absolute 
        maximum. Remember, it’s your review; within these guidelines you still get to decide what her score should be.  You do not have to give her the ‘extra point’.  If your review rates a companion higher than her 
        maximum score, the score will be adjusted down to her maximum.</li>

    <li>you may also submit an update on a companion that you have already reviewed. your new review must be a complete rewrite that adds something new to the experience. your old review will remain.</li>

    <li>reviews written more than 60 days after an encounter will not be accepted.</li>

    <li>both the General and Juicy Details section have been set up to house elaborate descriptions of your encounter. the General Details section should contain generalized, broader comments about the companion 
        and your experience. At this point, do not mention specific sexual acts or price; just give the members a tease. The Juicy Details section should be used to describe the companion, the experience, and 
        whether or not you enjoyed it in graphic emotional and sexual terms. Don’t make this space a recap of the General section. Instead, go for a blow-by-blow tell-all of your session with the companion from 
        your own unique point of view. Remember, your opinion matters! Both the General and Juicy details should be at least four lines long or (preferably) longer.  Make your reviews candid, informative and 
        interesting to read.  What are the most important qualities you look for before deciding to meet a companion?  Be sure to put those things in your review.</li>

    <li>reviews submitted by the companion (self-review) or submitted by anyone directly related to the companion will not be accepted.  reviews cannot be submitted from the companion's computer.</li>

    <li>exchanging good reviews for favors is prohibited. remember, you are trying to be help other members get the best bang for their buck, and fake or inaccurate reviews don’t do that.</li>

    <li>reviews which violate da&trade;’s policies (as periodically updated), as determined by da&trade; in its sole discretion, will be removed.  da&trade; reserves the right to cancel the membership of anyone 
        that violates any of da&trade;’s policies, and may ban a member if da&trade; believes that the violations were willful, repeated or flagrant.</li>

    <li>by providing us with content or posting content, including reviews, you assign to dateassure all intellectual property rights you have in the content.  this assignment includes, but is not limited to, 
        all rights of copyright in the content you post and in all derivative works, and to any and all causes of action accrued or accruing for infringement of the copyright in the content. further, to the fullest 
        extent permitted under applicable law, you waive your moral rights and promise not to assert such rights against dateassure, its sublicensees or assignees. you represent and warrant that none of the following 
        infringe any intellectual property right: your provision of content to dateassure, your posting of content using this website, and dateassure’s use of such content (including of works derived from it). You 
        also represent and warrant that the content you post on dateassure has not and will not be posted anywhere else by you. we are not obligated to post any review we receive.</li>
</ol>

<div style="text-align:center">

    <a href="../mbrs/da-cli-mbr_submit_exst_comp_review_compose.php?id=<?php echo $person['UserID'];?>" class="btn btn-pro-cli"><span class="">i agree</span></a><!----><!---->

</div>

</div>         