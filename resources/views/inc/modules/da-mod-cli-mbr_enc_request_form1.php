<script type="text/javascript">
    $.ter.TimezoneUtils.setTzOffsetCookieIfAbsent();

    $(window).on("load", (function () {
        
    }));

    $(function() {
        
    });
    //-->
</script>


<script language="javascript">
    function showDraftWarning() {
        bootbox.dialog({
            className: ' modal-alert warning',
            title: "<button type='button' class='sr-only close-x' data-dismiss='modal'>Close</button><i class='icon-warning'></i>",
            message: '<h5 class="modal-alert-subtitle">Warning!</h5>' +
            '<p class="modal-alert-text">It looks like you already started writing a review for a different provider. You would need to finish that review first or delete the draft prior to reviewing another provider.</p><button class="btn btn-primary" data-dismiss="modal">Finish Review</button>',
            onEscape: true,
            backdrop: true
        });
    }
</script>

        <script type="text/javascript">
            $(function () {
                var validator;

                $('.selectpicker').on('change', function () {
                    $(this).valid();
                });

                $("#CompPerformanceID").on("change", function(){
                    var selectedValue = $(this).val();
                    if (selectedValue !== '') {
                        var intValue = parseInt(selectedValue);
                        if (!isNaN(intValue) && intValue > 7) {
                            $.ter.DialogManager.showWarning('you have given a score higher than 7.  encounters with scores higher than 7 should include indulgences such as: kisses with tongue, bj without condom, more than one guy, really bi session or anal.');
                        }
                    }
                });

                var triggerAutosave = function () {
                    $(".js-autosave").trigger("ter.forceAutosave");
                };

                $(".js-submit-form").on("submit", function(e) {
                    if ($(".js-submit-form").valid()) {
                        $(".js-autosave").trigger("ter.stopAutosave");
                    }
                });

                $(".js-chemistry-rating").each( function() {
                    var rating = $(this).attr("data-rating"),
                        formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                    $(this).rateYo({
                        
                        fullStar: true,
                            totalStars: 5,
                    starWidth: "16px",
                    ratedFill: "#f300fe",
                    normalFill: "#dbe0e4",
                    precision: 2,
                    onSet: function (rating, rateYoInstance) {
                        formControl.val(rating);
                        if (formControl.val() != 0) {
                            formControl.valid();
                        };
                        triggerAutosave();
                    }
                });
            });
        
            $(".js-incalllocation-rating").each( function() {
                var rating = $(this).attr("data-rating"),
                    formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                $(this).rateYo({
                    
                    fullStar: true,
                        totalStars: 5,
                starWidth: "16px",
                ratedFill: "#f300fe",
                normalFill: "#dbe0e4",
                precision: 2,
                onSet: function (rating, rateYoInstance) {
                    formControl.val(rating);
                    if (formControl.val() != 0) {
                        formControl.valid();
                    };
                    triggerAutosave();
                }
            });
            });
        
            $(function(){
                $(".js-incalllocation-clear").on("click", function(){
                    var chemistry = $(".js-incalllocation-rating");
                    chemistry.rateYo("option", "rating", "0");
                    chemistry.parents(".form-control-wrapper").find($('.form-control')).val("");
                    $("#IncallLocationRating").val('');
                    triggerAutosave();
                });
            });


                $(".js-autosave .selectpicker").on('hidden.bs.select', function (e) {
                    triggerAutosave();
                });

                $(".js-autosave input:checkbox").on('change', function(e) {
                    triggerAutosave();
                });

                //delay for autosave in milliseconds
                var AUTO_SAVE_PERIOD = 2000;

                function timeFormatter(dateTime) {
                    var timeNow = new Date(dateTime);
                    var hours = timeNow.getHours();
                    var minutes = timeNow.getMinutes();
                    var seconds = timeNow.getSeconds();
                    var timeString = "" + ((hours > 12) ? hours - 12 : hours);
                    timeString += ((minutes < 10) ? ":0" : ":") + minutes;
                    timeString += ((seconds < 10) ? ":0" : ":") + seconds;
                    timeString += (hours >= 12) ? " PM" : " AM";

                    return timeString;
                }

                var submitDraftAutosaveCallback = function () {
                    updateAutosaveMessage();
                }

                var submitDraftAutosave = function () {
                    var form = $("#reviewDetailsForm");
                    var url = form.attr('action');
                    $("#isDraft").val("1");
                    $("#isAutosave").val("1");
                    var formData = form.serialize();
                    $("#isDraft").val("");
                    $("#isAutosave").val("");
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        success: function (data, textStatus, jqXHR) {
                            submitDraftAutosaveCallback();
                        },
                        error: function (jqXHR, status, error) {
                            if (status == 401) {
                                location.href = "/memberlaunch/login.asp?dest=/reviews/submitReviewDetails.asp?terId%3D253746";
                            }
                            //console.log(status + ": " + error);
                        }
                    });
                };

                var updateAutosaveMessage = function () {
                    var time = new Date().getTime();
                    $(".js-draft-autosave-message").show();
                    $(".js-draft-autosave-message").text("Review Draft autosaved " + timeFormatter(time));
                };

                $(".js-autosave").autoSave(function () {
                    submitDraftAutosave();
                }, AUTO_SAVE_PERIOD);

                $(".js-save-draft-click").on("click", function (event) {
                    $("#reviewDetailsForm").data('validator').cancelSubmit = true;
                    $("#reviewDetailsForm").validate().settings.ignore = "*";
                    $("#isDraft").val("1");
                    $("#reviewDetailsForm").submit();
                    return false;
                });

                
                validator = $('.js-submit-form').validate({
                    rules: {
                        Website: "required",
                        Location: "required",
                        //session atmosphere
                        Smokes: "required",
                        OverallLooks: "required",
                        Attitude: "required",
                        OverallAtmosphere: "required",
                        OverallPerformance: "required",
                        //visit details
                        GeneralDetails: "required",
                        TheJuicyDetails: "required",
                        ChemistryRating: "required"
                    },
                    messages: {
                        ChemistryRating: "Please select Chemistry star rating."
                    },
                    highlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").addClass("error");
                        $element.next(".btn-group").addClass("error");
                    },
                    unhighlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").removeClass("error");
                        $element.removeClass("error").next(".btn-group").removeClass("error");
                    },
                    ignore:"",
                    //TODO: move out to common js
                    invalidHandler: function(form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            if($(validator.errorList[0].element).is(":visible"))
                            {
                                validator.errorList[0].element.focus();
                            }
                            else
                            {
                                var elementOffset = $("#" + $(validator.errorList[0].element).attr("data-focusID")).offset().top;
                                if (elementOffset > $(window).height()){
                                    header = $(".header").height();
                                    $('html, body').animate({
                                        scrollTop: elementOffset - header - 30
                                    }, 1000);
                                }
                            }
                        }
                    }
                });
            });

        </script>

                                            <div class="js-draft-autosave-message warning-block" hidden="hidden"></div>

                                            
                                                
                                                    <form action="../mbrs/simple_review_submitted.php" method="POST" id="reviewDetailsForm" name="form1" class="js-autosave js-submit-form" novalidate="novalidate">
                                                        <input type="hidden" name="ReviewerStatusID" value="1">
                                                        <input type="hidden" name="ReviewerID" value="22">
                                                        <input type="hidden" name="CompanionID" value="<?php echo $person['UserID'];?>">
                                                        <input type="hidden" name="SubmittedOn" value="<?php date_default_timezone_set("America/New_York"); echo date("Y-m-d h:i:sa");?>">
                                                        <input type="hidden" name="ReviewStatus" value="0">                           

                                                <script language="javascript">
                                                    $(document).ready(function(){
                                                        $('#EncCountryID').change(function() {
                                                            var valchange = this.value;
                                                                $('#EncStateID').load('../includes/loadstates.php?countryId=' + valchange);
                                                            });
                                                        });
                                                </script>                                                        
                                                        
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    
                                                    <h5 style="color:#813ee8;">proposed location for encounter</h5>
                                                    <hr style="color:#813ee8;">
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <label for="enccountry" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">proposed country: </label>

                                                                <div class="col-sm-5">
                                                                    <select class="bootstrap-select form-control" name="EncCountryID" id="EncCountryID">
                                                                    <option value="0"><?php echo $select;?></option>

                                                                    <?php
                                                                        $sel = isset($_POST['EncCountryID'])?$_POST['EncCountryID']:0;
                                                                        $sqlcountry = 'select CountryID as Id, Country as LName from '.$table_prefix.'countries order by TopSorted desc, LName asc';
                                                                            dropdownlist($sqlcountry, $sel);
                                                                    ?>

                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="enccountry" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">proposed state: </label>

                                                                <div class="col-sm-5">
                                                                    <select class="bootstrap-select form-control" name="EncStateID" id="EncStateID">

                                                                    <?php
                                                                        if(isset($_POST['EncCountryID']) && $_POST['EncCountryID']>0){
                                                                            $sql = 'select StateID as Id, State as LName from '.$table_prefix.'states where CountryID = '.intval($_POST['EncCountryID']).' order by TopSorted desc, LName asc';
                                                                                dropdownlist($sql, $_POST['EncStateID']);
                                                                                }
                                                                            else{
                                                                            echo '<option value="0">'.$select.'</option>';
                                                                        }
                                                                    ?>

                                                                    </select>
                                                                </div>
                                                            </div>                                                                
                                                                
                                                            <div class="form-group row">
                                                                <label for="enccity" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">proposed city: </label>

                                                                <div class="col-sm-5">
                                                                    <input type="text" class="form-control js-websitegroup-validation" name="EncCity" value="" maxlength="100" required>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="encloc" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">proposed location: </label>
                                                                <div class="col-sm-5">
                                                                    <select class="bootstrap-select form-control" id="EnLocationID" name="EncLocationID" title="" tabindex="-98">
                                                                        <option value="" label="- select one -"></option>
                                                                        <option value="1">her place</option>
                                                                        <option value="2">her hotel</option>
                                                                        <option value="3">my place</option>
                                                                        <option value="4">my hotel</option>
                                                                        <option value="5">other</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group row">
                                                                <label for="enccity" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">if your place/hotel, what place/hotel: </label>

                                                                <div class="col-sm-5">
                                                                    <input type="text" class="form-control js-websitegroup-validation" name="EncCity" value="" maxlength="100" required>
                                                                    <small id="aboutmeHelpBlock" class="form-text text-muted">
                                                                        if your place, give area name. if hotel, give hotel name.
                                                                    </small>
                                                                </div>
                                                            </div>                                                            
                                                            
                                                            
                                                        </div>
                                                        
                                                        <div class="col-lg-1">&nbsp;</div>
                                                            
                                                    </div>
                                                            
                                                    <h5 style="color:#813ee8;">proposed encounter date and time</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">proposed encounter date: </label>
                                                                    
                                                                <script type="text/javascript">
                                                                    $(function () {
                                                                        $('#datetimepicker4').datetimepicker({
                                                                            format: 'L'
                                                                        });
                                                                    });
                                                                </script>
                                                                
                                                                <div class="input-group date col-sm-5" id="datetimepicker4" data-target-input="nearest">
                                                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                                                                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                                                        <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                                                    </div>
                                                                </div>        
                                                                    
                                                            </div>
                                                            
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">proposed encounter time: </label>
                                                                    
                                                                <script type="text/javascript">
                                                                    $(function () {
                                                                        $('#datetimepicker3').datetimepicker({
                                                                            format: 'LT'
                                                                        });
                                                                    });
                                                                </script>
                                                                
                                                                <div class="input-group date col-sm-5" id="datetimepicker3" data-target-input="nearest">
                                                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker3"/>
                                                                    <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                                                                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                                                                    </div>
                                                                </div>        
                                                                    
                                                            </div>
                                                            
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">proposed encounter duration: </label>
                                                             
                                                                <div class="col-sm-5">   
                                                                
                                                                    <select class="bootstrap-select form-control" name="EncTypeID" title="" tabindex="-98">
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="30 mins">30 mins</option>
                                                                        <option value="60 mins">60 mins</option>
                                                                        <option value="90 mins">90 mins</option> 
                                                                        <option value="2 hrs">2 hrs</option>
                                                                        <option value="3 hrs">3 hrs</option>
                                                                        <option value="4 hrs">4 hrs</option>
                                                                        <option value="overnight">overnight</option>
                                                                                                       
                                                                    </select>
                                                                
                                                                </div>       
                                                                    
                                                            </div>  
                                                                    
                                                        </div>                                                                          

                                                        
                                
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">proposed encounter type</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">type of encounter: </label>                                                    
                                                                <div class="col-sm-5">   
                                                                
                                                                    <select class="bootstrap-select form-control" name="EncTypeID" title="" tabindex="-98">
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="1">gfe - girlfriend exp</option>
                                                                        <option value="2">pse - pornstar exp</option>
                                                                        <option value="3">erotic massage - body rub</option> 
                                                                        <option value="4">blow job</option>
                                                                        <option value="5">body slide - companion slid their naked body along your body</option>
                                                                        <option value="6">hand job</option>
                                                                        <option value="7">anal sex (also known as greek)</option>
                                                                        <option value="8">tie &amp; tease - companion gently tied your hands and/or feet</option>
                                                                        <option value="9">fantasy - companion dressed up in a costume and /or role-play</option>
                                                                        <option value="10">b &amp; d - being whipped, flogged, caned, tied up, told what to do</option>
                                                                        <option value="11">s &amp; m - a more extreme form of b&amp;d</option>                                   
                                                                    </select>
                                                                
                                                                </div>
                                                            </div>
            
                               
                                                        </div>
                                                        
                                                        <div class="col-lg-1">&nbsp;</div>
                                                        
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">previous encounter(s)</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <legend class="col-form-label col-sm-5 pt-0" style="font-weight:400;font-size:14px;">have you had a previous encounter with <?php echo $person['ProfileName'];?>: </legend>
                                                                
                                                                <!-- <label class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">have you had a previous encounter with this companion:</label> -->
                                                                
                                                                <?php
                                                                    if(isset($_POST['rdGender'])){
                                                                        if($_POST['rdGender']==1){
                                                                            $gen1 = ' checked="checked"';
                                                                            $gen2 = '';
                                                                            }
                                                                        elseif($_POST['rdGender']==2){
                                                                            $gen1 = '';
                                                                            $gen2 = ' checked="checked"';
                                                                            }
                                                                        }
                                                                        else{
                                                                            $gen1 = ' checked="checked"';
                                                                            $gen2 = '';
                                                                            }
                                                                ?>
                                                                
                                                                <div class="col-sm-5">
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" <?php echo $gen1;?> name="rdGender" value="1">
                                                                        <label class="form-check-label" for="rdGender" style="font-weight:400;font-size:14px;">yes</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" <?php echo $gen2;?> name="rdGender" value="2">
                                                                        <label class="form-check-label" for="rdGender" style="font-weight:400;font-size:14px;">no</label>
                                                                    </div>
                                                                </div>  
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            <!--    <div class="col-sm-8 form-check-inline">
                                                                    <input class="form-check-input" type="radio" name="IndulgeTrue" id="IndulgeTrue" value="1" checked>&nbsp;yes&nbsp;
                                                                    <input class="form-check-input" type="radio" name="IndulgeTrue" id="IndulgeTrue" value="2">&nbsp;no&nbsp;
                                                                </div> -->
                                                            </div>
                                                            
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">if yes, approximately when: </label>                                                    
                                                                    
                                                                <script type="text/javascript">
                                                                    $(function () {
                                                                        $('#datetimepicker2').datetimepicker({
                                                                            format: 'MM/YYYY'
                                                                        });
                                                                    });
                                                                </script>
                                                                
                                                                <div class="input-group date col-sm-5" id="datetimepicker2" data-target-input="nearest">
                                                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2"/>
                                                                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                                                        <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="form-group row">
                                                                <label for="propprevenccity" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">previous encounter city: </label>

                                                                <div class="col-sm-5">
                                                                    <input type="text" class="form-control js-websitegroup-validation" name="PropPrevEncCity" value="" maxlength="100" required>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                        <div class="col-lg-1">&nbsp;</div>
                                                        
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">special requests</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <label class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">special requests: </label>
                                                                <div class="col-sm-6">    
                                                                    <input name="ReviewHeadline" value="" type="text" class="form-control" maxlength="60">
                                                                    <small id="aboutmeHelpBlock" class="form-text text-muted" style="color:#ff0000;">
                                                                        please be respectful and polite, or your companion may not respond at all!
                                                                    </small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-1">&nbsp;</div>
                                                        
                                                    </div>
                                                
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <div class="col-sm-5">&nbsp;</div>
                                                                <div class="col-sm-5">
                                                                    <button class="btn btn-cli-pro" data-toggle="modal2" data-target="#submitReview">send request</button>
                                                                    <button type="button" formnovalidate="formnovalidate" class="btn btn-pro-cli" reset>reset</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-1">&nbsp;</div>
                                                    
                                                    </div>
                                                
                                                <div class="modal fade" id="submitReview" tabindex="-1" role="dialog" aria-labelledby="submitReviewLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                    <h5 class="modal-title" id="submitReviewLabel">Error</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <span class="icon-error">!</span>
                                                                <p>Your review wasn't added. Please try to do it later.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                            
