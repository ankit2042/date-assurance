<script type="text/javascript">
    $.ter.TimezoneUtils.setTzOffsetCookieIfAbsent();

    $(window).on("load", (function () {
        
    }));

    $(function() {
        
    });
    //-->
</script>


<script language="javascript">
    function showDraftWarning() {
        bootbox.dialog({
            className: ' modal-alert warning',
            title: "<button type='button' class='sr-only close-x' data-dismiss='modal'>Close</button><i class='icon-warning'></i>",
            message: '<h5 class="modal-alert-subtitle">Warning!</h5>' +
            '<p class="modal-alert-text">It looks like you already started writing a review for a different provider. You would need to finish that review first or delete the draft prior to reviewing another provider.</p><button class="btn btn-primary" data-dismiss="modal">Finish Review</button>',
            onEscape: true,
            backdrop: true
        });
    }
</script>

        <script type="text/javascript">
            $(function () {
                var validator;

                $('.selectpicker').on('change', function () {
                    $(this).valid();
                });

                $("#CompPerformanceID").on("change", function(){
                    var selectedValue = $(this).val();
                    if (selectedValue !== '') {
                        var intValue = parseInt(selectedValue);
                        if (!isNaN(intValue) && intValue > 7) {
                            $.ter.DialogManager.showWarning('you have given a score higher than 7.  encounters with scores higher than 7 should include indulgences such as: kisses with tongue, bj without condom, more than one guy, really bi session or anal.');
                        }
                    }
                });

                var triggerAutosave = function () {
                    $(".js-autosave").trigger("ter.forceAutosave");
                };

                $(".js-submit-form").on("submit", function(e) {
                    if ($(".js-submit-form").valid()) {
                        $(".js-autosave").trigger("ter.stopAutosave");
                    }
                });

                $(".js-chemistry-rating").each( function() {
                    var rating = $(this).attr("data-rating"),
                        formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                    $(this).rateYo({
                        
                        fullStar: true,
                            totalStars: 5,
                    starWidth: "16px",
                    ratedFill: "#f300fe",
                    normalFill: "#dbe0e4",
                    precision: 2,
                    onSet: function (rating, rateYoInstance) {
                        formControl.val(rating);
                        if (formControl.val() != 0) {
                            formControl.valid();
                        };
                        triggerAutosave();
                    }
                });
            });
        
            $(".js-incalllocation-rating").each( function() {
                var rating = $(this).attr("data-rating"),
                    formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                $(this).rateYo({
                    
                    fullStar: true,
                        totalStars: 5,
                starWidth: "16px",
                ratedFill: "#f300fe",
                normalFill: "#dbe0e4",
                precision: 2,
                onSet: function (rating, rateYoInstance) {
                    formControl.val(rating);
                    if (formControl.val() != 0) {
                        formControl.valid();
                    };
                    triggerAutosave();
                }
            });
            });
        
            $(function(){
                $(".js-incalllocation-clear").on("click", function(){
                    var chemistry = $(".js-incalllocation-rating");
                    chemistry.rateYo("option", "rating", "0");
                    chemistry.parents(".form-control-wrapper").find($('.form-control')).val("");
                    $("#IncallLocationRating").val('');
                    triggerAutosave();
                });
            });


                $(".js-autosave .selectpicker").on('hidden.bs.select', function (e) {
                    triggerAutosave();
                });

                $(".js-autosave input:checkbox").on('change', function(e) {
                    triggerAutosave();
                });

                //delay for autosave in milliseconds
                var AUTO_SAVE_PERIOD = 2000;

                function timeFormatter(dateTime) {
                    var timeNow = new Date(dateTime);
                    var hours = timeNow.getHours();
                    var minutes = timeNow.getMinutes();
                    var seconds = timeNow.getSeconds();
                    var timeString = "" + ((hours > 12) ? hours - 12 : hours);
                    timeString += ((minutes < 10) ? ":0" : ":") + minutes;
                    timeString += ((seconds < 10) ? ":0" : ":") + seconds;
                    timeString += (hours >= 12) ? " PM" : " AM";

                    return timeString;
                }

                var submitDraftAutosaveCallback = function () {
                    updateAutosaveMessage();
                }

                var submitDraftAutosave = function () {
                    var form = $("#reviewDetailsForm");
                    var url = form.attr('action');
                    $("#isDraft").val("1");
                    $("#isAutosave").val("1");
                    var formData = form.serialize();
                    $("#isDraft").val("");
                    $("#isAutosave").val("");
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        success: function (data, textStatus, jqXHR) {
                            submitDraftAutosaveCallback();
                        },
                        error: function (jqXHR, status, error) {
                            if (status == 401) {
                                location.href = "/memberlaunch/login.asp?dest=/reviews/submitReviewDetails.asp?terId%3D253746";
                            }
                            //console.log(status + ": " + error);
                        }
                    });
                };

                var updateAutosaveMessage = function () {
                    var time = new Date().getTime();
                    $(".js-draft-autosave-message").show();
                    $(".js-draft-autosave-message").text("Review Draft autosaved " + timeFormatter(time));
                };

                $(".js-autosave").autoSave(function () {
                    submitDraftAutosave();
                }, AUTO_SAVE_PERIOD);

                $(".js-save-draft-click").on("click", function (event) {
                    $("#reviewDetailsForm").data('validator').cancelSubmit = true;
                    $("#reviewDetailsForm").validate().settings.ignore = "*";
                    $("#isDraft").val("1");
                    $("#reviewDetailsForm").submit();
                    return false;
                });

                
                validator = $('.js-submit-form').validate({
                    rules: {
                        Website: "required",
                        Location: "required",
                        //session atmosphere
                        Smokes: "required",
                        OverallLooks: "required",
                        Attitude: "required",
                        OverallAtmosphere: "required",
                        OverallPerformance: "required",
                        //visit details
                        GeneralDetails: "required",
                        TheJuicyDetails: "required",
                        ChemistryRating: "required"
                    },
                    messages: {
                        ChemistryRating: "Please select Chemistry star rating."
                    },
                    highlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").addClass("error");
                        $element.next(".btn-group").addClass("error");
                    },
                    unhighlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").removeClass("error");
                        $element.removeClass("error").next(".btn-group").removeClass("error");
                    },
                    ignore:"",
                    //TODO: move out to common js
                    invalidHandler: function(form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            if($(validator.errorList[0].element).is(":visible"))
                            {
                                validator.errorList[0].element.focus();
                            }
                            else
                            {
                                var elementOffset = $("#" + $(validator.errorList[0].element).attr("data-focusID")).offset().top;
                                if (elementOffset > $(window).height()){
                                    header = $(".header").height();
                                    $('html, body').animate({
                                        scrollTop: elementOffset - header - 30
                                    }, 1000);
                                }
                            }
                        }
                    }
                });
            });

        </script>

        <div class="js-draft-autosave-message warning-block" hidden="hidden"></div>

                                            
                                                
            <form action="../mbrs/da-cli-mbr_exst_comp_review_submitted.php" method="POST" id="reviewDetailsForm" name="form1" class="js-autosave js-submit-form" novalidate="novalidate">
                <input type="hidden" name="ReviewerStatusID" value="1">
                <input type="hidden" name="ReviewerID" value="<?php echo $_SESSION['memberid'];?>">
                <input type="hidden" name="CompanionID" value="<?php echo $person['UserID'];?>">
                <input type="hidden" name="SubmittedOn" value="<?php date_default_timezone_set("America/New_York"); echo date("Y-m-d h:i:sa");?>">
                <input type="hidden" name="ReviewStatus" value="0">                           

                                                <script language="javascript">
                                                    $(document).ready(function(){
                                                        $('#EncCountryID').change(function() {
                                                            var valchange = this.value;
                                                                $('#EncStateID').load('../includes/loadstates.php?countryId=' + valchange);
                                                            });
                                                        });
                                                </script>                                                        
                                                        
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
   
                                                    <h5 style="color:#813ee8;">about your companion</h5>
                                                    <hr style="color:#813ee8;">
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>

                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <label for="profilename" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">enter your companion's name: </label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" name="profilename" value="<?php echo isset($_POST['profilename'])?$_POST['profilename']:'';?>" required>
                                                                    <small id="profileNameHelpBlock" class="form-text text-muted">
                                                                        please enter your companion's name between 6-20 characters. this will be your companion's profile name will be seen by members and other companions.
                                                                    </small>

                                    <!-- begin PROFILE NAME ERROR MESSAGE -->
                                                                <?php
                                                                    if(isset($err_p) && !empty($err_p))
                                                                        echo '<br><span style="color:#FF0000;font-size:16px;font-weight:500;font-style:italic;">'.$err_p.'</span>';
                                                                ?>
                                    <!-- end PROFILE NAME ERROR MESSAGE -->

                                                                </div>    
                                                            </div>
                              
                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">companion's gender: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdGender'])){
                                                                            if($_POST['rdGender']==1){
                                                                                $gen1 = ' checked="checked"';
                                                                                $gen2 = '';
                                                                                }
                                                                            elseif($_POST['rdGender']==2){
                                                                                $gen1 = '';
                                                                                $gen2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $gen1 = '';
                                                                                $gen2 = ' checked="checked"';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-8">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $gen1;?> name="rdGender" value="1">
                                                                            <label class="form-check-label" for="rdGender" style="font-weight:400;font-size:14px;"><?php echo $male;?></label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $gen2;?> name="rdGender" value="2">
                                                                            <label class="form-check-label" for="rdGender" style="font-weight:400;font-size:14px;"><?php echo $female;?></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>


                                                            <div class="form-group row">
                                                                <label for="slAge" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's approximate age: </label>
                                                                <div class="col-sm-2">
                                                                    <select class="form-control" name="slAge">
                                                                    <?php
                                                                        for($i=18; $i<76; $i++){
                                                                            $val = isset($_POST['slAge'])?$_POST['slAge']:18;
                                                                            $sel = ($val==$i)?' selected="selected"':'';
                                                                                echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
                                                                            }
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            


                                                            <div class="form-group row">
                                                                <label for="slEthnicity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's <?php echo $ethnicity;?>: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control" id="slEthnicity" name="slEthnicity">
                                                                    <?php
                                                                        $sel = isset($_POST['slEthnicity'])?$_POST['slEthnicity']:1;
                                                                        $sql = 'select EthnicityID as Id, '.$_SESSION['lang'].'Ethnicity as LName from '.$table_prefix.'ethnicity order by TopSorted desc, LName asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>                                     

                                                            <div class="form-group row">
                                                                <label for="slSexOr" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's sexual orientation: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control" id="slSexO" name="slSexOr">
                                                                    <?php
                                                                        $sel = isset($_POST['slSexOr'])?$_POST['slSexOr']:1;
                                                                        $sql = 'select SexOrID as Id, '.$_SESSION['lang'].'SexOr as LName from '.$table_prefix.'sexor order by TopSorted desc, LName asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div> 

                                    <!-- begin TRANSSEXUAL -->                        

                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">is your companion a transsexual: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdTranssexual'])){
                                                                            if($_POST['rdTranssexual']==1){
                                                                                $transs1 = ' checked="checked"';
                                                                                $transs2 = '';
                                                                                }
                                                                            elseif($_POST['rdTranssexual']==2){
                                                                                $transs1 = '';
                                                                                $transs2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $transs1 = '';
                                                                                $transs2 = ' checked="checked"';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-6">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $transs1;?> name="rdTranssexual" value="1">
                                                                            <label class="form-check-label" for="rdTranssexual" style="font-weight:400;font-size:14px;">yes</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $transs2;?> name="rdTranssexual" value="2">
                                                                            <label class="form-check-label" for="rdTranssexual" style="font-weight:400;font-size:14px;">no</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>  

                                    <!-- end TRANSSEXUAL -->

                                    <!-- begin TRANSGENDER -->

                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">is your companion a transgender: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdTransgender'])){
                                                                            if($_POST['rdTransgender']==1){
                                                                                $transg1 = ' checked="checked"';
                                                                                $transg2 = '';
                                                                                }
                                                                            elseif($_POST['rdTransgender']==2){
                                                                                $transg1 = '';
                                                                                $transg2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $transg1 = '';
                                                                                $transg2 = ' checked="checked"';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-8">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $transg1;?> name="rdTransgender" value="1">
                                                                            <label class="form-check-label" for="rdTransgender" style="font-weight:400;font-size:14px;">yes</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $transg2;?> name="rdTransgender" value="2">
                                                                            <label class="form-check-label" for="rdTransgender" style="font-weight:400;font-size:14px;">no</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset> 
                                    <!-- end TRANSGENDER -->

                                    <!-- begin INDEPENDENT -->
                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">is your companion independent: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdIndep'])){
                                                                            if($_POST['rdIndep']==1){
                                                                                $indep1 = ' checked="checked"';
                                                                                $indep2 = '';
                                                                                }
                                                                            elseif($_POST['rdIndep']==2){
                                                                                $indep1 = '';
                                                                                $indep2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $indep1 = ' checked="checked"';
                                                                                $indep2 = '';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-8">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $indep1;?> name="rdIndep" value="1">
                                                                            <label class="form-check-label" for="rdIndep" style="font-weight:400;font-size:14px;">yes</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $indep2;?> name="rdIndep" value="2">
                                                                            <label class="form-check-label" for="rdIndep" style="font-weight:400;font-size:14px;">no</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>    
                                    <!-- end INDEPENDENT -->           

                                    <!-- begin PORNSTAR -->
                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">is your companion a porn star: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdPornstar'])){
                                                                            if($_POST['rdPornstar']==1){
                                                                                $pornstar1 = ' checked="checked"';
                                                                                $pornstar2 = '';
                                                                                }
                                                                            elseif($_POST['rdPornstar']==2){
                                                                                $pornstar1 = '';
                                                                                $pornstar2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $pornstar1 = '';
                                                                                $pornstar2 = ' checked="checked"';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-8">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $pornstar1;?> name="rdPornstar" value="1">
                                                                            <label class="form-check-label" for="rdPornstar" style="font-weight:400;font-size:14px;">yes</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $pornstar2;?> name="rdPornstar" value="2">
                                                                            <label class="form-check-label" for="rdPornstar" style="font-weight:400;font-size:14px;">no</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>    
                                    <!-- end PORNSTAR --> 
                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                                        
                                                        <div class="col-lg-10">
                                                            
                                                            <div class="form-group row">
                                                                <label for="enccountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's location - country: </label>

                                                                <div class="col-sm-8">
                                                                    <select class="bootstrap-select form-control" name="EncCountryID" id="EncCountryID">
                                                                    <option value="0"><?php echo $select;?></option>

                                                                    <?php
                                                                        $sel = isset($_POST['CompCountryID'])?$_POST['CompCountryID']:0;
                                                                        $sqlcountry = 'select CountryID as Id, Country as LName from '.$table_prefix.'countries order by TopSorted desc, LName asc';
                                                                            dropdownlist($sqlcountry, $sel);
                                                                    ?>

                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="enccountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's location - state: </label>

                                                                <div class="col-sm-8">
                                                                    <select class="bootstrap-select form-control" name="EncStateID" id="EncStateID">

                                                                    <?php
                                                                        if(isset($_POST['CompCountryID']) && $_POST['CompCountryID']>0){
                                                                            $sql = 'select StateID as Id, State as LName from '.$table_prefix.'states where CountryID = '.intval($_POST['EncCountryID']).' order by TopSorted desc, LName asc';
                                                                                dropdownlist($sql, $_POST['CompStateID']);
                                                                                }
                                                                            else{
                                                                            echo '<option value="0">'.$select.'</option>';
                                                                        }
                                                                    ?>

                                                                    </select>
                                                                </div>
                                                            </div>                                                                
                                                                
                                                            <div class="form-group row">
                                                                <label for="enccity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's location - city: </label>

                                                                <div class="col-sm-8">
                                                                    <input type="text" class="form-control js-websitegroup-validation" name="CompCity" value="" maxlength="100" required>
                                                                </div>
                                                            </div>
                                                        </div>                                    
                                    
                                    
                                    <!-- begin COMP PHONE --> 
                                                            <div class="form-group row">
                                                                <label for="phone" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's posted phone: </label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" size="16" id="phone" name="phone" maxlength="16" value="<?php echo isset($_POST['phone'])?$_POST['phone']:'';?>" onkeypress="return numberPressed(event);">
                                                                    <small id="phoneHelpBlock" class="form-text text-muted">
                                                                        please post only the phone number your companion uses to communicate with you. no one from da&trade; will ever call.
                                                                    </small> 

                                                                    <script language="javascript">
                                                                        // Format the phone number as the user types it
                                                                        document.getElementById('phone').addEventListener('keyup',function(evt){
                                                                            var phone = document.getElementById('phone');
                                                                            var charCode = (evt.which) ? evt.which : evt.keyCode;
                                                                                phone.value = phoneFormat(phone.value);
                                                                            });

                                                                        // We need to manually format the phone number on page load
                                                                        document.getElementById('phone').value = phoneFormat(document.getElementById('phone').value);

                                                                        // A function to determine if the pressed key is an integer
                                                                        function numberPressed(evt){
                                                                            var charCode = (evt.which) ? evt.which : evt.keyCode;
                                                                                if(charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 36 || charCode > 40)){
                                                                                return false;
                                                                                }
                                                                            return true;
                                                                            }

                                                                        // A function to format text to look like a phone number
                                                                        function phoneFormat(input){
                                                                        // Strip all characters from the input except digits
                                                                            input = input.replace(/\D/g,'');

                                                                        // Trim the remaining input to ten characters, to preserve phone number format
                                                                            input = input.substring(0,10);

                                                                        // Based upon the length of the string, we add formatting as necessary
                                                                        var size = input.length;
                                                                            if(size == 0){
                                                                                input = input;
                                                                                }else if(size < 4){
                                                                                    input = '('+input;
                                                                                    }else if(size < 7){
                                                                                        input = '('+input.substring(0,3)+') '+input.substring(3,6);
                                                                                        }else{
                                                                                            input = '('+input.substring(0,3)+') '+input.substring(3,6)+' - '+input.substring(6,10);
                                                                                            }
                                                                                        return input; 
                                                                                }
                                                                    </script>

                                                                </div>
                                                            </div>                                       
                                    <!-- end COMP PHONE -->

                                    <!-- begin COMPANION WEBSITE -->               
                                                            <div class="form-group row">
                                                                <label for="compwebsite" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your companion's website: </label>
                                                                <div class="col-sm-5">
                                                                    <input type="text" class="form-control" id="compwebsite" name="compwebsite" value="<?php echo isset($_POST['compwebsite'])?$_POST['compwebsite']:'';?>" maxlength="250">
                                                                </div>
                                                            </div>                                     
                                    <!-- end COMPANION WEBSITE -->

                                    <!-- begin COMPANION WEB PROFILE -->               
                                                            <div class="form-group row">
                                                                <label for="compwebprof" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your companion's profile on the web: </label>
                                                                <div class="col-sm-5">
                                                                    <input type="text" class="form-control" id="compwebprof" name="compwebprof" value="<?php echo isset($_POST['compwebprof'])?$_POST['compwebprof']:'';?>" maxlength="250">
                                                                </div>
                                                            </div>                                     
                                    <!-- end COMPANION WEB PROFILE -->             

                                    <!-- begin COMPANION WHATSAPP -->                   
                                                            <div class="form-group row">
                                                                <label for="compwhatsapp" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your companion's whatsapp: </label>
                                                                <div class="col-sm-5">
                                                                    <input type="text" class="form-control" id="compwhatsapp" name="compwhatsapp" value="<?php echo isset($_POST['compwhatsapp'])?$_POST['compwhatsapp']:'';?>" maxlength="250">
                                                                </div>
                                                            </div>                               
                                    <!-- end COMPANION WHATSAPP -->             

                                    <!-- begin COMPANION INSTAGRAM -->                   
                                                            <div class="form-group row">
                                                                <label for="compinstagram" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">your companion's instagram: </label>
                                                                <div class="col-sm-5">
                                                                    <input type="text" class="form-control" id="compinstagram" name="compinstagram" value="<?php echo isset($_POST['compinstagram'])?$_POST['compinstagram']:'';?>" maxlength="250">
                                                                </div>
                                                            </div>                               
                                    <!-- end COMPANION INSTAGRAM -->          
                                    
                                                        </div>
                                                        <div class="col-lg-1">&nbsp;</div>
                                                    </div>
                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-lg-12">
                                                            <br>
                                                            <h5 style="color:#813ee8;">please describe your companion's looks</h5>
                                                            <hr style="color:#813ee8;">
                                                            <br>
                                                        </div>
                                                    </div>
                                
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                                        
                                                        <div class="col-lg-10">                                    
                                    
                                    <!-- begin BODYTYPE -->                        
                                                            <div class="form-group row">
                                                                <label for="slBodyType" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companions's <?php echo $bodytype;?>: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control" id="slBodyType" name="slBodyType">
                                                                    <?php
                                                                        $sel = isset($_POST['slBodyType'])?$_POST['slBodyType']:1;
                                                                        $sql = 'select BodyTypeID as Id, '.$_SESSION['lang'].'BodyType as LName from '.$table_prefix.'bodytypes order by TopSorted desc, LName asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>                                   
                                    <!-- end BODYTYPE -->
            
                                    <!-- begin HEIGHT -->
                                                            <div class="form-group row">
                                                                <label for="slHeight" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's approximate <?php echo $height;?>: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control" id="slHeight" name="slHeight">
                                                                        <option value="0"><?php echo $select;?></option>

                                                                        <?php 
                                                                            $sel = isset($_POST['slHeight'])?$_POST['slHeight']:358;
                                                                            $sql = 'select HeightID as Id, HeightDescription as LName from '.$table_prefix.'height order by HeightID asc';
                                                                                dropdownlist($sql, $sel);
                                                                        ?>

                                                                    </select> 
                                                                </div>
                                                            </div>
                                    <!-- end HEIGHT -->

                                    <!-- begin HAIRCOLOR -->
                                                            <div class="form-group row">
                                                                <label for="slHairColor" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's<?php echo $haircolor;?>: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control" id="slHairColor" name="slHairColor">
                                                                    <?php
                                                                        $sel = isset($_POST['slHairColor'])?$_POST['slHairColor']:1;
                                                                        $sql = 'select HairColorID as Id, '.$_SESSION['lang'].'HairColor as LName from '.$table_prefix.'haircolors order by TopSorted desc, LName asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>                                   
                                    <!-- end HAIRCOLOR -->

                                    <!-- begin HAIRLENGTH -->
                                                            <div class="form-group row">
                                                                <label for="slHairLength" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's hair length: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control" id="slHairLength" name="slHairLength">
                                                                    <?php
                                                                        $sel = isset($_POST['slHairLength'])?$_POST['slHairLength']:1;
                                                                        $sql = 'SELECT HairLengthID as Id, '.$_SESSION['lang'].'HairLength as LName from '.$table_prefix.'hairlengths order by TopSorted desc, LName asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>                                   
                                    <!-- end HAIRLENGTH -->

                                    <!-- begin PIERCINGS -->
                                                            <div class="form-group row">
                                                                <label for="slPiercings" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's piercings: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control" id="slPiercings" name="slPiercings">
                                                                    <?php
                                                                        $sel = isset($_POST['slPiercings'])?$_POST['slPiercings']:1;
                                                                        $sql = 'select PiercingTypeID as Id, '.$_SESSION['lang'].'PiercingType as LName from '.$table_prefix.'piercings order by LName asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>                                                                       
                                    <!-- end PIERCINGS -->

                                    <!-- begin TATTOOS -->
                                                            <div class="form-group row">
                                                                <label for="slTattoos" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's tattoos: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control" id="slTattoos" name="slTattoos">
                                                                    <?php
                                                                        $sel = isset($_POST['slTattoos'])?$_POST['slTattoos']:1;
                                                                        $sql = 'select TattoosTypeID as Id, '.$_SESSION['lang'].'TattooType as LName from '.$table_prefix.'tattoos order by LName asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>                                                                       
                                    <!-- end TATTOOS -->

                                    <!-- begin PUSSY GROOMING -->
                                                            <div class="form-group row">
                                                                <label for="slPussy" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's pussy grooming: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control" id="slPussy" name="slPussy">
                                                                    <?php
                                                                        $sel = isset($_POST['slPussy'])?$_POST['slPussy']:1;
                                                                        $sql = 'select PussyID as Id, '.$_SESSION['lang'].'Pussy as LName from '.$table_prefix.'pussy order by LName asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>                                                                       
                                    <!-- end PUSSY GROOMING -->            

                                    <!-- begin BREASTS -->
                                                            <div class="form-group row">
                                                                <label for="slCompBreastSizeDesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's breast description: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control" id="slCompBreastSizeDesc" name="slCompBreastSizeDesc">
                                                                    <?php
                                                                        $sel = isset($_POST['slCompBreastSizeDesc'])?$_POST['slCompBreastSizeDesc']:1;
                                                                        $sql = 'select BreastSizeDescID as Id, '.$_SESSION['lang'].'BreastSizeDesc as LName from '.$table_prefix.'breastsizedesc order by BreastSizeDescID asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>                                                                       
                                    <!-- end BREASTS -->                        

                                    <!-- begin BREAST IMPLANTS -->
                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">companion breast implants: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdBreastImp'])){
                                                                            if($_POST['rdBreastImp']==1){
                                                                                $breastimp1 = ' checked="checked"';
                                                                                $breastimp2 = '';
                                                                                }
                                                                            elseif($_POST['rdBreastImp']==2){
                                                                                $breastimp1 = '';
                                                                                $breastimp2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $breastimp1 = ' checked="checked"';
                                                                                $breastimp2 = '';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-6">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $breastimp1;?> name="rdBreastImp" value="1">
                                                                            <label class="form-check-label" for="rdBreastImp" style="font-weight:400;font-size:14px;">yes</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $breastimp2;?> name="rdBreastImp" value="2">
                                                                            <label class="form-check-label" for="rdBreastImp" style="font-weight:400;font-size:14px;">no</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>    
                                    <!-- end BREAST IMPLANTS -->             

                                    <!-- begin ASS -->
                                                            <div class="form-group row">
                                                                <label for="slCompAss" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's ass: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control" id="slCompAss" name="slCompAss">
                                                                    <?php
                                                                        $sel = isset($_POST['slCompAss'])?$_POST['slCompAss']:1;
                                                                        $sql = 'select CompAssID as Id, '.$_SESSION['lang'].'CompAss as LName from '.$table_prefix.'compass order by CompAssID asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>                                                                       
                                    <!-- end ASS -->                              
            
                                                        </div>
                                                        <div class="col-lg-1">&nbsp;</div>
                                                        
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <h5 style="color:#813ee8;">about your companion</h5>
                                                            <hr style="color:#813ee8;">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>

                                                        <div class="col-lg-10">
                                                            <div class="form-group row">

                                                                <label for="slSmoking" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">did your companion smoke: </label>
                                                                <div class="col-sm-5">
                                                                    <select class="form-control" id="slSmoking" name="slSmoking">
                                                                    <?php
                                                                        $sel = isset($_POST['slSmoking'])?$_POST['slSmoking']:1;
                                                                        $sql = 'select SmokingID as Id, '.$_SESSION['lang'].'Smoking as LName from '.$table_prefix.'smoking order by TopSorted desc, LName asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="slDrinking" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">did your companion drink: </label>
                                                                <div class="col-sm-5">
                                                                    <select class="form-control" id="slDrinking" name="slDrinking">
                                                                    <?php
                                                                        $sel = isset($_POST['slDrinking'])?$_POST['slDrinking']:1;
                                                                        $sql = 'select DrinkingID as Id, '.$_SESSION['lang'].'Drinking as LName from '.$table_prefix.'drinking order by TopSorted desc, LName asc';
                                                                            dropdownlist($sql, $sel);
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        
                                                            <div class="form-group row">
                                                                <label for="enccountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter country: </label>

                                                                <div class="col-sm-8">
                                                                    <select class="bootstrap-select form-control" name="EncCountryID" id="EncCountryID">
                                                                    <option value="0"><?php echo $select;?></option>

                                                                    <?php
                                                                        $sel = isset($_POST['EncCountryID'])?$_POST['EncCountryID']:0;
                                                                        $sqlcountry = 'select CountryID as Id, Country as LName from '.$table_prefix.'countries order by TopSorted desc, LName asc';
                                                                            dropdownlist($sqlcountry, $sel);
                                                                    ?>

                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="enccountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter state: </label>

                                                                <div class="col-sm-8">
                                                                    <select class="bootstrap-select form-control" name="EncStateID" id="EncStateID">

                                                                    <?php
                                                                        if(isset($_POST['EncCountryID']) && $_POST['EncCountryID']>0){
                                                                            $sql = 'select StateID as Id, State as LName from '.$table_prefix.'states where CountryID = '.intval($_POST['EncCountryID']).' order by TopSorted desc, LName asc';
                                                                                dropdownlist($sql, $_POST['EncStateID']);
                                                                                }
                                                                            else{
                                                                            echo '<option value="0">'.$select.'</option>';
                                                                        }
                                                                    ?>

                                                                    </select>
                                                                </div>
                                                            </div>                                                                
                                                                
                                                            <div class="form-group row">
                                                                <label for="enccity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter city: </label>

                                                                <div class="col-sm-8">
                                                                    <input type="text" class="form-control js-websitegroup-validation" name="EncCity" value="" maxlength="100" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-1">&nbsp;</div>
                                                            
                                                    </div>                                                    
                                                    
                                                    
                                                    
                                                    <h5 style="color:#813ee8;">encounter location</h5>
                                                    <hr style="color:#813ee8;">
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            
                                                            <div class="form-group row">
                                        <legend class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</legend>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulSexPref[]" value="cfs">
                                                <label class="form-check-label" for="chCompIndulSexPref">
                                                    CFS = covered full service sex (sex with condom)
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulSexPref[]" value="gfe">
                                                <label class="form-check-label" for="chCompIndulSexPref">
                                                    GFE = girlfriend experience
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulSexPref[]" value="pse">
                                                <label class="form-check-label" for="chCompIndulSexPref">
                                                    PSE = porn star experience
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulSexPref[]" value="nr">
                                                <label class="form-check-label" for="chCompIndulSexPref">
                                                    NR = no rush
                                                </label>
                                            </div>
                                        </div>
                                    </div>
        <!-- end INDULGENCE SEX PREFS --> 
        
                                    <hr>
                                    
        <!-- begin INDULGENCE BJ PREFS -->                            
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">bj: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulBJ'])){
                                                    if($_POST['rdCompIndulBJ']==1){
                                                        $compindulbj1 = ' checked="checked"';
                                                        $compindulbj2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulBJ']==2){
                                                        $compindulbj1 = '';
                                                        $compindulbj2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulbj1 = ' checked="checked"';
                                                        $compindulbj2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulbj1;?> name="rdCompIndulBJ" value="1">
                                                    <label class="form-check-label" for="rdCompIndulBJ" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulbj2;?> name="rdCompIndulBJ" value="2">
                                                    <label class="form-check-label" for="rdCompIndulBJ" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-9">
                                                <small id="bjHelpBlock" class="form-text text-muted">
                                                if you selected &quot;yes&quot; above, please check all that apply below.
                                                </small>
                                            </div>
                                        </div>                                        
                                        
                                        
                                    </fieldset>

                                    <div class="form-group row">
                                        <div class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbj">
                                                <label class="form-check-label" for="chCompIndulBJPref">
                                                    BBBJ = bare back blow job = bj without condom
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbjtc">
                                                <label class="form-check-label" for="chCompIndulBJPref">
                                                    BBBJTC = bare back blow job to completion
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <div class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbjtcim">
                                                <label class="form-check-label" for="chCompIndulBJPref">
                                                    BBBJTCIM = bare back blow job to completion, cum in mouth
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbjtcnqns">
                                                <label class="form-check-label" for="chCompIndulBJPref">
                                                    BBBJTCNQNS = bare back blow job to completion, no quit, no spit
                                                </label>
                                            </div>
                                        </div>
                                    </div>                                    
                                    
                                    <div class="form-group row">
                                        <div class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbjtcws">
                                                <label class="form-check-label" for="chCompIndulBJPref">
                                                    BBBJTCWS = bare back blow job to completion with swallow
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="bbbjwf">
                                                <label class="form-check-label" for="chCompIndulBJPref">
                                                    BBBJWF = bare back blow job with facial
                                                </label>
                                            </div>
                                        </div>
                                    </div> 
                                    
                                    <div class="form-group row">
                                        <div class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulBJPref[]" value="cbj">
                                                <label class="form-check-label" for="chCompIndulBJPref">
                                                    CBJ = covered blow job = BJ with condom
                                                </label>
                                            </div>
                                        </div>
                                       
                                    </div> 
        <!-- end INDULGENCE BJ PREFS -->
        
                                    <hr>

        <!-- begin INDULGENCE HJ PREFS -->                            
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">hj: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulHJ'])){
                                                    if($_POST['rdCompIndulHJ']==1){
                                                        $compindulhj1 = ' checked="checked"';
                                                        $compindulhj2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulHJ']==2){
                                                        $compindulhj1 = '';
                                                        $compindulhj2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulhj1 = ' checked="checked"';
                                                        $compindulhj2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulhj1;?> name="rdCompIndulHJ" value="1">
                                                    <label class="form-check-label" for="rdCompIndulHJ" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulhj2;?> name="rdCompIndulHJ" value="2">
                                                    <label class="form-check-label" for="rdCompIndulHJ" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </fieldset>    
        <!-- begin INDULGENCE HJ PREFS --> 
        
                                    <hr>
                                    
        <!-- begin INDULGENCE KISSING PREFS --> 
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">kissing: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulKiss'])){
                                                    if($_POST['rdCompIndulKiss']==1){
                                                        $compindulkiss1 = ' checked="checked"';
                                                        $compindulkiss2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulKiss']==2){
                                                        $compindulkiss1 = '';
                                                        $compindulkiss2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulkiss1 = ' checked="checked"';
                                                        $compindulkiss2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulkiss1;?> name="rdCompIndulKiss" value="1">
                                                    <label class="form-check-label" for="rdCompIndulKiss" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulkiss2;?> name="rdCompIndulKiss" value="2">
                                                    <label class="form-check-label" for="rdCompIndulKiss" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-9">
                                                <small id="kissHelpBlock" class="form-text text-muted">
                                                if you selected &quot;yes&quot; above, please check all that apply below.
                                                </small>
                                            </div>
                                        </div>                                        
                                        
                                    </fieldset>

                                    <div class="form-group row">
                                        <div class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulKissPref[]" value="dfk">
                                                <label class="form-check-label" for="chCompIndulKissPref">
                                                    DFK = deep french kissing, open mouth with tongue
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulKissPref[]" value="fk">
                                                <label class="form-check-label" for="chCompIndulKissPref">
                                                    FK = french kiss = kissing with tongue insertion
                                                </label>
                                            </div>
                                        </div>
                                    </div>                                                                        
                                    
                                    <div class="form-group row">
                                        <div class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulKissPref[]" value="lk">
                                                <label class="form-check-label" for="chCompIndulKissPref">
                                                    LK = light kissing, closed mouth
                                                </label>
                                            </div>
                                        </div>
                                    </div>
        <!-- end INDULGENCE KISSING PREFS -->
        
                                    <hr>
                                    
        <!-- begin INDULGENCE LICK PUSSY PREFS -->                            
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">lick pussy: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulLickpussy'])){
                                                    if($_POST['rdCompIndulLickpussy']==1){
                                                        $compindullickpussy1 = ' checked="checked"';
                                                        $compindullickpussy2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulLickpussy']==2){
                                                        $compindullickpussy1 = '';
                                                        $compindullickpussy2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindullickpussy1 = ' checked="checked"';
                                                        $compindullickpussy2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindullickpussy1;?> name="rdCompIndulLickpussy" value="1">
                                                    <label class="form-check-label" for="rdCompIndulLickpussy" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindullickpussy2;?> name="rdCompIndulLickpussy" value="2">
                                                    <label class="form-check-label" for="rdCompIndulLickpussy" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>  
                                    
                                    <hr>
                                    
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">touch pussy: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulTouchpussy'])){
                                                    if($_POST['rdCompIndulTouchpussy']==1){
                                                        $compindultouchpussy1 = ' checked="checked"';
                                                        $compindultouchpussy2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulTouchpussy']==2){
                                                        $compindultouchpussy1 = '';
                                                        $compindultouchpussy2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindultouchpussy1 = ' checked="checked"';
                                                        $compindultouchpussy2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindultouchpussy1;?> name="rdCompIndulTouchpussy" value="1">
                                                    <label class="form-check-label" for="rdCompIndulTouchpussy" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindultouchpussy2;?> name="rdCompIndulTouchpussy" value="2">
                                                    <label class="form-check-label" for="rdCompIndulTouchpussy" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-9">
                                                <small id="touchpussyHelpBlock" class="form-text text-muted">
                                                if you selected &quot;yes&quot; above, please check all that apply below.
                                                </small>
                                            </div>
                                        </div>                                        
                                        
                                    </fieldset>
                                    
                                    
                                    <div class="form-group row">
                                        <div class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulTouchpussyPref[]" value="fiv">
                                                <label class="form-check-label" for="chCompIndulTouchpussyPref">
                                                    FIV = finger in vagina
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulTouchpussyPref[]" value="fov">
                                                <label class="form-check-label" for="chCompIndulTouchpussyPref">
                                                    FOV = finger outside vagina
                                                </label>
                                            </div>
                                        </div>
                                    </div>                                                                        
                                    
                                    <hr>
                                    
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">rimming: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulRim'])){
                                                    if($_POST['rdCompIndulRim']==1){
                                                        $compindulrim1 = ' checked="checked"';
                                                        $compindulrim2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulRim']==2){
                                                        $compindulrim1 = '';
                                                        $compindulrim2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulrim1 = ' checked="checked"';
                                                        $compindulrim2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulrim1;?> name="rdCompIndulRim" value="1">
                                                    <label class="form-check-label" for="rdCompIndulRim" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulrim2;?> name="rdCompIndulRim" value="2">
                                                    <label class="form-check-label" for="rdCompIndulRim" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-9">
                                                <small id="rimHelpBlock" class="form-text text-muted">
                                                if you selected &quot;yes&quot; above, please check all that apply below.
                                                </small>
                                            </div>
                                        </div>                                        
                                        
                                    </fieldset>
                                    
                                    
                                    <div class="form-group row">
                                        <div class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulRimPref[]" value="rimr">
                                                <label class="form-check-label" for="chCompIndulRimPref">
                                                    RIMR = receive analingus
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulRimPref[]" value="rimg">
                                                <label class="form-check-label" for="chCompIndulRimPref">
                                                    RIMG = give analingus
                                                </label>
                                            </div>
                                        </div>
                                    </div>                                                                                      

                                    <hr>
                                    
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">anal: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulAnal'])){
                                                    if($_POST['rdCompIndulAnal']==1){
                                                        $compindulanal1 = ' checked="checked"';
                                                        $compindulanal2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulAnal']==2){
                                                        $compindulanal1 = '';
                                                        $compindulanal2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulanal1 = ' checked="checked"';
                                                        $compindulanal2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulanal1;?> name="rdCompIndulAnal" value="1">
                                                    <label class="form-check-label" for="rdCompIndulAnal" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulanal2;?> name="rdCompIndulAnal" value="2">
                                                    <label class="form-check-label" for="rdCompIndulAnal" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-9">
                                                <small id="analHelpBlock" class="form-text text-muted">
                                                if you selected &quot;yes&quot; above, please check all that apply below.
                                                </small>
                                            </div>
                                        </div>                                        
                                        
                                    </fieldset>
                                    
                                    
                                    <div class="form-group row">
                                        <div class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulAnalPref[]" value="ddp">
                                                <label class="form-check-label" for="chCompIndulAnalPref">
                                                    DDP = double digit penetration (pussy and ass)
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulAnalPref[]" value="dp">
                                                <label class="form-check-label" for="chCompIndulAnalPref">
                                                    DP = double penetration, two guys on one girl
                                                </label>
                                            </div>
                                        </div>
                                    </div>                                              
                                    
                                    <div class="form-group row">
                                        <div class="col-form-label col-md-3 pt-0" for="complikes" style="font-weight:400;font-size:14px;">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                                <input type="checkbox" class="form-check-input" name="chCompIndulAnalPref[]" value="grk">
                                                <label class="form-check-label" for="chCompIndulAnalPref">
                                                    GRK = greek (anal sex, back door)
                                                </label>
                                            </div>
                                        </div>
                    
                                    </div> 
                                    
                                    <hr>
                                    
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">multiple pops: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulMsog'])){
                                                    if($_POST['rdCompIndulMsog']==1){
                                                        $compindulmsog1 = ' checked="checked"';
                                                        $compindulmsog2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulMsog']==2){
                                                        $compindulmsog1 = '';
                                                        $compindulmsog2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulmsog1 = ' checked="checked"';
                                                        $compindulmsog2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulmsog1;?> name="rdCompIndulMsog" value="1">
                                                    <label class="form-check-label" for="rdCompIndulMsog" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulmsog2;?> name="rdCompIndulMsog" value="2">
                                                    <label class="form-check-label" for="rdCompIndulMsog" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    
                                    <hr>

                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">two girl action: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulTwogirl'])){
                                                    if($_POST['rdCompIndulTwogirl']==1){
                                                        $compindultwogirl1 = ' checked="checked"';
                                                        $compindultwogirl2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulTwogirl']==2){
                                                        $compindultwogirl1 = '';
                                                        $compindultwogirl2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindultwogirl1 = ' checked="checked"';
                                                        $compindultwogirl2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindultwogirl1;?> name="rdCompIndulTwogirl" value="1">
                                                    <label class="form-check-label" for="rdCompIndulTwogirl" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindultwogirl2;?> name="rdCompIndulTwogirl" value="2">
                                                    <label class="form-check-label" for="rdCompIndulTwogirl" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    
                                    <hr>
                                    
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">bring second companion: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulSecondcomp'])){
                                                    if($_POST['rdCompIndulSecondcomp']==1){
                                                        $compindulsecondcomp1 = ' checked="checked"';
                                                        $compindulsecondcomp2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulSecondcomp']==2){
                                                        $compindulsecondcomp1 = '';
                                                        $compindulsecondcomp2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulsecondcomp1 = ' checked="checked"';
                                                        $compindulsecondcomp2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulsecondcomp1;?> name="rdCompIndulSecondcomp" value="1">
                                                    <label class="form-check-label" for="rdCompIndulSecondcomp" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulsecondcomp2;?> name="rdCompIndulSecondcomp" value="2">
                                                    <label class="form-check-label" for="rdCompIndulSecondcomp" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <hr>
                                    
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">two guys at one time: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulTwoguy'])){
                                                    if($_POST['rdCompIndulTwoguy']==1){
                                                        $compindultwoguy1 = ' checked="checked"';
                                                        $compindultwoguy2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulTwoguy']==2){
                                                        $compindultwoguy1 = '';
                                                        $compindultwoguy2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindultwoguy1 = ' checked="checked"';
                                                        $compindultwoguy2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindultwoguy1;?> name="rdCompIndulTwoguy" value="1">
                                                    <label class="form-check-label" for="rdCompIndulTwoguy" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindultwoguy2;?> name="rdCompIndulTwoguy" value="2">
                                                    <label class="form-check-label" for="rdCompIndulTwoguy" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    
                                    <hr>
                                    
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">video with permission: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulVideo'])){
                                                    if($_POST['rdCompIndulVideo']==1){
                                                        $compindulvideo1 = ' checked="checked"';
                                                        $compindulvideo2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulVideo']==2){
                                                        $compindulvideo1 = '';
                                                        $compindulvideo2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulvideo1 = ' checked="checked"';
                                                        $compindulvideo2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulvideo1;?> name="rdCompIndulVideo" value="1">
                                                    <label class="form-check-label" for="rdCompIndulVideo" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulvideo2;?> name="rdCompIndulVideo" value="2">
                                                    <label class="form-check-label" for="rdCompIndulVideo" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <hr>

                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">photos with permission: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulPhotos'])){
                                                    if($_POST['rdCompIndulPhotos']==1){
                                                        $compindulphotos1 = ' checked="checked"';
                                                        $compindulphotos2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulPhotos']==2){
                                                        $compindulphotos1 = '';
                                                        $compindulphotos2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulphotos1 = ' checked="checked"';
                                                        $compindulphotos2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulphotos1;?> name="rdCompIndulPhotos" value="1">
                                                    <label class="form-check-label" for="rdCompIndulPhotos" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulphotos2;?> name="rdCompIndulPhotos" value="2">
                                                    <label class="form-check-label" for="rdCompIndulPhotos" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    
                                    <hr>
                                    
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">massage: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulMassage'])){
                                                    if($_POST['rdCompIndulMassage']==1){
                                                        $compindulmassage1 = ' checked="checked"';
                                                        $compindulmassage2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulMassage']==2){
                                                        $compindulmassage1 = '';
                                                        $compindulmassage2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulmassage1 = ' checked="checked"';
                                                        $compindulmassage2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulmassage1;?> name="rdCompIndulMassage" value="1">
                                                    <label class="form-check-label" for="rdCompIndulMassage" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulmassage2;?> name="rdCompIndulMassage" value="2">
                                                    <label class="form-check-label" for="rdCompIndulMassage" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <hr>

                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">s &amp; m/b &amp; d: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulSMBD'])){
                                                    if($_POST['rdCompIndulSMBD']==1){
                                                        $compindulsmbd1 = ' checked="checked"';
                                                        $compindulsmbd2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulSMBD']==2){
                                                        $compindulsmbd1 = '';
                                                        $compindulsmbd2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulsmbd1 = ' checked="checked"';
                                                        $compindulsmbd2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulsmbd1;?> name="rdCompIndulSMBD" value="1">
                                                    <label class="form-check-label" for="rdCompIndulSMBD" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulsmbd2;?> name="rdCompIndulSMBD" value="2">
                                                    <label class="form-check-label" for="rdCompIndulSMBD" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    
                                    <hr>
                                    
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-3 pt-0" style="font-weight:400;font-size:14px;">squirt: </legend>
                                            
                                            <?php
                                                if(isset($_POST['rdCompIndulSquirt'])){
                                                    if($_POST['rdCompIndulSquirt']==1){
                                                        $compindulsquirt1 = ' checked="checked"';
                                                        $compindulsquirt2 = '';
                                                        }
                                                    elseif($_POST['rdCompIndulSquirt']==2){
                                                        $compindulsquirt1 = '';
                                                        $compindulsquirt2 = ' checked="checked"';
                                                        }
                                                    }
                                                    else{
                                                        $compindulsquirt1 = ' checked="checked"';
                                                        $compindulsquirt2 = '';
                                                        }
                                            ?>
                                            
                                            <div class="col-sm-9">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulsquirt1;?> name="rdCompIndulSquirt" value="1">
                                                    <label class="form-check-label" for="rdCompIndulSquirt" style="font-weight:400;font-size:14px;">yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo $compindulsquirt2;?> name="rdCompIndulSquirt" value="2">
                                                    <label class="form-check-label" for="rdCompIndulSquirt" style="font-weight:400;font-size:14px;">no</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    
                                </div>
                                <div class="col-lg-1">&nbsp;</div>
                                                        
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">encounter location</h5>
                                                    <hr style="color:#813ee8;">
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                                        
                                                        <div class="col-lg-10">
                                                            
                                                            <div class="form-group row">
                                                                <label for="enccountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter country: </label>

                                                                <div class="col-sm-8">
                                                                    <select class="bootstrap-select form-control" name="EncCountryID" id="EncCountryID">
                                                                    <option value="0"><?php echo $select;?></option>

                                                                    <?php
                                                                        $sel = isset($_POST['EncCountryID'])?$_POST['EncCountryID']:0;
                                                                        $sqlcountry = 'select CountryID as Id, Country as LName from '.$table_prefix.'countries order by TopSorted desc, LName asc';
                                                                            dropdownlist($sqlcountry, $sel);
                                                                    ?>

                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="enccountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter state: </label>

                                                                <div class="col-sm-8">
                                                                    <select class="bootstrap-select form-control" name="EncStateID" id="EncStateID">

                                                                    <?php
                                                                        if(isset($_POST['EncCountryID']) && $_POST['EncCountryID']>0){
                                                                            $sql = 'select StateID as Id, State as LName from '.$table_prefix.'states where CountryID = '.intval($_POST['EncCountryID']).' order by TopSorted desc, LName asc';
                                                                                dropdownlist($sql, $_POST['EncStateID']);
                                                                                }
                                                                            else{
                                                                            echo '<option value="0">'.$select.'</option>';
                                                                        }
                                                                    ?>

                                                                    </select>
                                                                </div>
                                                            </div>                                                                
                                                                
                                                            <div class="form-group row">
                                                                <label for="enccity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter city: </label>

                                                                <div class="col-sm-8">
                                                                    <input type="text" class="form-control js-websitegroup-validation" name="EncCity" value="" maxlength="100" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-1">&nbsp;</div>
                                                            
                                                    </div>
                                                            
                                                    <h5 style="color:#813ee8;">description of encounter with your companion</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">proposed encounter date: </label>
                                                                    
                                                                <script type="text/javascript">
                                                                    $(function () {
                                                                        $('#datetimepicker4').datetimepicker({
                                                                            format: 'L'
                                                                        });
                                                                    });
                                                                </script>
                                                                
                                                                <div class="input-group date col-sm-5" id="datetimepicker4" data-target-input="nearest">
                                                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                                                                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                                                        <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                                                    </div>
                                                                </div>
                                                            </div>                   
                                                            
                                                            <div class="form-group row">
                                                                <label for="encloc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter duration: </label>
                                                                <div class="col-sm-4">
                                                                    <select class="bootstrap-select form-control" id="EncDuration" name="EncLocationID" title="" tabindex="-98">
                                                                        <option value="" label="- select one -"></option>
                                                                        <option value="30">30 mins</option>
                                                                        <option value="60">60 mins</option>
                                                                        <option value="90">90 mins</option>
                                                                        <option value="2">2 hrs</option>
                                                                        <option value="3">3 hrs</option>
                                                                        <option value="4">4 hrs</option>
                                                                        <option value="overnight">overnight</option>
                                                                        <option value="other">other</option>
                                                                    </select>
                                                                </div>
                                                            </div>    
                                                            
                                                            <div class="form-group row">
                                                                <label for="encDonation" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">donation: </label>
                                                                <div class="col-sm-3">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">$</span>
                                                                        <input type="text" class="form-control js-websitegroup-validation" name="EncDonation" value="" maxlength="100" required>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="encloc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">encounter location: </label>
                                                                <div class="col-sm-5">
                                                                    <select class="bootstrap-select form-control" id="EnLocationID" name="EncLocationID" title="" tabindex="-98">
                                                                        <option value="" label="- select one -"></option>
                                                                        <option value="1">her place</option>
                                                                        <option value="2">her hotel</option>
                                                                        <option value="3">my place</option>
                                                                        <option value="4">my hotel</option>
                                                                        <option value="5">other</option>
                                                                    </select>
                                                                </div>
                                                            </div>                                                            
                                                                
                                                        </div>
                                
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">encounter atmosphere</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">type of encounter: </label>                                                    
                                                                <div class="col-sm-6">   
                                                                
                                                                    <select class="bootstrap-select form-control" name="EncTypeID" title="" tabindex="-98">
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="1">gfe - girlfriend exp</option>
                                                                        <option value="2">pse - pornstar exp</option>
                                                                        <option value="3">erotic massage - body rub</option> 
                                                                        <option value="4">blow job</option>
                                                                        <option value="5">body slide - companion slid their naked body along your body</option>
                                                                        <option value="6">hand job</option>
                                                                        <option value="7">anal sex (also known as greek)</option>
                                                                        <option value="8">tie &amp; tease - companion gently tied your hands and/or feet</option>
                                                                        <option value="9">fantasy - companion dressed up in a costume and /or role-play</option>
                                                                        <option value="10">b &amp; d - being whipped, flogged, caned, tied up, told what to do</option>
                                                                        <option value="11">s &amp; m - a more extreme form of b&amp;d</option>                                   
                                                                    </select>
                                                                
                                                                </div>
                                                            </div>
            <!-- begin OVERALL LOOKS? -->                                                
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's overall looks: </label>
                                                                <div class="col-sm-6"> 
                                                                    
                                                                    <select class="bootstrap-select form-control" name="CompLooksID" title="" tabindex="-98">
                                                                        <option value="" label="<?php echo $select;?>"></option>
                                                                        <option value="1">1 - i was frightened</option>
                                                                        <option value="2">2 - needed a blindfold</option>
                                                                        <option value="3">3 - ugly</option>
                                                                        <option value="4">4 - ok if i was drunk</option>
                                                                        <option value="5">5 - average</option>
                                                                        <option value="6">6 - pretty</option>
                                                                        <option value="7">7 - very attractive</option>
                                                                        <option value="8">8 - super hot</option>
                                                                        <option value="9">9 - could be a model</option>
                                                                        <option value="10">10 - i was in awe</option>
                                                                    </select>
                                                                
                                                                </div>
                                                            </div>
            <!-- end OVERALL LOOKS? -->                                                
            
            <!-- begin SMOKES? -->
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">does companion smoke: </label>
                                                                    <div class="col-sm-6"> 
                                                                        
                                                                        <select class="bootstrap-select form-control" name="CompSmokeID" title="" tabindex="-98" required>
                                                                            <option value="" label="<?php echo $select;?>"></option>
                                                                            <option value="1">no</option>
                                                                            <option value="2">yes - but not during session</option>
                                                                            <option value="3">yes - during session</option>
                                                                            <option value="4">yes - thats all i could smell</option>
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
            <!-- end SMOKES? -->

            <!-- begin COMP ATTITUDE -->
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's attitude: </label>
                                                                    <div class="col-sm-6">
                                                                        <select class="bootstrap-select form-control" name="CompAttitudeID" title="" tabindex="-98">
                                                                            <option value="" label="<?php echo $select;?>"></option>
                                                                            <option value="1">apathetic - indifferent - lacked energy or concern</option>
                                                                            <option value="2">bitter - exhibited strong animosity</option>
                                                                            <option value="3">bored - gave off an air that they would rather be elsewhere</option>
                                                                            <option value="3">cynical - questioned sincerity and goodness of people</option>
                                                                            <option value="4">condescending - gave off a feeling of superiority</option>
                                                                            <option value="5">callous - unfeeling - insensitive</option>
                                                                            <option value="6">chill - super relaxed</option>
                                                                            <option value="7">contemplative - thoughtful, reflective</option>
                                                                            <option value="8">conventional - lacked spontaneity</option>
                                                                            <option value="9">fanciful - used their imagination</option>
                                                                            <option value="10">fanciful - used their imagination</option>
                                                                            <option value="11">gloomy - dark - sad</option>
                                                                            <option value="12">haughty - arrogant</option>
                                                                            <option value="13">hot-tempered - easily angered</option>
                                                                            <option value="14">intimate - very familiar</option>
                                                                            <option value="15">judgmental - critical</option>
                                                                            <option value="16">jovial - happy</option>
                                                                            <option value="17">matter-of-fact - not fanciful or emotional</option>
                                                                            <option value="18">mocking - treated with contempt</option>
                                                                            <option value="19">optimistic - hopeful, cheerful</option>
                                                                            <option value="20">patronizing - had air of condescension</option>
                                                                            <option value="21">pessimistic - saw the worst side of things</option>
                                                                            <option value="21">professional - performed like a pro</option>
                                                                            <option value="22">ridiculing - made fun of</option>
                                                                            <option value="23">sincere - without deceit or pretense; genuine</option>
                                                                            <option value="24">whimsical - odd, strange, fantastic; fun</option>
                                                                        </select>
                                                                    </div>
                                                                    
                                                                </div>
            <!-- end COMP ATTITUDE -->
            
            <!-- begin OVERALL ATMOSPHERE -->
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">overall atmosphere: </label>
                                                                    <div class="col-sm-6">
                                                                        <select class="bootstrap-select form-control" id="EncAtmosphereID" name="EncAtmosphereID" title="" tabindex="-98">
                                                                            <option value="" label="<?php echo $select;?>"></option>
                                                                            <option value="1">very scary</option>
                                                                            <option value="2">a bit scary</option>
                                                                            <option value="3">extremely awkward</option>
                                                                            <option value="4">uptight</option>
                                                                            <option value="5">like a first date</option>
                                                                            <option value="6">chill</option>
                                                                            <option value="7">sexy vibe</option>
                                                                            <option value="8">like a party</option>
                                                                            <option value="9">like a workout</option>
                                                                        </select>
                                                                    </div>
                                                                    
                                                                </div>
            <!-- end OVERALL ATMOSPHERE -->
            
            <!-- begin OVERALL PERFORMANCE -->
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">overall performance: </label>
                                                                    <div class="col-sm-6">
                                                                        <select class="bootstrap-select form-control" id="CompPerformanceID" name="CompPerformanceID" title="" tabindex="-98">
                                                                            <option value="" label="<?php echo $select;?>"></option>
                                                                            <option value="1">1 - a complete rip-off</option>
                                                                            <option value="2">2 - i wish we hadn't met</option>
                                                                            <option value="3">3 - not worth the effort</option>
                                                                            <option value="4">4 - she just laid there</option>
                                                                            <option value="5">5 - average</option>
                                                                            <option value="6">6 - nice Time</option>
                                                                            <option value="7">7 - good time</option>
                                                                            <option value="8">8 - went the extra mile</option>
                                                                            <option value="9">9 - we were meant to be together</option>
                                                                            <option value="10">10 - she blew my mind</option>
                                                                        </select>
                                                                    </div>
                                                                    
                                                                </div>
            <!-- end OVERALL PERFORMANCE -->
                               
                                                        </div>
                                                        
                                                        <div class="col-lg-1">&nbsp;</div>
                                                        
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">verifications</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            
                                    <!-- begin INDULTRUE -->
                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">indulgences were as described: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdIndulTrue'])){
                                                                            if($_POST['rdIndulTrue']==1){
                                                                                $indultrue1 = ' checked="checked"';
                                                                                $indultrue2 = '';
                                                                                }
                                                                            elseif($_POST['rdIndulTrue']==2){
                                                                                $indultrue1 = '';
                                                                                $indultrue2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $indultrue1 = '';
                                                                                $indultrue2 = ' checked="checked"';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-8">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $indultrue1;?> name="rdIndulTrue" value="1">
                                                                            <label class="form-check-label" for="rdIndulTrue" style="font-weight:400;font-size:14px;">yes</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $indultrue2;?> name="rdIndulTrue" value="2">
                                                                            <label class="form-check-label" for="rdIndulTrue" style="font-weight:400;font-size:14px;">no</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>    
                                    <!-- end INDULTRUE -->                                                             
                                                            
                                    <!-- begin PHOTOSTRUE -->
                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">companion looked like photos: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdCompPhotoTrue'])){
                                                                            if($_POST['rdCompPhotoTrue']==1){
                                                                                $compphototrue1 = ' checked="checked"';
                                                                                $compphototrue2 = '';
                                                                                }
                                                                            elseif($_POST['rdCompPhotoTrue']==2){
                                                                                $compphototrue1 = '';
                                                                                $compphototrue2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $compphototrue1 = '';
                                                                                $compphototrue2 = ' checked="checked"';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-8">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $compphototrue1;?> name="rdCompPhotoTrue" value="1">
                                                                            <label class="form-check-label" for="rdCompPhotoTrue" style="font-weight:400;font-size:14px;">yes</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $compphototrue2;?> name="rdCompPhotoTrue" value="2">
                                                                            <label class="form-check-label" for="rdCompPhotoTrue" style="font-weight:400;font-size:14px;">no</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>    
                                    <!-- end PHOTOSTRUE -->                                                                                                    
                                                            
                                    <!-- begin COMPONTIMETRUE -->
                                                            <fieldset class="form-group">
                                                                <div class="row">
                                                                    <legend class="col-form-label col-sm-4 pt-0" style="font-weight:400;font-size:14px;">companion was on time: </legend>

                                                                    <?php
                                                                        if(isset($_POST['rdCompOntimeTrue'])){
                                                                            if($_POST['rdCompOntimeTrue']==1){
                                                                                $compontimetrue1 = ' checked="checked"';
                                                                                $compontimetrue2 = '';
                                                                                }
                                                                            elseif($_POST['rdCompOntimeTrue']==2){
                                                                                $compontimetrue1 = '';
                                                                                $compontimetrue2 = ' checked="checked"';
                                                                                }
                                                                            }
                                                                            else{
                                                                                $compontimetrue1 = '';
                                                                                $compontimetrue2 = ' checked="checked"';
                                                                                }
                                                                    ?>

                                                                    <div class="col-sm-8">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $compontimetrue1;?> name="rdCompOntimeTrue" value="1">
                                                                            <label class="form-check-label" for="rdCompOntimeTrue" style="font-weight:400;font-size:14px;">yes</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" <?php echo $compontimetrue2;?> name="rdCompOntimeTrue" value="2">
                                                                            <label class="form-check-label" for="rdCompOntimeTrue" style="font-weight:400;font-size:14px;">no</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>    
                                    <!-- end COMPONTIMETRUE -->                                                                                          
                                                            
                                                            
                                                            
                                                            <div class="form-group form-check-inline row">
                                                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">indulgences were as described:</label>
                                                                <div class="col-sm-8">
                                                                    <input class="form-check-input" type="radio" name="IndulgeTrue" id="IndulgeTrue" value="1" checked>&nbsp;yes&nbsp;
                                                                    <input class="form-check-input" type="radio" name="IndulgeTrue" id="IndulgeTrue" value="2">&nbsp;no&nbsp;
                                                                </div>
                                                            </div>
                                                        
                                                            <div class="form-group form-check-inline row">
                                                                
                                                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion looked like photos:&nbsp;</label>
                                                                <div class="col-sm-8">
                                                                    <input type="radio" id="CompPhotoTrue" name="CompPhotoTrue" value="1" checked>&nbsp;yes&nbsp;
                                                                    <input type="radio" id="CompPhotoTrue" name="CompPhotoTrue" value="0">&nbsp;no&nbsp;                                                                            
                                                                </div>
                                                            </div>                                                       
                                                            
                                                            <div class="form-group form-check-inline row">
                                                                
                                                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion was on time:&nbsp;</label>
                                                                <div class="col-sm-8">
                                                                    <input type="radio" id="CompOnTimeTrue" name="CompOnTimeTrue" value="1" checked>&nbsp;yes&nbsp;
                                                                    <input type="radio" id="CompOnTimeTrue" name="CompOnTimeTrue" value="0">&nbsp;no&nbsp;
                                                                </div>
                      
                                                            </div>
<!-- add in later                                                                        
                                                            <div class="form-group form-check-inline row">
                                                                
                                                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">companion's phone number was correct:&nbsp;</label>
                                                                <div class="col-sm-8">
                                                                    <input type="radio" id="CompOnTimeTrue" name="CompPhoneTrue" value="1" checked>&nbsp;yes&nbsp;
                                                                    <input type="radio" id="CompOnTimeTrue" name="CompPhoneTrue" value="0">&nbsp;no&nbsp;

                                                                </div>
                                                        
                                                            </div> -->
                                                        </div>
                                                        
                                                        <div class="col-lg-2">&nbsp;</div>
                                                        
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">general description of your encounter</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-2">&nbsp;</div>
                                
                                                        <div class="col-lg-8">
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">headline for your review:</label>
                                                                <div class="col-sm-8">    
                                                                    <input name="ReviewHeadline" value="" type="text" class="form-control" maxlength="60">
                                                                        <div class="textarea-feedback" style="color:#ff0000;">max 60 characters</div>
                                                                </div>
                                                            </div>
                                                                
                                                            general summary should include broad comments about your companion and your initial experience. please do not mention specific acts or donation - just tease your fellow members.
                                                            
                                                            <br>
                                                            <br>
                                                            
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">general summary:</label>
                                                                <div class="col-sm-8">
                                                                    <textarea class="bootstrap-select form-control review-form-textarea js-textarea " data-limit="4000" name="GenRevSummary" id="GenRevSummary" rows="2" style="overflow: hidden; word-wrap: break-word;"></textarea>
                                                                    <div class="textarea-feedback" style="color:#ff0000;">characters: <span class="textarea-feedback-content js-textarea-feedback" id="len1">4000 left</span>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                        <div class="col-lg-2">&nbsp;</div>
                                                        
                                                    </div>
                                                    
                                                    <h5 style="color:#813ee8;">detailed description of encounter</h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-2">&nbsp;</div>
                                
                                                        <div class="col-lg-8">
                                                            describe your companion, the experience, and whether or not you enjoyed it in graphic emotional and sexual terms. don't make this a recap of the general summary, instead, go for a blow-by-blow tell-all of your encounter from your own point of view.
                                                        
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">sexy details: </label>
                                                                <div class="col-sm-8">
                                                                    <textarea class="bootstrap-select form-control review-form-textarea js-textarea " data-limit="6000" name="SexyRevDetails" id="SexyRevDetails" rows="4" style="overflow: hidden; word-wrap: break-word;"></textarea>
                                                                    <div class="textarea-feedback" style="color:#ff0000;">characters: <span class="textarea-feedback-content js-textarea-feedback" id="len1">6000 left</span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-2">&nbsp;</div>
                                                    </div>
                                                
                                                    <div class="row">
                                                        <div class="col-lg-2">&nbsp;</div>
                                
                                                        <div class="col-lg-8">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">&nbsp;</div>
                                                                <div class="col-sm-8">
                                                                    <button class="btn btn-cli-pro" data-toggle="modal2" data-target="#submitReview">submit</button>
                                                                    <button type="button" formnovalidate="formnovalidate" class="btn btn-pro-cli js-save-draft-click">save draft</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-2">&nbsp;</div>
                                                    
                                                    </div>
                                                
                                                <div class="modal fade" id="submitReview" tabindex="-1" role="dialog" aria-labelledby="submitReviewLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                    <h5 class="modal-title" id="submitReviewLabel">Error</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <span class="icon-error">!</span>
                                                                <p>Your review wasn't added. Please try to do it later.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                            
