<script type="text/javascript">
    $.ter.TimezoneUtils.setTzOffsetCookieIfAbsent();

    $(window).on("load", (function () {
        
    }));

    $(function() {
        
    });
    //-->
</script>


<script language="javascript">
    function showDraftWarning() {
        bootbox.dialog({
            className: ' modal-alert warning',
            title: "<button type='button' class='sr-only close-x' data-dismiss='modal'>Close</button><i class='icon-warning'></i>",
            message: '<h5 class="modal-alert-subtitle">Warning!</h5>' +
            '<p class="modal-alert-text">It looks like you already started writing a review for a different provider. You would need to finish that review first or delete the draft prior to reviewing another provider.</p><button class="btn btn-primary" data-dismiss="modal">Finish Review</button>',
            onEscape: true,
            backdrop: true
        });
    }
</script>

        <script type="text/javascript">
            $(function () {
                var validator;

                $('.selectpicker').on('change', function () {
                    $(this).valid();
                });

                $("#CompPerformanceID").on("change", function(){
                    var selectedValue = $(this).val();
                    if (selectedValue !== '') {
                        var intValue = parseInt(selectedValue);
                        if (!isNaN(intValue) && intValue > 7) {
                            $.ter.DialogManager.showWarning('you have given a score higher than 7.  encounters with scores higher than 7 should include indulgences such as: kisses with tongue, bj without condom, more than one guy, really bi session or anal.');
                        }
                    }
                });

                var triggerAutosave = function () {
                    $(".js-autosave").trigger("ter.forceAutosave");
                };

                $(".js-submit-form").on("submit", function(e) {
                    if ($(".js-submit-form").valid()) {
                        $(".js-autosave").trigger("ter.stopAutosave");
                    }
                });

                $(".js-chemistry-rating").each( function() {
                    var rating = $(this).attr("data-rating"),
                        formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                    $(this).rateYo({
                        
                        fullStar: true,
                            totalStars: 5,
                    starWidth: "16px",
                    ratedFill: "#f300fe",
                    normalFill: "#dbe0e4",
                    precision: 2,
                    onSet: function (rating, rateYoInstance) {
                        formControl.val(rating);
                        if (formControl.val() != 0) {
                            formControl.valid();
                        };
                        triggerAutosave();
                    }
                });
            });
        
            $(".js-incalllocation-rating").each( function() {
                var rating = $(this).attr("data-rating"),
                    formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                $(this).rateYo({
                    
                    fullStar: true,
                        totalStars: 5,
                starWidth: "16px",
                ratedFill: "#f300fe",
                normalFill: "#dbe0e4",
                precision: 2,
                onSet: function (rating, rateYoInstance) {
                    formControl.val(rating);
                    if (formControl.val() != 0) {
                        formControl.valid();
                    };
                    triggerAutosave();
                }
            });
            });
        
            $(function(){
                $(".js-incalllocation-clear").on("click", function(){
                    var chemistry = $(".js-incalllocation-rating");
                    chemistry.rateYo("option", "rating", "0");
                    chemistry.parents(".form-control-wrapper").find($('.form-control')).val("");
                    $("#IncallLocationRating").val('');
                    triggerAutosave();
                });
            });


                $(".js-autosave .selectpicker").on('hidden.bs.select', function (e) {
                    triggerAutosave();
                });

                $(".js-autosave input:checkbox").on('change', function(e) {
                    triggerAutosave();
                });

                //delay for autosave in milliseconds
                var AUTO_SAVE_PERIOD = 2000;

                function timeFormatter(dateTime) {
                    var timeNow = new Date(dateTime);
                    var hours = timeNow.getHours();
                    var minutes = timeNow.getMinutes();
                    var seconds = timeNow.getSeconds();
                    var timeString = "" + ((hours > 12) ? hours - 12 : hours);
                    timeString += ((minutes < 10) ? ":0" : ":") + minutes;
                    timeString += ((seconds < 10) ? ":0" : ":") + seconds;
                    timeString += (hours >= 12) ? " PM" : " AM";

                    return timeString;
                }

                var submitDraftAutosaveCallback = function () {
                    updateAutosaveMessage();
                }

                var submitDraftAutosave = function () {
                    var form = $("#reviewDetailsForm");
                    var url = form.attr('action');
                    $("#isDraft").val("1");
                    $("#isAutosave").val("1");
                    var formData = form.serialize();
                    $("#isDraft").val("");
                    $("#isAutosave").val("");
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        success: function (data, textStatus, jqXHR) {
                            submitDraftAutosaveCallback();
                        },
                        error: function (jqXHR, status, error) {
                            if (status == 401) {
                                location.href = "/memberlaunch/login.asp?dest=/reviews/submitReviewDetails.asp?terId%3D253746";
                            }
                            //console.log(status + ": " + error);
                        }
                    });
                };

                var updateAutosaveMessage = function () {
                    var time = new Date().getTime();
                    $(".js-draft-autosave-message").show();
                    $(".js-draft-autosave-message").text("Review Draft autosaved " + timeFormatter(time));
                };

                $(".js-autosave").autoSave(function () {
                    submitDraftAutosave();
                }, AUTO_SAVE_PERIOD);

                $(".js-save-draft-click").on("click", function (event) {
                    $("#reviewDetailsForm").data('validator').cancelSubmit = true;
                    $("#reviewDetailsForm").validate().settings.ignore = "*";
                    $("#isDraft").val("1");
                    $("#reviewDetailsForm").submit();
                    return false;
                });

                
                validator = $('.js-submit-form').validate({
                    rules: {
                        Website: "required",
                        Location: "required",
                        //session atmosphere
                        Smokes: "required",
                        OverallLooks: "required",
                        Attitude: "required",
                        OverallAtmosphere: "required",
                        OverallPerformance: "required",
                        //visit details
                        GeneralDetails: "required",
                        TheJuicyDetails: "required",
                        ChemistryRating: "required"
                    },
                    messages: {
                        ChemistryRating: "Please select Chemistry star rating."
                    },
                    highlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").addClass("error");
                        $element.next(".btn-group").addClass("error");
                    },
                    unhighlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").removeClass("error");
                        $element.removeClass("error").next(".btn-group").removeClass("error");
                    },
                    ignore:"",
                    //TODO: move out to common js
                    invalidHandler: function(form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            if($(validator.errorList[0].element).is(":visible"))
                            {
                                validator.errorList[0].element.focus();
                            }
                            else
                            {
                                var elementOffset = $("#" + $(validator.errorList[0].element).attr("data-focusID")).offset().top;
                                if (elementOffset > $(window).height()){
                                    header = $(".header").height();
                                    $('html, body').animate({
                                        scrollTop: elementOffset - header - 30
                                    }, 1000);
                                }
                            }
                        }
                    }
                });
            });

        </script>

                                            <div class="js-draft-autosave-message warning-block" hidden="hidden"></div>

                                            
                                                
                                                    <form action="../mbrs/simple_review_submitted.php" method="POST" id="reviewDetailsForm" name="form1" class="js-autosave js-submit-form" novalidate="novalidate">
                                                        <input type="hidden" name="ReviewerStatusID" value="1">
                                                        <input type="hidden" name="ReviewerID" value="22">
                                                        <input type="hidden" name="CompanionID" value="<?php echo $person['UserID'];?>">
                                                        <input type="hidden" name="SubmittedOn" value="<?php date_default_timezone_set("America/New_York"); echo date("Y-m-d h:i:sa");?>">
                                                        <input type="hidden" name="ReviewStatus" value="0">                           

                                                <script language="javascript">
                                                    $(document).ready(function(){
                                                        $('#EncCountryID').change(function() {
                                                            var valchange = this.value;
                                                                $('#EncStateID').load('../includes/loadstates.php?countryId=' + valchange);
                                                            });
                                                        });
                                                </script>                                                        
                                                        
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    
                                                    <h5 style="color:#813ee8;">previous encounter with <?php echo $person['ProfileName'];?></h5>
                                                    <hr>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            
                                                            <div class="form-group row">
                                                                <label for="encdate" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">approximately when was your encounter: </label>                                                    
                                                                    
                                                                <script type="text/javascript">
                                                                    $(function () {
                                                                        $('#datetimepicker2').datetimepicker({
                                                                            format: 'MM/YYYY'
                                                                        });
                                                                    });
                                                                </script>
                                                                
                                                                <div class="input-group date col-sm-5" id="datetimepicker2" data-target-input="nearest">
                                                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2"/>
                                                                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                                                        <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="form-group row">
                                                                <label for="propprevenccity" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">previous encounter city: </label>

                                                                <div class="col-sm-5">
                                                                    <input type="text" class="form-control js-websitegroup-validation" name="PropPrevEncCity" value="" maxlength="100" required>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group row">
                                                                <label for="propprevenccity" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">briefly describe yourself: </label>

                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control js-websitegroup-validation" name="PropPrevEncCity" value="" maxlength="100" required>
                                                                    <small id="aboutmeHelpBlock" class="form-text text-muted">
                                                                        anything you can add here will help jog your companion's memory
                                                                    </small>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                        <div class="col-lg-1">&nbsp;</div>
                                                        
                                                    </div>
                                                
                                                    <div class="row">
                                                        <div class="col-lg-1">&nbsp;</div>
                                
                                                        <div class="col-lg-10">
                                                            <div class="form-group row">
                                                                <div class="col-sm-5">&nbsp;</div>
                                                                <div class="col-sm-5">
                                                                    <button class="btn btn-cli-pro" data-toggle="modal2" data-target="#submitReview">send request</button>
                                                                    <button type="button" formnovalidate="formnovalidate" class="btn btn-pro-cli" reset>reset</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-1">&nbsp;</div>
                                                    
                                                    </div>
                                                

                                            </form>
                                        </div>
                                    </div>
                                            
