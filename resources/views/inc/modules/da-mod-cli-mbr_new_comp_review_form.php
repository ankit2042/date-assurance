<script type="text/javascript">
    $.ter.TimezoneUtils.setTzOffsetCookieIfAbsent();

    $(window).on("load", (function () {
        
    }));

    $(function() {
        
    });
    //-->
</script>


<script language="javascript">
    function showDraftWarning() {
        bootbox.dialog({
            className: ' modal-alert warning',
            title: "<button type='button' class='sr-only close-x' data-dismiss='modal'>Close</button><i class='icon-warning'></i>",
            message: '<h5 class="modal-alert-subtitle">Warning!</h5>' +
            '<p class="modal-alert-text">It looks like you already started writing a review for a different provider. You would need to finish that review first or delete the draft prior to reviewing another provider.</p><button class="btn btn-primary" data-dismiss="modal">Finish Review</button>',
            onEscape: true,
            backdrop: true
        });
    }
</script>

        <script type="text/javascript">
            $(function () {
                var validator;

                $('.selectpicker').on('change', function () {
                    $(this).valid();
                });

                $("#OverallPerformance").on("change", function(){
                    var selectedValue = $(this).val();
                    if (selectedValue !== '') {
                        var intValue = parseInt(selectedValue);
                        if (!isNaN(intValue) && intValue > 7) {
                            $.ter.DialogManager.showWarning('You have selected a score higher than 7.  In order to qualify for extra points, services giving additional points must have been provided during the session.  The services which add an additional point are: Kiss with Tongue, Blow job without condom, more than one guy, really bi session or anal.');
                        }
                    }
                });

                var triggerAutosave = function () {
                    $(".js-autosave").trigger("ter.forceAutosave");
                };

                $(".js-submit-form").on("submit", function(e) {
                    if ($(".js-submit-form").valid()) {
                        $(".js-autosave").trigger("ter.stopAutosave");
                    }
                });

                $(".js-chemistry-rating").each( function() {
                    var rating = $(this).attr("data-rating"),
                        formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                    $(this).rateYo({
                        
                        fullStar: true,
                            totalStars: 5,
                    starWidth: "16px",
                    ratedFill: "#f300fe",
                    normalFill: "#dbe0e4",
                    precision: 2,
                    onSet: function (rating, rateYoInstance) {
                        formControl.val(rating);
                        if (formControl.val() != 0) {
                            formControl.valid();
                        };
                        triggerAutosave();
                    }
                });
            });
        
            $(".js-incalllocation-rating").each( function() {
                var rating = $(this).attr("data-rating"),
                    formControl =  $(this).parents(".form-control-wrapper").find($('.form-control'));
                $(this).rateYo({
                    
                    fullStar: true,
                        totalStars: 5,
                starWidth: "16px",
                ratedFill: "#ffce37",
                normalFill: "#dbe0e4",
                precision: 2,
                onSet: function (rating, rateYoInstance) {
                    formControl.val(rating);
                    if (formControl.val() != 0) {
                        formControl.valid();
                    };
                    triggerAutosave();
                }
            });
            });
        
            $(function(){
                $(".js-incalllocation-clear").on("click", function(){
                    var chemistry = $(".js-incalllocation-rating");
                    chemistry.rateYo("option", "rating", "0");
                    chemistry.parents(".form-control-wrapper").find($('.form-control')).val("");
                    $("#IncallLocationRating").val('');
                    triggerAutosave();
                });
            });


                $(".js-autosave .selectpicker").on('hidden.bs.select', function (e) {
                    triggerAutosave();
                });

                $(".js-autosave input:checkbox").on('change', function(e) {
                    triggerAutosave();
                });

                //delay for autosave in milliseconds
                var AUTO_SAVE_PERIOD = 2000;

                function timeFormatter(dateTime) {
                    var timeNow = new Date(dateTime);
                    var hours = timeNow.getHours();
                    var minutes = timeNow.getMinutes();
                    var seconds = timeNow.getSeconds();
                    var timeString = "" + ((hours > 12) ? hours - 12 : hours);
                    timeString += ((minutes < 10) ? ":0" : ":") + minutes;
                    timeString += ((seconds < 10) ? ":0" : ":") + seconds;
                    timeString += (hours >= 12) ? " PM" : " AM";

                    return timeString;
                }

                var submitDraftAutosaveCallback = function () {
                    updateAutosaveMessage();
                }

                var submitDraftAutosave = function () {
                    var form = $("#reviewDetailsForm");
                    var url = form.attr('action');
                    $("#isDraft").val("1");
                    $("#isAutosave").val("1");
                    var formData = form.serialize();
                    $("#isDraft").val("");
                    $("#isAutosave").val("");
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        success: function (data, textStatus, jqXHR) {
                            submitDraftAutosaveCallback();
                        },
                        error: function (jqXHR, status, error) {
                            if (status == 401) {
                                location.href = "/memberlaunch/login.asp?dest=/reviews/submitReviewDetails.asp?terId%3D253746";
                            }
                            //console.log(status + ": " + error);
                        }
                    });
                };

                var updateAutosaveMessage = function () {
                    var time = new Date().getTime();
                    $(".js-draft-autosave-message").show();
                    $(".js-draft-autosave-message").text("Review Draft autosaved " + timeFormatter(time));
                };

                $(".js-autosave").autoSave(function () {
                    submitDraftAutosave();
                }, AUTO_SAVE_PERIOD);

                $(".js-save-draft-click").on("click", function (event) {
                    $("#reviewDetailsForm").data('validator').cancelSubmit = true;
                    $("#reviewDetailsForm").validate().settings.ignore = "*";
                    $("#isDraft").val("1");
                    $("#reviewDetailsForm").submit();
                    return false;
                });

                
                validator = $('.js-submit-form').validate({
                    rules: {
                        Website: "required",
                        Location: "required",
                        //session atmosphere
                        Smokes: "required",
                        OverallLooks: "required",
                        Attitude: "required",
                        OverallAtmosphere: "required",
                        OverallPerformance: "required",
                        //visit details
                        GeneralDetails: "required",
                        TheJuicyDetails: "required",
                        ChemistryRating: "required"
                    },
                    messages: {
                        ChemistryRating: "Please select Chemistry star rating."
                    },
                    highlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").addClass("error");
                        $element.next(".btn-group").addClass("error");
                    },
                    unhighlight: function(element) {
                        var $element = $(element);
                        $element.addClass("error");
                        $element.parent(".form-control").removeClass("error");
                        $element.removeClass("error").next(".btn-group").removeClass("error");
                    },
                    ignore:"",
                    //TODO: move out to common js
                    invalidHandler: function(form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            if($(validator.errorList[0].element).is(":visible"))
                            {
                                validator.errorList[0].element.focus();
                            }
                            else
                            {
                                var elementOffset = $("#" + $(validator.errorList[0].element).attr("data-focusID")).offset().top;
                                if (elementOffset > $(window).height()){
                                    header = $(".header").height();
                                    $('html, body').animate({
                                        scrollTop: elementOffset - header - 30
                                    }, 1000);
                                }
                            }
                        }
                    }
                });
            });

        </script>

                                            <div class="js-draft-autosave-message warning-block" hidden="hidden"></div>

                                            <div class="da-form">
                                                <div class="review-form">
                                                    <form action="https://www.theeroticreview.com/reviews/submitReviewDetails.asp" method="POST" id="reviewDetailsForm" name="form1" class="js-autosave js-submit-form" novalidate="novalidate">
                                                        <input type="hidden" id="ServiceAddOnText1" name="ServiceAddOnText1" value="">
                                                        <input type="hidden" id="ServiceAddOnText2" name="ServiceAddOnText2" value="">
                                                        <input type="hidden" id="ServiceAddOnText3" name="ServiceAddOnText3" value="">
                                                        <input type="hidden" id="daId" name="daId" value="253746">
                                                        <input type="hidden" name="FromSubmit" value="1">
                                                        <input type="hidden" name="language" value="en">
                                                        <!--<input type="hidden" name="MemberName" value="doublenice2013"/>-->
                                                        <input type="hidden" name="memberID" value="2286218">
                                                        <input type="hidden" name="MemberEmail" value="bradsmitts@mail.com">
                                                        <input type="hidden" name="SessionID" value="2286218">
                                                        <input type="hidden" name="SessionUserName" value="doublenice2013">
                                                        <input type="hidden" name="ACTION" value="Submit">
                           
                                                        <input type="hidden" name="isDraft" id="isDraft" value="">
                                                        <input type="hidden" name="isAutosave" id="isAutosave" value="">

                                                        <div class="review-form-content">
                                                        
                                                        <h5>your information</h5>
        
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="form-group-label">reviewed by: </label>
                                                                <select class="bootstrap-select form-control" name="rAliasID" tabindex="-98">
                                                                    <option value="0"><?php echo $cliprofname;?></option>
                                                                </select>
                                                            </div>
                                                        </div>  

                                                        <br>
                                                
                                                        <div class="review-form-content">
                                
                                                            <h5>companion's contact information</h5>
                                    
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="form-group-label">companion's website/posting: </label>
                                                                <input type="text" class="form-control js-websitegroup-validation" name="Website" value="" maxlength="1000">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-group-label">session location: </label>
                                                                <input name="Location" value="" type="text" class="form-control" maxlength="50"> (e.g. her place, my hotel, etc.)
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                    <br>
                                                
                                                    <div class="review-form-content">
                                                        
                                                        <h5>session atmosphere</h5>
    
                                                        <div class="form-horizontal">                               
            <!-- begin OVERALL LOOKS? -->                                                
                                                            <div class="form-group">
                                                                <label class="form-group-label">overall looks: </label>
                                                                
                                                                <select class="bootstrap-select form-control" name="OverallLooks" title="" tabindex="-98">
                                                                    <option value="" label=" "></option>
                                                                    <option value="1">1 - I was really scared</option>
                                                                    <option value="2">2 - Ugly</option>
                                                                    <option value="3">3 - Homely</option>
                                                                    <option value="4">4 - Ok if you are drunk</option>
                                                                    <option value="5">5 - Plain</option>
                                                                    <option value="6">6 - Nice</option>
                                                                    <option value="7">7 - Attractive</option>
                                                                    <option value="8">8 - Really Hot</option>
                                                                    <option value="9">9 - Model material</option>
                                                                    <option value="10">10 - One in a lifetime</option>
                                                                </select>
                                                                
                                                            </div>
            <!-- end OVERALL LOOKS? -->                                                
            
            <!-- begin SMOKES? -->
                                                            <div class="form-group">
                                                                <label class="form-group-label">smokes: </label>
                                                                <select class="bootstrap-select form-control" name="Smokes" title="" tabindex="-98">
                                                                    <option value="" label=" "></option>
                                                                    <option value="0">No</option>
                                                                    <option value="1">Yes - But not during session</option>
                                                                    <option value="2">Yes - During session</option>
                                                                    <option value="3">Yes - Thats all I could smell</option>
                                                                </select>
                                                            </div>
            <!-- end SMOKES? -->

            <!-- begin ATTITUDE -->
                                                            <div class="form-group">
                                                                <label class="form-group-label ">overall attitude: </label>
                                                                <input name="Attitude" value="" type="text" class="form-control" maxlength="50">
                                                            </div>
            <!-- end ATTITUDE -->
            
            <!-- begin ATMOSPHERE -->
                                                            <div class="form-group">
                                                                <label class="form-group-label ">overall atmosphere: </label>
                                                                <input name="OverallAtmosphere" value="" type="text" class="form-control" maxlength="50">
                                                            </div>
            <!-- end ATMOSPHERE -->
            
            <!-- begin PERFORMANCE -->
                                                            <div class="form-group">
                                                                <label class="form-group-label ">overall performance: </label>
                                                                <select class="bootstrap-select form-control" id="OverallPerformance" name="OverallPerformance" title="" tabindex="-98">
                                                                    <option value="" label=" "></option>
                                                                    <option value="1">1 - A total rip-off</option>
                                                                    <option value="2">2 - Should have stayed home</option>
                                                                    <option value="3">3 - Barely worth the effort</option>
                                                                    <option value="4">4 - She just laid there</option>
                                                                    <option value="5">5 - Average</option>
                                                                    <option value="6">6 - Nice Time</option>
                                                                    <option value="7">7 - Hot time</option>
                                                                    <option value="8">8 - Went the extra mile</option>
                                                                    <option value="9">9 - Forgot it was a service</option>
                                                                    <option value="10">10 - One in a lifetime</option>
                                                                </select>
                                                            </div>
            <!-- end PERFORMANCE -->
            
            <!-- begin CHEMISTRY -->
                                                            <div class="form-group">
                                                                <label class="form-group-label ">chemistry: </label>
                                                                <div class="form-control-wrapper">
                                                                    <span class="rating-container">
                                                                        <span id="chemistryRatingContainer" data-rating="" class="rating js-submit-rating js-chemistry-rating jq-ry-container" style="width: 80px;">
                                                                            <div class="jq-ry-group-wrapper">
                                                                                <div class="jq-ry-normal-group jq-ry-group"><!--?xml version="1.0" encoding="utf-8"?-->
                                                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#dbe0e4"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#dbe0e4" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#dbe0e4" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#dbe0e4" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#dbe0e4" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg></div><div class="jq-ry-rated-group jq-ry-group" style="width: 0%;"><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#ffce37"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#ffce37" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#ffce37" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#ffce37" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#ffce37" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </span>
                                                                    <input class="form-control" type="hidden" data-focusid="chemistryRatingContainer" name="ChemistryRating" id="ChemistryRating">
                                                                </div>
                                                            </div>
            <!-- end CHEMISTRY -->                                      
            
            <!-- begin COMPANIONS LOCATION -->
                                                            <div class="form-group">
                                                                <label class="form-group-label ">companion's location: </label>
                                                                <span class="rating-container">
                                                                    <span data-rating="" class="rating js-submit-rating js-incalllocation-rating jq-ry-container" style="width: 80px;"><div class="jq-ry-group-wrapper"><div class="jq-ry-normal-group jq-ry-group"><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#dbe0e4"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#dbe0e4" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#dbe0e4" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#dbe0e4" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#dbe0e4" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg></div><div class="jq-ry-rated-group jq-ry-group" style="width: 0%;"><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#ffce37"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#ffce37" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#ffce37" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#ffce37" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="16px" height="16px" fill="#ffce37" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg></div></div></span>
                                                                </span>
                                                                <input class="form-control" type="hidden" name="IncallLocationRating" value="" id="IncallLocationRating">
                                                                <a href="javascript:void(0);" class="rating-reset js-incalllocation-clear">Clear</a>
                                                            </div>
            <!-- end COMPANIONS LOCATION -->
            
            <!-- begin VERIFICATIONS -->
                                                            <div class="form-group-checkbox">
                                                                
                                                                <div class="checkbox">
                                                                    <label for="servicesDelivered" class="">
                                                                    <input type="checkbox" id="servicesDelivered" name="deliveredaspromised">
                                                                    <span class="icon-ok"></span>
                                                                    indulgences were as described</label>
                                                                </div>
                
                                                                <div class="checkbox">
                                                                    <label for="photosAccurate" class="">
                                                                    <input type="checkbox" id="photosAccurate" name="photos_are_accurate">
                                                                    <span class="icon-ok"></span>
                                                                    companion looked like photos</label>
                                                                </div>
                                                                
                                                                <div class="checkbox">
                                                                    <label for="onTime" class="">
                                                                    <input type="checkbox" id="onTime" name="OnTime">
                                                                    <span class="icon-ok"></span>
                                                                    companion was on time</label>
                                                                </div>

                                                            </div>
                                                        </div>
            <!-- end VERIFICATIONS -->
                                                        
                                                        <br>

                                                        
                                                        <div class="form-horizontal">
                                                            
                                                            <h5>description of encounter with your companion</h5>
                                                            
                                                            <div class="form-group">
                                                                <label class="form-group-label ">date of encounter: </label>
                                                                <select class="bootstrap-select form-control" style="max-width:200px;" name="LastVisitMonth" tabindex="-98">
                                                                    <option value="01">January</option><option value="02">February</option><option value="03" selected="">March</option><option value="04">April</option><option value="05">May</option><option value="06">June</option><option value="07">July</option><option value="08">August</option><option value="09">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option>
                                                                </select>
                                                                
                                                                <select class="bootstrap-select form-control" style="max-width:100px;" name="LastVisitYear" tabindex="-98">
                                                                    <option value="2018">2018</option>
                                                                    <option value="2017">2017</option>
                                                                    <option value="2016">2016</option>
                                                                    <option value="2015">2015</option>
                                                                    <option value="2014">2014</option>
                                                                    <option value="2013">2013</option>
                                                                </select>
                                                            </div>
                
                                                        
                                                            <br>
                                                            
                                                            <h5>general summary of your encounter and companion</h5>
                                                            should include general, broad comments about your companion and your initial experience. please do not mention specific acts or donation - just tease your fellow members.
                                                            
                                                            <div class="form-group">
                                                                <label class="form-group-label ">headline for your review:</label>
                                                                <input name="ReviewHeadline" value="" type="text" id="ReviewHeadline" class="form-control" maxlength="60"> 
                                                                
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label class="form-group-label ">general summary:</label>
                                                                <textarea class="bootstrap-select form-control review-form-textarea js-textarea " data-limit="4000" name="GeneralDetails" id="GeneralDetails" style="overflow: hidden; word-wrap: break-word; height: 540px;"></textarea>
                                                                <div class="textarea-feedback" style="margin-left:145px">characters: <span class="textarea-feedback-content js-textarea-feedback" id="len1">4000 left</span>
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                            <br>
                                                            
                                                            <h5>detailed description of encounter</h5>
                                                            describe the provider, the experience, and whether or not you enjoyed it in graphic emotional and sexual terms. Don't make this space a recap of the General section. Instead, go for a blow-by-blow tell-all of your session with the provider from your own unique point of view.
                                                            <div class="form-group">
                                                                <label class="form-group-label ">juicy details: </label>
                                                                <textarea class="bootstrap-select form-control review-form-textarea js-textarea " data-limit="6000" name="JuicyDetails" id="JuicyDetails" style="overflow: hidden; word-wrap: break-word; height: 10px;"></textarea>
                                                                <div class="textarea-feedback" style="margin-left:145px">characters: <span class="textarea-feedback-content js-textarea-feedback" id="len1">6000 left</span></div>
                                                            </div>
                                                            

                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <br>
                                                
                                                <div class="bottom-buttons">
                                                    <button class="btn btn-cli-pro" data-toggle="modal2" data-target="#submitReview">submit</button>
                                                        <button type="button" formnovalidate="formnovalidate" class="btn btn-pro-cli js-save-draft-click">save draft</button>
                                                </div>
                                                
                                                <div class="modal fade" id="submitReview" tabindex="-1" role="dialog" aria-labelledby="submitReviewLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                    <h5 class="modal-title" id="submitReviewLabel">Error</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <span class="icon-error">!</span>
                                                                <p>Your review wasn't added. Please try to do it later.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>                                            
                                            
