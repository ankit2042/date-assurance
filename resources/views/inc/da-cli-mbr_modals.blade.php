<!-- Search Modal -->
<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><img src="images/c-d-logo-small-for-wht-bg-ng-final.png" alt="confidate"> - search</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card border-primary mb-3" style="max-width: 18rem;">
            <div class="card-header">Header</div>
            <div class="card-body text-primary">
                <h5 class="card-title">Primary card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


<!-- Client Login Modal -->
<div class="modal fade" id="cli-loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><img src="images/c-d-logo-small-for-wht-bg-ng-final.png" alt="confidate"> - client login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Provider Login Modal -->
<div class="modal fade" id="pro-loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><img src="images/c-d-logo-small-for-wht-bg-ng-final.png" alt="confidate"> - provider login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- member registration - why questions modal -->
<div class="modal fade" id="cli-reg-why-questionsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLabel"><img src="images/c-d-logo-small-for-wht-bg-ng-final.png" alt="confidate"> - client member registration</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          we ask many questions to provide the best possible date experience for both you and your provider.<br>
          <br>
            none of the information collected is connected to your billing account, as billing is done by a third party processor.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- modal purchase transaction declined -->

        <div class="modal fade" id="transactionDeclined" tabindex="-1" role="dialog" aria-labelledby="transactionDeclinedLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h5 class="modal-title" id="transactionDeclinedLabel">Transaction declined</h5>
                    </div>
                    <div class="modal-body">
                        <p>Unfortunately it looks like your attempt to purchase VIP membership has failed due to your card being declined. Good news is that we have several other payment options that you could try.</p>
                    </div>
                </div>
            </div>
        </div>

<!-- modal for upgraded info -->  
   
    <div class="modal fade big" id="upgradedInfo" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-middle" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="ter-congratulation-block">
                    <div class="ter-vip-badge-wrapper">
                        <div class="ter-vip-badge">
                            <span class="ter-badge-circle"></span>
                            <span class="ter-badge">VIP MEMBER</span>
                            <span class="ter-badge-circle"></span>
                        </div>
                    </div>
                    <div class="ter-congratulation-text">
                        <div class="ter-congratulation-title">doublenice2013, you are now a VIP member!</div>
                        <p>We thank you for your purchase!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- modal for payment -->
    
    <div class="modal fade big" id="paymentDelayedInfo" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-middle" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="ter-congratulation-block">
                    <div class="ter-vip-badge-wrapper">
                        <div class="ter-vip-badge">
                            <span class="ter-badge-circle"></span>
                            <span class="ter-badge">VIP MEMBER</span>
                            <span class="ter-badge-circle"></span>
                        </div>
                    </div>
                    <div class="ter-congratulation-text">
                        <div class="ter-congratulation-title">doublenice2013, Thank you for becoming VIP member!</div>
                        <p>Membership should be activated shortly. You'll receive notification!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- COUNTRY NOT SELECTED modal -->                                            
                                            
        <div class="modal fade" id="countryNotSelected" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h5 class="modal-title">warning</h5>
                    </div>
                    <div class="modal-body">
                        <p>please select a country before choosing a city!</p>
                    </div>
                </div>
            </div>
        </div>
    
<!-- PRIVACY Modal -->
<div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="privacyModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="privacyModalLabel">dateassure&trade; privacy policay</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        privacy stuff here
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
    <!-- end COUNTRY NOT SELECTED modal --> 
    
<!-- delete favorite companion Modal -->
<div class="modal" id="deleteSure" tabindex="-1" role="dialog" aria-labelledby="deleteSureLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteSureLabel">confirm delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        are you sure you want to delete this companion from your favorites?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">no</button>
        <button type="button" class="btn btn-primary" onclick="location.href='<?php //echo $base_url.'mbrs/da-cli-mbr_alerts.php?ty=4&pr='.$rows['UserID'];?>'"/>yes - delete companion</button>
      </div>
    </div>
  </div>
</div>     