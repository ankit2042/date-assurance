
<a name="pricing"></a> 

<!--PRICING SECTION  -->    

<section id="pricing" class="white-wrapper">
    <div class="container">
        <div class="title text-center">
            <h2>pricing plans</h2>
        </div><!-- end title -->
        <div class="row text-center">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" data-effect="helix">
                <div class="pricing-box animated fadeInRight">
                    <span class="hideme"><i class="fas fa-star fa-2x"></i></span>
                    <div class="title"><h3>Basic - Free!</h3></div>
                    <div class="price">
                                <p class="price-value">$0</p>
                                <p class="price-month">per month</p>
                            </div>
                            <div class="text-center">
                                <a href="c-d-cli-signup-1.php" class="btn-lg btn-cli-signup">SIGN UP FOR FREE!</a><br><br>
                            </div>                    
                            <ul class="pricing clearfix">
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Search Provider Profiles</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Provider Ratings</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Browse Provider Gallery</li>
                                <li><i class="fas fa-times" style="color:#fd7bcb;"></i>&nbsp;<span style="text-decoration:line-through;">View Provider Reviews</span></li>
                                <li><i class="fas fa-times" style="color:#fd7bcb;"></i>&nbsp;<span style="text-decoration:line-through;">Write Provider Reviews</span></li>
                                <li><i class="fas fa-times" style="color:#fd7bcb;"></i>&nbsp;<span style="text-decoration:line-through;">Secure Communication with Providers</span></li>
                                <li><i class="fas fa-times" style="color:#fd7bcb;"></i>&nbsp;<span style="text-decoration:line-through;">Secure Communication with Other Members</span></li>
                            </ul>
                            <div class="text-center">
                                <a href="c-d-cli-signup-1.php" class="btn-lg btn-cli-signup">SIGN UP FOR FREE!</a>
                            </div>
                        </div><!-- Pricing Box -->
                    </div><!-- Column 1 -->
            
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-effect="helix">
                        <div class="pricing-box">
                                <span class="hideme"><i class="fas fa-star fa-2x"></i></span>
                            <div class="title"><h3>VIP - Awesome!</h3></div>
                            <div class="price">
                                <p class="price-value">$24</p>
                                <p class="price-month">for 30 days (recurring)</p>
                            </div>
                            <div class="text-center">
                                <a href="c-d-cli-signup-2.php" class="btn-lg btn-cli-signup">SIGN UP NOW!</a><br><br>
                            </div>                            
                            <ul class="pricing clearfix">
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Search Provider Profiles</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Provider Ratings</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Browse Provider Gallery</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Provider Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Write Provider Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Providers</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Other Members</li>
                            </ul>
                            <div class="text-center">
                                <a href="c-d-cli-signup-2.php" class="btn-lg btn-cli-signup">SIGN UP NOW!</a>
                            </div>
                        </div><!-- Pricing Box -->
                    </div><!-- Column 2 -->
            
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-effect="helix">
                        <div class="pricing-box">
                               <span class="hideme"><i class="fas fa-star fa-2x"></i></span>
                                <div class="title"><h3>VIP - Great Deal!</h3></div>
                                <div class="price">
                                    <p class="price-value">$59</p>
                                    <p class="price-month">for 90 days (recurring)</p>
                                </div>
                                <div class="text-center">
                                    <a href="c-d-cli-signup-3.php" class="btn-lg btn-cli-signup">protect me now!</a><br><br>
                                </div>
                            <ul class="pricing clearfix">
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Search Provider Profiles</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Provider Ratings</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Browse Provider Gallery</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Provider Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Write Provider Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Providers</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Other Members</li>
                            </ul>
                            <div class="text-center">
                                <a href="c-d-cli-signup-3.php" class="btn-lg btn-cli-signup">protect me now!</a>
                            </div>
                           
                        </div><!-- Pricing Box -->
                    </div><!-- Column 3 -->
            
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-effect="helix">
                        <div class="pricing-box">
                              <span class="hideme"><i class="fas fa-star fa-2x"></i></span>
                            <div class="title"><h3>VIP - Best Value!</h3></div>
                             <div class="price">
                                <p class="price-value">$169</p>
                                <p class="price-month">Annually</p>
                            </div>
                            <div class="text-center">
                                <a href="c-d-cli-signup-4.php" class="btn-lg btn-cli-signup">get me this deal!</a><br><br>
                            </div>                                                        
                            <ul class="pricing clearfix">
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Search Provider Profiles</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Provider Ratings</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Browse Provider Gallery</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;View Provider Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Write Provider Reviews</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Providers</li>
                                <li><i class="fas fa-check" style="color:#8dc3fe;"></i>&nbsp;Secure Communication with Other Members</li>
                            </ul>
                            <div class="text-center">
                                <a href="c-d-cli-signup-4.php" class="btn-lg btn-cli-signup">get me this deal!</a>
                            </div>                            
                        </div><!-- Pricing Box -->
                    </div><!-- Column 4 -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- End Pricing -->
    <!--/ END PRICING SECTION  --> 