
<style>

    /* Dropdown Button */
.dropdowntitle {
    color: #696969;
    padding: 10px;
    font-size: 14px;
    font-weight: 400;
    border: none;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
    position: relative;
    display: inline-block;
    float: none;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 180px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
    color: #0094ff;
    padding: 6px 10px;
    font-size:12px;
    font-weight:400;
    text-decoration: none;
    display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {
    background-color: #ddd;
    color: #813ee8;
    text-decoration: none;
}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}

.site-sub-nav {
    padding: 10px 0 0 0;
    float: none;
    text-align: center;
}
</style>


<!--- START COMP SUB NAV --->

<div class="row" style="background-color:#f6f6f6;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="site-sub-nav">
            <ul class="list-inline">
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <a class="dropdowntitle" style="font-weight:500;font-size:16px;color:#f300fe;"><i class="fas fa-home"></i>&nbsp;my&nbsp;<span style="font-weight:300;color:#0094ff;font-size:17px;">d</span><span style="font-weight:700;color:#f300fe;">a</span><span style="font-weight:300;color:#f300fe;">&trade;</span>&nbsp;<i class="fas fa-caret-down"></i>&nbsp;</a>
                        <div class="dropdown-content">
                            
                            <a style="color:#f300fe;margin:15px;font-size:15px" href="<?php echo $base_url;?>comps/da-comp-mbr_dash.php">my dashboard</a>
                            
                            <span style="font-size:16px;font-weight:600;margin-left:10px;">membership settings</span>
                                <a style="font-size: 15px;color:#f300fe;margin-left:15px;" href="<?php echo $base_url;?>comps/da-comp-mbr_ms_my_profile.php">my profile</a>
                                <a style="font-size: 15px;color:#f300fe;margin-left:15px;" href="<?php echo $base_url;?>comps/da-comp-mbr_ms_update_my_profile.php">update my profile</a>
                                <a style="font-size: 15px;color:#f300fe;margin-left:15px;" href="<?php echo $base_url;?>comps/da-comp-mbr_ms_profile_settings.php">hide/show/delete my profile</a>
                                <a style="font-size: 15px;color:#f300fe;margin-left:15px;" href="<?php echo $base_url;?>comps/da-comp-mbr_ms_changepass.php">change password</a>
                            <br>
                            <span style="font-size:16px;font-weight:600;margin-left:10px;">&nbsp;photos</span>
                                <a style="font-size: 15px;color:#f300fe;margin-left:15px;" href="<?php echo $base_url;?>comps/da-comp-mbr_ms_upload_photo.php">upload photos</a>
                                <a style="font-size: 15px;color:#f300fe;margin-left:15px;" href="<?php echo $base_url;?>comps/da-comp-mbr_ms_delete_photos.php">delete photos</a>
                                <a style="font-size: 15px;color:#f300fe;margin-left:15px;" href="<?php echo $base_url;?>comps/da-comp-mbr_ms_set_primary_photo.php">set primary photo</a>
                            <br>
                            <span style="font-size:16px;font-weight:600;;margin-left:10px;">&nbsp;member support</span>
                                <a style="font-size: 15px;color:#f300fe;margin-left:15px;" href="<?php echo $base_url;?>comps/da-comp-mbr_ms_cancel_my_membership.php">cancel my membership</a>
                                <a style="font-size: 15px;color:#f300fe;margin-left:15px;" href="<?php echo $base_url;?>comps/da-comp-mbr_ms_support.php">help</a>
                                <a style="font-size: 15px;color:#f300fe;margin-left:15px;" href="<?php echo $base_url;?>comps/da-comp-mbr_ms_contact_support.php">contact</a>
                            <br>    
                                <a style="font-size: 18px;color:#0094ff;font-weight:400;margin-left:15px;margin-bottom:15px;" href="<?php echo $base_url;?>comps/da-comp-mbr_signout.php"><i class="fas fa-sign-out-alt"></i>&nbsp;<?php echo $signout;?></a>
                        </div>
                    </div>
                </li>
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <a class="dropdowntitle" style="font-weight:400;font-size:16px;">reviews &amp; ratings&nbsp;<i class="fas fa-caret-down"></i>&nbsp;</a>
                        <div class="dropdown-content">
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;margin-top:10px;" href="/comps/da-comp-mbr_reviews_of_me.php">reviews of me</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="/comps/da-comp-mbr_submit_mbr_rating.php">submit a member rating</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="/comps/da-comp-mbr_search_reviews_by_mbr.php">search for reviews by a member</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;margin-bottom:10px;" href="/comps/da-comp-mbr_refute_neg_rating.php">refute a negative review/rating of me</a>
                        </div>
                    </div>
                </li>
                
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <a class="dropdowntitle" style="font-weight:400;font-size:16px;">favorites&nbsp;<i class="fas fa-caret-down"></i>&nbsp;</a>
                        <div class="dropdown-content">
                          <a style="font-size:15px;color:#0094ff;margin-left:10px;margin-top:10px;" href="/comps/da-comp-mbr_my_fav_mbrs.php">my favorite members</a>
                          <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="/comps/da-comp-mbr_my_fav_comp_reviews.php">my favorite reviews</a>
                          <a style="font-size:15px;color:#0094ff;margin-left:10px;margin-bottom:10px;" href="/comps/da-comp-mbr_comps_favd_me.php">members who favorited me</a>
                        </div>
                    </div>
                </li>
                
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <?php 
                            if($mninbox>0)
                                echo '<a class="dropdowntitle with-alert" style="font-weight:400;font-size:16px;" title="'.$mninbox.'&nbsp;'.$newemail.'">private messages &amp; requests<span class="badge" style="background-color:#813ee8;">'.$mninbox.'</span><i class="fas fa-caret-down"></i>&nbsp;</a>';
                                    else echo '<a class="dropdowntitle with-alert" style="font-weight:400;font-size:16px;"title="'.$mninbox.'&nbsp;'.$newemail.'">private messages &amp; requests<span class="badge" style="background-color:#0094ff;">0</span><i class="fas fa-caret-down"></i>&nbsp;</a>';                                           
                        ?> 
                        <div class="dropdown-content">
                            <?php 
                            if($mninbox>0)
                                echo '<a style="margin-left:10px;margin-top:10px;font-size:15px" href="/comps/da-comp-mbr_mc_mailbox.php">private messages &amp; requests<span class="badge" style="background-color:#813ee8;">'.$mninbox.'</span>&nbsp;</a>';
                                    else echo '<a style="margin-left:10px;margin-top:10px;font-size:15px" href="/comps/da-comp-mbr_mc_mailbox.php">private messages &amp; requests<span class="badge" style="background-color:#0094ff;">0</span>&nbsp;</a>';                                           
                            ?> 
                            
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="/comps/da-comp-mbr_mc_compose.php">send a private message</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="/comps/da-comp-mbr_mc_view_all_enc_requests.php">view/manage encounter requests</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="/comps/da-comp-mbr_mc_prev_enc_requests.php">previous encounter requests</a>
                            <a style="font-size:15px;color:#0094ff;margin-left:10px;margin-bottom:10px;" href="/comps/da-comp-mbr_mc_view_all_okay_requests.php">view/manage okay requests</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<!--- END SUB NAV --->