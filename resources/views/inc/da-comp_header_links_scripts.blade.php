
    <!-- BOOTSTRAP V4.0 CSS -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <!-- BEGIN GOOGLE FONT CSS -->

        <link href='http://fonts.googleapis.com/css?family=Lato:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:100,200,300,400,500,600,700,800,900' rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900' rel="stylesheet">
    <!-- BEGIN OWL CAROUSEL CSS -->    

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">

    <!-- BEGIN ANIMATION EFFECT CSS -->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    
    <!-- MAIN STYLESHEET CSS -->

    <!--    <link rel="stylesheet" href="da-css/ter-style-from-c-d-style_redo.css"> -->

    <!-- MCUSTOMSCROLLBAR CSS - Highly customizable custom scrollbar jQuery plugin, featuring vertical/horizontal scrollbars, scrolling momentum, mouse-wheel, keyboard and touch support user defined callbacks -->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!--  BGG I THINK I WILL REPLACE WITH NANO -->

        <link rel="stylesheet" href="{{ asset('/assets/css/da-ter-based-files/magnific-popup.min.css') }}">
    
    <!-- LIGHTWEIGHT 5 STAR RATING CSS -->

        <link rel="stylesheet" href="{{ asset('assets/css/da-ter-based-files/jquery.rateyo.min.css') }}">

    <!-- FONT AWESOME ICONS SCRIPT -->

        <script defer src="https://use.fontawesome.com/releases/v5.0.11/js/all.js"></script>
    
    <!-- AJAX JQUERY LINK -->

        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
    
    <!-- POPPER.JS - A KICKASS LIBRARY USED TO MANAGE POPPERS IN WEB APPLICATIONS -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <!-- BOOTSTRAP V4.0 JS -->    

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>     

    <!-- RECAPTCHA JS -->

        <script type="text/javascript" async="" src="<?php //echo $base_url;?>da-ter-based-files/recaptcha__en.js"></script>
    
    <!-- ANALYTICS JS - makes it simple to send your data to any tool without having to learn, test or implement a new API every time. -->    

        <script async="" src="<?php //echo $base_url;?>da-ter-based-files/analytics.js"></script>

    <!-- BOOTBOX JS - create programmatic dialog boxes using Bootstrap modals -->    
    
        <script src="<?php ////echo $base_url;?>da-ter-based-files/bootbox.min.js"></script> 

    <!-- BOOTSTRAP-SELECT - a jQuery plugin that utilizes Bootstrap's dropdown.js to style and bring additional functionality to standard select elements -->    
    
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script> 

    <!-- MCUSTOMSCROLLBAR JS - Highly customizable custom scrollbar jQuery plugin, featuring vertical/horizontal scrollbars, scrolling momentum, mouse-wheel, keyboard and touch support user defined callbacks -->    
    
        <script src="<?php ////echo $base_url;?>da-ter-based-files/jquery.mCustomScrollbar.concat.min.js"></script>
        
    <!-- BLOODHOUND JS - is the typeahead.js suggestion engine -->    

        <script src="<?php ////echo $base_url;?>da-ter-based-files/bloodhound.js"></script>
        
    <!-- BOOTSTRAP3 TYPEAHEAD JS -->
    
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
        
    <!-- MAGNIFIC POPUP JS -->   

        <script src="<?php //echo $base_url;?>da-ter-based-files/jquery.magnific-popup.min.js"></script>

    <!-- BOOTSTRAP HOVER DROPDOWN -->
        
        <script src="<?php //echo $base_url;?>da-ter-based-files/bootstrap-hover-dropdown.js"></script>
        
    <!-- IMAGE SCALE JS - -->    

        <script src="<?php //echo $base_url;?>da-ter-based-files/image-scale.js"></script>
        
    <!-- AUTOSIZE JS - is a small, stand-alone script to automatically adjust textarea height to fit text. -->    

        <script src="<?php //echo $base_url;?>da-ter-based-files/autosize.js"></script>

    <!-- MODERNIZR-CUSTOM JS - is a JavaScript library that detects HTML5 and CSS3 features in the user’s browser -->    
        
        <script src="<?php //echo $base_url;?>da-ter-based-files/modernizr-custom.js"></script>
        
    <!-- RETINA JS - makes it easy to serve high-resolution images to devices with retina displays -->
    
    <!--    <script src="./da-ter-based-files/retina.min.js"></script> -- eliminate for nanogallery -->
    
    

    <script src="<?php //echo $base_url;?>da-ter-based-files/jquery.chromeinsertfix.js"></script>

    <script src="<?php //echo $base_url;?>da-ter-based-files/stickyfill.js"></script>

    <script src="<?php //echo $base_url;?>js/script.js"></script>

    <script src="<?php //echo $base_url;?>da-ter-based-files/jquery.validate.min.js"></script>

    <script src="<?php //echo $base_url;?>da-ter-based-files/jquery.rateyo.js"></script>

    <noscript>
        &lt;style type="text/css"&gt;
            .navbar-collapse {
                display: block;
            }
            .js-news-paragraph {
                display: block !important;
            }
            @media screen and (max-width: 768px) {
                .navbar-item-logout {
                    display: block !important;
                }
            }
        &lt;/style&gt;
    </noscript>

<link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/kbcecmmckgcpknjccmipoakkajjppmif">

<script type="text/javascript" src="<?php //echo $base_url;?>da-ter-based-files/fixpng.js"></script>
<script type="text/javascript" src="<?php //echo $base_url;?>da-ter-based-files/help_popup.js"></script>
<script type="text/javascript" src="<?php //echo $base_url;?>da-ter-based-files/main.js"></script>

<!-- TODO: -->

<script type="text/javascript">
    $.ter.TimezoneUtils.setTzOffsetCookieIfAbsent();

    $(window).on("load", (function () {
        
    }));

    $(function() {
        
    });
    //-->
</script>

<!-- nano-gallery stuff -->

    <!-- nanoGALLERY CSS files -->
        <link href="<?php //echo $base_url;?>nanogallery/nanogallery_clean.css" rel="stylesheet" type="text/css">

    <!-- nanoGALLERY javascript  -->
        <script type="text/javascript" src="<?php //echo $base_url;?>nanogallery/jquery.nanogallery.js"></script>
        
	<script>
		$(document).ready(function () {
      
      // you define the albumID when you make an ID with kind:'album'
                    var contentGalleryMLN=[
			{ ID:1, kind:'album', src: 'pro-amy1.jpg', srct: 'pro-amy1.jpg', title: 'pro-amy', 
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><br>houston, tx', },
			{ ID:2, kind:'album', src: 'pro-milan_millions1.jpg', srct: 'pro-milan_millions1.jpg', title: 'milan_millions', 
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star-half"></i><br>san diego, ca',},
                        { ID:3, kind:'album', src: 'pro-sandy1.jpg', srct: 'pro-sandy1.jpg', title: 'blondie69', 
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star-half"></i><br>dallas, tx', },
			{ ID:4, kind:'album', src: 'pro-amy1.jpg', srct: 'pro-amy1.jpg', title: 'pro-amy', 
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><br>new orleans, la', },
			{ ID:5, kind:'album',src: 'pro-milan_millions1.jpg', srct: 'pro-milan_millions1.jpg', title: 'stormy weather',
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><br>new orleans, la',},
                        { ID:6, kind:'album',src: 'pro-sandy1.jpg', srct: 'pro-sandy1.jpg', title: 'blondie69', 
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><br>new orleans, la',
                        },
                        { ID:7, kind:'album',src: 'pro-sandy1.jpg', srct: 'pro-sandy1.jpg', title: 'blondie69', 
                            description: '<i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><br>new orleans, la',
                        },
        
        {   
                            src: 'image_03.jpg', 
                            srct: 'image_03ts.jpg', 
                            title: 'album 2', 
                            albumID:103, 
                            kind:'album' 
                        },
			{ src: 'pro-amy1.jpg',	// image url
                            srct: 'pro-amy1.jpg',	// thumbnail url
                            title: 'pro-amy', 		// thumbnail title
                            albumID:101,
                            kind:'album',
                        },
			{   
                            src: 'image_03.jpg', srct: 'image_03ts.jpg', title: 'image 1a', ID:23, albumID:3	
                        },
			{   
                            src: 'image_02.jpg', srct: 'image_02ts.jpg', title: 'image 1b', ID:24, albumID:3	
                        },
			{   
                            src: 'image_01.jpg', srct: 'image_01ts.jpg', title: 'image 1c', ID:25, albumID:2	
                        },
			{   
                            src: 'image_03.jpg', srct: 'image_03ts.jpg', title: 'image 1d', ID:26, albumID:1	
                        },
			{   
                            src: 'image_03.jpg', srct: 'image_03ts.jpg', title: 'image 1e', ID:27, albumID:1 
                        },
			{   
                            src: 'image_03.jpg', srct: 'image_03ts.jpg', title: 'image 1f', ID:28, albumID:2	
                        },
			
        ];
			
            jQuery("#nanoGalleryMLN").nanoGallery({thumbnailWidth:120,thumbnailHeight:160,
                    items:contentGalleryMLN,
            // paginationMaxItemsPerPage:3,
                        paginationSwipe:true,
                        thumbnailAlignment: 'center',
                        thumbnailDisplayTransition:'flipUp',
                        thumbnailLabel:     { position: 'overImageOnBottom', hideIcons: true },
                        displayDescription:true,
                        paginationMaxLinesPerPage:1,
                        paginationDots : true,
                        paginationVisiblePages: 10,
			thumbnailHoverEffect:'imageScale150',
                        viewerDisplayLogo:true,
			useTags:false,
                        locationHash:false,
                        breadcrumbAutoHideTopLevel:true,
                        galleryToolbarHideIcons:true,
                        maxItemsPerLine:6,
                        locationHash: false,
                        theme:'clean',
                        itemsBaseURL:'../imgs/pro-profiles/'
			});
			
      jQuery('#btnPaginationCount').on('click', function() {
        alert(jQuery('#nanoGalleryMLN').nanoGallery('paginationCountPages'));
      });
      jQuery('#btnPaginationNext').on('click', function() {
        jQuery('#nanoGalleryMLN').nanoGallery('paginationNextPage');
      });
      jQuery('#btnPaginationPrevious').on('click', function() {
        jQuery('#nanoGalleryMLN').nanoGallery('paginationPreviousPage');
      });
      jQuery('#btnPaginationGoto').on('click', function() {
        jQuery('#nanoGalleryMLN').nanoGallery('paginationGotoPage',2);
      });

	
		});
	</script>
        
        
        
<!-- end nano-gallery stuff -->

<!-- from king  -->    

        <script src="<?php //echo $base_url;?>js/king-tmpl.min.js"></script>
        