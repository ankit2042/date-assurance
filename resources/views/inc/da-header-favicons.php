
<!-- BGG begin favicons for apples 
    <link rel="apple-touch-icon" sizes="57x57" href="https://www.theeroticreview.com/Library/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://www.theeroticreview.com/Library/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://www.theeroticreview.com/Library/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://www.theeroticreview.com/Library/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://www.theeroticreview.com/Library/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://www.theeroticreview.com/Library/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://www.theeroticreview.com/Library/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://www.theeroticreview.com/Library/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://www.theeroticreview.com/Library/images/favicon/apple-icon-180x180.png">
-->
    <link rel="icon" type="image/png" sizes="192x192" href="../imgs/brand/da-favicon8-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../imgs/brand/da-favicon10-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../imgs/brand/da-favicon8-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../imgs/brand/da-favicon10-16x16.png">

<!-- need to look at what this is -->
<!--    <link rel="manifest" href="https://www.theeroticreview.com/Library/images/favicon/manifest.json"> -->
    
    <meta name="msapplication-TileImage" content="/Library/images/favicon/ms-icon-144x144.png">
        
    <!-- FAVICON IMAGE -->
    <link rel="shortcut icon" type="image/ico" href="../imgs/brand/da-favicon10-16x16.ico">
    
   