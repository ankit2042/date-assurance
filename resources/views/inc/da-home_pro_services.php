    <!--/ PROVIDER MEMBERS FEATURES SECTION -->   
    <section id="features" class="gray-wrapper">
        <div class="container">
            <div class="title text-center">
                <h2>Features for Companion Members</h2>
                <p>READ &amp; POST MEMBER REVIEWS, POWERFUL SEARCH TOOLS, RATE YOUR DATE, COMMUNICATE SECURELY WITH MEMBERS</p>
            </div><!-- end title -->
        
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><font color="#fd7bcb"><i class="far fa-comment-alt fa-3x alignleft"></i></font></div>
                        <h3>Secure Communications with Members</h3>
                        <p>Feel confident reaching out privately to verified members. Communicate using secure email, web-to-text, or What's App.</p>
                    </div>
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><font color="#fd7bcb"><i class="far fa-id-card fa-3x alignleft"></i></font></div>
                        <h3>Screen and Verify Your Members</h3>
                        <p>Participate in our network by giving prompt feedback on the member you are in contact with. By working together, we can create a safer community for all. </p>
                    </div>
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><font color="#fd7bcb"><i class="fas fa-laptop fa-3x alignleft"></i></font></div>
                        <h3>Your Own Customized Website</h3>
                        <p>Coming soon, in addition to creating a Companion Profile, you can choose a your own website...completely free. Stunning templates updated regularly. Choose from a gorgeous selection of designs that reflect your style.</p>
                    </div>
                </div>
            </div> <!-- end row 1 -->
        
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><font color="#fd7bcb"><i class="fas fa-camera fa-3x alignleft"></i></font></div>
                        <h3>Unlimited Photos & Videos</h3>
                        <p>Your free Companion Profile comes without any hosting limits.</p>
                    </div>
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><font color="#fd7bcb"><i class="fas fa-mobile-alt fa-3x alignleft"></i></font></div>
                        <h3>GO MOBILE</h3>
                        <p>Manage your profile, post photos and videos, communicate with Members, screen Members, respond to reviews...all from your smart phone.</p>
                    </div>
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="service-box">
                        <div><font color="#fd7bcb"><i class="fas fa-users fa-3x alignleft"></i></font></div>
                        <h3>SOCIAL MEDIA</h3>
                        <p>Tweet or post updates on your travel,specials, or whatever you like.</p>
                    </div>
                </div>
            </div> <!-- end row 2 -->
            
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div style="text-align:center;">
                        It is completely free for Companions to <a href="c-d-pro-signup.php">signup</a>.<br>
                        <br>
                    </div>
                    <div class="text-center">
                        <a href="c-d-pro-signup.php" class="btn-lg btn-pro-signup">COMPANIONS SIGN UP FOR FREE!</a>
                    </div>                    
                    
                </div>

            </div> <!-- end row 3 -->                   
        
        
        </div> <!-- end container -->
    </section><!-- Service and Testimonial End -->  