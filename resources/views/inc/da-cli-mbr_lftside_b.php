    <!-- BEGIN da-cli-mbr_lftside_b -->  
 
    <aside class="left-container">
        
            <!-- My Quick Links -->

        <div class="left-container-block hidden-sm hidden-xs">
            <h4 class="start-title"><i class="fas fa-link"></i>&nbsp;quick links</h4>
            <div class="left-container-link"><a href="listMyReviews.asp"><span>my reviews</span></a></div>
            <div class="left-container-link"><a href="myfavoriteReviews.asp"><span>my favorite reviews</span></a></div>
            <div class="left-container-link"><a href="myfavorites.asp"><span>my favorite providers</span></a></div>
            <div class="left-container-link"><a href="myfavorites.asp"><span>my profile</span></a></div>
            <div class="left-container-link"><a href="account_manager/myPosts.asp"><span>my membership settings</span></a></div>
	    <div class="left-container-link"><a href="account_manager/feedback.asp"><span>my feedback</span></a></div>
	    <div class="left-container-link"><a href="account_manager/problemReports.asp"><span>manage my account</span></a></div>
	    <div class="left-container-link"><a href="account_manager/myWhiteList.asp"><span>my referrals</span></a></div>
	    <div class="left-container-link"><a href="discussion-boards/newbie-33/ters-instruction-manual-75528"><span>member support</span></a></div>
        </div>  

        <div class="left-container-block" style="text-align:center;">
            <div class="block-content">
            
            <!-- begin "write a review" button -->            
            
                    <a href="<?php echo $base_url;?>mbrs/cli-submitReview.asp" class="action-write">
                        <i class="far fa-edit"></i>
                        write a review
                    </a><!-- end "write a review" button -->
            </div>
        </div>            

    <!-- begin PRIVATE MESSAGES section in left block -->

        <div class="left-container-block">
            <h4><i class="far fa-comment"></i>&nbsp;private messages</h4>
            <div class="block-content">
                <div class="block-content-mail">
                    <div class="block-icon-container">
                        <img src="<?php echo $base_url;?>da-ter-based-files/c-d-pro-profile-blondie69-sq-80x80.jpg" alt="" class="img-thumbnail-profile">
                    </div>
                    <div class="block-info">
                        <h5><a href="<?php echo $base_url;?>/mbrs/private-msg.php">come and get me!</a></h5> <!--get email updates-->
                        <span style="font-weight:400;font-size:10px;line-height:8px;">today 12:45pm - <a href="cli-pro-profile.php">blondie69</a><br>
                        Baltimore, MD</span>
                    </div>
                    <hr class="p-msg">
                </div>

                <div class="block-content-mail">
                    <div class="block-icon-container">
                        <img src="<?php echo $base_url;?>da-ter-based-files/c-d-pro-profile-blondie69-sq-80x80.jpg" alt="" class="img-thumbnail-profile">
                    </div>
                    <div class="block-info">
                        <h5><a href="<?php echo $base_url;?>mbrs/private-msg.php">come and get me!</a></h5> <!--get email updates-->
                        <span style="font-weight:400;font-size:10px;line-height:8px;">today 12:45pm - <a href="cli-pro-profile.php">blondie69</a><br>
                        Baltimore, MD</span>
                    </div>
                    <hr class="p-msg">
                </div>

                <div class="block-content-mail">
                    <div class="block-icon-container">
                        <img src="<?php echo $base_url;?>da-ter-based-files/c-d-pro-profile-blondie69-sq-80x80.jpg" alt="" class="img-thumbnail-profile">
                    </div>
                    <div class="block-info">
                        <h5><a href="<?php echo $base_url;?>mbrs/private-msg.php">come and get me!</a></h5> <!--get email updates-->
                        <span style="font-weight:400;font-size:10px;line-height:8px;">today 12:45pm - <a href="cli-pro-profile.php">blondie69</a><br>
                        Baltimore, MD</span>
                    </div>
                    <hr class="p-msg">
                </div>
                <div class="block-content-mail">
                    <span style="float:right"><a href="cli-priv-msg.php">view my private messages</a></span>
                </div>
                <br>
            </div>
        </div>



        <div class="left-container-block">
            <h4><i class="fab fa-hotjar"></i>&nbsp;hot new reviews</h4>
            <div class="block-content">
                <div class="block-content-mail">
                   
                    <a href="private-msg.php">bombshell_99</a> - 
                    <span style="color:#fd7bcb"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></span><br>
                    <span style="font-weight:400;font-size:10px;line-height:8px;">today 12:45pm - Baltimore, MD</span>
                    
                    <hr class="p-msg">
                </div>

                <div class="block-content-mail">
                    <a href="private-msg.php">bombshell_99</a> - 
                    <span style="color:#fd7bcb"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></span><br>
                    <span style="font-weight:400;font-size:10px;line-height:8px;">today 12:45pm - Baltimore, MD</span>
                    
                    <hr class="p-msg">
                </div>

                <div class="block-content-mail">
                    <a href="private-msg.php">bombshell_99</a> - 
                    <span style="color:#fd7bcb"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></span><br>
                    <span style="font-weight:400;font-size:10px;line-height:8px;">today 12:45pm - Baltimore, MD</span>
                    
                    <hr class="p-msg">
                </div>
                <div class="block-content-mail">
                    <span style="float:right"><a href="cli-new-reviews.php">view new reviews</a></span>
                </div>
                <br>
            </div>

        </div>

    <!-- DID YOU KNOW? -->
    
        <div class="left-container-block">
            <h4><i class="far fa-question-circle"></i>&nbsp;things you should know?</h4>
            <ul class="unordered-list-green">
                <li><span>Non-VIP members can read PMs sent by da support</span></li>
            </ul>
        </div>

    </aside>

<!-- END left side-bar -->    
