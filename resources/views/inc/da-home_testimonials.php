    <!--/ TESTIMONIALS SECTION -->
    
    <section id="testimonials" class="gray-wrapper">
        <div class="container">
            <div class="title text-center">
                <h2>testimonials</h2>
                <p>don't just take our word for it, read what our members have to say</p>
            </div><!-- end title -->
            <div class="col-sm-12">
                <div id="testimonial-slider" class="owl-carousel">
                    <div class="testimonial" style="text-align:center;">
                        <div class="pic">
                            <img class="img-circle" src="imgs/cli-profiles/cli_av7.jpg" width="100" height="100">
                        </div>
                        <div class="testimonial-title" style="color:#8dc3fe">
                            <h4>bowman</h4>
                        </div>
                        <div class="testimonial-review" style="color:#999999">
                            <p class="quote" style="padding:30px">
                                <i class="fas fa-quote-left"></i>&nbsp;it only took me 3 minutes to sign up and i received 11 quotes saving me hours and hours of my time, not 
                                to mention the thousands of dollars i saved. bid on my solar has made the whole experience of going solar 
                                so easy. thanks again for this website!&nbsp;<i class="fas fa-quote-right"></i>
                            </p>
                        </div>
                    </div>
 
                    <div class="testimonial" style="text-align:center">
                        <div class="pic">
                                <img class="img-circle" src="imgs/cli-profiles/cli_av8.jpg" width="100" height="100">
                        </div>
                        <div class="testimonial-title" style="color:#8dc3fe">
                                <h4>k-man</h4>
                        </div>
                        <div class="testimonial-review" style="color:#999999">
                            <p class="description" style="padding:30px">
                            <i class="fas fa-quote-left"></i>&nbsp;at first view, the site looked like a nice innovative tool, i liked the idea, i also like how simple it was to 
                            sign up and interact with providers in my area. i got to control the entire process from start
                            to finish. thanks!&nbsp;<i class="fas fa-quote-right"></i>
                            </p>
                        </div>
                    </div>
                    
                    <div class="testimonial" style="text-align:center">
                        <div class="pic">
                            <img class="img-circle" src="imgs/cli-profiles/cli_av9.jpg" width="100" height="100">
                        </div>
                        <div class="testimonial-title" style="color:#8dc3fe">
                            <h4>william k.</h4>
                        </div>
                        <div class="testimonial-review" style="color:#999999">
                            <p class="description" style="padding:30px">
                            <i class="fas fa-quote-left"></i>&nbsp;wow. i just checked my quotes and it was SO SIMPLE. i am blown away. 
                            you guys truly kick ass. thanks for being so awesome. high fives!&nbsp;<i class="fas fa-quote-right"></i>
                            </p>
                        </div>
                    </div>
                    
                    <div class="testimonial" style="text-align:center">
                            <div class="pic">
                                <img class="img-circle" src="imgs/cli-profiles/cli_av10.jpg" width="100" height="100">
                            </div>
                            <div class="testimonial-title" style="color:#8dc3fe">
                                <h4>sam b.</h4>
                            </div>
                            <div class="testimonial-review" style="color:#999999">
                                <p class="quote" style="padding:30px">
                                <i class="fas fa-quote-left"></i>&nbsp;i was having real trouble managing a myriad of solar providers calling 
                                me at all hours AND sorting out the different proposals drove me crazy. 
                                bid on my solar made everything so easy!&nbsp;<i class="fas fa-quote-right"></i>
                                </p>
                            </div>
                    </div>
                </div>
            </div>
        </div> <!-- end container -->
    </section><!-- Service and Testimonial End -->     