
<a name="escort_dir"></a>

<!--/ ESCORT DIRECTORY SECTION -->     
    <section id="directory" class="white-wrapper">
        <div class="container">
            <div class="title text-center">
                <h2>COMPANION DIRECTORY</h2>
                <p>browse Companion profiles in your area</p>
            </div><!-- end title -->
            <div style="text-align:center;font-size: 0.85rem;color:#999999">your current location is: <span style="font-weight:600"><span id="city"></span>, <span id="state"></span></span><br>
                <a href="c-d-esc-search.php">(change location)</a><br>
                <br>
            </div>     
                 
            <div class="norow">
		<div class="masonry_wrapper">
                    <div class="item entry item-h2 photography print">
                        <img src="images/pro-profiles/pro-milan_millions1.jpg" alt="" class="img-responsive">
                            <div class="hovereffect">
                                <a data-gal="prettyPhoto[product-gallery]" rel="bookmark" href="images/pro-profiles/pro-milan_millions1.jpg"><span class="icon"><i class="fa fa-plus"></i></span></a>
                                <div class="buttons">
                                    <h4>Milana Millions</h4>
                                    <h5>Los Angeles, CA</h5>
                                </div><!-- end buttons -->
                            </div><!-- end hovereffect -->
                    </div>
                        
                    <div class="item entry item-h2 webdesign print">
                        <img src="images/pro-profiles/pro-amy1.jpg" alt="" class="img-responsive">
                            <div class="hovereffect">
                                <a data-gal="prettyPhoto[product-gallery]" rel="bookmark" href="images/pro-profiles/pro-amy1.jpg"><span class="icon"><i class="fa fa-plus"></i></span></a>
                                <div class="buttons">
                                    <h4>Amy</h4>
                                    <h5>Los Angeles, CA</h5>
                                </div><!-- end buttons -->
                            </div><!-- end hovereffect -->
                    </div>
                        
                    <div class="item entry item-h2 videos print">
                      	<img src="images/pro-profiles/pro-sandy1.jpg" alt="" class="img-responsive">
                            <div class="hovereffect">
                                <a data-gal="prettyPhoto[product-gallery]" rel="bookmark" href="images/pro-profiles/pro-sandy1.jpg"><span class="icon"><i class="fa fa-plus"></i></span></a>
                                <div class="buttons">
                                    <h4>Sandy</h4>
                                    <h5>Los Angeles, CA</h5>
                                </div><!-- end buttons -->
                            </div><!-- end hovereffect -->
                    </div>
                        
                    <div class="item entry item-h2 photography">
                        <img src="demos/work_04.jpg" alt="" class="img-responsive">
                            <div class="hovereffect">
                                <a data-gal="prettyPhoto[product-gallery]" rel="bookmark" href="demos/work_04.jpg"><span class="icon"><i class="fa fa-plus"></i></span></a>
                                <div class="buttons">
                                    <h4>Portfolio Work</h4>
                                    <h5>WEB DESIGN, LOGO, PRINT, VIDEO</h5>
                                </div><!-- end buttons -->
                            </div><!-- end hovereffect -->
                    </div>
                        
                    <div class="item entry item-h2 videos">
                        <img src="demos/work_05.jpg" alt="" class="img-responsive">
                            <div class="hovereffect">
                                <a data-gal="prettyPhoto[product-gallery]" rel="bookmark" href="demos/work_05.jpg"><span class="icon"><i class="fa fa-plus"></i></span></a>
                                <div class="buttons">
                                    <h4>Portfolio Work</h4>
                                    <h5>WEB DESIGN, LOGO, PRINT, VIDEO</h5>
                                </div><!-- end buttons -->
                            </div><!-- end hovereffect -->
                    </div>

                    <div class="item entry item-h2 webdesign">
                        <img src="demos/work_06.jpg" alt="" class="img-responsive">
                            <div class="hovereffect">
                                <a data-gal="prettyPhoto[product-gallery]" rel="bookmark" href="demos/work_06.jpg"><span class="icon"><i class="fa fa-plus"></i></span></a>
                                <div class="buttons">
                                    <h4>Portfolio Work</h4>
                                    <h5>WEB DESIGN, LOGO, PRINT, VIDEO</h5>
                                </div><!-- end buttons -->
                            </div><!-- end hovereffect -->
                    </div>                                              
		</div><!-- end portfolio-masonry -->
            </div><!-- end row -->      
        </div> <!-- end Container-->      
    </section><!-- end work -->
    
<script>
	$.ajax({
		url: "https://geoip-db.com/jsonp",
		jsonpCallback: "callback",
		dataType: "jsonp",
		success: function( location ) {
			$('#country').html(location.country_name);
			$('#state').html(location.state);
			$('#city').html(location.city);
			$('#latitude').html(location.latitude);
			$('#longitude').html(location.longitude);
			$('#ip').html(location.IPv4);  
		}
	});		
    </script>