<!--- START SUB NAV --->

<style>

    /* Dropdown Button */
.dropdowntitle {
    color: #0094ff;
    padding: 10px;
    font-size: 14px;
    font-weight: 400;
    border: none;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
    position: relative;
    display: inline-block;
    float: none;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 180px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
    color: #0094ff;
    padding: 6px 10px;
    color:#813ee8;
    font-size:12px;
    font-weight:400;
    text-decoration: none;
    display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {
    background-color: #ddd;
    color: #813ee8;
    text-decoration: none;
}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}

.site-sub-nav {
    padding: 10px 0 0 0;
    float: none;
    text-align: center;
}
</style>

   

<div class="row" style="background-color:#f6f6f6;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="site-sub-nav">
            <ul class="list-inline">
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <a class="dropdowntitle" style="color:#0094ff;">companions&nbsp;<i class="fas fa-caret-down"></i>&nbsp;</a>
                        <div class="dropdown-content">
                          <a href="/mbrs/da-cli-mbr_browse_comp_profiles.php" style="color:#813ee8;font-size:12px;font-weight:400;"><?php echo $brws_comp_profiles;?></a>
                          <a href="/mbrs/da-cli-mbr_my_fav_comps.php"><?php echo $myfavoritecomps;?></a>
                          <a href="/mbrs/da-cli-mbr_search_comp.php">search companion profiles</a>
                        </div>
                    </div>
                </li>
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <a class="dropdowntitle" style="color:#0094ff;">reviews &amp; ratings&nbsp;<i class="fas fa-caret-down"></i>&nbsp;</a>
                        <div class="dropdown-content">
                          <a href="/mbrs/da-cli-mbr_browse_comp_reviews.php">browse reviews</a>
                          <a href="/mbrs/da-cli-mbr_submit_comp_review.php">submit a companion review</a>
                          <a href="/mbrs/da-cli-mbr_my_reviews.php">my reviews</a>
                          <a href="/mbrs/da-cli-mbr_fav_comp_reviews.php">my favorite reviews</a>
                          <a href="/mbrs/da-cli-mbr_search_reviews.php">search reviews &amp; ratings</a>
                        </div>
                    </div>
                </li>
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <a class="dropdowntitle" style="color:#0094ff;">favorites&nbsp;<i class="fas fa-caret-down"></i>&nbsp;</a>
                        <div class="dropdown-content">
                          <a href="/mbrs/da-cli-mbr_my_fav_comps.php">my favorite companions</a>
                          <a href="/mbrs/da-cli-mbr_my_fav_reviews.php">my favorite reviews &amp; ratings</a>
                          <a href="/mbrs/da-cli-mbr_comps_favd_me.php">companions who've favorited me</a>
                          <a href="/mbrs/da-cli-mbr_saved_searches.php">my saved searches</a>
                        </div>
                    </div>
                </li>
                <li class="list-inline-item">
                    <div class="dropdown" style="text-align:left;">
                        <?php 
                            if($mninbox>0)
                                echo '<a class="dropdowntitle js-submenu-item with-alert" style="color:#0094ff;" title="'.$mninbox.'&nbsp;'.$newemail.'">private messages &amp; requests<span class="badge" style="background-color:#0094ff;">3</span><i class="fas fa-caret-down"></i>&nbsp;</a>';
                                    else echo '<a class="dropdowntitle js-submenu-item with-alert" style="color:#0094ff;" title="'.$mninbox.'&nbsp;'.$newemail.'">private messages &amp; requests<span class="badge" style="background-color:#0094ff;">0</span><i class="fas fa-caret-down"></i>&nbsp;</a>';                                           
                        ?> 
                        <div class="dropdown-content">
                          <a href="/mbrs/da-cli-mbr_mc_mailbox.php">my private messages</a>
                          <a href="/mbrs/da-cli-mbr_mc_compose.php">send a private message</a>
                          <a href="/mbrs/da-cli-mbr_mc_make_enc_request.php">make an encounter request</a>
                          <a href="/mbrs/da-cli-mbr_mc_prev_enc_requests.php">previous encounter requests</a>
                          <a href="/mbrs/da-cli-mbr_mc_request_comp_ok.php">request an okay from a companion</a>
                        </div>
                    </div>
                </li>
                
            </ul>
        </div>
    </div>
</div>
<!--- END SUB NAV --->