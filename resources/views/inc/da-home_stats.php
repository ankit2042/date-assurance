
    <!--/ STATS SECTION -->       
    <section id="count_parallax" class="parallax" style="background-image: url('images/sr_front1-1600x900.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
        <div class="overlay">
            <div class="container">
                <div class="row" style="text-align:center;line-height:1.5rem;color:#ffffff;">
                    <div class="col-sm-3">
                        <div class="wow fadeIn animated" style="visibility: visible; -webkit-animation: fadeIn 700ms 300ms;">
                            <i class="fas fa-users fa-4x" data-fa-transform="rotate-10"></i>
                            <span class="stat-count highlight">1321</span>
                            <span style="font-size:1.15rem;">members</span>
                        </div>						
                    </div>
                    <div class="col-sm-3">
                        <div class="wow fadeIn animated" style="visibility: visible; -webkit-animation: fadeIn 700ms 400ms;">
                            <i class="far fa-edit fa-4x" data-fa-transform="rotate-10"></i>
                            <span class="stat-count highlight">1457</span>
                            <span style="font-size:1.15rem;">reviews</span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="wow fadeIn animated"  style="visibility: visible; -webkit-animation: fadeIn 700ms 500ms;">
                            <i class="fas fa-venus-double fa-4x" data-fa-transform="rotate-10"></i>
                            <span class="stat-count highlight">1676</span>
                            <span style="font-size:1.15rem;">companion members</span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="wow fadeIn animated"  style="visibility: visible; -webkit-animation: fadeIn 700ms 500ms;">
                            <i class="far fa-money-bill-alt fa-4x" data-fa-transform="rotate-10"></i>
                            <span class="stat-count highlight">16765</span>
                            <span style="font-size:1.15rem;">review credits paid out</span>
                        </div>
                    </div>
                </div>
            </div><!-- end container -->
        </div><!-- end overlay -->
    </section>  