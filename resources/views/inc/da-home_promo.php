

<!--/SLIDER SECTION -->

<style>

.carousel-indicators {
    bottom: 0; /* BOTTOM SLIDE INDICATORS */
    } 

.carousel-control.right,
.carousel-control.left {
    background-image: none;
    }
    
.carousel .item {
    min-height: 350px; 
    height: 100%;
    width:100%; 
    }

.carousel-caption {
    top: 120px;
    text-align: center;
    }
    
.carousel-caption h3 {
    font-family: Montserrat;
    font-size: 2.0rem;
    color: #ffffff;
    font-weight: 200;
    letter-spacing: 1px;
    margin-top: 0px;
    }

.c-d-tagline1 {
    font-size: 0.75rem;
    margin: 2.0rem 0 2.0rem 0;
    line-height: 1.0;
    text-transform: uppercase;
    text-align: center;
    font-weight: 200;
    letter-spacing: 2px;
    }    
    
.carousel .icon-container {
    display: inline-block;
    font-size: 2.5rem;
    line-height: 25px;
    padding: 1em;
    text-align: center;
    border-radius: 50%;
    }
    
.carousel-caption button {
    border-color: #00bfff;
    margin-top: 1em; 
    }

/* Animation delays */
.carousel-caption h3:first-child {
	animation-delay: 1s;
}
.carousel-caption h3:nth-child(2) {
	animation-delay: 2s;
}
.carousel-caption button {
	animation-delay: 3s;
}

</style>

<section id="c-d-promo">	
    <div class="tp-banner-container">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                
<!-- SLIDE 1 -->                
                
                <div class="carousel-item active">
                    <img class="d-block w-100" src="../imgs/home/da_front1-1600x900.jpg"  alt="da_front1-1600x900"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    <div class="tp-dottedoverlay twoxtwo"></div>
                    <div class="carousel-caption d-none d-md-block" style="text-align:center">
                        <h3 class="animated fadeInDownBig">welcome to</h3>
                        <div style="text-align:center;animation-delay: 2s" class="animated zoomIn"><img src="../imgs/brand/dateassurance-logo-for-body.png" alt="dateassurance(tm)">
                        </div>
                        <div style="text-align:center;animation-delay: 2.5s" class="c-d-tagline1 animated zoomIn">the platform for safe, secure &amp; discreet encounters</div>
                        <div style="text-align:center">    
                            <a class="btn btn-lg btn-cli-signup-outline animated zoomInLeft" style="animation-delay: 3s" href="../reg/da-home-cli_sign_up_account5.php">members signup now!</a>&nbsp;
                            <a class="btn btn-lg btn-pro-signup-outline animated zoomInRight" style="animation-delay: 3s" href="../reg/da-home-pro_sign_up_account.php">companions signup for free!</a><br>
                            <br>
                        </div>
                        <div class="brand-promotion">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="card brand-content" style="visibility: visible">
                                            <div class="card-body">
                                                <i class="far fa-thumbs-up fa-4x" data-fa-transform="rotate--15" style="margin:10px;float:left"></i>
                                                <span style="text-transform:lowercase;font-size:28px;font-weight: 200;line-height:28px;padding: 10px 10px;letter-spacing:1px;text-align:left;">read &amp; post reviews &amp; ratings</span>
                                                <br>
                                                <br>
                                                <span style="font-weight:200;">both members and companions can read and post reviews and ratings on each other!</span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="card brand-content" style="visibility: visible">
                                            <div class="card-body">
                                                <i class="fa fa-laptop fa-4x" data-fa-transform="rotate--15" style="margin:10px;float:left"></i>
                                                <span style="text-transform:lowercase;font-size:28px;font-weight: 200;line-height:28px;padding: 10px 10px;letter-spacing:1px;text-align:left;">verify companions &amp; screen members</span>
                                                <br>
                                                <br>
                                                <span style="font-weight:200;">once you and your companion are verified, you can feel confident communicating and meeting up safely</span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

<!-- END SLIDE 1 -->

<!-- SLIDE 2 -->

                <div class="carousel-item">
                    <img class="d-block w-100" src="../imgs/home/da_front2-1600x900.jpg"  alt="da_front1-1600x900"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    <div class="tp-dottedoverlay twoxtwo"></div>
                    <div class="carousel-caption d-none d-md-block" style="text-align:center">
                        <div style="text-align:center;animation-delay: 2s" class="animated zoomIn"><img src="../imgs/home/da-logo-home1.png" alt="dateassurance(tm)"><br><br>
                        </div>
                        <h3 class="animated fadeInDownBig">where members and companions connect safely and securely...<br>
                            enjoying the confidence of verified information</h3>
                        <div style="text-align:center;animation-delay: 3s" class="c-d-tagline1 animated zoomInUp">endless possibilities</div>
                    </div>
                </div>

<!-- END SLIDE 2 -->

<!-- SLIDE 3 -->

                <div class="carousel-item">
                    <img class="d-block w-100" src="../imgs/home/da_front3-1600x900.jpg"  alt="sr_front3-1600x900"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    <div class="tp-dottedoverlay twoxtwo"></div>
                    <div class="carousel-caption d-none d-md-block" style="text-align:center">
                        <h3 class="animated fadeInDownBig">companions get free branding<br>with their own free profile page</h3><br>
                        <div style="text-align:center;animation-delay: 2s" class="animated zoomIn"><img src="../imgs/home/da-logo-home1.png" alt="dateassurance(tm)"><br>
                        </div>
                        <div style="text-align:center;animation-delay: 2.5s" class="c-d-tagline1 animated zoomIn">the social network for safe, secure &amp; discreet encounters</div>
                        <div style="text-align:center">    
                            <a class="btn btn-lg btn-cli-signup-outline animated zoomInLeft" style="animation-delay: 3s" href="../da-home-cli_sign_up_account.php">members signup now!</a>&nbsp;
                            <a class="btn btn-lg btn-pro-signup-outline animated zoomInRight" style="animation-delay: 3s" href="../da-home-pro_sign_up_account.php">companions signup for free!</a><br>
                            <br>
                        </div>
                        <div class="brand-promotion">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="card brand-content" style="visibility: visible">
                                            <div class="card-body">
                                                <i class="fa fa-laptop fa-4x" data-fa-transform="rotate--15" style="margin:10px;float:left"></i>
                                                <span style="text-transform:lowercase;font-size:28px;font-weight: 200;line-height:28px;padding: 10px 10px;letter-spacing:1px;text-align:left;">verify companions &amp; screen members</span>
                                                <br>
                                                <br>
                                                <span style="font-weight:200;">once you and your companion are verified, you can feel confident communicating and meeting up safely</span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="card brand-content" style="visibility: visible">
                                            <div class="card-body">
                                                <i class="fa fa-laptop fa-4x" data-fa-transform="rotate--15" style="margin:10px;float:left"></i>
                                                <span style="text-transform:lowercase;font-size:28px;font-weight: 200;line-height:28px;padding: 10px 10px;letter-spacing:1px;text-align:left;">communicate safely &amp; securely</span>
                                                <br>
                                                <br>
                                                <span style="font-weight:200;">use dateassurance's secure messaging to communicate via email and live chat</span>
                                            </div>
                                        </div>
                                    </div>		
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

<!-- END SLIDE 3 -->                    
                
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section><!-- end slider-wrapper -->
    
        
    


