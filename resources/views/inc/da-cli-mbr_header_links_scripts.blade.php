
    <!-- BOOTSTRAP V4.0 CSS -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <!-- BEGIN GOOGLE FONT CSS -->

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">    
    
    <link href='http://fonts.googleapis.com/css?family=Lato:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:100,200,300,400,500,600,700,800,900' rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900' rel="stylesheet">
    <!-- BEGIN OWL CAROUSEL CSS -->    

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">

    <!-- BEGIN ANIMATION EFFECT CSS -->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    
    <!-- MAIN STYLESHEET CSS -->

    <!--    <link rel="stylesheet" href="da-css/ter-style-from-c-d-style_redo.css"> -->

    <!-- MCUSTOMSCROLLBAR CSS - Highly customizable custom scrollbar jQuery plugin, featuring vertical/horizontal scrollbars, scrolling momentum, mouse-wheel, keyboard and touch support user defined callbacks -->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!--  BGG I THINK I WILL REPLACE WITH NANO -->

        <link rel="stylesheet" href="{{ asset('assets/css/da-ter-based-files/magnific-popup.min.css')}}">
    
    <!-- LIGHTWEIGHT 5 STAR RATING CSS -->

        <link rel="stylesheet" href="{{ asset('assets/css/da-ter-based-files/jquery.rateyo.min.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha18/css/tempusdominus-bootstrap-4.min.css" />
        
    <!-- FONT AWESOME ICONS SCRIPT -->
    
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

        <script defer src="https://use.fontawesome.com/releases/v5.1.1/js/all.js" integrity="sha384-BtvRZcyfv4r0x/phJt9Y9HhnN5ur1Z+kZbKVgzVBAlQZX4jvAuImlIz+bG7TS00a" crossorigin="anonymous"></script>
    
    <!-- AJAX JQUERY LINK -->

        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
    
    <!-- POPPER.JS - A KICKASS LIBRARY USED TO MANAGE POPPERS IN WEB APPLICATIONS -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <!-- BOOTSTRAP V4.0 JS -->    

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>     

<!-- DATEPICKER  --->

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha18/js/tempusdominus-bootstrap-4.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha18/css/tempusdominus-bootstrap-4.min.css" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datepicker.css')}}">


        
    <!-- RECAPTCHA JS -->

        <script type="text/javascript" async="" src="{{ asset('assets/js/da-ter-based-files/recaptcha__en.js')}}"></script>
    
    <!-- ANALYTICS JS - makes it simple to send your data to any tool without having to learn, test or implement a new API every time. -->    

        <script async="" src="{{ asset('assets/js/da-ter-based-files/analytics.js')}}"></script>

    <!-- BOOTBOX JS - create programmatic dialog boxes using Bootstrap modals -->    
    
        <script src="{{ asset('assets/js/da-ter-based-files/bootbox.min.js')}}"></script> 

    <!-- BOOTSTRAP-SELECT - a jQuery plugin that utilizes Bootstrap's dropdown.js to style and bring additional functionality to standard select elements -->    
    
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script> 

    <!-- MCUSTOMSCROLLBAR JS - Highly customizable custom scrollbar jQuery plugin, featuring vertical/horizontal scrollbars, scrolling momentum, mouse-wheel, keyboard and touch support user defined callbacks -->    
    
        <script src="{{ asset('assets/js/da-ter-based-files/jquery.mCustomScrollbar.concat.min.js') }}"></script>
        
    <!-- BLOODHOUND JS - is the typeahead.js suggestion engine -->    

        <script src="{{ asset('assets/js/da-ter-based-files/bloodhound.js')}}"></script>
        
    <!-- BOOTSTRAP3 TYPEAHEAD JS -->
    
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
        
    <!-- MAGNIFIC POPUP JS -->   

        <script src="{{ asset('assets/js/da-ter-based-files/jquery.magnific-popup.min.js')}}"></script>

    <!-- BOOTSTRAP HOVER DROPDOWN -->
        
        <script src="{{ asset('assets/js/da-ter-based-files/bootstrap-hover-dropdown.js')}}"></script>
        
    <!-- IMAGE SCALE JS - -->    

        <script src="{{ asset('assets/js/da-ter-based-files/image-scale.js')}}"></script>
        
    <!-- AUTOSIZE JS - is a small, stand-alone script to automatically adjust textarea height to fit text. -->    

        <script src="{{ asset('assets/js/da-ter-based-files/autosize.js')}}"></script>

    <!-- MODERNIZR-CUSTOM JS - is a JavaScript library that detects HTML5 and CSS3 features in the user’s browser -->    
        
        <script src="{{ asset('assets/js/da-ter-based-files/modernizr-custom.js') }}"></script>
        
    <!-- RETINA JS - makes it easy to serve high-resolution images to devices with retina displays -->
    
       <script src="{{ asset('assets/js/da-ter-based-files/retina.min.js')}}"></script> <!-- eliminate for nanogallery-->
    
    <!-- CHAT SCRIPT -->

    <script src="{{ asset('js/chat.js')}}"></script>
    
    <script language="javascript" type="text/javascript" src="{{ asset('js/styles.js')}}"></script>

    <script src="{{ asset('assets/js/da-ter-based-files/jquery.chromeinsertfix.js')}}"></script>

    <script src="{{ asset('assets/js/da-ter-based-files/stickyfill.js')}}"></script>

    <script src="{{ asset('js/script.js')}}"></script>

    <script src="{{ asset('assets/js/da-ter-based-files/jquery.validate.min.js')}}"></script>

    <script src="{{asset('assets/js/da-ter-based-files/jquery.rateyo.js')}}"></script>
    
    <input type="hidden" id="urlchat" value="chat/chat.php" />

    <noscript>
        &lt;style type="text/css"&gt;
            .navbar-collapse {
                display: block;
            }
            .js-news-paragraph {
                display: block !important;
            }
            @media screen and (max-width: 768px) {
                .navbar-item-logout {
                    display: block !important;
                }
            }
        &lt;/style&gt;
    </noscript>

<link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/kbcecmmckgcpknjccmipoakkajjppmif">

<script type="text/javascript" src="{{ asset('assets/js/da-ter-based-files/fixpng.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/da-ter-based-files/help_popup.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/da-ter-based-files/main.js') }}"></script>

<!-- TODO: -->

<script type="text/javascript">
    $.ter.TimezoneUtils.setTzOffsetCookieIfAbsent();

    $(window).on("load", (function () {
        
    }));

    $(function() {
        
    });
    //-->
</script>

