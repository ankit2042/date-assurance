<!--- START SUB NAV --->

<div class="position-sticky submenu_wrapper">
<!--    <div class="scroll-hint js-scroll-hint-prev prev"></div> -->
    <div class="submenu js-submenu"> 
        <ul class="container">
            <li class="submenu-dropdown-wrapper js-submenu-dropdown-wrapper dropdown-submenu">
                <a class="submenu-item dropdown-toggle js-dropdown-toggle js-submenu-item" href="#" data-toggle="dropdown" data-hover="dropdown">companions</a>
                <ul class="dropdown-menu submenu-dropdown js-dropdown-menu" style="">
       
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_browse_comp_profiles.php"><?php echo $brws_comp_profiles;?></a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_my_fav_comps.php"><?php echo $myfavoritecomps;?></a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_search_comp.php">search companion profiles</a></li>

                </ul>
            </li> 
            <li class="submenu-dropdown-wrapper js-submenu-dropdown-wrapper dropdown-submenu">
                <a class="submenu-item dropdown-toggle js-dropdown-toggle js-submenu-item" href="#" data-toggle="dropdown" data-hover="dropdown">reviews & ratings</a>
                <ul class="dropdown-menu submenu-dropdown js-dropdown-menu" style="">
       
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_browse_comp_reviews.php">browse reviews</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_submit_comp_review.php">submit a companion review</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_my_reviews.php">my reviews</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_fav_comp_reviews.php">my favorite reviews</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_search_reviews.php">search reviews &amp; ratings</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_comp_rating_of_me.php">view companions ratings of me</a></li>

                </ul>
            </li>              
                
            <li class="submenu-dropdown-wrapper js-submenu-dropdown-wrapper dropdown-submenu">
                <a class="submenu-item dropdown-toggle js-dropdown-toggle js-submenu-item" href="#" data-toggle="dropdown" data-hover="dropdown">favorites</a>
                <ul class="dropdown-menu submenu-dropdown js-dropdown-menu" style="">
                    
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_my_fav_comps.php">my favorite companions</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_my_fav_reviews.php">my favorite reviews &amp; ratings</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_comps_favd_me.php">companions who've favorited me</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_saved_searches.php">my saved searches</a></li>
                    
                </ul>
            </li>
                
            <li class="submenu-dropdown-wrapper js-submenu-dropdown-wrapper dropdown-submenu">

                <?php 
                    if($mninbox>0)
                        echo '<a class="submenu-item dropdown-toggle js-dropdown-toggle js-submenu-item with-alert" href="'.$base_url.'msgs/da-cli-mbr_msg_mailbox.php" title="'.$mninbox.'&nbsp;'.$newemail.'" data-toggle="dropdown" data-hover="dropdown">private messages &amp; requests<span class="badge" style="background-color:#0094ff;">3</span></a>';
                            else echo '<a class="submenu-item dropdown-toggle js-dropdown-toggle js-submenu-item with-alert" href="'.$base_url.'msgs/da-cli-mbr_msg_mailbox.php" title="'.$mninbox.'&nbsp;'.$newemail.'" data-toggle="dropdown" data-hover="dropdown">private messages &amp; requests<span class="badge" style="background-color:#0094ff;">0</span></a>';                                           
                ?>                    
                    
                <!--    <a class="submenu-item dropdown-toggle js-dropdown-toggle js-submenu-item with-alert" href="/cli-mbr/cli_mail.php" data-toggle="dropdown" data-hover="dropdown">my private messages</a> -->

                <ul class="dropdown-menu submenu-dropdown js-dropdown-menu" style="">
                        
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_mc_mailbox.php">my private messages</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_mc_compose.php">send a private message</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_mc_make_enc_request.php">make an encounter request</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_mc_prev_enc_requests.php">previous encounter requests</a></li>
                    <li class="" style="text-align:left;"><a href="/mbrs/da-cli-mbr_mc_request_comp_ok.php">request an okay from a companion</a></li>
                    
                </ul>
            </li>     

        </ul>

    <div class="scroll-hint js-scroll-hint-next next"></div>
</div>

<!--- END SUB NAV --->