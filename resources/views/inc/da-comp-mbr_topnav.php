<!--/HEADER SECTION -->

<div class="header"> 
<nav class="navbar navbar-fixed-top navbar-dark bg-dark d-flex align-items-center">
    <div class="navbar-header col-lg-2 col-md-4 col-sm-4 col-xs-5">
        <a class="navbar-brand" href="<?php echo $base_url;?>da-index.php"><img src="<?php echo $base_url;?>imgs/brand/dateassurance-logo-shield-home.png" class="align-top" alt=""></a>  
    </div>
    
        <ul class="navbar-nav mr-auto col-lg-7 col-md-4 col-sm-4 hidden-xs-down">
            <li class="nav-item">
                &nbsp;
            </li>
        </ul>

        <?php
            if(isset($_SESSION['compid']) && isset($_SESSION['compemail'])){
        ?>        
        
        <ul class="navbar-nav mr-auto col-lg-3 col-md-4 col-sm-4 col-xs-7" style="font-weight:500;font-size:15px;text-align:right;float:right;">

            <li class="nav-item" style="float:right">
                <a href="<?php echo $base_url;?>comps/da-comp-mbr_signout.php" id="navbarDropdownMbr"><span style="color:#f300fe;"><?php echo $companionname;?>&nbsp;<i class="fas fa-sign-out-alt"></i>&nbsp;<?php echo $signout;?></span></a>
                <small class="form-text text-muted">please signout<br>for your security</small>
            </li>
            
        </ul>
    

</nav>
</div>
    <?php
        }
        else{
    ?>
                            

    <?php
    }
    ?>


    <!-- end da-cli-mbr_topnav  -->



