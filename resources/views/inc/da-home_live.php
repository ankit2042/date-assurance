
<a name="live"></a> 

     <!--/ LIVE SECTION -->   
    <section id="livefeed" class="gray-wrapper">
        <div class="container">
            <div class="title text-center">
                <h2>live feed</h2>
                <p>LIVE UPDATES POSTED BY COMPANIONS</p>
            </div>
            <div class="row">
                <div class="col-lg-12" style="margin: 0 auto;width:100%;">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="media media-comment">
                                <a class="twitter-timeline" data-width="640" data-height="500" data-theme="light" data-link-color="#981CEB" href="https://twitter.com/info_confidate?ref_src=twsrc%5Etfw">Tweets by info_confidate</a> 
                                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                            </div>
                        </div>
                        <div class="col-lg-6">
                        <div class="media media-comment">
                            <img class="d-flex g-width-150 g-height-100 rounded-circle g-mr-15" src="imgs/pro-profiles/c-d-profile1.jpg" alt="Image Description">
                            <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30" style="font-size:0.9rem;">
                        <div>
                            <a href="#!" style="font-size:1.15rem;color:#fd7bcb;font-weight:600">Jane Jane</a><br>
                            <span style="font-size:0.85rem;color:#777;">Albuquerque, NM</span><br>
                            <span style="font-size:0.65rem;color:#777;">1 day ago</span>
                        </div>
                        <p>hey guys, i'm going to be in phoenix all week and would love to meet up</p>
        
                        <ul class="list-inline d-sm-flex my-0">
                            <li class="list-inline-item g-mr-20">
                                <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">
                                    <i class="fa fa-thumbs-up g-top-1 g-mr-3"></i>
                                    124
                                </a>
                            </li>
                            <li class="list-inline-item g-mr-20">
                                <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">
                                    <i class="fa fa-thumbs-down g-top-1 g-mr-3"></i>
                                    24
                                </a>
                            </li>
                            <li class="list-inline-item ml-auto">
                                <span class="g-font-size-12">
                                    <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">
                                        <i class="fa fa-reply"></i>
                                        reply
                                    </a>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="media media-comment">
                    <img class="d-flex g-width-150 g-height-100 rounded-circle g-mr-15" src="imgs/pro-profiles/c-d-profile2.jpg" alt="Image Description">
                    <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30" style="font-size:0.9rem;">
                        <div>
                            <a href="#!" style="font-size:1.15rem;color:#fd7bcb;font-weight:600">Jane Doe</a><br>
                            <span style="font-size:0.85rem;color:#777;">Albuquerque, NM</span><br>
                            <span style="font-size:0.65rem;color:#777;">1 day ago</span>
                        </div>
                        <p>Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue
                        felis in faucibus ras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
        
                        <ul class="list-inline d-sm-flex my-0">
                            <li class="list-inline-item g-mr-20">
                                <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">
                                    <i class="fa fa-thumbs-up g-top-1 g-mr-3"></i>
                                    124
                                </a>
                            </li>
                            <li class="list-inline-item g-mr-20">
                                <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">
                                    <i class="fa fa-thumbs-down g-top-1 g-mr-3"></i>
                                    24
                                </a>
                            </li>
                            <li class="list-inline-item ml-auto">
                                <span class="g-font-size-12">
                                    <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">
                                        <i class="fa fa-reply"></i>
                                        reply
                                    </a>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="media media-comment">
                    <img class="d-flex g-width-150 g-height-100 rounded-circle g-mr-15" src="imgs/pro-profiles/c-d-profile3.jpg" alt="Image Description">
                    <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30" style="font-size:0.9rem;">
                        <div>
                            <a href="#!" style="font-size:1.15rem;color:#fd7bcb;font-weight:600">Jane Doe</a><br>
                            <span style="font-size:0.85rem;color:#777;">Albuquerque, NM</span><br>
                            <span style="font-size:0.65rem;color:#777;">1 day ago</span>
                        </div>
                        <p>Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue
                        felis in faucibus ras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
        
                        <ul class="list-inline d-sm-flex my-0">
                            <li class="list-inline-item g-mr-20">
                                <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">
                                    <i class="fa fa-thumbs-up g-top-1 g-mr-3"></i>
                                    124
                                </a>
                            </li>
                            <li class="list-inline-item g-mr-20">
                                <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">
                                    <i class="fa fa-thumbs-down g-top-1 g-mr-3"></i>
                                    24
                                </a>
                            </li>
                            <li class="list-inline-item ml-auto">
                                <span class="g-font-size-12">
                                    <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">
                                        <i class="fa fa-reply"></i>
                                        reply
                                    </a>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                                            </div>
                </div>
            </div>
        </div>
    </div>
</section>
