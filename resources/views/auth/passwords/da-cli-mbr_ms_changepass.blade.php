@extends('layouts.app') 
@section('content') 
@include('common/static-header') 
@include('inc/da-header-meta') 
@include("inc/da-cli-mbr_header_links_scripts") 
@include("inc/da-cli-mbr_subnav")
<div class="container">
	<!-- BEGIN TOP HEADER PROFILE -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="reg">
				<div class="card">
					<div class="card-body">
						<div class="section-title-center">change your password</div>
						<div class="row">
							<div class="col-lg-2">&nbsp;</div>
							<div class="col-lg-8">
								<form action="{{ route('update-password') }}" method="post">
									@csrf
									@if(session()->has('success'))
							            <p class="alert alert-success">
							              {{session()->get('success')}}
							            </p>
						          	@endif
						          	<br>
									<div class="form-group row">
										<label for="email" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">email (member id): </label>
										<div class="col-sm-8 col-form-label"><b>{{Auth::user()->email}}</b></div>
									</div>
									<div class="form-group row">
										<label for="email" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">current password:</label>
										<div class="col-sm-8">
											<input type="password" class="form-control @error('curpass') is-invalid @enderror" style="width:265px;" name="curpass" value="{{ old('curpass') }}"/>
											 @error('curpass')
						                      <span class="invalid-feedback" role="alert">
						                        <strong>{{ $message }}</strong>
						                      </span>
						                    @enderror
										</div>
									</div>
									<div class="form-group row">
										<label for="email" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">new password:</label>
										<div class="col-sm-8">
											<input type="password" class="form-control @error('password') is-invalid @enderror" style="width:265px;" name="password" value="{{ old('password') }}" />
											@error('password')
						                      <span class="invalid-feedback" role="alert">
						                        <strong>{{ $message }}</strong>
						                      </span>
						                    @enderror
										</div>
									</div>
									<div class="form-group row">
										<label for="email" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">re-enter new password:</label>
										<div class="col-sm-8">
											<input type="password" style="width:265px;"  class="form-control @error('repassword') is-invalid @enderror" name="repassword" />
											@error('repassword')
						                      <span class="invalid-feedback" role="alert">
						                        <strong>{{ $message }}</strong>
						                      </span>
						                    @enderror
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-4">&nbsp;</div>
										<div class="col-sm-8">
											<input type="submit" value="Change Password" class="btn btn-success btn-sm btn-cli-pro" name="smchangepass"/>&nbsp;
											<input type="reset" value="Cancel" class="btn btn-danger btn-sm btn-pro-cli"/><!-- </p> -->
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@include("inc/da-cli-mbr_footer")
@include("inc/da-cli-mbr_modals")
@endsection	