@extends('layouts.app')
@section('content')
@include('common/static-header')
@include('inc/da-header-meta')
@include("inc/da-cli-mbr_header_links_scripts")
@include('inc.da-cli-mbr_subnav')
<!-- INSERT BOOTSTRAP REDO -->
<div class="container"><!---->
    <!-- BEGIN TOP HEADER PROFILE -->
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="reg">
          <div class="card">
            <div class="card-body">
              <div class="section-title-center">delete profile photos</div>
              <form action="delete-user-photo" method="post" id="myform" name="myform">
                select a photo or photos to delete
                <div class="form-group">
                  <div class="form-check form-check-inline">or click here &nbsp;
                  @csrf 
                    @if(Auth::user()->id > 0)
                    <input type="checkbox" class="form-check-input" id="chkall" onclick="checkall()"/><label class="form-check-label" for="checkall"> to delete all</label>
                    @endif
                    
                    <hr>
                  </div>
                </div>
                @if(session('error'))
                <p style="margin:0px; padding:5px 20px"><font color="#FF0000"><small><i>{{ session('error') }}</i></small></font></p>
                @endif
                @if(session('success'))
                <p style="margin:0px; padding:5px 20px"><font color="#009933"><i>{{ session('success') }}</i></font></p>
                @endif
                @if(Auth::user()->id > 0)
                <div class="row">
                  @foreach($user as $val)
                  @if(file_exists(public_path().'/images/upload-photos'.'/'.Auth::user()->id.'u'.$val->PhotoID.'.'.$val->PhotoExtension))
                  <div class="col-md-4">
                    <p style="padding-bottom:0px;">
                      <div class="card" style="background:#f9f9f9;border:2px"><div class="card-body"><img src="{{asset('images/upload-photos')}}/{{Auth::user()->id}}u{{$val->PhotoID}}.{{$val->PhotoExtension}}" border="0" style="height:200px;" onclick="viewphoto(\'prphoto\',\'{{asset('images/photo-uploads')}}/{{Auth::user()->id}}u{{$val->PhotoID}}.{{$val->PhotoExtension}})"/>
                    </p>
                    @if($val->PrimaryPhoto=='0')
                    <!-- <input type="text" hidden name="photoid[]" value="{{$val->PhotoID}}"> -->
                    <input type="checkbox" value="{{Auth::user()->id}}u{{$val->PhotoID}}.{{$val->PhotoExtension}} " name="imgdel[]" id="hddtotal" /> Select
                    @else
                    <p>primary photo</p>
                    @endif
                  </div>
            </div>
             
             
                  </div>
                  @else
                  <p class="text-center text-warning">image not found</p>
                   <input type="checkbox" value="{{Auth::user()->id}}u{{$val->PhotoID}}.{{$val->PhotoExtension}} " name="imgdel[]" id="hddtotal" /> Select
                  @endif
                  @endforeach
                </div>
                
           </div>
           <p align="center"><input type="submit" value="Delete Selected Photo" class="btn btn-cli-pro" name="smdelphoto" onclick="return confirm('Are you sure want to delete photo?')"/>
            <input type="hidden" value="<?php //echo $i;?>" id="hddtotal" />
          </p>
        </form>
        @endif
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript">
  //  $("#myform").click(function(){
  //       $("input[type=checkbox]").prop('checked', $(this).prop('checked'));

  // });
    $('#chkall').click(function () {    
     $('input:checkbox').prop('checked', this.checked);    
 });
</script>
@endsection

