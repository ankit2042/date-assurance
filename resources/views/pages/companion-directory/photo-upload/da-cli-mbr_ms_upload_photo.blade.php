@section('content')
@extends('layouts.app')
@include('common/static-header')
@include('inc/da-header-meta')
@include("inc/da-cli-mbr_header_links_scripts")
@include('inc.da-cli-mbr_subnav')
<!-- BGG original TER style sheet -->

<!-- INSERT BOOTSTRAP REDO -->
<div class="container"><!---->

 <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <div class="card-middle">
    <div class="card-body">
     <div class="section-title-center">
        upload new photo(s)
     </div>
      <div align="center">
        <!-- begin GENERAL ERROR MESSAGE -->
        <!-- end GENERAL ERROR MESSAGE -->
      </div>
    <div class="section-subtitle-center">rules for uploading profile photos</div>
   
    <span style="font-size:16px;">
        <div class="col-md-12">
            
        <span class="text-success" style="font-weight:400;width: 100%;">please only upload</span> 

        the following photos with file types: <span style="font-weight:400;">GIF, JPG, JPEG, and PNG</span>
        </div>
        <div class="col-md-12">
        <span class="text-success" style="font-weight:400;">please only upload</span> 

        your own photos. photos of models or famous people, or other copyrighted material 

        <span class="text-danger" style="font-weight:400;">are not accepted and will be deleted.</span>
    </div>
       <div class="col-md-12">
        <span class="text-danger" style="font-weight:400;">please do not upload</span> pictures with website logos, business or personal names, AND no nudes!
    </div>
        <div class="col-md-12">
        <span class="text-danger" style="font-weight:400;">please do not upload</span> cartoons, pictures of flowers, animals, children and properties.
    </div>
    </span>
    
    <div class="card col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:#f9f9f9;">
        <div class="card-body">
            <form action="user-photo-upload" method="post" name="myform" enctype="multipart/form-data">
                @if(session('success'))
        <p style="margin:0px;padding:5px 20px"><span style="color:#009933;font-size:18px;font-weight:600;">
            <i>{{ session('success') }}</i>
        </p>
        @endif
                <div class="section-subtitle-left">select a photo file to upload: </div>
                
                @csrf
                <div class="form-group-row">
                    <div class="col-xl-12 col-form-label">
                        <input type="file" class="form-control-file" name="photo" required>
                        @error('photo')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-group-row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 col-form-label" style="font-weight:400;font-size:14px;">photo description:
                    </div>
                    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10">
                        <input type="text" class="form-control col-xl-10" name="description" required>
                        @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" value="upload photos" class="btn btn-cli-pro" name="smuplphoto">
                    </div>
                </div>
                <div class="form-group-row">
                    <div class="col-xl-4 col-form-label" style="font-weight:400;font-size:14px;">&nbsp;
                    </div>
                    <div class="col-xl-12">
                        <div class="text-primary" style="font-size:12px;">
                            <i class="fas fa-exclamation-triangle"></i> notice: uploaded photos will be not be displayed on your profile until they are approved; if a photo violates our photo rules, it will be deleted!
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                    </div>
                </div>
            </form>
        </div>
        <!-- begin SUCCESSFUL UPLOAD MESSAGE -->
        
        <!-- end SUCCESSFUL UPLOAD MESSAGE -->
    </div>
    
    <div class="section-subtitle-left">your current photos</div>
    
    @forelse($user as $val)
    <div class="align-center">
        <div class="card m-3">
            <div class="card-body">
                @if(file_exists(public_path().'/images/upload-photos'.'/'.Auth::user()->id.'u'.$val->PhotoID.'.'.$val->PhotoExtension))

                <p style="padding-bottom:0px;text-align:center;">
                    <img src="{{asset('images/upload-photos')}}/{{Auth::user()->id}}u{{$val->PhotoID}}.{{$val->PhotoExtension}}" border="0" style="height:300px;" />
                </p>
                @if($val->IsApproved=='0')
                <span style="font-size:18px;font-weight:300;color:#0094ff;text-align:center;">&nbsp;status:
                </span>
                <span style="color:#ffba00;font-size:18px;"><b>waiting for approval</b>
                </span>
                @elseif($val->IsApproved=='1')
                <span style="font-size:18px;font-weight:300;color:#0094ff;text-align:center;">
                &nbsp;status: </span>
                <span style="color:#009933;font-size:18px;"><b>Approved</b>
                </span>
                @else
                <span style="font-size:18px;font-weight:300;color:#0094ff;text-align:center;">&nbsp;status:
                </span>
                <span style="color:#ffba00;font-size:18px;"><b>waiting for approval</b>
                </span>
                @endif

                @else
                <p>image not found</p>
                @endif
            </div>
        </div>
    </div>
    @empty
        <p class="text-danger">No image found!</p>
    @endforelse                                    

                       </div>                            
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@include('inc.da-cli-mbr_footer')
@include('inc.da-cli-mbr_modals')
@endsection



