@extends('layouts.app')

@section('content')
    @include('common/static-header')
    @include('inc/da-header-meta')
    @include("inc/da-cli-mbr_header_links_scripts")
    @include("inc/da-cli-mbr_subnav")
        
<script src="{{ asset('assets/js/da-ter-based-files/api.js') }}"></script>
    <?php
        //require_once "../inc/da-header-favicons.php";
    ?>        

<!-- BGG original TER style sheet -->

    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/da-site_style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/da-mbrs-nav_style.css') }}">
     -->
    <?php 
        //include_once "../includes/da-star_class.php";
    ?>

<div class="da-margin-top">    
    
    <?php
    ?>
    
    <!-- INSERT BOOTSTRAP REDO -->

    <div class="container"><!---->
                
    <!-- BEGIN TOP HEADER PROFILE -->            

        <div class="row">
           <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="card-middle">
                   <div class="card-body">
                        <div class="section-title-center"><span style="color:#0094ff">date<span style="color:#f300fe;font-weight:500;">assure</span><span style="color:#f300fe;font-weight:300;">&trade;</span></span> <?php ////echo $compdir;?></div>
                        <?php    
                            /*if(isset($_SESSION['memberid']) && intval($_SESSION['memberid'])>0)
                                $sqlsearch .= 'u.UserID <> '.$_SESSION['memberid'].' and ';
                                    $config['showeachside'] = 4;
                                    $config['per_page'] = 10;
                                    $config['js_numrows_page'] = //FillListMembers($sqlsearch);
                                    $config['curpage'] = empty($_GET['p'])?1:$_GET['p'];
                                    $config['rs_start'] = ($config['curpage']*$config['per_page'])-$config['per_page'];
                                if($config['js_numrows_page'] < $config['per_page'])
                                    $config['per_page'] = $config['js_numrows_page'];
                                       $t = isset($_GET['t'])?intval($_GET['t']):0;
                                            $config['cururl'] =// $base_url.'mbrs/da-cli-mbr_comp_dir.php?t='.intval($_GET['t']);
                                        $paging = Pagination($config);*/
                        ?>
                                    
                        <br>
                                    
        <!-- begin COMP PROFILE SORT MODULE -->         
                        <script language="javascript">
                            function onchangeSearch(){
                                var str = '?g='+document.getElementById('slgender').value+'&a='+document.getElementById('slage').value+'&c='+document.getElementById('slcountry').value;
                                window.location.href='<?php ////echo $base_url;?>da-mbr_dash.php'+str;
                                }
                        </script>
                                    
                        <div id="list-sort">
                            sort by:&nbsp;&nbsp;<a href="<?php ////echo $base_url;?>mbrs/da-cli-mbr_comp_dir.php" style="<?php ////echo (!isset($_GET['t']))?'font-weight:300;text-decoration:underline;color:#813ee8;':'';?>">newest</a><a href="<?php ////echo $base_url;?>mbrs/da-cli-mbr_comp_dir.php?t=1" style="margin-left:15px; <?php ////echo (isset($_GET['t']) && intval($_GET['t'])==1)?'font-weight:300;text-decoration:underline;color:#813ee8;':'';?>">most viewed</a>
                            <a href="<?php ////echo $base_url;?>mbrs/da-cli-mbr_comp_dir.php?t=2" style="margin-left:15px; <?php ////echo (isset($_GET['t']) && intval($_GET['t'])==2)?'font-weight:300;text-decoration:underline;color:#813ee8':'';?>">who's online</a>
                            <a href="<?php// //echo $base_url;?>mbrs/da-cli-mbr_comp_dir.php?t=3" style="margin-left:15px; <?php// //echo (isset($_GET['t']) && intval($_GET['t'])==3)?'font-weight:300;text-decoration:underline;color:#813ee8':'';?>">top rated</a>
                            <a href="<?php ////echo $base_url;?>mbrs/da-cli-mbr_comp_dir.php?t=4" style="margin-left:15px; <?php ////echo (isset($_GET['t']) && intval($_GET['t'])==4)?'font-weight:300;text-decoration:underline;color:#813ee8':'';?>">my preferred location</a>
                        </div>
        <!-- end COMP PROFILE SORT MODULE --> 
        
                        <span style="color:#813ee8;"><i><?php ////echo str_replace('<from>', ($config['curpage']==1)?1:($config['curpage']*$config['per_page'] - ($config['per_page'] - 1)), str_replace('<to>', ($config['curpage']*$config['per_page'] > $config['js_numrows_page'])?$config['js_numrows_page']:$config['curpage']*$config['per_page'], str_replace('<total>', $config['js_numrows_page'], $stringdisplay)));?></i></span>

                        <br>

                        <br>
                            
        <!-- begin MAIN COMP PROFILES LIST -->
                            
                        <?php 
                            /*$sql = "select u.UserID
                                    , LOWER(SUBSTRING_INDEX(ProfileName, ' ', 4)) as ProfileName
                                    , Age
                                    , REPLACE(REPLACE(AboutMe, '.', '. '), ',', ', ') as AboutMe
                                    , SUBSTRING_INDEX(REPLACE(REPLACE(REPLACE(AboutMe, '-', ' - '), '.', '. '), ',', ', '),' ', 4) as subAboutMe
                                    , City
                                    , MatchAgeFrom
                                    , MatchAgeTO
                                    , LastLogon

                                    , PrimaryPhotoID
                                    , State
                                    , Country
                                    , ".$_SESSION['lang']."Gender as LGender
                                    , PhotoExtension
                                    , ".$_SESSION['lang']."Ethnicity as LEthnicity
                                    , IsOnline
                                    , u.TotalRating
                                    , u.NumberOfVote
                                    , u.TotalRating/u.NumberOfVote as toprate FROM 
                                    ".$table_prefix."companions as u LEFT JOIN
                                    ".$table_prefix."states as s on u.StateID = s.StateID LEFT JOIN 
                                    ".$table_prefix."countries as c on u.CountryID = c.CountryID LEFT JOIN
                                    ".$table_prefix."ethnicity as e on u.EthnicityID = e.EthnicityID LEFT JOIN  
                                    ".$table_prefix."gender as g on u.MatchGenderID = g.GenderID LEFT JOIN 
                                    ".$table_prefix."photos as p on u.PrimaryPhotoID = p.PhotoID
                                    WHERE 
                                    ".$sqlsearch." u.ProfileStatusID = 1 and u.GenderID IS NOT NULL and MembershipID <> 3";
                            $sql = $sql." limit ".$config['rs_start'].", ".$config['per_page'];*/
                            /*$qry = mysql_query($sql);
                                if(!$qry)
                                    exit($errordata);
                                elseif(mysql_num_rows($qry)>0){
                                    $i=1;
                                    $j=1;
                                        while($rows = mysql_fetch_array($qry)){

                                            $io = ($rows['IsOnline']==1)?'<span style="color:#5cb85c"><i class="fas fa-user-circle"></i> online</span>':'<span style="color:#d9534f;"><i class="far fa-user-circle"></i> offline</span>';
                                            $ethnicity = $rows['LEthnicity'];
                                            $totalrating = $rows['TotalRating']/$rows['NumberOfVote'];
                                            $lastlogon = date('m/d/Y h:i:s', strtotime($rows['LastLogon']));
                                            $state = $rows['State']; 
                                            $city = '';
                                                if(!empty($rows['State'])) $city .= $rows['City'];
                                                    $aboutme = (strlen($rows['AboutMe'])>22)?str_replace('....', ' ', $rows['subAboutMe']).' ...':$rows['AboutMe'];
                                                if(strpos($rows['ProfileName'], ' ')===false && strlen($rows['ProfileName'])>20){
                                                        $prof = substr($rows['ProfileName'], 0, 16).' ...';
                                                        }
                                                else{
                                                        $prof = (strlen($rows['ProfileName'])>20)?substr(strip_tags($rows['ProfileName']), 0, 25).' ...':strip_tags($rows['ProfileName']);
                                                        }
                                                if($rows['IsOnline']==1 && $_SESSION['memberid'] > 0){
                                                        $_SESSION['chatuser'] = $_SESSION['memberid'];
                                                        $_SESSION['chatuser_name'] = $_SESSION['memberemail'];
                                                        $onclick = 'onclick="javascript:chatWith(\''.$rows['UserID'].'\',\''.$prof.'\')"';
                                                        $label = 'Chat';
                                                        }
                                                else{
                                                        $label = $signal;
                                                        $onclick = 'onclick="redirect(\''.$base_url.'alerts.php?ty=2&pr='.$rows['UserID'].'\', 0)"';
                                                        }
                                                if($rows['PrimaryPhotoID']>0 && file_exists('../fuploads/'.$rows['UserID'].'u'.$rows['PrimaryPhotoID'].'.'.$rows['PhotoExtension']))
                                                        $ispt = '../fuploads/'.$rows['UserID'].'u'.$rows['PrimaryPhotoID'].'.'.$rows['PhotoExtension'];
                                                else $ispt = '../imgs/noimage.jpg';
                                                if(is_int($j/2)){*/
                        ?>                            
                            
            <!-- begin COMP PROFILE ODD --> 
                       
                        <div class="card">
                            <div class="card-body">
                                
                                <?php 
                                   /* $compnumphotos = getCompPhotoCount(intval($rows['UserID']));
                                 
                                    $compnumreviews = getCompReviewCount(intval($rows['UserID']));

                                    $compnumfavs = countFavComps(intval($rows['UserID']));*/
                                ?>
                                
                                <div class="rli_left" style="width:17%; min-height:175px;">
                                    <a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php //echo $rows['UserID'];?>" class="rli_left_thumb" style="background-image: url(<?php //echo $ispt;?>)">
                                        <span class="rli_left_thumb_imgcount rli_left_txt_photos"><?php //echo $compnumphotos;?></span>
                                    </a>
                                </div>

                                <div class="rli_right" style="width:83%">
                                    <div class="rli_header">
                                        <div class="rli_header_left">
                                            <h5><a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php //echo $rows['UserID'];?>" title="<?php //echo $prof;?>"><?php //echo $prof;?></a></h5>
                                            <span style="color:#595959;font-weight:300;">home: <span style="font-weight:400;"><?php //echo $city;?>, <?php //echo $state;?>, <?php //echo $country;?></span>
                                                <br>
                                                
                                                age: <span style="font-weight:400;"><?php //echo $rows['Age'];?></span>&nbsp;&nbsp;ethnicity: <span style="font-weight:400;"><?php //echo $ethnicity;?></span>
                                                
                                                <br>
                                                
                                                da ID: <span style="font-weight:400;"><?php //echo $rows['UserID'];?></span>
                                                
                                                <br>
                                                
                                                last logon: <span style="font-weight:400;"><?php //echo $lastlogon;?></span>&nbsp;&nbsp;currently: <span style="font-weight:400;"><?php //echo $io;?></span>
                                                
                                                <br>
                                                
                                                <br>
                                                
                                                <span style="font-size:18px;font-weight:400;color:#0094ff;">about me: </span><span style="font-size:18px;"><?php //echo $rows['AboutMe'];?></span>
                                                
                                                <br>
                                                
                                                <br>
                                                
                                                <i class="fas fa-pencil-alt text-pink" data-original-title="views"></i>
                                                <a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_profile.php?id=<?php //echo $rows['UserID'];?>"> <?php //echo $compnumreviews;?> <span class="hidden-sm hidden-xs" style="font-weight:300">&nbsp;reviews</span></a>&nbsp;&nbsp;
                                                <i class="far fa-heart text-pink" data-original-title="favorites"></i>
                                                <a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_profile.php?id=<?php //echo $rows['UserID'];?>"> <?php //echo $compnumfavs;?> <span class="hidden-sm hidden-xs" style="font-weight:300">&nbsp;fans</span></a>&nbsp;&nbsp;
                                                
                                                <?php
                                                   /* $totaldarating = number_format(($rows['TotalRating']/$rows['NumberOfVote']), 2);

                                                    $rate = ($totaldarating/2);
                                                    if($rate!="") 
                                                    {
                                                    global $obj_star;
                                                    $obj_star = new star_class;*/
                                                   //echo  $obj_star->show_star($rate);   
                                                    // }
                                                ?>
                                                
                                                <a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php //echo $rows['UserID'];?>"> <?php //echo round($totalrating, 2);?> <span class="hidden-sm hidden-xs" style="font-weight:300">&nbsp;da rating</span></a>
                                            
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div> 
                        </div>
            
                        <br>
            
            <!-- END REVIEW ODD -->
                        <?php
                            /*}
                                    elseif(is_int($j/3)){*/
                        ?>
            <!-- BEGIN REVIEW EVEN -->
                                    
                        <div class="card">
                            <div class="card-body">
                                
                                <?php 
                                   /* $compnumphotos = getCompPhotoCount(intval($rows['UserID']));
                                 
                                    $compnumreviews = getCompReviewCount(intval($rows['UserID']));

                                    $compnumfavs = countFavComps(intval($rows['UserID']));*/
                                ?>
                                
                                <div class="rli_left" style="width:17%;min-height:175px;">
                                    <a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php //echo $rows['UserID'];?>" class="rli_left_thumb" style="background-image: url(<?php //echo $ispt;?>)">
                                        <span class="rli_left_thumb_imgcount rli_left_txt_photos"><?php //echo $compnumphotos;?></span>
                                    </a>
                                </div>

                                <div class="rli_right" style="width:83%">
                                    <div class="rli_header">
                                        <div class="rli_header_left">
                                            <h5><a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php //echo $rows['UserID'];?>" title="<?php //echo $prof;?>"><?php //echo $prof;?></a></h5>
                                            <span style="color:#595959;font-weight:300;">home: <span style="font-weight:400;"><?php //echo $city;?>, <?php //echo $state;?>, <?php //echo $country;?></span>
                                                <br>
                                                
                                                age: <span style="font-weight:400;"><?php //echo $rows['Age'];?></span>&nbsp;&nbsp;ethnicity: <span style="font-weight:400;"><?php //echo $ethnicity;?></span>
                                                
                                                <br>
                                                
                                                da ID: <span style="font-weight:400;"><?php //echo $rows['UserID'];?></span>
                                                
                                                <br>
                                                
                                                last logon: <span style="font-weight:400;"><?php //echo $lastlogon;?></span>&nbsp;&nbsp;currently: <span style="font-weight:400;"><?php //echo $io;?></span>
                                                
                                                <br>
                                                
                                                <br>
                                                
                                                <span style="font-size:18px;font-weight:400;color:#0094ff;">about me: </span><span style="font-size:18px;"><?php //echo $rows['AboutMe'];?></span>
                                                
                                                <br>
                                                
                                                <br>
                                                
                                                <i class="fas fa-pencil-alt text-pink" data-original-title="views"></i>
                                                <a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_profile.php?id=<?php //echo $rows['UserID'];?>"> <?php //echo $compnumreviews;?> <span class="hidden-sm hidden-xs" style="font-weight:300">&nbsp;reviews</span></a>&nbsp;&nbsp;
                                                <i class="far fa-heart text-pink" data-original-title="favorites"></i>
                                                <a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_profile.php?id=<?php //echo $rows['UserID'];?>"> <?php //echo $compnumfavs;?> <span class="hidden-sm hidden-xs" style="font-weight:300">&nbsp;fans</span></a>&nbsp;&nbsp;
                                                
                                                <?php
                                                   /* $totaldarating = number_format(($rows['TotalRating']/$rows['NumberOfVote']), 2);

                                                    $rate = ($totaldarating/2);
                                                    if($rate!="") 
                                                    {
                                                    global $obj_star;
                                                    $obj_star = new star_class;
                                                   //echo  $obj_star->show_star($rate);   
                                                    }*/
                                                ?>
                                                
                                                <a href="<?php ////echo $base_url;?>mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php ////echo $rows['UserID'];?>"> <?php //echo round($totalrating, 2);?> <span class="hidden-sm hidden-xs" style="font-weight:300">&nbsp;da rating</span></a>
                                            
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div> 
                        </div>
            
                        <br>
                                    
            
            <!-- END REVIEW EVEN -->
            
                                    <?php
                                       /* }
                                        else{*/
                                    ?>
            
                                    
                                        <div class="card">
                            <div class="card-body">
                                
                                <?php 
                                    /*$compnumphotos = getCompPhotoCount(intval($rows['UserID']));
                                 
                                    $compnumreviews = getCompReviewCount(intval($rows['UserID']));

                                    $compnumfavs = countFavComps(intval($rows['UserID']));*/
                                ?>
                                
                                <div class="rli_left" style="width:17%;min-height:175px;">
                                    <a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php //echo $rows['UserID'];?>" class="rli_left_thumb" style="background-image: url(<?php //echo $ispt;?>)">
                                        <span class="rli_left_thumb_imgcount rli_left_txt_photos"><?php //echo $compnumphotos;?></span>
                                    </a>
                                </div>

                                <div class="rli_right" style="width:83%">
                                    <div class="rli_header">
                                        <div class="rli_header_left">
                                            <h5><a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php //echo $rows['UserID'];?>" title="<?php //echo $prof;?>"><?php //echo $prof;?></a></h5>
                                            <span style="color:#595959;font-weight:300;">home: <span style="font-weight:400;"><?php //echo $city;?>, <?php //echo $state;?>, <?php //echo $country;?></span>
                                                <br>
                                                
                                                age: <span style="font-weight:400;"><?php //echo $rows['Age'];?></span>&nbsp;&nbsp;ethnicity: <span style="font-weight:400;"><?php //echo $ethnicity;?></span>
                                                
                                                <br>
                                                
                                                da ID: <span style="font-weight:400;"><?php //echo $rows['UserID'];?></span>
                                                
                                                <br>
                                                
                                                last logon: <span style="font-weight:400;"><?php //echo $lastlogon;?></span>&nbsp;&nbsp;currently: <span style="font-weight:400;"><?php //echo $io;?></span>
                                                
                                                <br>
                                                
                                                <br>
                                                
                                                <span style="font-size:18px;font-weight:400;color:#0094ff;">about me: </span><span style="font-size:18px;"><?php //echo $rows['AboutMe'];?></span>
                                                
                                                <br>
                                                
                                                <br>
                                                
                                                <i class="fas fa-pencil-alt text-pink" data-original-title="views"></i>
                                                <a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_profile.php?id=<?php //echo $rows['UserID'];?>"> <?php //echo $compnumreviews;?> <span class="hidden-sm hidden-xs" style="font-weight:300">&nbsp;reviews</span></a>&nbsp;&nbsp;
                                                <i class="far fa-heart text-pink" data-original-title="favorites"></i>
                                                <a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_profile.php?id=<?php //echo $rows['UserID'];?>"> <?php //echo $compnumfavs;?> <span class="hidden-sm hidden-xs" style="font-weight:300">&nbsp;fans</span></a>&nbsp;&nbsp;
                                                
                                                <?php
                                                   /* $totaldarating = number_format(($rows['TotalRating']/$rows['NumberOfVote']), 2);

                                                    $rate = ($totaldarating/2);
                                                    if($rate!="") 
                                                    {
                                                    global $obj_star;
                                                    $obj_star = new star_class;
                                                   //echo  $obj_star->show_star($rate);   
                                                    }*/
                                                ?>
                                                
                                                <a href="<?php //echo $base_url;?>mbrs/da-cli-mbr_view_full_comp_profile.php?id=<?php //echo $rows['UserID'];?>"> <?php //echo round($totalrating, 2);?> <span class="hidden-sm hidden-xs" style="font-weight:300">&nbsp;da rating</span></a>
                                            
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div> 
                        </div>
            
                        <br>
                                    

                                        <?php
                                            /*}
                                            if($j==3)
                                                $j=0;
                                                $j++;
                                                $i++;
                                                }
                                            }
                                            else *///echo '<p style="height:300px">There are no profiles !!</p>';
                                        ?>

                                        </div>
                                        <div class="clear"></div>


                                                    <div class="reviews_pagination">
                                                        <ul class="pagination">
                                                            <?php //echo $paging.'<br>';?>
                                                        </ul>
                                                    </div>                        


                                        <div class="reviewnbr">Results 1 - 10 of 49</div>
                                        <div class="reviews_pagination">
                                            <ul class="pagination">
                                                <li>
                                                    <span><b>1</b></span></li>
                                                <li>
                                                    <a href="https://www.eroticmonkey.com/?pag=2">2</a></li>
                                                                                <li>
                                                    <a href="https://www.eroticmonkey.com/?pag=3">3</a></li>
                                                <li>
                                                    <a href="https://www.eroticmonkey.com/?pag=4">4</a></li>
                                                <li>
                                                    <span>...</span></li>
                                            </ul>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                    </div>
            </div>
        
        
</div> <!-- /three-columns-layout container -->


        
        <footer class="footer ">
            
            <div class="statistics-container-mobile">
                <div class="statistics-container-wrapper">
                    <a href="https://www.theeroticreview.com/reviews/searchReviews.asp" class="statistics-item">
                        <span class="statistics-number">1,553,371</span>
                        <span class="statistics-text">Total Reviews</span>
                    </a>
                    <a href="https://www.theeroticreview.com/reviews/newReviews.asp" class="statistics-item">
                        <span class="statistics-number">6,418</span>
                        <span class="statistics-text">New Reviews</span>
                    </a>
                    <div class="statistics-item">
                        <span class="statistics-number"><?php //echo "".$numProMbrs.""?></span>
                        <span class="statistics-text">provider members</span>
                    </div>
                </div>
            </div>
        </footer>
@include("inc/da-cli-mbr_footer")
@include("inc/da-cli-mbr_modals")

    


<?php/*
mysql_close();
$_SESSION['process'] = true;
    require_once "../inc/da-cli-mbr_footer.php";
    require_once "../inc/da-cli-mbr_modals.php";*/
?> 




</body>
@endsection