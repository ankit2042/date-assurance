@extends('layouts.app')
@section('content')
@include('common/static-header')
@include('inc/da-header-meta')
@include("inc/da-cli-mbr_header_links_scripts")
<div class="da-margin-top">
	@include("inc/da-cli-mbr_subnav")
	<!-- INSERT BOOTSTRAP REDO -->
	<div class="container"><!---->
		<!-- BEGIN TOP HEADER PROFILE -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="reg">
					<div class="card">
						<div class="card-body">
							<div class="section-title-center">show/hide/delete profile<?php //echo $title;?></div>
							<br>
							<div align="center">
								<?php
								/*if(isset($error) && !empty($error))
									echo '<p style="margin:0px; padding:5px 20px"><font color="#FF0000"><small><i>'.$error.'</i></small></font></p>';
								if(isset($succ) && !empty($succ))
									echo '<p style="margin:0px; padding:5px 20px"><span style="font-size:18px;color:#009933;"><b><i>'.$succ.'</i></b></font></p>';*/
								?>
							</div>
							<div class="row">
								<div class="col-lg-1 col-md-1">&nbsp;</div>
								<div class="col-lg-10 col-md-10">
									@if(Session::has('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>
										<strong>{{ Session::get('success') }}</strong>
									</div>
									@endif
									<form action="{{ route('update-profile-setting')}}" method="post">
										@csrf
										manage your profile's visibility to other members and companions. you can change these settings back at any time
										(except delete profile!)<br>
										<br>
										<div class="row">
											<div class="col-lg-4">your current profile status :<?php //echo $curprofile;?></div>
											<div class="col-lg-8">
												@if(Auth::user()->profile_status_id == 1)
													enabled/visible
												@elseif(Auth::user()->profile_status_id == 2)
													disable/hide my profile
												@elseif(Auth::user()->profile_status_id == 3)
													remove my profile
												@endif
												<?php
												/*if($setting['ProfileStatusID']==4){
													echo '<b>'.$waitapprove.'</b>';
													$disable = ' disabled="disabled"';
												}
												else{
													echo '<b>'.$setting['LProfileStatus'].'</b>';
													$disable = '';
												}*/
												?>
												<br>
												<br>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">select your profile visibility options :</div>
											<div class="col-lg-8">
												<input type="radio" style="margin-left:0px; padding-left:0px;" name="setting" {{Auth::user()->profile_status_id == 1 ? "checked":""}} value="1">&nbsp;enabled/visible
												<input type="radio" style="margin-left:40px; padding-left:0px;" name="setting" {{Auth::user()->profile_status_id == 2 ? "checked":""}} value="2">&nbsp;disable/hide my profile
												<input type="radio" style="margin-left:40px; padding-left:0px;" name="setting" {{Auth::user()->profile_status_id == 3 ? "checked":""}} value="3">&nbsp;remove my profile
												<?php
												/*$sql = 'select ProfileStatusID, '.$_SESSION['lang'].'ProfileStatus as LProfileStatus from '.$table_prefix.'profilestatus where ProfileStatusID < 4';
												$qry = mysql_query($sql);
												if(!$qry)
													echo $errordata;
												else{
													$i=1;
													while($rows = mysql_fetch_array($qry)){
														$style = ($i>1)?' style="margin-left:40px; padding-left:0px;"':' style="margin-left:0px; padding-left:0px;"';
														echo '<input type="radio" '.$style.' name="setting" value="'.$rows['ProfileStatusID'].'" '.$disable.'/>&nbsp;'.$rows['LProfileStatus'];
														$i++;
													}
												}*/
												?>
												<br>
												<i><font color="#FF0000">* please note: if you select disable/hide profile, members will not be able to communicate with you.<?php //echo '* '.$noteprofile;?></font></i>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-lg-12">
												get notified when your receive a message or okay/encounter request<?php //echo $notifyemail;?>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-lg-4">
												currently, you will be notified: :<?php //echo $curalert.': ';?>
											</div>
											<div class="col-lg-8">
												<?php //echo '<b>'.$setting['LNOTIFY'].'<b>';?>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-lg-4">
												<b>if you receive new messages, how often do you want to be notified by email?<?php //echo $newalert;?></b>
											</div>
											<div class="col-lg-8">
												<select name="notify">
													<option value="1" {{Auth::user()->NotificationSchedule_id == 1 ? "selected" : ""}} >Daily</option>
													<option value="2" {{Auth::user()->NotificationSchedule_id == 2 ? "selected" : ""}}>Monthly</option>
													<option value="3" {{Auth::user()->NotificationSchedule_id == 3 ? "selected" : ""}}>Instantly</option>
													<option value="4" {{Auth::user()->NotificationSchedule_id == 4 ? "selected" : ""}}>Do not email</option>
													<?php
													/*$sql = 'select Id, '.$_SESSION['lang'].'NOTIFY as LName from '.$table_prefix.'notifyemail';
													dropdownlist($sql, 0);*/
													?>
												</select>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-lg-5">&nbsp;</div>
											<div class="col-lg-7">
												<input type="submit" value="Save Setting" class="btn btn-cli-pro" name="smsettings"/>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include("inc/da-cli-mbr_footer")
@include("inc/da-cli-mbr_modals")
@endsection