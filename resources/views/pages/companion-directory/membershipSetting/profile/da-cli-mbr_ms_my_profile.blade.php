@extends('layouts.app')
@section('content')
@include('common/static-header')
@include('inc/da-header-meta')
@include("inc/da-cli-mbr_header_links_scripts")
{{-- @include("inc/da-cli-mbr_topnav") --}}
<div class="da-margin-top">
   @include("inc/da-cli-mbr_subnav")
   <!-- INSERT BOOTSTRAP REDO -->
   <div class="container">
      <!-- BEGIN TOP HEADER PROFILE -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card-middle">
               <div class="card-body">
                  <div class="section-title-center">{{ Auth::user()->name }}</div>
                  <br>
                  @if(Session::has('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('success') }}</strong>
                    </div>
                    @endif
                  <form action="" method="post">
                     <div class="row">
                        <div class="col-lg-4 col-md-5 col-sm-6">
                           <div class="photos">
                              <div class="card">
                                 <div class="aligncenter1">
                                 	@if(Auth::user()->primary_photo_id == 0)
                                 		<!-- <img src="../imgs/no_cli_prof_img.jpg" border="0" style="max-width:200px;"> -->
                                 		<img src="{{ asset('imgs/no_cli_prof_img.jpg')}}" class="img-thumbnail">
                                 	@else
                                 		<img src="'.$base_url.'fuploads/'.$person['UserID'].'u'.$person['PrimaryPhotoID'].'.'.$person['PhotoExtension'].'" border="0" id="prphoto" style="max-width:250px;">
                                 	@endif
                                    <?php
                                       /*if($person['PrimaryPhotoID']==0 || !file_exists('../fuploads/'.$person['UserID'].'u'.$person['PrimaryPhotoID'].'.'.$person['PhotoExtension']) || empty($person['PhotoExtension']))

                                       echo '<img src="../imgs/no_cli_prof_img.jpg" border="0" style="max-width:200px;">';
                                       //
                                       else echo '<img src="'.$base_url.'fuploads/'.$person['UserID'].'u'.$person['PrimaryPhotoID'].'.'.$person['PhotoExtension'].'" border="0" id="prphoto" style="max-width:250px;">';*/

                                       ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-6">
                           <br>
                           <span style="font-size:20px;">your
                           <span style="color:#0094ff;font-weight:300;">date</span>
                           <span style="color:#f300fe;font-weight:600;">assure&trade; ID</span> (
                           <span style="color:#0094ff;font-weight:300;">d</span>
                           <span style="color:#f300fe;font-weight:600;">a ID&trade;</span>):
                           <span style="color:#0094ff;font-weight:600;">{{ Auth::user()->id }}</span></span>
                           <br>
                           <br>
                           @if(Auth::user()->profile_status_id == 1)
                           <span style="font-size:20px;">profile status:
                           @if(Auth::user()->profile_status_id == 1)
                           	<img src="{{ asset('imgs/symbols/verified-profile.jpeg') }}" height = 20>
                           @else
                           	<h6>Not verified</h6>
                           @endif
                           <!-- <span style="color:#009933;font-weight:600;">
                           </span>
                           </span>:
                           <span style="font-size:18px;">profile status:
                           <span style="color:#f98d00;font-weight:600;">{{ Auth::user()->profile_status_id }}</span>
                           </span> -->
                           @endif<!--
                           <br> -->
                           <br>
                           <span style="font-size:20px;">your <span style="color:#0094ff;font-weight:300;">d</span><span style="color:#f300fe;font-weight:600;">a</span>&trade; profile name: <span style="color:#0094ff;font-weight:600;">'{{ Auth::user()->name }}'</span></span>
                           <br>
                           <br>
                           <span class="align-center"><a href="#../mbrs/da-cli-mbr_view_full_mbr_profile.php?id={{ Auth::user()->id }}" class="btn btn-sm btn-primary">view your public profile</a></span>
                           <!-- end YOUR DETAILS table -->
                        </div>
                     </div>
                     <br>
                     <br>
                     <div class="col-md-12 col-sm-12">
                        <div class="section-title-left" >your profile details</div>
                        <hr>
                        <div class="section-subtitle-left">general information</div>
                        <br>
                        <div class="row">
                           <div class="col-lg-1">&nbsp;</div>
                           <div class="col-lg-10">
                              <div class="form-group row">
                                 <label for="prevenccountryid" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">name: </label>
                                 <div class="col-sm-4 form-control">{{ Auth::user()->name }}
                                 </div>
                                 <label for="prevenccountryid" class="col-sm-2 col-form-label">
                                 <small class="text-primary">
                                 not visible to companions
                                 </small>
                                 </label>
                              </div>
                              <div class="form-group row">
                                 <label for="prevenccity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">profile name: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->profile_name }}
                                 </div>
                                 <label for="prevenccountryid" class="col-sm-2 col-form-label">
                                 <small class="text-primary">
                                 not visible to companions
                                 </small>
                                 </label>
                              </div>
                              <div class="form-group row">
                                 <label for="prevenclocationid" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">email: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->email }}
                                 </div>
                                 <label for="prevenccountryid" class="col-sm-2 col-form-label">
                                 <small class="text-primary">
                                 not visible to companions
                                 </small>
                                 </label>
                              </div>
                              <div class="form-group row">
                                 <label for="prevenclocation" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">age: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->age }}
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevenclocation" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">gender: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->gender_id }}
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">country: </label>
                                 <div class="col-sm-4 form-control">
                                    @foreach($country as $countryKey => $countryValue)
                                       {{ Auth::user()->country_id == $countryValue->CountryID ? $countryValue->Country :""}}
                                    @endforeach
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">state: </label>
                                 <div class="col-sm-4 form-control">
                                    @foreach($state_data as $stateKey => $stateValue)
                                       {{ Auth::user()->state_id == $stateValue->StateID ? $stateValue->State :""}}
                                    @endforeach
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">home area/city: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->city }}<?php ////echo $person['City'];?>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">phone: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->phone }}<?php ////echo $person['Phone'];?>
                                 </div>
                                 <label for="prevenccountryid" class="col-sm-2 col-form-label">
                                 <small class="text-primary">
                                 not visible to companions
                                 </small>
                                 </label>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">occupation: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->occupation_id }}<?php //echo $person['OccupationID'];?>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">sexual orientation: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->sex_or_id}}<?php //echo getSexOr($person['SexOrID']);?>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-1">&nbsp;</div>
                        </div>
                        <div class="section-subtitle-left">physical description</div>
                        <br>
                        <div class="row">
                           <div class="col-lg-1">&nbsp;</div>
                           <div class="col-lg-10">
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">ethnicity: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->ethnicity_id}}<?php //echo getEthnicity($person['EthnicityID']);?>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">body type: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->body_type_id}}<?php //echo getBodyType($person['BodyTypeID']);?>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">hair color: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->hair_color_id }}<?php //echo getHairColor($person['HairColorID']);?>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">height: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->height_id }}<?php //echo getHeight($person['HeightID']);?>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">eye color: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->eye_color_id }}<?php //echo getEyeColor($person['EyeColorID']);?>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-1">&nbsp;</div>
                        </div>
                        <div class="section-subtitle-left">character description</div>
                        <br>
                        <div class="row">
                           <div class="col-lg-1">&nbsp;</div>
                           <div class="col-lg-10">
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">about me: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->about_me }}<?php //echo $person['AboutMe'];?>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">do you smoke?: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->smoking_id == 1 ? "Yes" : "No" }}<?php //echo getSmoking($person['SmokingID']);?>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">do you drink?: </label>
                                 <div class="col-sm-4 form-control">
                                    {{ Auth::user()->drinking_id == 1 ? "Yes" : "No"}}
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-1">&nbsp;</div>
                        </div>
                        <div class="section-title-left">update your personal and profile information</div>
                        <hr>
                        <div class="row">
                           <div class="col-lg-1">&nbsp;</div>
                           <div class="col-lg-10">
                              <div class="form-group row">
                                 <label for="prevencdesc" class="col-sm-4 col-form-label">&nbsp;</label>
                                 <div class="col-sm-5">
                                    <a class="btn btn-success btn-cli-pro" href="{{ route('membership-setting.edit-profile',['id' => Auth::user()->id])}}">update your information</a>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-1">&nbsp;</div>
                        </div>
                     </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
@include("inc/da-cli-mbr_footer")
@include("inc/da-cli-mbr_modals")
@endsection