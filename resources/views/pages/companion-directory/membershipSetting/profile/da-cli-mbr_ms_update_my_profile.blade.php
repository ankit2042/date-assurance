@extends('layouts.app')
@section('content')
@include('common/static-header')
@include('inc/da-header-meta')
<div>
@include("inc/da-cli-mbr_header_links_scripts")
<div class="container">
@include("inc/da-cli-mbr_subnav")

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card-middle">
      <div class="card-body">
        <div class="section-title-center">update your profile</div>
        <br>
        <form action="{{route('membership-setting.update-profile')}}" method="post">
          @csrf
          <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-6">
              <div class="photos">
                <div class="card">
                  <div class="aligncenter">

                    @forelse($getPhotos as $getPhotosKey => $getPhotosValue)
                      @if($profile_data->primary_photo_id == 0 && file_exists(public_path().'/assets/img/fuploads/'.$profile_data->id.'u'.$getPhotosValue->PhotoID.'.'.$getPhotosValue->PhotoExtension))
                        <img src="{{ asset('/assets/img/fuploads/'.$profile_data->id.'u'.$getPhotosValue->PhotoID.'.'.$getPhotosValue->PhotoExtension) }}" height="50" style="max-width:200px;">
                      @else
                        <img src="{{ asset('imgs/no_cli_prof_img.jpg')}}" class="" style="max-width:200px;">
                        @php break; @endphp
                      @endif
                    @empty
                      <img src="{{ asset('assets/img/fuploads/').'/'.$profile_data->id.'u'.$getPhotos[0]->PhotoID.'.'.$getPhotos[0]->PhotoExtension }}" height="50" style="max-width:200px;">
                    @endforelse
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-8 col-md-7 col-sm-6">
              <br>
              <span style="font-size:20px;">your <span style="color:#0094ff;font-weight:300;">date</span><span style="color:#f300fe;font-weight:600;">assure&trade; ID</span> (<span style="color:#0094ff;font-weight:300;">d</span><span style="color:#f300fe;font-weight:600;">a ID&trade;</span>): <span style="color:#0094ff;font-weight:600;">{{Auth::user()->membership_id}}</span></span>
              <br>
              <br>
              @if(Auth::user()->profile_status_id ==1)
              <span style="font-size:20px;">profile status: <span style="color:#009933;font-weight:600;">Approved</span></span>:
              @else
              <span style="font-size:18px;">profile status: <span style="color:#f98d00;font-weight:600;">{{ Auth::user()->profile_status }} <!-- echo $person['LProfileStatus'].' --></span></span>';?>
              @endif
              <br>
              <br>
              <span class="align-center"><a href="#../mbrs/da-cli-mbr_view_full_mbr_profile.php?id=<?php //echo $memberid;?>" class="btn btn-sm btn-pro-cli btn-primary">view your public profile</a></span>
              <!-- end YOUR DETAILS table -->
            </div>
          </div>
          <br>
          <br>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="section-title-left" >your profile details</div>
            <hr>
            <div class="section-subtitle-left">general information</div>
            <br>
            <script language="javascript">
              $(document).ready(function(){
                $('#slcountry').change(function() {
                  var valchange = this.value;
                  $('#slstate').load('../includes/loadstates.php?countryId=' + valchange);
                });
              });
            </script>
            <div class="row">
              <div class="col-lg-1">&nbsp;</div>
              <div class="col-lg-10">
                <div class="form-group row">
                  <label for="prevenccountryid" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">name: </label>
                  <label class="col-sm-4 col-form-label" style="font-weight:300;font-size:14px;color:#0094ff">
                    {{Auth::user()->name}}
                  </label>
                  <label for="prevenccountryid" class="col-sm-2 col-form-label">
                    <small class="text-primary">
                      not visible to companions
                    </small>
                  </label>
                </div>
                <div class="form-group row">
                  <label for="prevenccity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">profile name: </label>
                  <label class="col-sm-4 col-form-label" style="font-weight:300;font-size:14px;color:#0094ff">
                    {{Auth::user()->profile_name}}
                  </label>
                  <label for="prevenccountryid" class="col-sm-2 col-form-label">
                    <small class="text-primary">
                      not visible to companions
                    </small>
                  </label>
                </div>
                <div class="form-group row">
                  <label for="prevenclocationid" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">email: </label>
                  <input type="text" class="col-sm-4 form-control" readonly="" style="font-weight:400;font-size:14px;" name="Email" value="{{Auth::user()->email}}">
                  <label for="prevenccountryid" class="col-sm-2 col-form-label">
                    <small class="text-primary">
                      not visible to companions
                    </small>
                  </label>
                </div>
                <div class="form-group row">
                  <label for="prevenclocation" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">age: </label>
                  <input type="text" class="col-sm-4 form-control" style="font-weight:400;font-size:14px;" name="Age" value="{{Auth::user()->age}}">
                </div>
                <div class="form-group row">
                  <label for="prevenclocation" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">gender: </label>
                  <label class="col-sm-4 col-form-label" style="font-weight:300;font-size:14px;color:#0094ff">
                    {{ Auth::user()->gender_id }}
                  </label>
                </div>
                <div class="form-group row">
                  <label for="slcountry" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">Country:</label>
                  <select class="col-sm-4 form-control" name="slcountry" id="country" style="font-weight:400;font-size:14px;">
                    <option value="0">Select Country</option>
                    @forelse($countries as $countryKey => $countryValue)
                      <option value="{{ $countryValue->CountryID }}" {{$countryValue->CountryID == $profile_data->country_id  ? 'selected' : ''}} >{{ $countryValue->Country }}</option>
                    @empty
                      <option>No Country available</option>
                    @endforelse
                  </select>
                  <!-- begin COUNTRY ERROR MESSAGE -->
                </div>
                <div class="form-group row">
                  <label for="slstate" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">State: </label>
                  <select class="col-sm-4 form-control" name="slstate" id="slstate" style="font-weight:400;font-size:14px;">
                    <option value="0">Select State</option>
                    <?php
                    /*if(isset($_POST['slcountry']) && $_POST['slcountry']>0){
                      $sql = 'select StateID as Id, State as LName from '.$table_prefix.'states where CountryID = '.intval($_POST['slcountry']).' order by TopSorted desc, LName asc';
                      dropdownlist($sql, $_POST['slstate']);
                    }
                    else{
                      //echo '<option value="0">'.$select.'</option>';
                    }*/
                    ?>
                  </select>
                </div>
                <div class="form-group row">
                  <label for="txtcity" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">City :</label>
                  <input type="text" class="col-sm-4 form-control" style="font-weight:400;font-size:14px;" id="txtcity" name="txtcity" value="{{ Auth::user()->city}}" maxlength="250">
                </div>
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">phone: </label>
                  <input type="text" class="col-sm-4 form-control" style="font-weight:400;font-size:14px;" id="phone" name="phone" value="{{Auth::user()->phone}}" maxlength="250" required>
                  <label for="prevenccountryid" class="col-sm-2 col-form-label">
                    <small class="text-primary">
                      not visible to companions
                    </small>
                  </label>
                </div>
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">occupation: </label>
                  <input type="text" class="col-sm-4 form-control" style="font-weight:400;font-size:14px;" id="phone" name="occupation" value="{{Auth::user()->occupation_id}}" maxlength="250" >
                </div>
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">sexual orientation: </label>
                  <input type="text" class="col-sm-4 form-control" style="font-weight:400;font-size:14px;" id="phone" name="sex_or_id" value="{{ Auth::user()->sex_or_id}}" maxlength="250" required>
                </div>
              </div>
              <div class="col-lg-1">&nbsp;</div>
            </div>
            <div class="section-subtitle-left">physical description</div>
            <br>
            <div class="row">
              <div class="col-lg-1">&nbsp;</div>
              <div class="col-lg-10">
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">ethnicity: </label>
                  <label class="col-sm-4 col-form-label" style="font-weight:300;font-size:14px;color:#0094ff">
                    {{Auth::user()->ethnicity_id}}
                  </label>
                </div>
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">body type: </label>
                  <input type="text" class="col-sm-4 form-control" style="font-weight:400;font-size:14px;" id="phone" name="body_type_id" value=" {{Auth::user()->body_type_id}}" maxlength="250" required>
                </div>
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">hair color: </label>
                  <input type="text" class="col-sm-4 form-control" style="font-weight:400;font-size:14px;" id="phone" name="hair_color_id" value="{{Auth::user()->hair_color_id}}" maxlength="250">
                </div>
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">height: </label>
                  <input type="text" class="col-sm-4 form-control" style="font-weight:400;font-size:14px;" id="phone" name="height_id" value="{{Auth::user()->height_id}}" maxlength="250">
                </div>
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">eye color: </label>
                  <div class="col-sm-4 form-control">
                    {{Auth::user()->eye_color_id}}
                  </div>
                </div>
              </div>
              <div class="col-lg-1">&nbsp;</div>
            </div>
            <div class="section-subtitle-left">character description</div>
            <br>
            <div class="row">
              <div class="col-lg-1">&nbsp;</div>
              <div class="col-lg-10">
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">about me: </label>
                  <div class="col-sm-4 form-control">
                    {{ Auth::user()->about_me }}
                  </div>
                </div>
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">do you smoke?: </label>
                  <select class="col-md-4 form-control" name="smoking_id">
                    <option value="" >Select Option</option>
                    <option value="1" {{Auth::user()->smoking_id == 1  ? 'selected' : ''}}>Yes</option>
                    <option value="2" {{Auth::user()->smoking_id == 2 ? 'selected' : ''}}>No</option>
                  </select>
                  <!-- <div class="col-sm-4 form-control">
                    {{--Auth::user()->smoking_id --}}
                  </div> -->
                </div>
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label" style="font-weight:400;font-size:14px;">do you drink?: </label>
                  <select class="col-md-4 form-control" name="drinking_id">
                    <option value="" >Select Option</option>
                    <option value="1" {{Auth::user()->smoking_id == 1  ? 'selected' : ''}}>Yes</option>
                    <option value="2" {{Auth::user()->smoking_id == 2  ? 'selected' : ''}}>No</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-1">&nbsp;</div>
            </div>
            <div class="section-title-left">update your personal and profile information</div>
            <hr>
            <div class="row">
              <div class="col-lg-1">&nbsp;</div>
              <div class="col-lg-10">
                <div class="form-group row">
                  <label for="prevencdesc" class="col-sm-4 col-form-label">&nbsp;</label>
                  <div class="col-sm-5">
                    <!-- <a class="btn btn-cli-pro" href="../mbrs/da-cli-mbr_ms_update_my_profile.php">update your information</a> -->
                    <input type="submit" name="update_profile" class="btn btn-primary" value="update your information">
                  </div>
                </div>
              </div>
              <div class="col-lg-1">&nbsp;</div>
            </div>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
@include("inc/da-cli-mbr_footer")
@include("inc/da-cli-mbr_modals")
@endsection
