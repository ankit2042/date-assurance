@extends('layouts.app')

@section('content')
    @if (Auth::check())
        @include('common/static-header')
    @else
        @include('common/header')
    @endif
    <section id="intro">
        <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

            <ol class="carousel-indicators"></ol>

            <div class="carousel-inner" role="listbox">

            <div class="carousel-item active" style="background-image: url({{ asset('assets/img/intro-carousel/1.jpg') }})">
                <div class="carousel-container">
                <div class="container">
                    <br>
                    <h2 class="animate__animated animate__fadeInDown">welcome to</h2>
                    <p class="animate__animated animate__fadeInUp"><img src="{{ asset('assets/img/brand/dateassurance-logo-for-body2.png') }}" alt="" class="img-fluid"></p>
                    <p class="animate__animated animate__fadeInUp">THE SOCIAL PLATFORM FOR SAFE, SECURE & DISCREET ENCOUNTERS&trade;</p>
                    <p class="animate__animated animate__fadeInUp">where Members and Companions connect safely and securely...<br>
                        enjoying the confidence of verified information.</p>
                    <a href="#pricing" class="btn-mbrs-get-started scrollto animate__animated animate__fadeInUp">Get Started Now!</a>&nbsp;<a href="#featured-services" class="btn-comps-get-started scrollto animate__animated animate__fadeInUp">Companions sign up free!</a>
                </div>
                </div>
            </div>

            <div class="carousel-item" style="background-image: url({{ asset('assets/img/intro-carousel/2.jpg') }})">
                <div class="carousel-container">
                <div class="container">
                    <h2 class="animate__animated animate__fadeInDown">Know Your Date</h2>
                    <p class="animate__animated animate__fadeInUp">read and post reviews, view verified Companions profiles including their favorite indulgences. </p>
                    <a href="#pricing" class="btn-mbrs-get-started scrollto animate__animated animate__fadeInUp">Get Started</a>
                </div>
                </div>
            </div>

            <div class="carousel-item" style="background-image: url({{ asset('assets/img/intro-carousel/3.jpg') }})">
                <div class="carousel-container">
                <div class="container">
                    <h2 class="animate__animated animate__fadeInDown">Always free for Companions</h2>
                    <p class="animate__animated animate__fadeInUp">Companions also get free branding with their own free profile page</p>
                    <a href="#pricing" class="btn-get-started scrollto animate__animated animate__fadeInUp">Sign up NOW!</a>
                </div>
                </div>
            </div>

            </div>

            <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>

        </div>
        </div>
    </section><!-- End Intro Section -->

    
    <main id="main">
        <!-- ======= Featured Services Section Section ======= -->
        <section id="featured-services">
        <div class="container">
            <div class="row">
            <div class="col-lg-4 box">
                <i class="far fa-bookmark"></i>
                <h4 class="title"><a href="">read &amp; post reviews &amp; ratings</a></h4>
                <p class="description">both members and companions can read and post reviews and ratings on each other!</p>
            </div>

            <div class="col-lg-4 box box-bg">
                <i class="far fa-calendar-alt"></i>
                <h4 class="title"><a href="">free companion profile page</a></h4>
                <p class="description">companions get free branding with their own free profile page</p>
            </div>

            <div class="col-lg-4 box">
                <i class="far fa-heart"></i>
                <h4 class="title"><a href="">members are screened &amp; companions verified</a></h4>
                <p class="description">once you and your companion are verified, you can feel confident communicating and meeting up safely!</p>
            </div>

            </div>
        </div>
        </section><!-- End Featured Services Section -->

            
            <!-- ======= Live Feed Section ======= -->
        <section id="live">
        <div class="container" data-aos="fade-up">

            <header class="section-header">
            <h3>live feeds</h3>
            <p>Live updates posted by Companions.</p>
            </header>

            <div class="row about-cols">

            <div class="col-md-6" data-aos="fade-up" data-aos-delay="100">
                <div class="about-col">
                <div class="img">
                    <img src="{{ asset('assets/img/da-sexy-woman-lying-on-back3.jpg') }}" alt="" class="img-fluid">
                    <div class="icon"><i class="fab fa-twitter"></i></div>
                </div>
                <h2 class="title"><a href="#">Twitter Feed</a></h2>
                <hr>
                <a class="twitter-timeline" data-width="640" data-height="500" data-theme="light" data-link-color="#981CEB" href="https://twitter.com/info_confidate?ref_src=twsrc%5Etfw">Tweets by info_confidate</a> 
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>

            <div class="col-md-6" data-aos="fade-up" data-aos-delay="200">
                <div class="about-col">
                <div class="img">
                    <img src="{{ asset('assets/img/da-sexy-woman-black-lingerie-lying-bed-using-laptop_144962-9158.jpg') }}" alt="" class="img-fluid">
                    <div class="icon"><i class="fas fa-bolt"></i></div>
                </div>
                <h2 class="title"><a href="#">Live Companion Feed</a></h2>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{ asset('assets/img/portfolio/da-comp-profile-sq1.jpg') }}" style="border-radius:50%;" width="80" alt="Image Description">
                            </div>
                            <div class="col-md-9">
                    
                                <a href="#!" style="font-size:15px;color:#fd7bcb;font-weight:600">Sarah Jane</a><br>
                                <span style="font-size:11px;color:#777;">Albuquerque, NM</span><br>
                                <span style="font-size:10px;color:#777;">1 day ago</span><br>
                            
                                hey guys, i'm going to be in phoenix all week and would love to meet up<br>
                                
                                <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">reply</a>
                                
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-reply" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M9.502 5.013a.144.144 0 0 0-.202.134V6.3a.5.5 0 0 1-.5.5c-.667 0-2.013.005-3.3.822-.984.624-1.99 1.76-2.595 3.876C3.925 10.515 5.09 9.982 6.11 9.7a8.741 8.741 0 0 1 1.921-.306 7.403 7.403 0 0 1 .798.008h.013l.005.001h.001L8.8 9.9l.05-.498a.5.5 0 0 1 .45.498v1.153c0 .108.11.176.202.134l3.984-2.933a.494.494 0 0 1 .042-.028.147.147 0 0 0 0-.252.494.494 0 0 1-.042-.028L9.502 5.013zM8.3 10.386a7.745 7.745 0 0 0-1.923.277c-1.326.368-2.896 1.201-3.94 3.08a.5.5 0 0 1-.933-.305c.464-3.71 1.886-5.662 3.46-6.66 1.245-.79 2.527-.942 3.336-.971v-.66a1.144 1.144 0 0 1 1.767-.96l3.994 2.94a1.147 1.147 0 0 1 0 1.946l-3.994 2.94a1.144 1.144 0 0 1-1.767-.96v-.667z"/>
                                </svg>

                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{ asset('assets/img/portfolio/da-comp-profile-sq2.jpg') }}" style="border-radius:50%;" width="80" alt="Image Description">
                            </div>
                            <div class="col-md-9">
                    
                                <a href="#!" style="font-size:15px;color:#fd7bcb;font-weight:600">Alexis M.</a><br>
                                <span style="font-size:11px;color:#777;">Albuquerque, NM</span><br>
                                <span style="font-size:10px;color:#777;">1 day ago</span><br>
                            
                                hey guys, i'm going to be in phoenix all week and would love to meet up<br>
                                
                                <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">reply</a>
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-reply" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M9.502 5.013a.144.144 0 0 0-.202.134V6.3a.5.5 0 0 1-.5.5c-.667 0-2.013.005-3.3.822-.984.624-1.99 1.76-2.595 3.876C3.925 10.515 5.09 9.982 6.11 9.7a8.741 8.741 0 0 1 1.921-.306 7.403 7.403 0 0 1 .798.008h.013l.005.001h.001L8.8 9.9l.05-.498a.5.5 0 0 1 .45.498v1.153c0 .108.11.176.202.134l3.984-2.933a.494.494 0 0 1 .042-.028.147.147 0 0 0 0-.252.494.494 0 0 1-.042-.028L9.502 5.013zM8.3 10.386a7.745 7.745 0 0 0-1.923.277c-1.326.368-2.896 1.201-3.94 3.08a.5.5 0 0 1-.933-.305c.464-3.71 1.886-5.662 3.46-6.66 1.245-.79 2.527-.942 3.336-.971v-.66a1.144 1.144 0 0 1 1.767-.96l3.994 2.94a1.147 1.147 0 0 1 0 1.946l-3.994 2.94a1.144 1.144 0 0 1-1.767-.96v-.667z"/>
                                </svg>                            

                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{ asset('assets/img/portfolio/da-comp-profile-sq3.jpg') }}" style="border-radius:50%;" width="80" alt="Image Description">
                            </div>
                            <div class="col-md-9">
                    
                                <a href="#!" style="font-size:15px;color:#fd7bcb;font-weight:600">Amanda B.</a><br>
                                <span style="font-size:11px;color:#777;">Albuquerque, NM</span><br>
                                <span style="font-size:10px;color:#777;">1 day ago</span><br>
                            
                                hey guys, i'm going to be in phoenix all week and would love to meet up<br>
                                
                                <a href="#!" style="font-size:0.8rem;color:#fd7bcb;">reply</a>
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-reply" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M9.502 5.013a.144.144 0 0 0-.202.134V6.3a.5.5 0 0 1-.5.5c-.667 0-2.013.005-3.3.822-.984.624-1.99 1.76-2.595 3.876C3.925 10.515 5.09 9.982 6.11 9.7a8.741 8.741 0 0 1 1.921-.306 7.403 7.403 0 0 1 .798.008h.013l.005.001h.001L8.8 9.9l.05-.498a.5.5 0 0 1 .45.498v1.153c0 .108.11.176.202.134l3.984-2.933a.494.494 0 0 1 .042-.028.147.147 0 0 0 0-.252.494.494 0 0 1-.042-.028L9.502 5.013zM8.3 10.386a7.745 7.745 0 0 0-1.923.277c-1.326.368-2.896 1.201-3.94 3.08a.5.5 0 0 1-.933-.305c.464-3.71 1.886-5.662 3.46-6.66 1.245-.79 2.527-.942 3.336-.971v-.66a1.144 1.144 0 0 1 1.767-.96l3.994 2.94a1.147 1.147 0 0 1 0 1.946l-3.994 2.94a1.144 1.144 0 0 1-1.767-.96v-.667z"/>
                                </svg>                            

                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                    <div class="container">
                        <a href="more" style="text-align:center;">more</a>
                    </div>
                    </div>
                </div>

            </div>

            </div>

        </div>
        </section><!-- End Live Feed Section -->



                <!-- ======= About Us Section ======= -->
        <section id="about">
        <div class="container" data-aos="fade-up">

            <header class="section-header">
            <h3>about</h3>
            <p>By joining a confidential, secure platform, <span style="color:#1560db;">date</span><span style="font-weight:700;color:#fd7bcb">assurance</span>&trade; can help keep both Members and Companions safe!</p>
            </header>

            <div class="row about-cols">

            <div class="col-md-3" data-aos="fade-up" data-aos-delay="100">
                <div class="about-col">
                <div class="img">
                    <div class="icon"><i class="fas fa-inbox"></i></div>
                </div>
                    <br>
                <h2 class="title"><a href="#">MESSAGE CENTER</a></h2>
                <p>
                    Allows for near real-time secure chat with providers. It also lets you see when others are typing, when messages are delivered, and when they are read.
                </p>
                </div>
            </div>

            <div class="col-md-3" data-aos="fade-up" data-aos-delay="200">
                <div class="about-col">
                <div class="img">
                    <div class="icon"><i class="fas fa-search"></i></div>
                </div>
                    <br>
                <h2 class="title"><a href="#">POWER SEARCH</a></h2>
                <p>
                    Use powerful filters to find the exact Companion for you. Search by hair color, height, age, indulgences, &amp; more!
                </p>
                </div>
            </div>

            <div class="col-md-3" data-aos="fade-up" data-aos-delay="300">
                <div class="about-col">
                <div class="img">
                    <div class="icon"><i class="fas fa-mobile-alt"></i></div>
                </div>
                    <br>
                <h2 class="title"><a href="#">100% MOBILE-READY</a></h2>
                <p>
                    When the urge hits you, we're ready with a completely mobile-ready web experience. Simply log in and go - nothing to download, nothing for anyone to find.
                </p>
                </div>
            </div>


            <div class="col-md-3" data-aos="fade-up" data-aos-delay="300">
                <div class="about-col">
                <div class="img">
                    <div class="icon"><i class="fas fa-user-secret"></i></div>
                </div>
                    <br>
                <h2 class="title"><a href="#">MILITARY GRADE ENCRYPTION</a></h2>
                <p>
                    Approved by the National Security Agency (NSA) to protect information at a “Top Secret” level. We encrypt our database to ensure your privacy and protect sensitive data.
                </p>
                </div>
            </div>            
            </div>

        </div>
        </section><!-- End About Us Section -->

        <!-- ======= Features Section ======= -->
        <section id="features">
        <div class="container" data-aos="fade-up">

            <header class="section-header wow fadeInUp">
            <h3>Features for Members</h3>
            <p>Read reviews, use powerful search tools, rate your date, post reviews, and secure communications with Companions.</p>
            </header>

            <div class="row">

            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="100">
                <div class="icon"><i class="far fa-comments"></i></div>
                <h4 class="title"><a href="">SECURE COMMUNICATIONS WITH COMPANIONS</a></h4>
                <p class="description">With dateassure's internal messaging system, you can be confident no one is going to stumble upon your texts or emails when you reach out to verified Companions.</p>
            </div>
            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="200">
                <div class="icon"><i class="far fa-check-circle"></i></div>
                <h4 class="title"><a href="">VERIFIED COMPANIONS</a></h4>
                <p class="description">We make every effort to verify Companions that join our site or that are reviewed or appearing in the photographs on our site are the individual providing adult companionship.</p>
            </div>
            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="300">
                <div class="icon"><i class="fas fa-search"></i></div>
                <h4 class="title"><a href="">POWERFUL SEARCH TOOLS</a></h4>
                <p class="description">Search from thousands of Companions for your favorite attributes or special indulgences.</p>
            </div>
            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="200">
                <div class="icon"><i class="fas fa-mobile-alt"></i></div>
                <h4 class="title"><a href="">COMPLETELY MOBILE-FRIENDLY</a></h4>
                <p class="description">No computer? Out on the road? No problem - easily search, review, and connect with your favorite Companions all from your smartphone.</p>
            </div>
            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="300">
                <div class="icon"><i class="far fa-thumbs-up"></i></div>
                <h4 class="title"><a href="">REVIEWS</a></h4>
                <p class="description">Read reviews written by fellow client members' experiences with Companions or write your own. The more reviews, the more valuable the site is to all its members. Get 5 membership credits for every review. 100 credits gets you a free month!</p>
            </div>
            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="400">
                <div class="icon"><i class="fas fa-users"></i></div>
                <h4 class="title"><a href="">SOCIAL MEDIA</a></h4>
                <p class="description">Many Companions are active on Twitter, Instagram, etc. and we provide a Twitter feed to allow followers a single place to keep up on all activity.</p>
            </div>

            </div>

        </div>
        </section><!-- End Features for Members Section -->

        <!-- ======= Facts Section ======= -->
        <section id="facts">
        <div class="container" data-aos="fade-up">

            <header class="section-header">
            <h3>Facts</h3>
            <p>We're striving to become the world's largest and safest "adult dating" community. Come and join us.</p>
            </header>

            <div class="row counters">

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">27</span>
                <p>Members</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">12</span>
                <p>Companions</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">14</span>
                <p>Reviews</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">5</span>
                <p>Review Credits Issued</p>
            </div>

            </div>

        </div>
        </section><!-- End Facts Section -->  

        <!-- ======= Features For Companions Section ======= -->
        <section id="features-comps">
        <div class="container" data-aos="fade-up">

            <header class="section-header wow fadeInUp">
            <h3>Features for Companions</h3>
            <p>Read &amp; post member reviews, use powerful search tools, rate your date, secure communications with members, and a FREE profile website.</p>
            </header>

            <div class="row">

            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="100">
                <div class="icon"><i class="far fa-comments"></i></div>
                <h4 class="title"><a href="">SECURE COMMUNICATIONS WITH COMPANIONS</a></h4>
                <p class="description">With dateassure's internal messaging system, you can be confident no one is going to stumble upon your texts or emails when you reach out to verified Companions.</p>
            </div>
            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="200">
                <div class="icon"><i class="far fa-check-circle"></i></div>
                <h4 class="title"><a href="">VERIFIED COMPANIONS</a></h4>
                <p class="description">We make every effort to verify Companions that join our site or that are reviewed or appearing in the photographs on our site are the individual providing adult companionship.</p>
            </div>
            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="300">
                <div class="icon"><i class="fas fa-search"></i></div>
                <h4 class="title"><a href="">POWERFUL SEARCH TOOLS</a></h4>
                <p class="description">Search from thousands of Companions for your favorite attributes or special indulgences.</p>
            </div>
            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="200">
                <div class="icon"><i class="fas fa-mobile-alt"></i></div>
                <h4 class="title"><a href="">COMPLETELY MOBILE-FRIENDLY</a></h4>
                <p class="description">No computer? Out on the road? No problem - easily search, review, and connect with your favorite Companions all from your smartphone.</p>
            </div>
            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="300">
                <div class="icon"><i class="far fa-thumbs-up"></i></div>
                <h4 class="title"><a href="">REVIEWS</a></h4>
                <p class="description">Read reviews written by fellow client members' experiences with Companions or write your own. The more reviews, the more valuable the site is to all its members. Get 5 membership credits for every review. 100 credits gets you a free month!</p>
            </div>
            <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="400">
                <div class="icon"><i class="fas fa-users"></i></div>
                <h4 class="title"><a href="">SOCIAL MEDIA</a></h4>
                <p class="description">Many Companions are active on Twitter, Instagram, etc. and we provide a Twitter feed to allow followers a single place to keep up on all activity.</p>
            </div>

            </div>

        </div>
        </section><!-- End Features for Members Section -->    

            <!-- ======= Pricing Section ======= -->
        <section id="pricing">
        <div class="container" data-aos="fade-up">

            <header class="section-header">
            <h3>membership plans</h3>
            <p>Select the plan that suits you best and you can cancel at any time before the end of your subscription.</p>
            <p style="font-weight:600;">***FOR A LIMITED TIME ONLY***<br>
            The first 1,000 people to sign up for a Membership will receive a FREE lifetime charter membership!! Simply select a plan below and we'll waive your Membership fee...forever.</p>
            </header>

            <div class="row about-cols">

            <div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
                <div class="about-col">
                <div class="img">
                    <img src="{{ asset('assets/img/membership-awesome1.jpg') }}" alt="" class="img-fluid">
                    <div class="icon"><i class="far fa-star"></i></div>
                </div>
                <h2 class="title"><a href="#">VIP - AWESOME!</a></h2>
                <h1 class="title"><a href="#">$24.95</a></h1>
                <p>
                    30 DAYS (RECURRING)
                    <br>
                    <a href="../reg/da-home-mbr_sign_up_account6.php" class="btn-mbrs-get-started scrollto animate__animated animate__fadeInUp">SIGN UP Now!</a>
                </p>
                <p><span style="text-align:center;">
                    <i class="fas fa-check"></i> Search Companion Profiles<br>
                    <i class="fas fa-check"></i> View Companion Ratings<br>
                    <i class="fas fa-check"></i> Browse Companion Gallery<br>
                    <i class="fas fa-check"></i> View Companion Reviews<br>
                    <i class="fas fa-check"></i> Write Companion Reviews<br>
                    <i class="fas fa-check"></i> Secure Communication with Companions<br>
                    <i class="fas fa-check"></i> Secure Communication with Other Members<br></span>
                                        <br>
                    <a href="../reg/da-home-mbr_sign_up_account6.php" class="btn-mbrs-get-started scrollto animate__animated animate__fadeInUp">SIGN UP Now!</a>
                </p>
                </div>
            </div>

            <div class="col-md-4" data-aos="fade-up" data-aos-delay="200">
                <div class="about-col">
                <div class="img">
                    <img src="{{ asset('assets/img/membership-greatdeal1.jpg') }}" alt="" class="img-fluid">
                    <div class="icon"><i class="far fa-star"></i></div>
                </div>
                <h2 class="title"><a href="#">VIP - GREAT DEAL!</a></h2>
                <h1 class="title"><a href="#">$59.95</a></h1>
                <p>
                    90 DAYS (RECURRING)
                    <br>
                    <a href="../reg/da-home-mbr_sign_up_account6.php" class="btn-mbrs-get-started scrollto animate__animated animate__fadeInUp">PROTECT ME Now!</a>
                </p>
                <p><span style="text-align:center;">
                    <i class="fas fa-check"></i> Search Companion Profiles<br>
                    <i class="fas fa-check"></i> View Companion Ratings<br>
                    <i class="fas fa-check"></i> Browse Companion Gallery<br>
                    <i class="fas fa-check"></i> View Companion Reviews<br>
                    <i class="fas fa-check"></i> Write Companion Reviews<br>
                    <i class="fas fa-check"></i> Secure Communication with Companions<br>
                    <i class="fas fa-check"></i> Secure Communication with Other Members<br></span>
                                        <br>
                    <a href="../reg/da-home-mbr_sign_up_account6.php" class="btn-mbrs-get-started scrollto animate__animated animate__fadeInUp">PROTECT ME Now!</a>
                </p>
                </div>
            </div>

            <div class="col-md-4" data-aos="fade-up" data-aos-delay="200">
                <div class="about-col">
                <div class="img">
                    <img src="{{ asset('assets/img/membership-bestvalue1.jpg') }}" alt="" class="img-fluid">
                    <div class="icon"><i class="far fa-star"></i></div>
                </div>
                <h2 class="title"><a href="#">VIP - BEST VALUE!</a></h2>
                <h1 class="title"><a href="#">$159.95</a></h1>
                <p>
                    ANNUALLY (RECURRING)
                    <br>
                    <a href="../reg/da-home-mbr_sign_up_account6.php" class="btn-mbrs-get-started scrollto animate__animated animate__fadeInUp">GET ME THIS DEAL!</a>
                </p>
                <p><span style="text-align:center;">
                    <i class="fas fa-check"></i> Search Companion Profiles<br>
                    <i class="fas fa-check"></i> View Companion Ratings<br>
                    <i class="fas fa-check"></i> Browse Companion Directory<br>
                    <i class="fas fa-check"></i> View Companion Reviews<br>
                    <i class="fas fa-check"></i> Write Companion Reviews<br>
                    <i class="fas fa-check"></i> Secure Communication with Companions<br>
                    <i class="fas fa-check"></i> Secure Communication with Other Members<br></span>
                                        <br>
                    <a href="../reg/da-home-mbr_sign_up_account6.php" class="btn-mbrs-get-started scrollto animate__animated animate__fadeInUp">GET ME THIS DEAL!</a>
                </p>
                </div>
            </div>

            </div>
        </div>
        </section><!-- End About Us Section -->    

        <!-- ======= Call To Action Section ======= -->
        <section id="call-to-action">
        <div class="container text-center" data-aos="zoom-in">
            <h3>COMPANIONS SIGN UP FOR FREE!</h3>
            <p>Your own customized profile website, screen and verify members, schedule meetups, and secure communications - all completely free for companions.</p>
            <a class="cta-btn" href="../reg/da-home-comp_sign_up_account6.php">Companion Sign Up</a>
            <br>
            <br>
            <br>
            <h3>sample screenshots of free Companion profiles</h3>
        <div class="facts-img">
            
            <img src="{{ asset('assets/img/banner2.png') }}" alt="" class="img-fluid">
        </div>
        </section><!-- End Call To Action Section -->

        <!-- ======= Portfolio Section ======= -->
        <section id="portfolio" class="section-bg">
        <div class="container" data-aos="fade-up">

            <header class="section-header">
            <h3 class="section-title">Companion Directory</h3>
            <p>A small sample of our Companion Directory. Members have access to our full Companion Directory with powerful search features.<br>
            <br>
                
            Your current location is: Bhopal, Madhya Pradesh <a href="that is not my current location.php">(not my location)</a></p>

            </header>

            <div class="row" data-aos="fade-up" data-aos-delay="100"">
        <div class=" col-lg-12">
            <ul id="portfolio-flters">
                <li data-filter="*" class="filter-active">All</li>
                <li data-filter=".filter-card">Local</li>
                <li data-filter=".filter-web">Online Now</li>
            </ul>
            </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
                <figure>
                <img src="{{ asset('assets/img/portfolio/da-comp-profile1.jpg') }}" class="img-fluid" alt="">
                <a href="{{ asset('assets/img/portfolio/da-comp-profile1.jpg') }}" data-lightbox="portfolio" data-title="App 1" class="link-preview"><i class="ion ion-eye"></i></a>
                <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                <h4><a href="portfolio-details.html">amanda - 23</a></h4>
                <p>Blonde</p>
                </div>
            </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
                <figure>
                <img src="{{ asset('assets/img/portfolio/da-comp-profile2.jpg') }}" class="img-fluid" alt="">
                <a href="{{ asset('assets/img/portfolio/da-comp-profile2.jpg') }}" class="link-preview venobox" data-gall="portfolioGallery" title="App 1"><i class="ion ion-eye"></i></a>
                <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                <h4><a href="portfolio-details.html">alyssa - 19</a></h4>
                <p>Blonde</p>
                </div>
            </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
                <figure>
                <img src="{{ asset('assets/img/portfolio/da-comp-profile3.jpg') }}" class="img-fluid" alt="">
                <a href="{{ asset('assets/img/portfolio/da-comp-profile3.jpg') }}" class="link-preview venobox" data-gall="portfolioGallery" title="App 2"><i class="ion ion-eye"></i></a>
                <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                <h4><a href="portfolio-details.html">chevaun - 24</a></h4>
                <p>Local</p>
                </div>
            </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
                <figure>
                <img src="{{ asset('assets/img/portfolio/da-comp-profile4.jpg') }}" class="img-fluid" alt="">
                <a href="{{ asset('assets/img/portfolio/da-comp-profile4.jpg') }}" class="link-preview venobox" data-gall="portfolioGallery" title="Card 2"><i class="ion ion-eye"></i></a>
                <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                <h4><a href="portfolio-details.html">samantha rox - 21</a></h4>
                <p>Local</p>
                </div>
            </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
                <figure>
                <img src="{{ asset('assets/img/portfolio/da-comp-profile5.jpg') }}" class="img-fluid" alt="">
                <a href="{{ asset('assets/img/portfolio/da-comp-profile5.jpg') }}" class="link-preview venobox" data-gall="portfolioGallery" title="Web 2"><i class="ion ion-eye"></i></a>
                <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                <h4><a href="portfolio-details.html">roxy b. - 19</a></h4>
                <p>Online Now</p>
                </div>
            </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
                <figure>
                <img src="{{ asset('assets/img/portfolio/da-comp-profile6.jpg') }}" class="img-fluid" alt="">
                <a href="{{ asset('assets/img/portfolio/da-comp-profile6.jpg') }}" class="link-preview venobox" data-gall="portfolioGallery" title="App 3"><i class="ion ion-eye"></i></a>
                <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                <h4><a href="portfolio-details.html">julia ness - 22</a></h4>
                <p>Local</p>
                </div>
            </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
                <figure>
                <img src="{{ asset('assets/img/portfolio/da-comp-profile7.jpg') }}" class="img-fluid" alt="">
                <a href="{{ asset('assets/img/portfolio/da-comp-profile7.jpg') }}" class="link-preview venobox" data-gall="portfolioGallery" title="Card 1"><i class="ion ion-eye"></i></a>
                <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                <h4><a href="portfolio-details.html">honey kitts - 18</a></h4>
                <p>Local</p>
                </div>
            </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
                <figure>
                <img src="{{ asset('assets/img/portfolio/da-comp-profile8.jpg') }}" class="img-fluid" alt="">
                <a href="{{ asset('assets/img/portfolio/da-comp-profile8.jpg') }}" class="link-preview venobox" data-gall="portfolioGallery" title="Card 3"><i class="ion ion-eye"></i></a>
                <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                <h4><a href="portfolio-details.html">jojo smith - 22</a></h4>
                <p>Local</p>
                </div>
            </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
                <figure>
                <img src="{{ asset('assets/img/portfolio/da-comp-profile9.jpg') }}" class="img-fluid" alt="">
                <a href="{{ asset('assets/img/portfolio/da-comp-profile9.jpg') }}" class="link-preview venobox" data-gall="portfolioGallery" title="Web 1"><i class="ion ion-eye"></i></a>
                <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                <h4><a href="portfolio-details.html">kim k. - 25</a></h4>
                <p>Online Now</p>
                </div>
            </div>
            </div>

        </div>

        </div>
        </section><!-- End Portfolio Section -->

        <!-- ======= Testimonials Section ======= -->
        <section id="testimonials" class="section-bg">
        <div class="container" data-aos="fade-up">

            <header class="section-header">
            <h3>Testimonials</h3>
            <p>Hear what other members have to say about <span style="color:#1560db;">date</span><span style="font-weight:700;color:#fd7bcb">assurance</span>&trade;</p>
            </header>

            <div class="owl-carousel testimonials-carousel">

            <div class="testimonial-item">
                <img src="{{ asset('assets/img/cli_av7.jpg') }}" class="testimonial-img" alt="">
                <h3>saul g.</h3>
                <p>
                <img src="{{ asset('assets/img/quote-sign-left.png') }}" class="quote-sign-left" alt="">
                i love this site! i can safely and securely meet up with a bevy of beautiful girls, all with the click of a mouse.
                <img src="{{ asset('assets/img/quote-sign-right.png') }}" class="quote-sign-right" alt="">
                </p>
            </div>

            <div class="testimonial-item">
                <img src="{{ asset('assets/img/cli_av8.jpg') }}" class="testimonial-img" alt="">
                <h3>mikey b.</h3>
                <p>
                <img src="{{ asset('assets/img/quote-sign-left.png') }}" class="quote-sign-left" alt="">
                Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                <img src="{{ asset('assets/img/quote-sign-right.png') }}" class="quote-sign-right" alt="">
                </p>
            </div>

            <div class="testimonial-item">
                <img src="{{ asset('assets/img/cli_av9.jpg') }}" class="testimonial-img" alt="">
                <h3>deshaun k.</h3>
                <p>
                <img src="{{ asset('assets/img/quote-sign-left.png') }}" class="quote-sign-left" alt="">
                Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                <img src="{{ asset('assets/img/quote-sign-right.png') }}" class="quote-sign-right" alt="">
                </p>
            </div>

            <div class="testimonial-item">
                <img src="{{ asset('assets/img/cli_av10.jpg') }}" class="testimonial-img" alt="">
                <h3>matt b.</h3>
                <p>
                <img src="{{ asset('assets/img/quote-sign-left.png') }}" class="quote-sign-left" alt="">
                Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                <img src="{{ asset('assets/img/quote-sign-right.png') }}" class="quote-sign-right" alt="">
                </p>
            </div>

            </div>

        </div>
        </section><!-- End Testimonials Section -->

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-header">
            <h3>Contact Us</h3>
            <p>Have a question? Ask us here for a prompt reply.</p>
            </div>

            <div class="form">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                <div class="row">
                <div class="form-group col-md-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                    <div class="validate"></div>
                </div>
                <div class="form-group col-md-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                    <div class="validate"></div>
                </div>
                </div>
                <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
                </div>
                <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validate"></div>
                </div>
                <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
                </div>
                <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
            </div>

        </div>
        </section><!-- End Contact Section -->

    </main><!-- End #main -->
    @include('common/footer')
@endsection
