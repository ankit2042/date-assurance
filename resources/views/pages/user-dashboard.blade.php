@extends('layouts.app')

@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ asset('css/da-site_style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/da-home-nav_style.css') }}">
@endpush

@section('content')
    @include('common/static-header')
    <div class="container"><!---->
        <br/><br/><br/><br/><br/><br/><br/>
        <div class="jumbotron">
            <h1 class="display-4">Dashboard!</h1>
            <p class="lead">There would be a dashboard of member after log in. </p>
            <hr class="my-4">
            <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
        </div>
        <br/><br/><br/>
    </div>
    @include('common/footer')
@endsection
