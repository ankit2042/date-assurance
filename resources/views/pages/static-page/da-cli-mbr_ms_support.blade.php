@extends('layouts.app') 
@section('content') 
@include('common/static-header') 
@include('inc/da-header-meta') 
@include("inc/da-cli-mbr_header_links_scripts")
@include("inc/da-cli-mbr_subnav")
<div class="container">
<div class="row">
   <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card-middle">
         <div class="card-body">
            <div class="section-title-center">member support center</div>
            <br>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card" style="background:#595959;">
                     <div class="card-body">
                        <br>
                        <div class="section-title-center" style="color:#ffffff;font-size:28px;font-weight:300;">what are you looking for?</div>
                        <br>
                        <form class="form-inline">
                           <div class="col-xl-2">&nbsp;</div>
                           <div class="form-group col-xl-6"> <input class="form-control col-xl-12" name="ProfileName" type="text" placeholder="enter a search term"></div>
                           <div class="form-group col-xl-2"><button type="submit" class="btn btn-primary">search</button></div>
                           <div class="col-xl-2">&nbsp;</div>
                        </form>
                        <div class="row">
                           <div class="col-xl-2">&nbsp;</div>
                           <div class="col-xl-8">&nbsp;&nbsp;&nbsp;<span style="font-size:12px;font-weight:400;color:#c9d7ed;">eg. "set password" or "block user" or "error code"</span></div>
                           <div class="col-xl-2">&nbsp;</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <br> <br>
            <div class="row">
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <div class="card h-100">
                     <div class="card-body">
                        <div class="section-subtitle-center"><i class="fas fa-2x fa-chalkboard-teacher"></i><br>learn about dateassure&trade;</div>
                        <hr>
                        <span style="font-size:15px;">the social network for safe, secure, &amp discrete encounters&trade;<br>one<br> one<br> one<br> one<br> one<br><br> <br></span>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <div class="card h-100">
                     <div class="card-body">
                        <div class="section-subtitle-center"><i class="fas fa-2x fa-users-cog"></i><br>manage my membership</div>
                        <hr>
                        <span style="font-size:15px;font-weight:400;">membership settings</span><br><br><a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_ms_my_profile.php">my profile</a><br> <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_ms_update_my_profile.php">update my profile</a><br> <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_ms_update_my_profile.php">profile display settings</a><br> <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_ms_changepass.php">change password</a><br><br><span style="font-size:15px;font-weight:400">photos</span><br><br><a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_ms_upload_photo.php">upload photos</a><br> <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_ms_delete_photos.php">delete photos</a><br> <a style="font-size:15px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_ms_set_primary_photo.php">set primary photo</a><br><br><br><span style="font-size:15px;font-weight:400;">subscription &amp; billing</span><br><br><a style="font-size: 14px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_ms_update_payment.php">update payment method</a><br> <a style="font-size: 14px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_ms_cancel_my_membership.php">cancel my membership</a><br> <a style="font-size: 14px;color:#0094ff;margin-left:10px;" href="<?php //echo $base_url;?>mbrs/da-cli-mbr_ms_cancel_my_membership.php">resolve issue</a><br> <br>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <div class="card h-100">
                     <div class="card-body">
                        <div class="section-subtitle-center"> <i class="fas fa-2x fa-mobile-alt"></i> <br>using date<span style="color:#f300fe;font-weight:500;">assure&trade;</span></div>
                        <hr>
                        <span style="font-size:15px;">the social network for safe, secure, &amp discrete encounters&trade;<br>first you will see icons as follows:</span><br><br>
                     </div>
                  </div>
               </div>
            </div>
            <br><br>
            <div class="row">
               <div class="col-xl-12">
                  <div class="section-subtitle-center">faqs</div>
                  <div class="card-middle card-body"> <span style="font-size:15px;"> <a href="">faq one</a><br> <a href="">faq one</a><br> <a href="">faq one</a><br> <a href="">faq one</a><br> <a href="">faq one</a><br> <a href="">faq one</a><br> </span></div>
               </div>
            </div>
            <br>
            <div class="row">
               <div class="col-xl-12">
                  <div class="card-middle" style="background-color:#eef7ff">
                     <div class="card-body">
                        <table width="100%">
                           <tr>
                              <td width="25%" style="text-align:center">
                                 <div class="section-subtitle-center">want to contact us?</div>
                              </td>
                              <td width="25%" style="text-align:center"> <a href="da-cli-mbr_ms_live_chat.php" class="btn btn-sm btn-secondary">live chat</a></td>
                              <td width="25%" style="text-align:center"> <a href="da-cli-mbr_ms_live_chat.php" class="btn btn-sm btn-primary">message us</a></td>
                           </tr>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@include("inc/da-cli-mbr_footer")
@include("inc/da-cli-mbr_modals")
@endsection	