@extends('layouts.app') 
@section('content') 
@include('common/static-header') 
@include('inc/da-header-meta') 
@include("inc/da-cli-mbr_header_links_scripts")
<div class="da-margin-top">
	@include("inc/da-cli-mbr_subnav")
	<!-- INSERT BOOTSTRAP REDO -->
	<div class="container"><!---->
		<!-- BEGIN TOP HEADER PROFILE -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card-middle">
					<div class="card-body">
						<div class="section-title-center">cancel your membership</div>
						<br>
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
							@if(Auth::user()->membership_status_id > 1)
								<span style="font-size:18px;" class="text-danger">
									<i class="fas fa-exclamation"></i> you have already cancelled or paused your membership
								</span>
							@elseif(Auth::user()->membership_status_id = 1)
								@include('inc/da-cli-mbr-mod_ms_cancel_my_membership')
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- BEGIN MODALS -->
<!-- BEGIN MODAL 'PAUSE MEMBERSHIP' -->
<div class="modal" tabindex="-1" role="dialog" id="pauseMembershipModal">
	<div class="modal-dialog" role="document">
		<form action="#../mbrs/da-cli-mbr_ms_membership_paused.php" name="pauseMembershipDetail" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" style="color:#0094ff">you have chosen to pause your membership</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p style="font-size:16px">again, we're sorry to see you go! to help us better our service please take a moment to tell us why you are leaving</p>
					<div class="row">
						<div class="col-lg-1">&nbsp;</div>
						<div class="col-lg-10">
							<div class="form-group row">
								<div class="col-sm-1">
									<input type="radio" value="1" name="pauseReason">

                            </div>
                            i have a duplicate membership
                        </div>
                        <div class="form-group row">
                        	<div class="col-sm-1">
                        		<input type="radio" value="2" name="pauseReason">
                        	</div>
                        	i am receiving too many emails
                        </div>
                        <div class="form-group row">
                        	<div class="col-sm-1">
                        		<input type="radio" value="3" name="pauseReason">
                        	</div>
                        	i am not getting value for my membership
                        </div>
                        <div class="form-group row">
                        	<div class="col-sm-1">
                        		<input type="radio" value="4" name="pauseReason">
                        	</div>
                        	privacy concerns
                        </div>
                        <div class="form-group row">
                        	<div class="col-sm-1">
                        	    <input type="radio" value="5" name="pauseReason">
                            </div>
                            i am receiving unwanted contact
                        </div>
                        <div class="form-group row">
                        	<div class="col-sm-1">
                        		<input type="radio" value="6" name="pauseReason">
                        	</div>
                        	other
                        </div>
                        <div class="form-group row">
                        	<div class="col-sm-12">
                        		<span style="font-size:16px;" class="text-danger"><i class="fas fa-exclamation-triangle"></i> are you sure you want to pause your membership?</span>
                        		<br>
                        		<br>
                        		<span style="font-size:18px;font-weight:400;color:#0094ff;">last step! please type <span style="color:#813ee8;font-weight:400;">'pause'</span> in the box below to confirm:</span>
                        		<br>
                        		<br>
                        		<div class="align-center"><input type="text" class="col-sm-4 form-control" style="font-weight:300;font-size:14px;" id="pauseConfirm" name="pauseConfirm" placeholder="type 'pause'" maxlength="5" required></div>
                        	</div>
                        </div>
                    </div>
                    <div class="col-lg-1">&nbsp;</div>
                </div>
            </div>
            <input type="hidden" name="pausembrshipstatusid" value="2">
            <div class="modal-footer">
            	<button type="submit" class="btn btn-primary">submit pause membership</button>
            	<button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>
            </div>
        </div>
    </form>
</div>
</div>
<!-- END MODAL 'PAUSE MEMBERSHIP' -->
<!-- BEGIN MODAL 'CANCEL MEMBERSHIP' -->
<div class="modal" tabindex="-1" role="dialog" id="cancelMembershipModal">
	<div class="modal-dialog" role="document">
		<form action="#../mbrs/da-cli-mbr_ms_membership_cancelled.php" name="cancelMembershipDetail" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" style="color:#0094ff">you have chosen to cancel your membership</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p style="font-size:16px">again, we're sorry to see you go! to help us better our service please take a moment to tell us why you are leaving</p>
					<div class="row">
						<div class="col-lg-1">&nbsp;</div>
						<div class="col-lg-10">
							<div class="form-group row">
								<div class="col-sm-1">
									<input type="radio" value="1" name="cancelReason">
								</div>
								i have a duplicate membership
							</div>
							<div class="form-group row">
								<div class="col-sm-1">
									<input type="radio" value="2" name="cancelReason">
								</div>
								i am receiving too many emails
							</div>
							<div class="form-group row">
								<div class="col-sm-1">
									<input type="radio" value="3" name="cancelReason">
								</div>
								i am not getting value for my membership
							</div>
							<div class="form-group row">
								<div class="col-sm-1">
									<input type="radio" value="4" name="cancelReason">
								</div>
								privacy concerns
							</div>
							<div class="form-group row">
								<div class="col-sm-1">
									<input type="radio" value="5" name="cancelReason">
								</div>
								i am receiving unwanted contact
							</div>
							<div class="form-group row">
								<div class="col-sm-1">
									<input type="radio" value="6" name="cancelReason">
								</div>
								other
							</div>
							<div class="form-group row">
								<div class="col-sm-12">
									<span style="font-size:16px;" class="text-danger"><i class="fas fa-exclamation-triangle"></i> are you sure you want to cancel your membership?</span>
									<br>
									<br>
									<span style="font-size:18px;font-weight:400;color:#0094ff;">last step! please type <span style="color:#813ee8;font-weight:400;">'cancel'</span> in the box below to confirm:</span>
									<br>
									<br>
									<div class="align-center"><input type="text" class="col-sm-4 form-control" style="font-weight:300;font-size:14px;" id="cancel" name="cancelConfirm" placeholder="type 'cancel'" maxlength="6" required></div>
								</div>
							</div>
						</div>
						<div class="col-lg-1">&nbsp;</div>
					</div>
				</div>
				<input type="hidden" name="mbrshipstatusid" value="3">
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">submit cancel membership</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>
				</div>
			</div>
		</form>
	</div>
</div>    
@include("inc/da-cli-mbr_footer")
@include("inc/da-cli-mbr_modals")
@endsection