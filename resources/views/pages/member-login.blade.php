@extends('layouts.app')

@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ asset('css/da-site_style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/da-home-nav_style.css') }}">
@endpush

@section('content')
    @include('common/static-header')
    <div class="container">
        <br/><br/><br/><br/><br/><br/><br/>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="reg">
                    <div class="card">
                        <div class="card-body">
                            <div class="section-title-center">member login</div>
                            <div class="tag-line">enjoy all the mutual benefits dateassurance™ can provide you</div>
                            <br>                         
                            <form action="{{ route('member-login-handler') }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-3">&nbsp;</div>
                                    <div class="col-lg-6">
                                        @if (session('alert-danger'))
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                {{ session('alert-danger') }}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        <div class="form-group row">
                                            <label for="email" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">email (member id): </label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="" required="">
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <small id="signinHelpBlock" class="form-text text-primary">
                                                please enter your member id.
                                                </small>
                                                
                                            </div>
                                        </div>
                                        <!-- end ENTER EMAIL/MEMBER ID -->
                                            
                                        <!-- begin ENTER PASSWORD --> 
                                        <div class="form-group row">
                                            <label for="password" class="col-sm-5 col-form-label" style="font-weight:400;font-size:14px;">password: </label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required="">

                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <a href="forgot.php"><i>forgot password?</i></a>
                                                    </div>
                                                </div>                   
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <input type="submit" value="login" class="btn btn-cli-pro" name="smsignin"><input type="reset" value="re-enter" class="btn btn-pro-cli">
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">&nbsp;</div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/><br/><br/>
    </div>
    @include('common/footer')
@endsection
