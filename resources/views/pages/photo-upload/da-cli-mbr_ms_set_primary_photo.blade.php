@extends('layouts.app')
@section('content')
@include('common/static-header')
@include('inc/da-header-meta')
@include("inc/da-cli-mbr_header_links_scripts")
@include('inc.da-cli-mbr_subnav')
<!-- INSERT BOOTSTRAP REDO -->
<div class="container">
 <!-- BEGIN TOP HEADER PROFILE -->
 <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="reg">
            <div class="card">
                <div class="card-body">
                    <div class="section-title-center">select your primary profile photo
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card-middle">
                                <div class="card-body">
                                <!-- begin GENERAL ERROR MESSAGE -->
                                <?php
                                if (isset($error) && !empty($error))
                                {
                                   echo '<p style="margin:0px; padding:5px 20px"><span style="color:#FF0000;font-size:18px;"><i>' . $error . '</i></font></p>';
                                }
                                if (isset($succ) && !empty($succ))
                                {
                                    echo '<p style="margin:0px; padding:5px 20px"><span style="color:#009933;font-size:18px;"><i class="fas fa-exclamation-triangle"></i>&nbsp;you have successfully changed your primary profile photo!</span></p>';
                                }
                                ?>
                                <form>
                                    @csrf
                                    <p align="center">
                                        <span style="float:none;" class="">your current primary photo</span>
                                        <br>
                                        @if(Auth::user()->id)
                                        @foreach($user as $val)
                                        @if($val->PrimaryPhoto=='1')
                                        <img src="{{asset('images/upload-photos')}}/{{Auth::user()->id}}u{{$val->PhotoID}}.{{$val->PhotoExtension}}" border="2" style="height:300px;" id="primary" name="primary_image" />
                                    </p>
                                    @else
                                    <!-- <img src="{{asset('images/upload-photos')}}/{{Auth::user()->id}}u{{$val->PhotoID}}.{{$val->PhotoExtension}}" border="0" style="height:300px;" id="prphoto"/> -->
                                    <i></i>
                                    @endif
                                    @endforeach
                                    @endif
                                    <div class="moreimg">
                                        <p>
                                            <span style="font-size:16px;font-weight:300;">click on an image below and select save to set a new primary profile photo</span>
                                        </p>
                                         <div class="row">
                                              @foreach ($user as $val)
                                             <div class="col-md-3">
                                                <div class="card" style="background:#f9f9f9;">
                                            <div class="card-body">
                                        
                                                <img src="{{asset('images/upload-photos')}}/{{Auth::user()->id}}u{{$val->PhotoID}}.{{$val->PhotoExtension}}" data-id="{{$val->PhotoID}}" border="1" style="height:200px;" id="strId" class="primary_images" class="m-2">
                                                &nbsp;
                                                
                                            </div>
                                          
                                        </div>
                                               
                                             </div>
                                               @endforeach 
                                         </div>
                                         

                                    </div>
                                    <p align="center">
                                        <input type="submit" value="Save" id="submit" class="btn btn-cli-pro" name="smsetpri"/>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
    $(".primary_images").on('click',function(){
        
         var image_src = $(this).attr('src');
         var image_id = $(this).attr('data-id');
         var _token = $("input[name='_token']").val();
         $('#primary').attr('src',image_src);
         
         $.ajax({
            url:"set-primary-photo",
            type:'POST',
            data:{_token:_token,image_id:image_id},
            success:function(data){
                alert(data);
            }
        }) 
 });
</script>

@include('inc.da-cli-mbr_footer')
@include('inc.da-cli-mbr_modals')
