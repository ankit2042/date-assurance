/*get country state dropdown*/
$("#country").on("change",function(){
  var country_id = $(this).val();
  $.ajax({
    url: '/get-state/'+country_id,
    type: 'GET',
    data: {country_id: country_id},
    success: function(response){
      $.each(response,function(index,value){
        console.log("value "+index,value);
        $('#slstate').append($("<option></option>").attr("value", value.StateID).text(value.State));
      })
    },
    error:function(error){
      console.log("err",error);
    }
  });
  return false;
});