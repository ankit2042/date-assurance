<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class CountryState extends Model
{
    use HasFactory;
    protected $table = 'dt_countries';

    public function getState($id){
    	$state_data = DB::table('dt_states')->where('CountryID',$id)->get();
    	return $state_data;
    }


}
