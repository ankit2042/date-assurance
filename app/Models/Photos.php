<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Photos extends Model
{
    use HasFactory;
    protected $table = 'dt_photos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'UserID',
        'PhotoExtension',
        'PhotoDescription',
        'IsApproved',
        'PrimaryPhoto',
        'InsertDate',
        'TotalRating',
        'NumberOfVote',
        'IsVerified',
    ];

    // public function getPhotosById(){
    // 	return $this->belongsTo(User::class);
    // }
}
