<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Photos;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 'users';
    // protected $primaryKey = 'UserID';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        "age",
        "country_id",    
        "state_id",      
        "city",          
        "phone",         
        "occupation_id", 
        "sex_or_id",     
        "body_type_id",  
        "hair_color_id", 
        "height_id",     
        "smoking_id",    
        "drinking_id",
        "NotificationSchedule_id",
        "profile_status_id"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'email_verified_at' => 'datetime',
    ];

    public function getPhotos(){
        return $this->hasMany('App\Models\Photos', 'UserID', 'id');
    }
}
