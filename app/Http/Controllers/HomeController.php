<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class HomeController extends Controller
{
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/
    public function index(Request $request) {
        return view('pages.home');
    }

    public function memberLogin(Request $request) 
    {
        return view('pages.member-login');
    }

    public function memberLoginHandler(Request $request) 
    {
        //echo bcrypt($request->password);die;
        //echo Hash::make($request->password);die;
        //$credentials = $request->only('email', 'password');

        // if (Auth::attempt($credentials)) {
        //     // Authentication passed...
        //     return redirect()->intended('member-dashboard');
        // }
        //return redirect('member-login')->with('alert-danger', 'Invalid credentials entered.');


        $email = $request->email;
        $password = $request->password;
        
        $user = User::where('Email', $email)->where('Password', $password)->first();
        if ($user) {
            Auth::login($user);
            return redirect()->intended('member-dashboard');
        }

        return redirect('member-login')->with('alert-danger', 'Invalid credentials entered.');
    }

    public function memberSignOut(Request $request) {
        Auth::logout();
        return redirect()->intended('/');
    }

    public function viewHelpPage(){
        return view('pages.static-page.da-cli-mbr_ms_support');
    }

    public function changePassword(){
        return view('auth.passwords.da-cli-mbr_ms_changepass');
    }

    public function updatePassword(Request $request){
        $user = Auth::user();
        $request->validate([
            'curpass' => [
                'required',
                function ($attribute, $value, $fail) use ($user)
                {
                    if (!Hash::check($value, $user->password)) {
                        return $fail(__('The current password is incorrect.'));
                    }
                }
            ],
            'password'      => 'required|min:8',
            'repassword'    => 'same:password'
        ], [
            'curpass.required'  => 'Please enter your current password',
            'password.required' => 'Please enter a new password',
            'repassword.same'   => 'The Confirm Password do not match'
        ]);
        $user->password = Hash::make($request->input('password'));
        $user->save();
        $request->session()->flash('success', 'Your password has been changed');
        return redirect()->route('change-password');
    } 
}
