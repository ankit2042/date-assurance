<?php

namespace App\Http\Controllers\CompanionDirectory\PhotoUploads;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Photos;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use File;

class PhotoUploadController extends Controller
{
    //
    public function index(){
        // to show photo upload file
         $user_photo=Photos::all()->where('UserID',Auth::user()->id);
        return view('pages.photo-upload.da-cli-mbr_ms_upload_photo',['user'=>$user_photo]);

    }

    public function user_upload_photo(Request $request){
    	// photo uploading process
		$request->validate([
			'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			'description'=>'required'
		]);
        // get the input value
		$user_id= Auth::user()->id;
		$user_photo=$request->file("photo");
		$user_description=$request->input("description");
		$user_photo_name = $user_photo->getClientOriginalName() ;
		$date = date("Y-m-d H:i:s"); 
		$extension = $user_photo->getClientOriginalExtension();
        // insert user photo with description in database
        if($file = $request->hasFile('photo')){
        //code to change the image name and upload in folder{
            $user_query=Photos::create([
                'UserID'=> $user_id,
                'PhotoExtension'=>$extension,
                'PhotoDescription'=>$user_description,
                'InsertDate'=>$date,
            ]);
            //insert image in the database
            $user_file_name=Auth::user()->id.'u'.$user_query->id;
                
            $file = $request->file('photo') ;
        	$fileName = $file->getClientOriginalName();    
            $extension_name = substr($fileName, strpos($fileName, '.'));
            $uploaded_file_name=$user_file_name.$extension_name;
            $destinationPath = public_path().'/images/upload-photos' ;
            $file->move($destinationPath,$uploaded_file_name);
		}
        else{
            return redirect()->route('photo-upload')->with('error','Photo not found');
        }
        return redirect()->route('photo-upload')->with('success','successfully uploaded, waiting for approval!');
    }

    public function delete(){
        // show delete photo file
        $user_photo=Photos::all()->where('UserID',Auth::user()->id);
    	return view('pages.photo-upload.da-cli-mbr_ms_delete_photos',['user'=>$user_photo]);
    }
    
    public function delete_user_photo(Request $request)
    {
        // user photo delete process 
        $photo_names=$request->input('imgdel');
        $i=0;
        if($photo_names)
        {
         foreach ($photo_names as $photo_name) 
            {
                // get the photo id 
                $rmv_ext = explode('.', $photo_name);
                $org_name = $rmv_ext[0];
                $value = substr($org_name,2);
                // delete the image from database
                $delete_photo = Photos::where([
                    ['UserID',Auth::user()->id],
                    ['PhotoId',$value]
                    ])->delete();
                // delete image file from folder
                $file_path=public_path().'/images/upload-photos/'.$photo_name;
                $unlink_file=unlink($file_path);
                $i++; //count how many photo are deleted
            }
            if($unlink_file)
            {
                return redirect('photo-delete')->with('success',"you successfully deleted (".$i.") images");
            }
            else
            {
                return redirect('photo-delete')->with('error',"Photo not Found!");
            }
        } 
        else{
            return redirect('/photo-delete')->with('error',"select picture to delete");
        }
        }

    function primary_photo(){
        // show primary photo file
        $user_photo=Photos::all()->where('UserID',Auth::user()->id);
        return view('pages.photo-upload.da-cli-mbr_ms_set_primary_photo',['user'=>$user_photo]);
    }
    // set image primary 
    public function set_primary_photo(Request $request){
        $image_id=$request->image_id;
        $user_id=Auth::user()->id;
        // check firstly primary image status and set '0' to all fields
        $user_image_data=Photos::where('UserID',$user_id)->get();
        foreach($user_image_data as $val)
        {
            $primaryPhoto=$val->PrimaryPhoto;
            if($primaryPhoto!=0)
            {
                // set all value in '0'
                $check_primary_image=Photos::where('PhotoID',$val->PhotoID)->update(['PrimaryPhoto'=>0]);//check primary image and set 0 value

            }  
        }
        $update_primary_image=Photos::where('PhotoID',$image_id)->update(['PrimaryPhoto'=>1]);// set primary image and set value 1
        if($update_primary_image)
        {
            echo "Primary Photo Updated";
        }
        else{
            echo "Something Wrongs";
        }
        
    }
    
}
	