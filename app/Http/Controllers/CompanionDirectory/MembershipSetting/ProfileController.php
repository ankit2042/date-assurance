<?php

namespace App\Http\Controllers\CompanionDirectory\MembershipSetting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CountryState;
use App\Models\State;
use Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country    = CountryState::all();
        $state_data = State::all();
        return view('pages.companion-directory.membershipSetting.profile.da-cli-mbr_ms_my_profile',compact(['country','state_data']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile_data   = User::findOrfail($id);
        $getPhotos      = $profile_data->getPhotos;
        $countries      = CountryState::all();

        return view('pages.companion-directory.membershipSetting.profile.da-cli-mbr_ms_update_my_profile',compact(['profile_data','getPhotos','countries']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user_id = Auth::user()->id;
        // dd($request->all());
        $data = User::find($user_id)->update([
            "age"           => $request->Age,
            "country_id"    => $request->slcountry,
            "state_id"      => $request->slstate,
            "city"          => $request->txtcity,
            "phone"         => $request->phone,
            "occupation_id" => $request->occupation,
            "sex_or_id"     => $request->sex_or_id,
            "body_type_id"  => $request->body_type_id,
            "hair_color_id" => $request->hair_color_id,
            "height_id"     => $request->height_id,
            "smoking_id"    => $request->smoking_id,
            "drinking_id"   => $request->drinking_id,        
        ]);
        $request->session()->flash('success', 'Update profile successfully.');
        return redirect()->route("membership-setting.my-profile");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function viewProfileSetting(){
        return view('pages.companion-directory.membershipSetting.profile.da-cli-mbr_ms_profile_settings');
    }

    public function updateProfileSetting(Request $request){
        $user_id = Auth::user()->id;
        $data = User::find($user_id)->update([
            "profile_status_id"         => $request->setting,
            "NotificationSchedule_id"   => $request->notify,
                  
        ]);
        $request->session()->flash('success', 'Profile setting successfully updated.');
        return redirect()->route("view-profile-setting");
    }

    public function cancelMembership(){
        return view('pages.static-page.da-cli-mbr_ms_cancel_my_membership');
    }
}
