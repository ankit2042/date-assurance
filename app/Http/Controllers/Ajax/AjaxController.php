<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CountryState;

class AjaxController extends Controller
{
    public function getState(Request $request){
    	$country_id = $request->country_id;
    	$state_data = new CountryState;
    	$data = $state_data->getState($country_id);
    	
    	return response()->json($data);
    }
}
