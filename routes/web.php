<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CompanionDirectory\CompanionDirectoryController;
use App\Http\Controllers\CompanionDirectory\Dashboard\DashboardController;
use App\Http\Controllers\CompanionDirectory\MembershipSetting\ProfileController;
use App\Http\Controllers\CompanionDirectory\PhotoUploads\PhotoUploadController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Ajax\AjaxController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/')
// Auth::routes();
Route::get('/', [HomeController::class, 'index'])->name('home-page');

/*member login routing*/
Route::get('/login', [HomeController::class, 'memberLogin'])->name('login');
Route::get('/member-login', [HomeController::class, 'memberLogin'])->name('member-login-handler');
Route::post('/member-login', [LoginController::class, 'login']);



Route::get('/member-sign-out', [HomeController::class, 'memberSignOut'])->name('member-sign-out');
// Route::post('/member-login', [HomeController::class, 'memberLoginHandler'])->name('member-login-handler');

/*Ajax routing*/
Route::get('/get-state/{id}',[AjaxController::class,'getState'])->name("get-state");


Route::group(['middleware' => 'auth'], function () {
    Route::get('/member-dashboard', [UserController::class, 'dashboard'])->name('member-dashboard');

	/*companion directory routimh start*/
	Route::get('/companion-directory',[CompanionDirectoryController::class,'index'])->name('companion-directory');
	Route::get('/my-dashboard',[DashboardController::class,'index'])->name('my-dashboard');

	/*Profile Routing Statrt*/
	Route::get('/membership-setting/my-profile',[ProfileController::class,'index'])->name('membership-setting.my-profile');
	
	Route::get('/membership-setting/edit-profile/{id}',[ProfileController::class,'edit'])->name('membership-setting.edit-profile');
	Route::post('/membership-setting/update-profile',[ProfileController::class,'update'])->name('membership-setting.update-profile');
	
	Route::get('/change-password',[HomeController::class,'changePassword'])->name('change-password');
	Route::post('/update-password',[HomeController::class,'updatePassword'])->name('update-password');

	Route::get('/profile-setting',[ProfileController::class,'viewProfileSetting'])->name('view-profile-setting');	
	Route::post('/update-profile-setting',[ProfileController::class,'updateProfileSetting'])->name('update-profile-setting');

	Route::get('/cancel-membership',[ProfileController::class,'cancelMembership'])->name('view-cancel-membership');

	/*help page*/
	Route::get('/help',[HomeController::class,'viewHelpPage'])->name('help');

	/*photo module routing*/
	/*upload photo*/
	Route::get('/membership-setting/photo-upload',[PhotoUploadController::class,'index'])->name('photo-upload');
	Route::post('/membership-setting/user-photo-upload',[PhotoUploadController::class,'user_upload_photo'])->name('upload-photo');

	/*Delete photo*/
	Route::get('/membership-setting/photo-delete',[PhotoUploadController::class,'delete'])->name('photo-delete');
	Route::post('/membership-setting/delete-user-photo',[PhotoUploadController::class,'delete_user_photo']);
	
	/*primary photo route*/
	Route::get('/membership-setting/primary-photo',[PhotoUploadController::class,'primary_photo'])->name('primary-photo');
	Route::post('/membership-setting/set-primary-photo',[PhotoUploadController::class,'set_primary_photo']);
	
});



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
